/***************************************************************************
 *   copyright       : (C) 2009-2022 by Pascal Brachet                     *
 *   https://www.xm1math.net/algobox/                                       *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   @license GPL-2.0+ <https://spdx.org/licenses/GPL-2.0+.html>           *
 ***************************************************************************/

#ifndef ALGOWEBVIEW_H
#define ALGOWEBVIEW_H


#include <QWebEngineView>
#include <QFileSystemWatcher>


class AlgoWebView : public QWebEngineView
{
    Q_OBJECT
 
public:
    AlgoWebView(QWidget *parent = 0);
    ~AlgoWebView();
private:
QString pdffichier;
QFileSystemWatcher *file_watcher;
protected:
    void contextMenuEvent(QContextMenuEvent *event);
protected slots:
    void finishLoading(bool);
public slots:
void exporterPdf();
private slots:
void voirPdf(QString);


};


#endif
