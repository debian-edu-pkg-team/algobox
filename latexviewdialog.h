/***************************************************************************
 *   copyright       : (C) 2009-2022 by Pascal Brachet                     *
 *   https://www.xm1math.net/algobox/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   @license GPL-2.0+ <https://spdx.org/licenses/GPL-2.0+.html>           *
 ***************************************************************************/

#ifndef LATEXVIEWDIALOG_H
#define LATEXVIEWDIALOG_H

#include "ui_latexviewdialog.h"
#include "latexhighlighter.h"

#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    #include <QtCore/QTextCodec>
#else
    #include <QtCore5Compat/QTextCodec>
#endif

class LatexviewDialog : public QDialog  {
   Q_OBJECT
public:
	LatexviewDialog(QWidget *parent=0, int mode=0);
	~LatexviewDialog();
	Ui::LatexviewDialog ui;
private :
LatexHighlighter *highlighter;
QTextCodec *codec;
private slots:
void Sauver(int enc);
};


#endif
