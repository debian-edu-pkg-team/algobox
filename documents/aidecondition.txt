<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN" "http://www.w3.org/TR/REC-html40/strict.dtd">
<html>
<head>
<meta name="qrichtext" content="1" />
<style type="text/css">
tt
{
font-family: 'Liberation Mono',MonoSpace, 'Courier','Courier New', Fixed;
color : #AC0B00;
}
</style>
</head>
<body>
<b>Exemples de syntaxe pour la condition :</b>
<ul>
<li>Pour vérifier si x est égal à 2, la condition à écrire est : <tt>x==2</tt></li>
<li>Pour vérifier si x est différent de 2, la condition à écrire est : <tt>x!=2</tt></li>
<li>Pour vérifier si x est strictement inférieur à 2, la condition à écrire est : <tt>x&lt;2</tt></li>
<li>Pour vérifier si x est inférieur ou égal à 2, la condition à écrire est : <tt>x&lt;=2</tt></li>
<li>Pour vérifier si x est strictement supérieur à 2, la condition à écrire est : <tt>x&gt;2</tt></li>
<li>Pour vérifier si x est supérieur ou égal à 2, la condition à écrire est : <tt>x&gt;=2</tt></li>
</ul>
<b>Il est possible de combiner plusieurs conditions avec ET et OU :</b>
<ul>
<li>La condition à écrire pour vérifier que x est strictement compris entre 1 et 5 est : <tt>x&gt;1 ET x&lt;5</tt></li>
<li>La condition à écrire pour vérifier que x est égal à 3 OU à 5 est : <tt>x==3 OU x==5</tt></li>
</ul>
Il est aussi possible d'inclure des calculs dans la condition. Exemple : <tt>sqrt(x)&lt;6</tt><br>
Pour insérer le nom d'une variable, cliquer sur celui-ci dans la zone "Variables déclarées".<br>
Pour insérer une commande, cliquer sur son intitulé en gras dans la zone "Commandes".
</body>
</html>