/***************************************************************************
 *   copyright       : (C) 2009-2022 by Pascal Brachet                     *
 *   https://www.xm1math.net/algobox/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   @license GPL-2.0+ <https://spdx.org/licenses/GPL-2.0+.html>           *
 ***************************************************************************/


#ifndef JSHIGHLIGHTER_H
#define JSHIGHLIGHTER_H


#include <QSyntaxHighlighter>
#include <QHash>
#include <QTextCharFormat>
#include <QColor>
#include <QTextBlockUserData>

class QTextDocument;

class JSHighlighter : public QSyntaxHighlighter
{
    Q_OBJECT

public:
    JSHighlighter(QTextDocument *parent = 0);
    ~JSHighlighter();
    QColor ColorNormal, ColorComment, ColorNumber, ColorString, ColorOperator, ColorIdentifier, ColorKeyword, ColorBuiltIn;
private :
QStringList m_keywords;
QStringList m_knownIds;
protected:
void highlightBlock(const QString &text);
};


#endif
