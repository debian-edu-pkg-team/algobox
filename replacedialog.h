/***************************************************************************
 *   copyright       : (C) 2009-2022 by Pascal Brachet                     *
 *   https://www.xm1math.net/algobox/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   @license GPL-2.0+ <https://spdx.org/licenses/GPL-2.0+.html>           *
 ***************************************************************************/

#ifndef REPLACEDIALOG_H
#define REPLACEDIALOG_H

#include "ui_replacedialog.h"
#include "algoeditor.h"

class ReplaceDialog : public QDialog
{ 
    Q_OBJECT

public:
    ReplaceDialog(QWidget* parent = 0, const char* name = 0);
    ~ReplaceDialog();
    Ui::ReplaceDialog ui;

public slots:
    virtual void doReplace();
    virtual void doReplaceAll();
    void SetEditor(AlgoEditor *ed);

protected:
    AlgoEditor *editor;
};

#endif // REPLACEDIALOG_H
