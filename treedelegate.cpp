/***************************************************************************
 *   copyright       : (C) 2009-2022 by Pascal Brachet                     *
 *   https://www.xm1math.net/algobox/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   @license GPL-2.0+ <https://spdx.org/licenses/GPL-2.0+.html>           *
 ***************************************************************************/

#include <QtGui>
#include <QAbstractItemModel>
#include <QPainter>
#include <QDebug>
#include <QTextDocument>
#include <QTreeWidgetItem>
#include <QAbstractItemDelegate>
#include "treedelegate.h"
#include "algohighlighter.h"



void TreeDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option,
                          const QModelIndex &index) const
{
    Q_ASSERT(index.isValid());
    const QAbstractItemModel *model = index.model();
    Q_ASSERT(model);
    QStyleOptionViewItem options = option;
    initStyleOption(&options, index);

    painter->save();

    QTextDocument doc;
    AlgoHighlighter highlight(&doc);
    doc.setPlainText(options.text);

    doc.setDocumentMargin(0);
    options.text = "";
    options.widget->style()->drawControl(QStyle::CE_ItemViewItem, &options, painter);

    // shift text right to make icon visible
    QSize iconSize = options.icon.actualSize(options.rect.size());
    painter->translate(options.rect.left()+iconSize.width()+2, options.rect.top());
    QRect clip(0, 0, options.rect.width()+iconSize.width(), options.rect.height());

    painter->setClipRect(clip);

        if (option.showDecorationSelected && (option.state & QStyle::State_Selected)) {
        QPalette::ColorGroup cg = option.state & QStyle::State_Enabled
                                  ? QPalette::Normal : QPalette::Disabled;
        painter->fillRect(clip, option.palette.brush(cg, QPalette::Highlight));
//painter->fillRect(option.rect,QColor("#78A9dc"));
painter->fillRect(clip,QColor("#dde2e8"));
    }
else {
        QVariant value = model->data(index, Qt::BackgroundRole);
        if (value.isValid() && qvariant_cast<QColor>(value).isValid())
            painter->fillRect(clip, qvariant_cast<QColor>(value));
    }
//        doc.drawContents(painter,clip);
     QAbstractTextDocumentLayout::PaintContext ctx;
     // set text color to red for selected item
     // if (option.state & QStyle::State_Selected) qDebug() << "sel";
//          ctx.palette.setColor(QPalette::Text, QColor("white"));
     ctx.clip = clip;
     doc.documentLayout()->draw(painter, ctx);

    painter->restore();


}

QSize TreeDelegate::sizeHint(const QStyleOptionViewItem &option,
                              const QModelIndex &index) const
{
    QStyleOptionViewItem options = option;
    initStyleOption(&options, index);

    QTextDocument doc;
    QFont ft=options.font;
    ft.setBold(true);
    doc.setPlainText(options.text);
    QFontMetrics fontMetrics(ft);
    #if QT_VERSION < QT_VERSION_CHECK(5, 11, 0)
    return QSize(fontMetrics.width(options.text)+2,fontMetrics.lineSpacing()+2);
    #else
    return QSize(fontMetrics.horizontalAdvance(options.text)+2,fontMetrics.lineSpacing()+2);
    #endif
}
