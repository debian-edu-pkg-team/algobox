/***************************************************************************
 *   copyright       : (C) 2009-2022 by Pascal Brachet                     *
 *   https://www.xm1math.net/algobox/                                       *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   @license GPL-2.0+ <https://spdx.org/licenses/GPL-2.0+.html>           *
 ***************************************************************************/

#include "aproposdialog.h"

#include <QFile>
#include <QTextStream>
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    #include <QtCore/QTextCodec>
#else
    #include <QtCore5Compat/QTextCodec>
#endif
#include <QDebug>

#define STRINGIFY_INTERNAL(x) #x
#define STRINGIFY(x) STRINGIFY_INTERNAL(x)

#define VERSION_STR STRINGIFY(ALGOBOXVERSION)

AproposDialog::AproposDialog(QWidget *parent)
    :QDialog( parent)
{
ui.setupUi(this);

QPixmap pixmap;

if (this->devicePixelRatio()>1)
{
pixmap.load(":/images/algobox128@2x.png");
pixmap.setDevicePixelRatio(qApp->devicePixelRatio());
}
else pixmap.load(":/images/algobox128.png");

ui.label->setPixmap(pixmap);
setModal(true);
QTextCodec *codec = QTextCodec::codecForName("UTF-8");
QString contenu=QString::fromUtf8("<b>AlgoBox ")+QLatin1String(VERSION_STR)+QString::fromUtf8("</b><br>(compiled with Qt ")+QLatin1String(QT_VERSION_STR);
ui.textBrowser1->setOpenExternalLinks(true);
ui.textBrowser1->setHtml(contenu+QString::fromUtf8(")<p><b>AlgoBox :</b> logiciel libre et multi-plateforme d'aide à l'élaboration et à l'exécution d'algorithmes.<i>(interface graphique de création de pseudo-code algorithmique et d'interprétation du code obtenu en javascript)</i><br><br><b><i>Copyright (c) 2009/2022 par Pascal Brachet</i></b><br><br><i>(Merci à <b>J.Amblard</b> pour sa précieuse collaboration, à Frédérique Mounier et aux utilisateurs pour leurs remarques et suggestions)</i><br><br>Site internet : <A href=\"https://www.xm1math.net/algobox/\" target=\"_blank\">https://www.xm1math.net/algobox/</A></p><ul><li>Ce programme libre est distribué selon les termes de la licence GPL (voir l'onglet correspondant)</li><li>AlgoBox est développé à partir de l'environnement de développement libre et multi-plateforme <A href=\"https://www.qt.io/developers/\" target=\"_blank\">Qt</a> .</li><li>Certaines icônes du programme sont issues du thème Breeze (Licence: GPL).</li></ul>"));
QString content;
QFile licence(":/utilities/license_html.txt");
licence.open(QIODevice::ReadOnly);
QTextStream inl(&licence);
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
inl.setCodec(codec);
#else
inl.setEncoding(QStringConverter::Encoding::Utf8);
#endif
while (!inl.atEnd()) 
	{
	content+= inl.readLine()+"\n";
	}
licence.close();
ui.textBrowser2->setHtml(content);
}

AproposDialog::~AproposDialog(){
}
