/***************************************************************************
 *   copyright       : (C) 2009-2022 by Pascal Brachet                     *
 *   https://www.xm1math.net/algobox/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   @license GPL-2.0+ <https://spdx.org/licenses/GPL-2.0+.html>           *
 ***************************************************************************/

#include "latexviewdialog.h"

#include <QFontDatabase>
#include <QFile>
#include <QFileDialog>
#include <QFileInfo>
#include <QTextStream>



LatexviewDialog::LatexviewDialog(QWidget *parent, int mode)
    :QDialog( parent)
{
ui.setupUi(this);
setModal(true);
connect(ui.barreOutils->actionSauver, SIGNAL(triggered()), this, SLOT(Sauver(mode)));
highlighter = new LatexHighlighter(ui.textEdit->document());
QFont efont;
QFontDatabase fdb;
QStringList xf = fdb.families();
QString deft;
if (xf.contains("Liberation Mono",Qt::CaseInsensitive)) deft="Liberation Mono";
else if (xf.contains("DejaVu Sans Mono",Qt::CaseInsensitive)) deft="DejaVu Sans Mono";
#if defined( Q_WS_MACX )
else if (xf.contains("Monaco",Qt::CaseInsensitive)) deft="Monaco";
else if (xf.contains("Courier New",Qt::CaseInsensitive)) deft="Courier New";
else if (xf.contains("PT Mono",Qt::CaseInsensitive)) deft="PT Mono";
else if (xf.contains("Andale Mono",Qt::CaseInsensitive)) deft="Andale Mono";
#endif
#if defined(Q_WS_WIN)
else if (xf.contains("Courier New",Qt::CaseInsensitive)) deft="Courier New";
#endif
else deft=qApp->font().family();  
efont.setFamily(deft);
efont.setPointSize(qApp->font().pointSize());
ui.textEdit->setFont(efont);
ui.textEdit->setWordWrapMode(QTextOption::NoWrap);
if (mode==1) codec = QTextCodec::codecForName("ISO-8859-1");
else codec = QTextCodec::codecForName("UTF-8");
ui.label->setText(QString::fromUtf8("Pas encore enregistré"));
show();
}

LatexviewDialog::~LatexviewDialog(){
}

void LatexviewDialog::Sauver(int enc)
{
QString exportFichier = QFileDialog::getSaveFileName(this,QString::fromUtf8("Enregistrer sous"),QDir::homePath(),QString::fromUtf8("Document LaTeX (*.tex)"));
if (!exportFichier.isEmpty()) 
    {
    QFile fichier;
    fichier.setFileName(exportFichier);
    fichier.open(QIODevice::WriteOnly);
    QTextStream out (&fichier);
    #if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    out.setCodec(codec);
    #else
    if (enc==1) out.setEncoding(QStringConverter::Encoding::Latin1);
    else out.setEncoding(QStringConverter::Encoding::Utf8);
    #endif
    out << ui.textEdit->toPlainText();
    fichier.close();
    ui.label->setText(QString::fromUtf8("Fichier enregistré"));
    }  
}
