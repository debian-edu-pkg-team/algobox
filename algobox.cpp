/***************************************************************************
 *   copyright       : (C) 2009-2022 by Pascal Brachet                     *
 *   https://www.xm1math.net/algobox/                                       *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   @license GPL-2.0+ <https://spdx.org/licenses/GPL-2.0+.html>           *
 ***************************************************************************/

#include "algobox.h"
#include "browserdialog.h"
#include "variabledialog.h"
#include "liredialog.h"
#include "afficherdialog.h"
#include "affichercalculdialog.h"
#include "messagedialog.h"
#include "affectationdialog.h"
#include "conditiondialog.h"
#include "pourdialog.h"
#include "tantquedialog.h"
#include "pointdialog.h"
#include "segmentdialog.h"
#include "aproposdialog.h"
#include "commentairedialog.h"
#include "verifdialog.h"
#include "x11fontdialog.h"
#include "modifierlignedialog.h"
#include "consoledialog.h"
#include "latexviewdialog.h"
#include "configdialog.h"
#include "treedelegate.h"
#include "geticon.h"
#include "fonctiondialog.h"
#include "renvoyerdialog.h"
#include "appeldialog.h"

#include <QSettings>
#include <QAction>
#include <QMessageBox>
#include <QApplication>
#include <QDir>
#include <QStyleFactory>
#include <QStyle>
#include <QDomDocument>
#include <QDomElement>
#include <QDomProcessingInstruction>
#include <QFile>
#include <QFileDialog>
#include <QFileInfo>
#include <QFontDatabase>
#include <QDebug>
#include <QTextStream>
#include <QFont>
#include <QColor>
#include <QDesktopServices>
#include <QUrl>
#include <QStyleFactory>
#include <QStyle>
#include <QTextDocument>
#include <QPrintDialog>
#include <QPixmap>
#include <QPrinter>
#include <QDate>
#include <QTextDocumentWriter>
#include <QTextCharFormat>
#include <QAbstractItemModel>
#include <QButtonGroup>
#include <QRegularExpression>

#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    #include <QtCore/QTextCodec>
#else
    #include <QtCore5Compat/QTextCodec>
#endif

#define STRINGIFY_INTERNAL(x) #x
#define STRINGIFY(x) STRINGIFY_INTERNAL(x)

#define VERSION_STR STRINGIFY(ALGOBOXVERSION)

MainWindow::MainWindow(QWidget *parent)
    :QMainWindow( parent)
{
ui.setupUi(this);
//ui.stackedWidget->removeWidget(ui.stackedWidget->findChild<QWidget *>("page")); 
#ifndef VERSION_MINIPC
ui.textEditTip->setPlainText(QString::fromUtf8("- Pour utiliser une variable, il faut d'abord la déclarer (bouton \"Déclarer nouvelle variable\").\n- Pour ajouter un nouvel élément à l'algorithme, il faut d'abord insérer une nouvelle ligne (bouton \"Nouvelle Ligne\"), puis cliquer sur un des boutons disponibles dans les panneaux disponibles en bas de la fenêtre."));
ui.textEditTipEditeur->setPlainText(QString::fromUtf8("- La \"syntaxe AlgoBox\" doit être pleinement respectée sous peine de non-reconnaissance du code.\n- Une ligne ne doit comporter qu'une seule instruction.\n- Lors de l'utilisation de l'auto-complétion et des commandes du panneau inférieur, le symbole • représente un champ à compléter : pour passer au champ suivant, il suffit d'utiliser la touche Tab.\n- Un clic-droit permet d'accéder aux commandes du menu spécial \"éditeur\".\n- Le symbole * après l'instruction AFFICHER indique que l'affichage sera suivi d'un retour à la ligne."));
ui.textEditTipFonction->setPlainText(QString::fromUtf8("- Pour créer une nouvelle fonction locale, se placer sur FONCTIONS_UTILISEES, cliquer sur \"Nouvelle ligne\" puis sur \"Déclarer nouvelle fonction\".\n- Pour déclarer une variable locale nécessaire à une fonction, se placer sur la ligne \"VARIABLES_FONCTION\" correspondant à la fonction et cliquer sur \"Déclarer variable locale\".\n- Le bouton \"Ajouter RENVOYER valeur\" permet de définir ce que renverra la fonction.\n- Le bouton \"Appeler Fonction\" sert à appeler une fonction qui ne renvoie pas de valeur."));
#else
ui.textEditTip->hide();
ui.textEditTipEditeur->hide();
#endif
LireConfig();
clipboardItem=new QTreeWidgetItem(QStringList(QString("")));
QFile file(":/documents/styleapplication.qss");
file.open(QFile::ReadOnly);
QString styleSheet = QLatin1String(file.readAll());
qApp->setStyleSheet(styleSheet);

QFont fontCommande=qApp->font();
fontCommande.setBold(true);
QFont fontCommentaire=qApp->font();
//fontCommentaire.setItalic(true);
#if defined(Q_OS_MAC)
fontCommentaire.setPointSize(10);
#else
fontCommentaire.setPointSize(8);
#endif

ui.treeWidget->setItemDelegate(new TreeDelegate());

QListWidgetItem *commande, *commentaire;
QStringList commandeList,commentaireList,roleList;

commandeList << "sqrt(x)";
commentaireList << QString::fromUtf8("-> racine carrée de x");
roleList << "sqrt()#5";

commandeList << "pow(x,n)";
commentaireList << QString::fromUtf8("-> x puissance n");
roleList << "pow(,)#4";

commandeList << "x%y";
commentaireList << QString::fromUtf8("-> reste de la division de x par y");
roleList << "%#0";

commandeList << "cos(x)";
commentaireList << QString::fromUtf8("-> cosinus de x");
roleList << "cos()#4";

commandeList << "sin(x)";
commentaireList << QString::fromUtf8("-> sinus de x");
roleList << "sin()#4";

commandeList << "tan(x)";
commentaireList << QString::fromUtf8("-> tangente de x");
roleList << "tan()#4";

commandeList << "exp(x)";
commentaireList << QString::fromUtf8("-> exponentielle de x");
roleList << "exp()#4";

commandeList << "log(x)";
commentaireList << QString::fromUtf8("-> logarithme népérien de x");
roleList << "log()#4";

commandeList << "abs(x)";
commentaireList << QString::fromUtf8("-> valeur absolue de x");
roleList << "abs()#4";

commandeList << "floor(x)";
commentaireList << QString::fromUtf8("-> partie entière de x");
roleList << "floor()#6";

commandeList << "round(x)";
commentaireList << QString::fromUtf8("-> entier le plus proche de x");
roleList << "round()#6";

commandeList << "acos(x)";
commentaireList << QString::fromUtf8("-> arccosinus de x");
roleList << "acos()#5";

commandeList << "asin(x)";
commentaireList << QString::fromUtf8("-> arcsinus de x");
roleList << "asin()#5";

commandeList << "atan(x)";
commentaireList << QString::fromUtf8("-> arctangente de x");
roleList << "atan()#5";

commandeList << "Math.PI";
commentaireList << QString::fromUtf8("-> constante PI");
roleList << "Math.PI#7";

commandeList << "random()";
commentaireList << QString::fromUtf8("-> nombre pseudo-aléatoire entre 0 et 1");
roleList << "random()#7";

commandeList << "ALGOBOX_ALEA_ENT(p,n)";
commentaireList << QString::fromUtf8("-> entier pseudo-aléatoire entre p et n");
roleList << "ALGOBOX_ALEA_ENT(,)#17";

commandeList << "ALGOBOX_COEFF_BINOMIAL(n,p)";
commentaireList << QString::fromUtf8("-> coefficient binomial 'p parmi n'");
roleList << "ALGOBOX_COEFF_BINOMIAL(,)#23";

commandeList << "ALGOBOX_LOI_BINOMIALE(n,p,k)";
commentaireList << QString::fromUtf8("-> p(X=k) pour la loi binomiale de paramètres (n,p)");
roleList << "ALGOBOX_LOI_BINOMIALE(,,)#22";

commandeList << "ALGOBOX_LOI_NORMALE_CR(x)";
commentaireList << QString::fromUtf8("-> p(X<x) pour la loi normale centrée réduite");
roleList << "ALGOBOX_LOI_NORMALE_CR()#23";

commandeList << "ALGOBOX_LOI_NORMALE(esp,ecart,x)";
commentaireList << QString::fromUtf8("-> p(X<x) pour la loi normale d'espérance 'esp' et d'écart-type 'ecart'");
roleList << "ALGOBOX_LOI_NORMALE(,,)#20";

commandeList << "ALGOBOX_INVERSE_LOI_NORMALE_CR(p)";
commentaireList << QString::fromUtf8("-> opération inverse de ALGOBOX_LOI_NORMALE_CR(x)");
roleList << "ALGOBOX_INVERSE_LOI_NORMALE_CR()#31";

commandeList << "ALGOBOX_INVERSE_LOI_NORMALE(esp,ecart,p)";
commentaireList << QString::fromUtf8("-> opération inverse de ALGOBOX_LOI_NORMALE(esp,ecart,x)");
roleList << "ALGOBOX_INVERSE_LOI_NORMALE(,,)#28";

commandeList << "ALGOBOX_FACTORIELLE(n)";
commentaireList << QString::fromUtf8("-> factorielle de n");
roleList << "ALGOBOX_FACTORIELLE()#20";

commandeList << "ALGOBOX_SOMME(liste,p,n)";
commentaireList << QString::fromUtf8("-> somme des termes de liste[p] à liste[n]");
roleList << "ALGOBOX_SOMME(,,)#14";

commandeList << "ALGOBOX_MOYENNE(liste,p,n)";
commentaireList << QString::fromUtf8("-> moyenne des termes de liste[p] à liste[n]");
roleList << "ALGOBOX_MOYENNE(,,)#16";

commandeList << "ALGOBOX_VARIANCE(liste,p,n)";
commentaireList << QString::fromUtf8("-> variance des termes de liste[p] à liste[n]");
roleList << "ALGOBOX_VARIANCE(,,)#17";

commandeList << "ALGOBOX_ECART_TYPE(liste,p,n)";
commentaireList << QString::fromUtf8("-> écart-type des termes de liste[p] à liste[n]");
roleList << "ALGOBOX_ECART_TYPE(,,)#19";

commandeList << "ALGOBOX_MEDIANE(liste,p,n)";
commentaireList << QString::fromUtf8("-> médiane des termes de liste[p] à liste[n]");
roleList << "ALGOBOX_MEDIANE(,,)#16";

commandeList << "ALGOBOX_QUARTILE1(liste,p,n)";
commentaireList << QString::fromUtf8("-> Q1 (calculatrice) des termes de liste[p] à liste[n]");
roleList << "ALGOBOX_QUARTILE1(,,)#18";

commandeList << "ALGOBOX_QUARTILE3(liste,p,n)";
commentaireList << QString::fromUtf8("-> Q3 (calculatrice) des termes de liste[p] à liste[n]");
roleList << "ALGOBOX_QUARTILE3(,,)#18";

commandeList << "ALGOBOX_QUARTILE1_BIS(liste,p,n)";
commentaireList << QString::fromUtf8("-> Q1 (25%) des termes de liste[p] à liste[n]");
roleList << "ALGOBOX_QUARTILE1_BIS(,,)#22";

commandeList << "ALGOBOX_QUARTILE3_BIS(liste,p,n)";
commentaireList << QString::fromUtf8("-> Q3 (75%) des termes de liste[p] à liste[n]");
roleList << "ALGOBOX_QUARTILE3_BIS(,,)#22";

commandeList << "ALGOBOX_MINIMUM(liste,p,n)";
commentaireList << QString::fromUtf8("-> minimum des termes de liste[p] à liste[n]");
roleList << "ALGOBOX_MINIMUM(,,)#16";

commandeList << "ALGOBOX_MAXIMUM(liste,p,n)";
commentaireList << QString::fromUtf8("-> maximum des termes de liste[p] à liste[n]");
roleList << "ALGOBOX_MAXIMUM(,,)#16";

commandeList << "ALGOBOX_POS_MINIMUM(liste,p,n)";
commentaireList << QString::fromUtf8("-> rang du minimum des termes de liste[p] à liste[n]");
roleList << "ALGOBOX_POS_MINIMUM(,,)#20";

commandeList << "ALGOBOX_POS_MAXIMUM(liste,p,n)";
commentaireList << QString::fromUtf8("-> rang du maximum des termes de liste[p] à liste[n]");
roleList << "ALGOBOX_POS_MAXIMUM(,,)#20";

commandeList << "ALGOBOX_ARRONDIR(x,n)";
commentaireList << QString::fromUtf8("-> arrondit x avec n chiffres après la virgule");
roleList << "ALGOBOX_ARRONDIR(,)#17";

for (int i = 0; i < commandeList.count(); i++)
{
commande=new QListWidgetItem(ui.listWidgetOp);
commande->setFont(fontCommande);
commande->setText(commandeList.at(i));
commande->setData(Qt::UserRole,roleList.at(i));
commentaire=new QListWidgetItem(ui.listWidgetOp);
commentaire->setFont(fontCommentaire);
commentaire->setText(commentaireList.at(i));
commentaire->setFlags(Qt::ItemIsEnabled);
}

connect(ui.listWidgetOp, SIGNAL(itemClicked ( QListWidgetItem*)), this, SLOT(InsertOperation(QListWidgetItem*)));

QTextCodec *codec = QTextCodec::codecForName("UTF-8");
/*QString contenu;
QFile aide(":/documents/aidefonction.txt");
aide.open(QIODevice::ReadOnly);
QTextStream in(&aide);
in.setCodec(codec);
while (!in.atEnd()) 
	{
	contenu+= in.readLine()+"\n";
	}
aide.close();
ui.textEditTipFonction->setHtml(contenu);*/

#if defined(Q_OS_MAC)
setWindowIcon(QIcon(":/images/algobox128.png"));
#else
setWindowIcon(getIcon(":/images/algobox22.png"));
#endif
connect(ui.pushButtonAjouterLigne, SIGNAL(clicked()), this, SLOT(AjouterLigne()));

connect(ui.pushButtonSupprimerLigne, SIGNAL(clicked()), this, SLOT(SupprimerLigne()));

connect(ui.pushButtonModifierLigne, SIGNAL(clicked()), this, SLOT(ModifierLigne()));

connect(ui.treeWidget, SIGNAL(currentItemChanged(QTreeWidgetItem *, QTreeWidgetItem *)),this,SLOT(ActualiserArbre()));

connect(ui.pushButtonVariable, SIGNAL(clicked()), this, SLOT(AjouterVariable()));

connect(ui.pushButtonLire, SIGNAL(clicked()), this, SLOT(AjouterLire()));

connect(ui.pushButtonAfficher, SIGNAL(clicked()), this, SLOT(AjouterAfficher()));

connect(ui.pushButtonMessage, SIGNAL(clicked()), this, SLOT(AjouterMessage()));

connect(ui.pushButtonCalcul, SIGNAL(clicked()), this, SLOT(AjouterCalcul()));

connect(ui.pushButtonPause, SIGNAL(clicked()), this, SLOT(AjouterPause()));

connect(ui.pushButtonAffectation, SIGNAL(clicked()), this, SLOT(AjouterAffectation()));

connect(ui.pushButtonCondition, SIGNAL(clicked()), this, SLOT(AjouterCondition()));

connect(ui.pushButtonBoucle, SIGNAL(clicked()), this, SLOT(AjouterBoucle()));

connect(ui.pushButtonTantque, SIGNAL(clicked()), this, SLOT(AjouterTantque()));

connect(ui.pushButtonPoint, SIGNAL(clicked()), this, SLOT(AjouterPoint()));

connect(ui.pushButtonSegment, SIGNAL(clicked()), this, SLOT(AjouterSegment()));

connect(ui.pushButtonEfface, SIGNAL(clicked()), this, SLOT(AjouterEffacer()));

connect(ui.pushButtonExecuter, SIGNAL(clicked()), this, SLOT(JavascriptExport()));

connect(ui.pushButtonCommentaire, SIGNAL(clicked()), this, SLOT(AjouterCommentaire()));

connect(ui.pushButtonNelleFct, SIGNAL(clicked()), this, SLOT(AjouterFonction()));

connect(ui.pushButtonVariableFct, SIGNAL(clicked()), this, SLOT(AjouterVariableLocale()));

connect(ui.pushButtonRetourFct, SIGNAL(clicked()), this, SLOT(AjouterRenvoyer()));

connect(ui.pushButtonAppelFonction, SIGNAL(clicked()), this, SLOT(AjouterAppelerFct()));

connect(ui.checkBoxFonction, SIGNAL(toggled(bool)),this, SLOT(ActiverFonction(bool)));

connect(ui.checkBoxRepere, SIGNAL(toggled(bool)),this, SLOT(ActiverRepere(bool)));

connect(ui.pushButtonExecuterBis, SIGNAL(clicked()), this, SLOT(JavascriptExport()));

connect(ui.pushButtonVerifier, SIGNAL(clicked()), this, SLOT(VerifierCodeTexte()));

connect(ui.checkBoxF2, SIGNAL(toggled(bool)),this, SLOT(ActiverF2(bool)));
connect(ui.pushButtonAjouterF2, SIGNAL(clicked()), this, SLOT(AjouterF2()));
connect(ui.pushButtonHautF2, SIGNAL(clicked()), this, SLOT(HautF2()));
connect(ui.pushButtonBasF2, SIGNAL(clicked()), this, SLOT(BasF2()));
connect(ui.pushButtonSupprimerF2, SIGNAL(clicked()), this, SLOT(SupprimerF2()));
connect(ui.listWidgetF2, SIGNAL(itemDoubleClicked ( QListWidgetItem*)), this, SLOT(ModifierLigneF2(QListWidgetItem*)));

fichierMenu = menuBar()->addMenu(QString::fromUtf8("&Fichier"));
QAction *Act;

Act = new QAction(getIcon(":/images/nouveau.png"),QString::fromUtf8("&Nouveau"), this);
Act->setShortcut(Qt::CTRL+Qt::Key_N);
connect(Act, SIGNAL(triggered()), this, SLOT(NouvelAlgo()));
fichierMenu->addAction(Act);

Act = new QAction(getIcon(":/images/ouvrir.png"),QString::fromUtf8("&Ouvrir"), this);
Act->setShortcut(Qt::CTRL+Qt::Key_O);
connect(Act, SIGNAL(triggered()), this, SLOT(ChargerAlgo()));
fichierMenu->addAction(Act);

recentMenu=fichierMenu->addMenu(QString::fromUtf8("Récemment ouverts"));
for (int i = 0; i < 5; ++i) 
	{
	recentFileActs[i] = new QAction(this);
	recentFileActs[i]->setVisible(false);
	connect(recentFileActs[i], SIGNAL(triggered()),this, SLOT(fileOpenRecent()));
	recentMenu->addAction(recentFileActs[i]);
	}
//********************************************************************************
// Act = new QAction(QString::fromUtf8("&Ouvrir code texte"), this);
// connect(Act, SIGNAL(triggered()), this, SLOT(ImporterCodeTexte()));
// fichierMenu->addAction(Act);
//********************************************************************************

Act = new QAction(getIcon(":/images/sauver.png"),QString::fromUtf8("&Sauver"), this);
Act->setShortcut(Qt::CTRL+Qt::Key_S);
connect(Act, SIGNAL(triggered()), this, SLOT(SauverAlgo()));
fichierMenu->addAction(Act);

Act = new QAction(getIcon(":/images/sauversous.png"),QString::fromUtf8("Sauver So&us"), this);
connect(Act, SIGNAL(triggered()), this, SLOT(SauverSousAlgo()));
fichierMenu->addAction(Act);

fichierMenu->addSeparator();

Act = new QAction(QString::fromUtf8("Exporter algorithme complet vers page &web"), this);
connect(Act, SIGNAL(triggered()), this, SLOT(ExporterVersHtml()));
fichierMenu->addAction(Act);

Act = new QAction(QString::fromUtf8("Exporter code vers fichier &texte"), this);
connect(Act, SIGNAL(triggered()), this, SLOT(ExporterVersTexte()));
fichierMenu->addAction(Act);

Act = new QAction(QString::fromUtf8("Exporter code au format O&DF"), this);
connect(Act, SIGNAL(triggered()), this, SLOT(ExporterVersODF()));
fichierMenu->addAction(Act);

Act = new QAction(QString::fromUtf8("Exporter code vers document &LaTeX"), this);
connect(Act, SIGNAL(triggered()), this, SLOT(ExporterVersLatex()));
fichierMenu->addAction(Act);

Act = new QAction(QString::fromUtf8("&Copie d'écran de l'algorithme"), this);
connect(Act, SIGNAL(triggered()), this, SLOT(CopieEcran()));
fichierMenu->addAction(Act);

fichierMenu->addSeparator();

Act = new QAction(getIcon(":/images/imprimer.png"),QString::fromUtf8("&Imprimer algorithme"), this);
connect(Act, SIGNAL(triggered()), this, SLOT(Imprimer()));
fichierMenu->addAction(Act);

fichierMenu->addSeparator();

Act = new QAction(QString::fromUtf8("Ouvrir un &exemple"), this);
connect(Act, SIGNAL(triggered()), this, SLOT(ChargerExemple()));
fichierMenu->addAction(Act);

fichierMenu->addSeparator();

Act = new QAction( QString::fromUtf8("&Quitter"), this);
Act->setShortcut(Qt::CTRL+Qt::Key_Q);
connect(Act, SIGNAL(triggered()), this, SLOT(Quitter()));
fichierMenu->addAction(Act);

editMenu= menuBar()->addMenu(QString::fromUtf8("&Edition"));

actionCopier=new QAction(getIcon(":/images/copier.png"),QString::fromUtf8("Cop&ier Ligne"), this);
actionCopier->setShortcut(Qt::CTRL+Qt::Key_C);
connect(actionCopier, SIGNAL(triggered()), this, SLOT(EditCopier()));
editMenu->addAction(actionCopier);
actionCopier->setEnabled(false);

actionColler=new QAction(getIcon(":/images/coller.png"),QString::fromUtf8("C&oller Ligne"), this);
actionColler->setShortcut(Qt::CTRL+Qt::Key_V);
connect(actionColler, SIGNAL(triggered()), this, SLOT(EditColler()));
editMenu->addAction(actionColler);
actionColler->setEnabled(false);

actionCouper=new QAction(getIcon(":/images/couper.png"),QString::fromUtf8("Co&uper Ligne"), this);
actionCouper->setShortcut(Qt::CTRL+Qt::Key_X);
connect(actionCouper, SIGNAL(triggered()), this, SLOT(EditCouper()));
editMenu->addAction(actionCouper);
actionCouper->setEnabled(false);

modeMenu=editMenu->addMenu(QString::fromUtf8("Mode &Edition"));
modeGroup=new QActionGroup(this);
actionModeNormal = new QAction(QString::fromUtf8("Mode &normal"), this);
actionModeNormal->setCheckable(true);
connect(actionModeNormal, SIGNAL(triggered()), this, SLOT(ActualiserMode()));
modeGroup->addAction(actionModeNormal);
modeMenu->addAction(actionModeNormal);
if (modeNormal) actionModeNormal->setChecked(true);
actionModeTexte = new QAction(QString::fromUtf8("Mode éditeur &texte"), this);
actionModeTexte->setCheckable(true);
connect(actionModeTexte, SIGNAL(triggered()), this, SLOT(ActualiserMode()));
modeGroup->addAction(actionModeTexte);
modeMenu->addAction(actionModeTexte);
if (!modeNormal) actionModeTexte->setChecked(true);

tutoMenu = menuBar()->addMenu(QString::fromUtf8("&Tutoriel"));
Act = new QAction(QString::fromUtf8("&Initiation en ligne à l'algorithmique"), this);
connect(Act, SIGNAL(triggered()), this, SLOT(Tutoriel()));
tutoMenu->addAction(Act);

affichageMenu=menuBar()->addMenu(QString::fromUtf8("Afficha&ge"));

#ifndef VERSION_MINIPC
fileToolBar = addToolBar("Barre d'outils");
fileToolBar->setObjectName("Barre d'outils");
fileToolBar->setIconSize(QSize(22,22 ));
fileToolBar->setOrientation(Qt::Horizontal);
fileToolBar->setMovable(false);
fileToolBar->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
fileToolBar->setFloatable(false);

Act = new QAction(getIcon(":/images/nouveau22.png"), QString::fromUtf8("Nouveau"), this);
connect(Act, SIGNAL(triggered()), this, SLOT(NouvelAlgo()));
fileToolBar->addAction(Act);

Act = new QAction(getIcon(":/images/ouvrir22.png"),QString::fromUtf8("Ouvrir"), this);
connect(Act, SIGNAL(triggered()), this, SLOT(ChargerAlgo()));
fileToolBar->addAction(Act);

Act = new QAction(getIcon(":/images/sauver22.png"), QString::fromUtf8("Sauver"), this);
connect(Act, SIGNAL(triggered()), this, SLOT(SauverAlgo()));
fileToolBar->addAction(Act);
fileToolBar->addSeparator();

Act = new QAction(getIcon(":/images/executer22.png"), QString::fromUtf8("Tester"), this);
connect(Act, SIGNAL(triggered()), this, SLOT(JavascriptExport()));
fileToolBar->addAction(Act);

affichageMenu->addAction(fileToolBar->toggleViewAction());

QFrame* toolbarFrame=new QFrame(fileToolBar);
buttonModeNormal = new QPushButton(QString::fromUtf8("Mode normal"),toolbarFrame);
connect(buttonModeNormal, SIGNAL(clicked()), this, SLOT(ActualiserModeButton()));
buttonModeTexte = new QPushButton(QString::fromUtf8("Mode éditeur texte") , toolbarFrame);
connect(buttonModeTexte, SIGNAL(clicked()), this, SLOT(ActualiserModeButton()));
buttonModeNormal->setCheckable(true);
buttonModeTexte->setCheckable(true);
QHBoxLayout* layout = new QHBoxLayout(toolbarFrame);
QSpacerItem * spacer = new QSpacerItem(1,1, QSizePolicy::Expanding, QSizePolicy::Fixed);
layout->addSpacerItem(spacer);
layout->addWidget(buttonModeNormal);
layout->addWidget(buttonModeTexte);
QButtonGroup* group = new QButtonGroup(toolbarFrame);
group->addButton(buttonModeNormal);
group->addButton(buttonModeTexte);
group->setExclusive(true);
fileToolBar->addWidget(toolbarFrame);
if (modeNormal) buttonModeNormal->setChecked(true);
else buttonModeTexte->setChecked(true);
#endif

ToggleAct = new QAction(this);
ToggleAct->setText(QString::fromUtf8("Cadre '&Présentation'"));
ToggleAct->setCheckable(true);
ToggleAct->setChecked(afficheCadrePresentation);
connect(ToggleAct, SIGNAL(triggered()), this, SLOT(ToggleCadrePresentation()));
affichageMenu->addAction(ToggleAct);

affichageMenu->addSeparator();
Act = new QAction(QString::fromUtf8("Changer Police &Interface"), this);
connect(Act, SIGNAL(triggered()), this, SLOT(SetInterfaceFont()));
affichageMenu->addAction(Act);

affichageMenu->addSeparator();
Act = new QAction(QString::fromUtf8("Changer Couleur &Console 'Résultats'"), this);
connect(Act, SIGNAL(triggered()), this, SLOT(CouleurConsole()));
affichageMenu->addAction(Act);

extensionMenu = menuBar()->addMenu(QString::fromUtf8("E&xtension"));
Act = new QAction(QString::fromUtf8("&Charger une extension"), this);
connect(Act, SIGNAL(triggered()), this, SLOT(ChargerExtension()));
extensionMenu->addAction(Act);
annulerExtensionAct= new QAction(QString::fromUtf8("&Annuler l'utilisation d'une extension"), this);
connect(annulerExtensionAct, SIGNAL(triggered()), this, SLOT(AnnulerExtension()));
extensionMenu->addAction(annulerExtensionAct);
extensionMenu->addSeparator();
Act = new QAction(QString::fromUtf8("&Ecrire/modifier une extension"), this);
connect(Act, SIGNAL(triggered()), this, SLOT(LancerJSEditeur()));
extensionMenu->addAction(Act);


optionsMenu = menuBar()->addMenu(QString::fromUtf8("Option&s"));
Act = new QAction(QString::fromUtf8("Con&figurer exécution des algorithmes"), this);
connect(Act, SIGNAL(triggered()), this, SLOT(Configurer()));
optionsMenu->addAction(Act);

connect(ui.pushButtonParam, SIGNAL(clicked()), this, SLOT(Configurer()));

aideMenu = menuBar()->addMenu(QString::fromUtf8("&Aide"));
Act = new QAction(getIcon(":/images/aide.png"),QString::fromUtf8("A&ide"), this);
connect(Act, SIGNAL(triggered()), this, SLOT(Aide()));
aideMenu->addAction(Act);
aideMenu->addSeparator();
Act = new QAction(QString::fromUtf8("A &propos d'AlgoBox"), this);
connect(Act, SIGNAL(triggered()), this, SLOT(APropos()));
aideMenu->addAction(Act);

completer = new QCompleter(this);
QAbstractItemModel *model;
QFile completefile(":/documents/completion.txt");
if (!completefile.open(QFile::ReadOnly)) model=new QStringListModel(completer);
QStringList words;
QString line;
QString completionContent=codec->toUnicode(completefile.readAll());
QTextStream tscompleter(&completionContent);

while (!tscompleter.atEnd()) 
	{
	line = tscompleter.readLine();
	if (!line.isEmpty()) words.append(line.remove("\n"));
	}
words.sort();
model=new QStringListModel(words, completer);
completer->setModel(model);
completer->setModelSorting(QCompleter::CaseInsensitivelySortedModel);
completer->setCaseSensitivity(Qt::CaseInsensitive);
completer->setWrapAround(false);
ui.EditorView->editor->setCompleter(completer);

UpdateRecentFile();

QFile interditsfile(":/documents/interdits.txt");
if (interditsfile.open(QFile::ReadOnly))
    {
    QString line;
    while (!interditsfile.atEnd()) 
	    {
	    line = interditsfile.readLine();
	    if (!line.isEmpty()) ListeNomsInterdits.append(line.trimmed());
	    }
    }

estVierge=true;
estModifie=false;
nomFichier="sanstitre";
setWindowTitle(QString("AlgoBox ")+QLatin1String(VERSION_STR)+QString(" : ")+nomFichier);
AnnulerExtension();
Init();
DesactiverBoutons();
ActualiserArbre();

connect(ui.textEditDescription, SIGNAL(textChanged()),this,SLOT(ActualiserStatut()));
connect(ui.lineEditFonction, SIGNAL(textChanged(const QString)),this,SLOT(ActualiserStatut()));
connect(ui.lineEditXmin, SIGNAL(textChanged(const QString)),this,SLOT(ActualiserStatut()));
connect(ui.lineEditXmax, SIGNAL(textChanged(const QString)),this,SLOT(ActualiserStatut()));
connect(ui.lineEditYmin, SIGNAL(textChanged(const QString)),this,SLOT(ActualiserStatut()));
connect(ui.lineEditYmax, SIGNAL(textChanged(const QString)),this,SLOT(ActualiserStatut()));
connect(ui.lineEditGradX, SIGNAL(textChanged(const QString)),this,SLOT(ActualiserStatut()));
connect(ui.lineEditGradY, SIGNAL(textChanged(const QString)),this,SLOT(ActualiserStatut()));
connect(ui.lineEditParametresF2, SIGNAL(textChanged(const QString)),this,SLOT(ActualiserStatut()));
connect(ui.lineEditConditionF2, SIGNAL(textChanged(const QString)),this,SLOT(ActualiserStatut()));
connect(ui.lineEditRetourF2, SIGNAL(textChanged(const QString)),this,SLOT(ActualiserStatut()));
connect(ui.lineEditDefautF2, SIGNAL(textChanged(const QString)),this,SLOT(ActualiserStatut()));
//connect(ui.EditorView->editor, SIGNAL(textChanged()), this, SLOT(ActualiserStatut()));
connect(ui.EditorView->editor->document(), SIGNAL(modificationChanged(bool)), this, SLOT(NouveauStatut(bool)));
ui.EditorView->editor->document()->setModified(false);
restoreState(windowstate, 0);

if (modeNormal) ui.stackedWidget->setCurrentWidget(ui.page_arbre);
else 
  {
  ui.stackedWidget->setCurrentWidget(ui.page_editeur);
  ui.EditorView->editor->setFocus();
  }
}

MainWindow::~MainWindow(){
if (progressDialog) delete progressDialog;
}

void MainWindow::closeEvent(QCloseEvent *e)
{
if (estModifie) 
	{
	switch(  QMessageBox::warning(this,QString::fromUtf8("AlgoBox : "),
					QString::fromUtf8("L'algorithme courant a été modifié.\nVoulez-vous l'enregistrer avant de quitter?"),
					QString::fromUtf8("Enregistrer"), QString::fromUtf8("Ne pas enregistrer"), QString::fromUtf8("Abandon"),
					0,
					2 ) )
		{
		case 0:
		SauverAlgo();
		break;
		case 1:
		break;
		case 2:
		default:
		e->ignore();
		return;
		break;
		}
	}
SauverConfig();
if (browserWindow && !browserWindow->isClosed) browserWindow->close();
if (jseditWindow) jseditWindow->close();
e->accept();
}

void MainWindow::Quitter()
{
if (estModifie) 
	{
	switch(  QMessageBox::warning(this,QString::fromUtf8("AlgoBox : "),
					QString::fromUtf8("L'algorithme courant a été modifié.\nVoulez-vous l'enregistrer avant de quitter?"),
					QString::fromUtf8("Enregistrer"), QString::fromUtf8("Ne pas enregistrer"), QString::fromUtf8("Abandon"),
					0,
					2 ) )
		{
		case 0:
		SauverAlgo();
		break;
		case 1:
		break;
		case 2:
		default:
		return;
		break;
		}
	}
SauverConfig();
if (browserWindow && !browserWindow->isClosed) browserWindow->close();
if (jseditWindow) jseditWindow->close();
qApp->quit();
}
//*************************
void MainWindow::AjouterLigne()
{
if (!modeNormal) return;
QTreeWidgetItem *curItem = ui.treeWidget->currentItem();
NouvelleLigne(curItem);
ActualiserArbre();
ActualiserStatut();
}

void MainWindow::NouvelleLigne(QTreeWidgetItem *item)
{
if (!item) return;
QTreeWidgetItem *newItem = 0;
QTreeWidgetItem *nextItem = 0;
if (item->text(0)=="FONCTIONS_UTILISEES")
    {
    newItem=new QTreeWidgetItem(QStringList(QString("")));
    newItem->setData(0,Qt::UserRole,QString("103#commentaire"));
    fctsItem->insertChild(0,newItem);
    ui.treeWidget->setCurrentItem(newItem);
    }
else if (item->text(0)=="DEBUT_ALGORITHME")
    {
    newItem=new QTreeWidgetItem(QStringList(QString("")));
    newItem->setData(0,Qt::UserRole,QString("103#commentaire"));
    debutItem->insertChild(0,newItem);
    ui.treeWidget->setCurrentItem(newItem);
    }
else if ((item->text(0).left(3)=="FIN") ||(item->text(0)=="SINON") )
    {
    if (item->parent())
	{
	int idx = item->parent()->childCount() - 1;
	nextItem = item->parent()->child(idx);
	NouvelleLigne(item->parent());
	}
    }
else if (item->text(0)=="DEBUT_FONCTION")
    {
    newItem=new QTreeWidgetItem(QStringList(QString("")));
    newItem->setData(0,Qt::UserRole,QString("103#commentaire"));
    if (item->parent())
	{
	item->parent()->insertChild(2,newItem);
	ui.treeWidget->setCurrentItem(newItem);	
	}
    }
else if (item->text(0).left(5)=="DEBUT")
    {
    newItem=new QTreeWidgetItem(QStringList(QString("")));
    newItem->setData(0,Qt::UserRole,QString("103#commentaire"));
    if (item->parent())
	{
	item->parent()->insertChild(1,newItem);
	ui.treeWidget->setCurrentItem(newItem);	
	}
    }
else
    {
    if (item->parent()) newItem = new QTreeWidgetItem(item->parent(), item);
    newItem->setText(0,"");
    newItem->setData(0,Qt::UserRole,QString("103#commentaire"));
    ui.treeWidget->setCurrentItem(newItem);
    }
}
void MainWindow::SupprimerLigne()
{
if (!modeNormal) return;
QTreeWidgetItem *curItem = ui.treeWidget->currentItem();
if (!curItem) return;
QString code=curItem->data(0,Qt::UserRole).toString();
QStringList parametres=code.split("#");
int type_para=parametres.at(0).toInt();
int query=1;
if (type_para==1)
    {
    query =QMessageBox::warning(this, "AlgoBox", QString::fromUtf8("Etes-vous sûr de vouloir supprimer la déclaration de cette variable?\n(si elle est utilisée dans le reste du code, l'algorithme ne pourra plus fonctionner)"),QString::fromUtf8("Confirmer la suppression"), QString::fromUtf8("Abandonner") );
    }
else if ((type_para==6) || (type_para==12) || (type_para==15)) 
    {
    query =QMessageBox::warning(this, "AlgoBox", QString::fromUtf8("La suppression de cette ligne entrainera la suppression de tout le bloc qui en dépend.\nEtes-vous sûr de vouloir supprimer tout ce bloc d'instructions?"),QString::fromUtf8("Confirmer la suppression"), QString::fromUtf8("Abandonner") );
    }
else
    {
    query =QMessageBox::warning(this, "AlgoBox", QString::fromUtf8("Etes-vous sûr de vouloir supprimer cette ligne?"),QString::fromUtf8("Confirmer la suppression"), QString::fromUtf8("Abandonner") );
    }
if (query==1) return;
QTreeWidgetItem *nextCurrent = 0;
if (curItem->parent()) 
	{
	int idx = curItem->parent()->indexOfChild(curItem);
	if (idx == curItem->parent()->childCount() - 1) idx--;
	else idx++;
	if (idx < 0) nextCurrent = curItem->parent();
	else nextCurrent = curItem->parent()->child(idx);
	} 
else 
	{
	int idx = ui.treeWidget->indexOfTopLevelItem(curItem);
	if (idx == ui.treeWidget->topLevelItemCount() - 1) idx--;
	else idx++;
	if (idx >= 0) nextCurrent = ui.treeWidget->topLevelItem(idx);
	}
delete curItem;
if (nextCurrent) ui.treeWidget->setCurrentItem(nextCurrent);
ActualiserArbre();
ActualiserStatut();
}

void MainWindow::ModifierLigne()
{
if (!modeNormal) return;
QTreeWidgetItem *curItem = ui.treeWidget->currentItem();
if (!curItem) return;
QString code=curItem->data(0,Qt::UserRole).toString();
QStringList parametres=code.split("#");
int type_para=parametres.at(0).toInt();
if (type_para==2) ModifierLire();
else if (type_para==3) ModifierAfficher();
else if (type_para==4) ModifierMessage();
else if (type_para==20) ModifierCalcul();
else if (type_para==5) ModifierAffectation();
else if (type_para==6) ModifierCondition();
else if (type_para==12) ModifierBoucle();
else if (type_para==15) ModifierTantque();
else if (type_para==19) ModifierCommentaire();
else if (type_para==50) ModifierPoint();
else if (type_para==51) ModifierSegment();
else if (type_para==201) ModifierFonction();
else if (type_para==205) ModifierRenvoyer();
else if (type_para==206) ModifierAppelerFct();
ActualiserArbre();
ActualiserStatut();
}

void MainWindow::ActualiserArbre()
{
if (modeNormal)
  {
  bool canAddline = true;
  bool canRemoveline = true;
  bool canModifyline=true;
  bool canCopyline=false;
  bool canPasteline=false;
  bool canCutline=false;
  bool canAddFct=false;
  bool canAddVariableFct=false;
  QTreeWidgetItem *current = ui.treeWidget->currentItem();
  QTreeWidgetItem *next = 0;
  if (current) 
	  {
	  QString code=current->data(0,Qt::UserRole).toString();
	  QStringList parametres=code.split("#");
	  int type_para=parametres.at(0).toInt();
	  switch (type_para)
	      {
	      case 1: //VARIABLES
		      if (parametres.count()==3)
			  {
			  canRemoveline=true;
			  canAddline = false;
			  canModifyline=false;
			  canCopyline=false;
			  canPasteline=false;
			  canCutline=false;
              canAddFct=false;
              canAddVariableFct=false;
			  }
		      break;
	      case 2: //LIRE
		      if (parametres.count()==3)
			  {
			  canRemoveline=true;
			  canAddline = true;
			  canModifyline=true;
			  canCopyline=true;
			  canPasteline=false;
			  canCutline=true;
              canAddFct=false;
              canAddVariableFct=false;
			  }
		      break;
	      case 3: //AFFICHER
		      if (parametres.count()==4)
			  {
			  canRemoveline=true;
			  canAddline = true;
			  canModifyline=true;
			  canCopyline=true;
			  canPasteline=false;
			  canCutline=true;
              canAddFct=false;
              canAddVariableFct=false;
			  }
		      break;
	      case 4: //MESSAGE
		      if (parametres.count()==3)
			  {
			  canRemoveline=true;
			  canAddline = true;
			  canModifyline=true;
			  canCopyline=true;
			  canPasteline=false;
			  canCutline=true;
              canAddFct=false;
              canAddVariableFct=false;
			  }
		      break;
	      case 5: //AFFECTATION
		      if (parametres.count()==4)
			  {
			  canRemoveline=true;
			  canAddline = true;
			  canModifyline=true;
			  canCopyline=true;
			  canPasteline=false;
			  canCutline=true;
              canAddFct=false;
              canAddVariableFct=false;
			  }
		      break;
	      case 6: //SI
		      if (parametres.count()==2)
			  {
			  canRemoveline=true;
			  canAddline = false;
			  canModifyline=true;
			  canCopyline=true;//false
			  canPasteline=false;
			  canCutline=true;//false
              canAddFct=false;
              canAddVariableFct=false;
			  }
		      break;
	      case 7: //DEBUT_SI
		      if (parametres.count()==2)
			  {
			  canRemoveline=false;
			  canAddline = true;
			  canModifyline=false;
			  canCopyline=false;
			  canPasteline=false;
			  canCutline=false;
              canAddFct=false;
              canAddVariableFct=false;
			  }
		      break;
	      case 8: //FIN_SI
		      if (parametres.count()==2)
			  {
			  canRemoveline=false;
			  canAddline = false;
			  canModifyline=false;
			  if (current->parent())
			      {
			      int idx = current->parent()->indexOfChild(current);
			      if (idx<current->parent()->childCount() - 1)
				  {
				  idx++;
				  if (idx>=0) 
				      {
				      next=current->parent()->child(idx);
				      if (next->text(0)!="SINON") canAddline = true;			  
				      }
				  }
			      else canAddline = true;
			      }
			  canCopyline=false;
			  canPasteline=false;
			  canCutline=false;
              canAddFct=false;
              canAddVariableFct=false;
			  }
		      break;
	      case 9: //SINON
		      if (parametres.count()==2)
			  {
			  canRemoveline=true;
			  canAddline = false;
			  canModifyline=false;
			  canCopyline=false;
			  canPasteline=false;
			  canCutline=false;
              canAddFct=false;
              canAddVariableFct=false;
			  }
		      break;
	      case 10: //DEBUT_SINON
		      if (parametres.count()==2)
			  {
			  canRemoveline=false;
			  canAddline = true;
			  canModifyline=false;
			  canCopyline=false;
			  canPasteline=false;
			  canCutline=false;
              canAddFct=false;
              canAddVariableFct=false;
			  }
		      break;
	      case 11: //FIN_SINON
		      if (parametres.count()==2)
			  {
			  canRemoveline=false;
			  canAddline = true;
			  canModifyline=false;
			  canCopyline=false;
			  canPasteline=false;
			  canCutline=false;
              canAddFct=false;
              canAddVariableFct=false;
			  }
		      break;
	      case 12: //POUR
		      if (parametres.count()==4)
			  {
			  canRemoveline=true;
			  canAddline = false;
			  canModifyline=true;
			  canCopyline=true;//false
			  canPasteline=false;
			  canCutline=true;//false
              canAddFct=false;
              canAddVariableFct=false;
			  }
		      break;
	      case 13: //DEBUT_POUR
		      if (parametres.count()==2)
			  {
			  canRemoveline=false;
			  canAddline = true;
			  canModifyline=false;
			  canCopyline=false;
			  canPasteline=false;
			  canCutline=false;
              canAddFct=false;
              canAddVariableFct=false;
			  }
		      break;
	      case 14: //FIN_POUR
		      if (parametres.count()==2)
			  {
			  canRemoveline=false;
			  canAddline = true;
			  canModifyline=false;
			  canCopyline=false;
			  canPasteline=false;
			  canCutline=false;
              canAddFct=false;
              canAddVariableFct=false;
			  }
		      break;
	      case 15: //TANT_QUE
		      if (parametres.count()==2)
			  {
			  canRemoveline=true;
			  canAddline = false;
			  canModifyline=true;
			  canCopyline=true;//false
			  canPasteline=false;
			  canCutline=true;//false
              canAddFct=false;
              canAddVariableFct=false;
			  }
		      break;
	      case 16: //DEBUT_TANT_QUE
		      if (parametres.count()==2)
			  {
			  canRemoveline=false;
			  canAddline = true;
			  canModifyline=false;
			  canCopyline=false;
			  canPasteline=false;
			  canCutline=false;
              canAddFct=false;
              canAddVariableFct=false;
			  }
		      break;
	      case 17: //FIN_TANT_QUE
		      if (parametres.count()==2)
			  {
			  canRemoveline=false;
			  canAddline = true;
			  canModifyline=false;
			  canCopyline=false;
			  canPasteline=false;
			  canCutline=false;
              canAddFct=false;
              canAddVariableFct=false;
			  }
		      break;
	      case 18: //PAUSE
		      if (parametres.count()==2)
			  {
			  canRemoveline=true;
			  canAddline = true;
			  canModifyline=false;
			  canCopyline=true;
			  canPasteline=false;
			  canCutline=true;
              canAddFct=false;
              canAddVariableFct=false;
			  }
		      break;
	      case 19: //COMMENTAIRE
		      if (parametres.count()==2)
			  {
			  canRemoveline=true;
			  canAddline = true;
			  canModifyline=true;
			  canCopyline=true;
			  canPasteline=false;
			  canCutline=true;
              canAddFct=false;
              canAddVariableFct=false;
			  }
		      break;
	      case 50: //POINT
		      if (parametres.count()==4)
			  {
			  canRemoveline=true;
			  canAddline = true;
			  canModifyline=true;
			  canCopyline=true;
			  canPasteline=false;
			  canCutline=true;
              canAddFct=false;
              canAddVariableFct=false;
			  }
		      break;
	      case 51: //SEGMENT
		      if (parametres.count()==6)
			  {
			  canRemoveline=true;
			  canAddline = true;
			  canModifyline=true;
			  canCopyline=true;
			  canPasteline=false;
			  canCutline=true;
              canAddFct=false;
              canAddVariableFct=false;
			  }
		      break;
	      case 52: //EFFACER
		      if (parametres.count()==2)
			  {
			  canRemoveline=true;
			  canAddline = true;
			  canModifyline=false;
			  canCopyline=true;
			  canPasteline=false;
			  canCutline=true;
              canAddFct=false;
              canAddVariableFct=false;
			  }
		      break;
	      case 100: //DECLARATIONS VARIABLES
		      if (parametres.count()==2)
			  {
			  canRemoveline=false;
			  canAddline = false;
			  canModifyline=false;
			  canCopyline=false;
			  canPasteline=false;
			  canCutline=false;
              canAddFct=false;
              canAddVariableFct=false;
			  }
		      break;
	      case 101: //DEBUT_ALGO
		      if (parametres.count()==2)
			  {
			  canRemoveline=false;
			  canAddline = true;
			  canModifyline=false;
			  canCopyline=false;
			  canPasteline=false;
			  canCutline=false;
              canAddFct=false;
              canAddVariableFct=false;
			  }
		      break;
	      case 102: //FIN_ALGO
		      if (parametres.count()==2)
			  {
			  canRemoveline=false;
			  canAddline = false;
			  canModifyline=false;
			  canCopyline=false;
			  canPasteline=false;
			  canCutline=false;
              canAddFct=false;
              canAddVariableFct=false;
			  }
		      break;
	      case 200: //FONCTIONS_UTILISEES
		      if (parametres.count()==2)
			  {
			  canRemoveline=false;
			  canAddline = true;
			  canModifyline=false;
			  canCopyline=false;
			  canPasteline=false;
			  canCutline=false;
              canAddFct=false;
              canAddVariableFct=false;
			  }
		      break;
	      case 201: //FONCTION
		      if (parametres.count()==3)
			  {
			  canRemoveline=true;
			  canAddline = false;
			  canModifyline=true;
			  canCopyline=false;
			  canPasteline=false;
			  canCutline=false;
              canAddFct=false;
              canAddVariableFct=false;
			  }
		      break;
	      case 202: //VARIABLES_FONCTION
		      if (parametres.count()==2)
			  {
			  canRemoveline=false;
			  canAddline = false;
			  canModifyline=false;
			  canCopyline=false;
			  canPasteline=false;
			  canCutline=false;
              canAddFct=false;
              canAddVariableFct=true;
			  }
		      break;
	      case 203: //DEBUT_FONCTION
		      if (parametres.count()==2)
			  {
			  canRemoveline=false;
			  canAddline = true;
			  canModifyline=false;
			  canCopyline=false;
			  canPasteline=false;
			  canCutline=false;
              canAddFct=false;
              canAddVariableFct=false;
			  }
		      break;
	      case 204: //FIN_FONCTION
		      if (parametres.count()==2)
			  {
			  canRemoveline=false;
			  canAddline = false;
			  canModifyline=false;
			  canCopyline=false;
			  canPasteline=false;
			  canCutline=false;
              canAddFct=false;
              canAddVariableFct=false;
			  }
		      break;
	      case 205: //RENVOYER
		      if (parametres.count()==2)
			  {
			  canRemoveline=true;
			  canAddline = true;
			  canModifyline=true;
			  canCopyline=true;
			  canPasteline=false;
			  canCutline=true;
              canAddFct=false;
              canAddVariableFct=false;
			  }
		      break;
	      case 206: //APPELER_FONCTION
		      if (parametres.count()==2)
			  {
			  canRemoveline=true;
			  canAddline = true;
			  canModifyline=true;
			  canCopyline=true;
			  canPasteline=false;
			  canCutline=true;
              canAddFct=false;
              canAddVariableFct=false;
			  }
		      break;                        
	      case 103: //autres
	      default:
		      if (parametres.count()==2)
			  {
			  canRemoveline=true;
			  canAddline = true;
			  canModifyline=false;
			  canCopyline=false;
			  canPasteline=false;
			  canCutline=false;
              canAddFct=false;
              canAddVariableFct=false;
              if (current->parent())
                {
                if (current->parent()->text(0)=="FONCTIONS_UTILISEES") canAddFct=true;
                }
			  }
		      break;
	      }
	  if (current->text(0).isEmpty()) 
	      {
              ActiverBoutons();
              if (current->parent())
                {
                if (current->parent()->text(0)=="FONCTIONS_UTILISEES") DesactiverBoutons();
                }              
	      
	      if (clipboardItem) 
                {
                if (!clipboardItem->text(0).isEmpty()) canPasteline=true;
                }
	      }
	  else DesactiverBoutons();
	  }
  else 
	  {
	  canAddline = false;
	  canModifyline=false;
	  DesactiverBoutons();
	  }
  ui.pushButtonAjouterLigne->setEnabled(canAddline);
  ui.pushButtonSupprimerLigne->setEnabled(canRemoveline);
  ui.pushButtonModifierLigne->setEnabled(canModifyline);

  ui.pushButtonNelleFct->setEnabled(canAddFct);
  ui.pushButtonVariableFct->setEnabled(canAddVariableFct);
  
  actionCopier->setEnabled(canCopyline);
  actionColler->setEnabled(canPasteline);
  actionCouper->setEnabled(canCutline);
  ui.treeWidget->setFocus();
  ActualiserVariables();
  ui.treeWidget->header()->resizeSections(QHeaderView::ResizeToContents);
  }
else
  {
  actionCopier->setEnabled(true);
  actionColler->setEnabled(true);
  actionCouper->setEnabled(true);
  ActiverBoutons();
  }
}

void MainWindow::ActualiserVariables()
{
ListeNomsVariables.clear();
ListeTypesVariables.clear();
QTreeWidgetItem *item, *varitem;
QString code;
QRegularExpression rxvar("(.*) EST_DU_TYPE (.*)");
for (int i = 0; i < variablesItem->childCount(); i++) 
    {
    code=variablesItem->child(i)->text(0);
    QRegularExpressionMatch rxvarMatch=rxvar.match(code);
    if (rxvarMatch.hasMatch())
        {
        ListeNomsVariables.append(rxvarMatch.captured(1));
        ListeTypesVariables.append(rxvarMatch.captured(2));
        }
    }
for (int i = 0; i < fctsItem->childCount(); i++) 
    {
    item=fctsItem->child(i);
    for (int j = 0; j < item->childCount(); j++)  
        {
        if (item->child(j)->text(0)=="VARIABLES_FONCTION")
            {
            varitem=item->child(j);
            for (int k = 0; k < varitem->childCount(); k++) 
                {
                    code=varitem->child(k)->text(0);
                    QRegularExpressionMatch rxvarMatch=rxvar.match(code);
                    if (rxvarMatch.hasMatch())
                    {
                    ListeNomsVariables.append(rxvarMatch.captured(1));
                    ListeTypesVariables.append(rxvarMatch.captured(2));
                    }
                }
            }
        }
    }
}
//*************************
void MainWindow::AjouterVariable()
{
if (modeNormal)
  {
  QTreeWidgetItem *curItem = ui.treeWidget->currentItem();
  VariableDialog *variableDialog = new VariableDialog(this);
  if (variableDialog->exec())
      {
      QString nomvariable=variableDialog->ui.lineEditVariable->text();
      nomvariable=FiltreNomVariable(nomvariable);
      QString type=variableDialog->ui.comboBoxVariable->currentText();
      QString code="";
      if (NomInterdit(nomvariable))
	  {
	  QMessageBox::warning( this,QString::fromUtf8("Erreur"),QString::fromUtf8("Nom de variable interdit"));
	  }
      else
	  {
	  QTreeWidgetItem *newItem =new QTreeWidgetItem(variablesItem);
	  newItem->setText(0,nomvariable+" EST_DU_TYPE "+type);
	  code="1#"+type+"#"+nomvariable;
	  newItem->setData(0,Qt::UserRole,QString(code));
	  if ((curItem) && (curItem!=variablesItem)) ui.treeWidget->setCurrentItem(curItem);
	  else ui.treeWidget->setCurrentItem(debutItem);
	  ActualiserStatut();
	  }
      }
  ActualiserArbre();
  }
else
  {
  ui.EditorView->editor->insertTag(QString::fromUtf8("• EST_DU_TYPE •"),0,0);
  ui.EditorView->editor->search(QString(QChar(0x2022)) ,true,true,true,true);
  }
}
void MainWindow::AjouterLire()
{
if (modeNormal)
  {
  QTreeWidgetItem *curItem = ui.treeWidget->currentItem();
  if (!curItem || !curItem->parent()) return;
  if (ListeNomsVariables.isEmpty())
      {
      QMessageBox::warning( this,QString::fromUtf8("Erreur"),QString::fromUtf8("Aucune variable n'a encore été définie."));
      return;
      }
  LireDialog *lireDialog = new LireDialog(this,ListeNomsVariables.join("#"),ListeTypesVariables.join("#"));
  if (lireDialog->exec())
      {
      QString nomvariable=lireDialog->ui.comboBoxLire->currentText();
      QString rang=lireDialog->ui.lineEditLire->text();
      QString code,type;
      code="";
      if (!nomvariable.isEmpty())
	  {
	  int i=ListeNomsVariables.indexOf(nomvariable);
	  if (i>-1)
	      {
	      type=ListeTypesVariables.at(i);
	      if ((type=="LISTE") && (!rang.isEmpty())) 
		  {
		  curItem->setText(0,QString::fromUtf8("LIRE ")+nomvariable+"["+rang+"]");
		  code="2#"+nomvariable+"#"+rang;
		  }
	      else 
		  {
		  curItem->setText(0,QString::fromUtf8("LIRE ")+nomvariable);
		  code="2#"+nomvariable+"#pasliste";
		  }
	      curItem->setData(0,Qt::UserRole,QString(code));
	      }
	  ui.treeWidget->setCurrentItem(curItem);
	  ActualiserStatut();
	  }
      else
	  {
	  QMessageBox::warning( this,QString::fromUtf8("Erreur"),QString::fromUtf8("Pas de variable définie"));
	  }
      }
  ActualiserArbre();
  }
else
  {
  ui.EditorView->editor->insertTag(QString::fromUtf8("LIRE •"),0,0);
  ui.EditorView->editor->search(QString(QChar(0x2022)) ,true,true,true,true);
  }
}

void MainWindow::AjouterAfficher()
{
if (modeNormal)
  {
  QTreeWidgetItem *curItem = ui.treeWidget->currentItem();
  if (!curItem || !curItem->parent()) return;
  if (ListeNomsVariables.isEmpty())
      {
      QMessageBox::warning( this,QString::fromUtf8("Erreur"),QString::fromUtf8("Aucune variable n'a encore été définie."));
      return;
      }
  AfficherDialog *afficherDialog = new AfficherDialog(this,ListeNomsVariables.join("#"),ListeTypesVariables.join("#"));
  if (afficherDialog->exec())
      {
      QString nomvariable=afficherDialog->ui.comboBoxAfficher->currentText();
      QString rang=afficherDialog->ui.lineEditAfficher->text();
      QString code="";
      if (!nomvariable.isEmpty())
	  {
	  int i=ListeNomsVariables.indexOf(nomvariable);
	  if (i>-1)
	      {
	      QString type=ListeTypesVariables.at(i);
	      if (afficherDialog->ui.checkBoxAfficher->isChecked()) code="3#"+nomvariable+"#1";
	      else code="3#"+nomvariable+"#0";
	      if ((type=="LISTE") && (!rang.isEmpty())) 
		  {
		  curItem->setText(0,QString::fromUtf8("AFFICHER ")+nomvariable+"["+rang+"]");
		  code+="#"+rang;
		  }
	      else 
		  {
		  curItem->setText(0,QString::fromUtf8("AFFICHER ")+nomvariable);
		  code+="#pasliste";
		  }
	      curItem->setData(0,Qt::UserRole,QString(code));
	      }
	  ui.treeWidget->setCurrentItem(curItem);
	  ActualiserStatut();
	  }
      else
	  {
	  QMessageBox::warning( this,QString::fromUtf8("Erreur"),QString::fromUtf8("Pas de variable définie"));
	  }
      }
  ActualiserArbre();
  }
else
  {
  ui.EditorView->editor->insertTag(QString::fromUtf8("AFFICHER •"),0,0);
  ui.EditorView->editor->search(QString(QChar(0x2022)) ,true,true,true,true);
  }
}

void MainWindow::AjouterMessage()
{
if (modeNormal)
  {
  QTreeWidgetItem *curItem = ui.treeWidget->currentItem();
  if (!curItem || !curItem->parent()) return;
  MessageDialog *messageDialog = new MessageDialog(this);
  if (messageDialog->exec())
      {
      QString message=messageDialog->ui.lineEditMessage->text();
      message.remove("#");
      message.remove("\"");
  //    message.remove("'");
      QString code="";
      if (!message.isEmpty())
	  {
		  curItem->setText(0,QString::fromUtf8("AFFICHER \"")+message+"\"");
		  if (messageDialog->ui.checkBoxMessage->isChecked()) code="4#"+message+"#1";
		  else code="4#"+message+"#0";
		  curItem->setData(0,Qt::UserRole,QString(code));
		  ui.treeWidget->setCurrentItem(curItem);
		  ActualiserStatut();
	  }
      else
	  {
	  QMessageBox::warning( this,QString::fromUtf8("Erreur"),QString::fromUtf8("Message vide"));
	  }
      }
  ActualiserArbre();
  }
else
  {
  ui.EditorView->editor->insertTag(QString::fromUtf8("AFFICHER \"•\""),0,0);
  ui.EditorView->editor->search(QString(QChar(0x2022)) ,true,true,true,true);
  }
}

void MainWindow::AjouterCalcul()
{
if (modeNormal)
  {
  QTreeWidgetItem *curItem = ui.treeWidget->currentItem();
  if (!curItem || !curItem->parent()) return;
  AffichercalculDialog *calculDialog = new AffichercalculDialog(this,ListeNomsVariables.join("#"),ListeTypesVariables.join("#"));
  if (calculDialog->exec())
      {
      QString calcul=calculDialog->ui.lineEditAffectation->text();
      calcul.remove("#");
      //calcul.remove("\"");
  //    message.remove("'");
      QString code="";
      if (!calcul.isEmpty())
	  {
		  curItem->setText(0,QString::fromUtf8("AFFICHERCALCUL ")+calcul);
		  if (calculDialog->ui.checkBoxAfficher->isChecked()) code="20#"+calcul+"#1";
		  else code="20#"+calcul+"#0";
		  curItem->setData(0,Qt::UserRole,QString(code));
		  ui.treeWidget->setCurrentItem(curItem);
		  ActualiserStatut();
	  }
      else
	  {
	  QMessageBox::warning( this,QString::fromUtf8("Erreur"),QString::fromUtf8("Calcul vide"));
	  }
      }
  ActualiserArbre();
  }
else
  {
  ui.EditorView->editor->insertTag(QString::fromUtf8("AFFICHERCALCUL •"),0,0);
  ui.EditorView->editor->search(QString(QChar(0x2022)) ,true,true,true,true);
  }
}

void MainWindow::AjouterPause()
{
if (modeNormal)
  {
  QTreeWidgetItem *curItem = ui.treeWidget->currentItem();
  if (!curItem || !curItem->parent()) return;
  QString code="18#pause";
  curItem->setText(0,QString::fromUtf8("PAUSE"));
  curItem->setData(0,Qt::UserRole,QString(code));
  ui.treeWidget->setCurrentItem(curItem);
  ActualiserStatut();
  ActualiserArbre();
  }
else
  {
  ui.EditorView->editor->insertTag(QString::fromUtf8("PAUSE"),5,0);
  }
}

void MainWindow::AjouterAffectation()
{
if (modeNormal)
  {
  QTreeWidgetItem *curItem = ui.treeWidget->currentItem();
  if (!curItem || !curItem->parent()) return;
  if (ListeNomsVariables.isEmpty())
      {
      QMessageBox::warning( this,QString::fromUtf8("Erreur"),QString::fromUtf8("Aucune variable n'a encore été définie."));
      return;
      }
  AffectationDialog *affectationDialog = new AffectationDialog(this,ListeNomsVariables.join("#"),ListeTypesVariables.join("#"));
  if (affectationDialog->exec())
      {
      QString nomvariable=affectationDialog->ui.comboBoxAffectation->currentText();
      QString contenu=affectationDialog->ui.lineEditAffectation->text();
      QString rang=affectationDialog->ui.lineEditAffectation2->text();
      contenu.remove("#");
      QString code="";
      if (!nomvariable.isEmpty())
	  {
	  if (!contenu.isEmpty())
	      {
	      int i=ListeNomsVariables.indexOf(nomvariable);
	      if (i>-1)
		  {
		  QString type=ListeTypesVariables.at(i);
		  code="5#"+nomvariable+"#"+contenu;
		  if ((type=="LISTE") && (!rang.isEmpty())) 
		      {
		      curItem->setText(0,nomvariable+"["+rang+"]"+QString::fromUtf8(" PREND_LA_VALEUR ")+contenu);
		      code+="#"+rang;
		      }
		  else 
		      {
		      curItem->setText(0,nomvariable+QString::fromUtf8(" PREND_LA_VALEUR ")+contenu);
		      code+="#pasliste";
		      }
		  curItem->setData(0,Qt::UserRole,QString(code));
		  }
	      ui.treeWidget->setCurrentItem(curItem);
	      ActualiserStatut();
	      }
	  else
	      {
	      QMessageBox::warning( this,QString::fromUtf8("Erreur"),QString::fromUtf8("Affectation vide"));
	      }
	  }
      else
	  {
	  QMessageBox::warning( this,QString::fromUtf8("Erreur"),QString::fromUtf8("Pas de variable définie"));
	  }
      }
  ActualiserArbre();
  }
else
  {
  ui.EditorView->editor->insertTag(QString::fromUtf8("• PREND_LA_VALEUR •"),0,0);
  ui.EditorView->editor->search(QString(QChar(0x2022)) ,true,true,true,true);
  }
}

void MainWindow::AjouterCondition()
{
if (modeNormal)
  {
  QTreeWidgetItem *curItem = ui.treeWidget->currentItem();
  if (!curItem || !curItem->parent()) return;
  ConditionDialog *conditionDialog = new ConditionDialog(this,ListeNomsVariables.join("#"),ListeTypesVariables.join("#"));
  if (conditionDialog->exec())
      {
      QString condition=conditionDialog->ui.lineEditCondition->text();
      condition.remove("#");
      QString code="";
      QTreeWidgetItem *newItem, *sinonItem;
      if (!condition.isEmpty())
	  {
	  curItem->setText(0,QString::fromUtf8("SI (")+condition+") "+QString::fromUtf8("ALORS"));
      curItem->setExpanded(true);
	  code="6#"+condition;
	  curItem->setData(0,Qt::UserRole,QString(code));
	  newItem = new QTreeWidgetItem(curItem);
	  newItem->setText(0,QString::fromUtf8("DEBUT_SI"));
	  newItem->setData(0,Qt::UserRole,QString("7#debutsi"));
	  newItem = new QTreeWidgetItem(curItem);
	  ui.treeWidget->setCurrentItem(newItem);
	  newItem = new QTreeWidgetItem(curItem);
	  newItem->setText(0,QString::fromUtf8("FIN_SI"));
	  newItem->setData(0,Qt::UserRole,QString("8#finsi"));
	  if (conditionDialog->ui.checkBoxCondition->isChecked())
	      {
	      sinonItem = new QTreeWidgetItem(curItem,true);
	      sinonItem->setText(0,QString::fromUtf8("SINON"));
          sinonItem->setExpanded(true);
	      code="9#sinon";
	      sinonItem->setData(0,Qt::UserRole,QString(code));
	      newItem = new QTreeWidgetItem(sinonItem);
	      newItem->setText(0,QString::fromUtf8("DEBUT_SINON"));
	      newItem->setData(0,Qt::UserRole,QString("10#debutsinon"));
	      newItem = new QTreeWidgetItem(sinonItem);
	      newItem = new QTreeWidgetItem(sinonItem);
	      newItem->setText(0,QString::fromUtf8("FIN_SINON"));
	      newItem->setData(0,Qt::UserRole,QString("11#finsinon"));
	      }
	  ActualiserStatut();
	  }
      else
	  {
	  QMessageBox::warning( this,QString::fromUtf8("Erreur"),QString::fromUtf8("Pas de condition définie"));
	  }
      }
  ActualiserArbre();
  }
else
  {
  QTextCursor cur=ui.EditorView->editor->textCursor();
  int pos=cur.position();
  ui.EditorView->editor->insertPlainText(QString::fromUtf8("SI (•) ALORS"));
  ui.EditorView->editor->insertNewLine();
  ui.EditorView->editor->insertPlainText(QString::fromUtf8("\tDEBUT_SI"));
  ui.EditorView->editor->insertNewLine();
  ui.EditorView->editor->insertNewLine();
  ui.EditorView->editor->insertPlainText(QString::fromUtf8("FIN_SI"));    
  cur.setPosition(pos,QTextCursor::MoveAnchor);
  ui.EditorView->editor->setTextCursor(cur);
  ui.EditorView->editor->setFocus();
  ui.EditorView->editor->search(QString(QChar(0x2022)) ,true,true,true,true);
  }
}

void MainWindow::AjouterBoucle()
{
if (modeNormal)
  {
  QTreeWidgetItem *curItem = ui.treeWidget->currentItem();
  if (!curItem || !curItem->parent()) return;
  QStringList ListePourVariables;
  for (int i = 0; i < ListeNomsVariables.count(); i++) 
      {
      if (ListeTypesVariables.at(i)=="NOMBRE") ListePourVariables.append(ListeNomsVariables.at(i));
      }
  PourDialog *pourDialog = new PourDialog(this,ListePourVariables.join("#"));
  if (pourDialog->exec())
      {
      QString nomvariable=pourDialog->ui.comboBoxBoucle->currentText();
      QString debut=pourDialog->ui.lineEditBoucle1->text();
      debut.remove("#");
      QString fin=pourDialog->ui.lineEditBoucle2->text();
      fin.remove("#");
      QString code="";
      QTreeWidgetItem *newItem;
      if (!nomvariable.isEmpty())
	  {
	  if (!debut.isEmpty() && !fin.isEmpty())
	      {
	      int i=ListeNomsVariables.indexOf(nomvariable);
	      if (i>-1)
		  {
		  curItem->setText(0,QString::fromUtf8("POUR ")+nomvariable+" ALLANT_DE "+debut+ " A "+fin);
		  code="12#"+nomvariable+"#"+debut+"#"+fin;
		  curItem->setData(0,Qt::UserRole,QString(code));
		  newItem = new QTreeWidgetItem(curItem);
		  newItem->setText(0,QString::fromUtf8("DEBUT_POUR"));
		  newItem->setData(0,Qt::UserRole,QString("13#debutpour"));
		  newItem = new QTreeWidgetItem(curItem);
		  ui.treeWidget->setCurrentItem(newItem);
		  newItem = new QTreeWidgetItem(curItem);
		  newItem->setText(0,QString::fromUtf8("FIN_POUR"));
		  newItem->setData(0,Qt::UserRole,QString("14#finpour"));
		  }
	      ActualiserStatut();
	      }
	  else
	      {
	      QMessageBox::warning( this,QString::fromUtf8("Erreur"),QString::fromUtf8("Les valeurs minimales et maximales du compteur ne sont pas définies"));
	      }
	  }
      else
	  {
	  QMessageBox::warning( this,QString::fromUtf8("Erreur"),QString::fromUtf8("Pas de variable définie"));
	  }
      }
  ActualiserArbre();
  }
else
  {
  QTextCursor cur=ui.EditorView->editor->textCursor();
  int pos=cur.position();
  ui.EditorView->editor->insertPlainText(QString::fromUtf8("POUR • ALLANT_DE • A •"));
  ui.EditorView->editor->insertNewLine();
  ui.EditorView->editor->insertPlainText(QString::fromUtf8("\tDEBUT_POUR"));
  ui.EditorView->editor->insertNewLine();
  ui.EditorView->editor->insertNewLine();
  ui.EditorView->editor->insertPlainText(QString::fromUtf8("FIN_POUR"));    
  cur.setPosition(pos,QTextCursor::MoveAnchor);
  ui.EditorView->editor->setTextCursor(cur);
  ui.EditorView->editor->setFocus();
  ui.EditorView->editor->search(QString(QChar(0x2022)) ,true,true,true,true);
  }
}

void MainWindow::AjouterTantque()
{
if (modeNormal)
  {
  QTreeWidgetItem *curItem = ui.treeWidget->currentItem();
  if (!curItem || !curItem->parent()) return;
  TantqueDialog *tantqueDialog = new TantqueDialog(this,ListeNomsVariables.join("#"),ListeTypesVariables.join("#"));
  if (tantqueDialog->exec())
      {
      QString condition=tantqueDialog->ui.lineEditTantque->text();
      condition.remove("#");
      QString code="";
      QTreeWidgetItem *newItem;
      if (!condition.isEmpty())
	  {
	  curItem->setText(0,QString::fromUtf8("TANT_QUE (")+condition+") "+QString::fromUtf8("FAIRE"));
      curItem->setExpanded(true);
	  code="15#"+condition;
	  curItem->setData(0,Qt::UserRole,QString(code));
	  newItem = new QTreeWidgetItem(curItem);
	  newItem->setText(0,QString::fromUtf8("DEBUT_TANT_QUE"));
	  newItem->setData(0,Qt::UserRole,QString("16#debuttantque"));
	  newItem = new QTreeWidgetItem(curItem);
	  ui.treeWidget->setCurrentItem(newItem);
	  newItem = new QTreeWidgetItem(curItem);
	  newItem->setText(0,QString::fromUtf8("FIN_TANT_QUE"));
	  newItem->setData(0,Qt::UserRole,QString("17#fintantque"));
	  ActualiserStatut();
	  }
      else
	  {
	  QMessageBox::warning( this,QString::fromUtf8("Erreur"),QString::fromUtf8("Pas de condition définie"));
	  }
      }
  ActualiserArbre();
  }
else
  {
  QTextCursor cur=ui.EditorView->editor->textCursor();
  int pos=cur.position();
  ui.EditorView->editor->insertPlainText(QString::fromUtf8("TANT_QUE (•) FAIRE"));
  ui.EditorView->editor->insertNewLine();
  ui.EditorView->editor->insertPlainText(QString::fromUtf8("\tDEBUT_TANT_QUE"));
  ui.EditorView->editor->insertNewLine();
  ui.EditorView->editor->insertNewLine();
  ui.EditorView->editor->insertPlainText(QString::fromUtf8("FIN_TANT_QUE"));    
  cur.setPosition(pos,QTextCursor::MoveAnchor);
  ui.EditorView->editor->setTextCursor(cur);
  ui.EditorView->editor->setFocus();
  ui.EditorView->editor->search(QString(QChar(0x2022)) ,true,true,true,true);
  }
}

void MainWindow::AjouterPoint()
{
if (modeNormal)
  {
  QTreeWidgetItem *curItem = ui.treeWidget->currentItem();
  if (!curItem || !curItem->parent()) return;
  PointDialog *pointDialog = new PointDialog(this,ListeNomsVariables.join("#"),ListeTypesVariables.join("#"));
  if (pointDialog->exec())
      {
      QString x=pointDialog->ui.lineEditX->text();
      QString y=pointDialog->ui.lineEditY->text();
      QString couleur=pointDialog->ui.comboBoxCouleur->currentText();
      x.remove("#");
      y.remove("#");
      if ((!x.isEmpty()) && (!y.isEmpty()))
	  {
	  curItem->setText(0,QString::fromUtf8("TRACER_POINT (")+x+","+y+")");
	  QString code="50#"+x+"#"+y+"#"+couleur;
	  curItem->setData(0,Qt::UserRole,QString(code));
	  ui.treeWidget->setCurrentItem(curItem);
	  ActualiserStatut();
	  }
      else
	  {
	  QMessageBox::warning( this,QString::fromUtf8("Erreur"),QString::fromUtf8("Au moins une des coordonnées n'est pas définie"));
	  }
      }
  ActualiserArbre();
  }
else
  {
  ui.EditorView->editor->insertTag(QString::fromUtf8("TRACER_POINT (•,•)"),0,0);
  ui.EditorView->editor->search(QString(QChar(0x2022)) ,true,true,true,true);
  }
}

void MainWindow::AjouterSegment()
{
if (modeNormal)
  {
  QTreeWidgetItem *curItem = ui.treeWidget->currentItem();
  if (!curItem || !curItem->parent()) return;
  SegmentDialog *segmentDialog = new SegmentDialog(this,ListeNomsVariables.join("#"),ListeTypesVariables.join("#"));
  if (segmentDialog->exec())
      {
      QString xdep=segmentDialog->ui.lineEditXdep->text();
      QString ydep=segmentDialog->ui.lineEditYdep->text();
      QString xfin=segmentDialog->ui.lineEditXfin->text();
      QString yfin=segmentDialog->ui.lineEditYfin->text();
      QString couleur=segmentDialog->ui.comboBoxCouleur->currentText();
      xdep.remove("#");
      ydep.remove("#");
      xfin.remove("#");
      yfin.remove("#");
      if ((!xdep.isEmpty()) && (!ydep.isEmpty()) && (!xfin.isEmpty()) && (!yfin.isEmpty()))
	  {
	  curItem->setText(0,QString::fromUtf8("TRACER_SEGMENT (")+xdep+","+ydep+")->("+xfin+","+yfin+")");
	  QString code="51#"+xdep+"#"+ydep+"#"+xfin+"#"+yfin+"#"+couleur;
	  curItem->setData(0,Qt::UserRole,QString(code));
	  ui.treeWidget->setCurrentItem(curItem);
	  ActualiserStatut();
	  }
      else
	  {
	  QMessageBox::warning( this,QString::fromUtf8("Erreur"),QString::fromUtf8("Au moins une des coordonnées n'est pas définie"));
	  }
      }
  ActualiserArbre();
  }
else
  {
  ui.EditorView->editor->insertTag(QString::fromUtf8("TRACER_SEGMENT (•,•)->(•,•)"),0,0);
  ui.EditorView->editor->search(QString(QChar(0x2022)) ,true,true,true,true);
  }
}

void MainWindow::AjouterEffacer()
{
if (modeNormal)
  {
  QTreeWidgetItem *curItem = ui.treeWidget->currentItem();
  if (!curItem || !curItem->parent()) return;
  QString code="52#effacer";
  curItem->setText(0,QString::fromUtf8("EFFACER_GRAPHIQUE"));
  curItem->setData(0,Qt::UserRole,QString(code));
  ui.treeWidget->setCurrentItem(curItem);
  ActualiserStatut();
  ActualiserArbre();
  }
else
  {
  ui.EditorView->editor->insertTag(QString::fromUtf8("EFFACER_GRAPHIQUE"),17,0);
  }
}

void MainWindow::AjouterCommentaire()
{
if (modeNormal)
  {
  QFont commentFont = qApp->font();
  commentFont.setItalic(true);
  QTreeWidgetItem *curItem = ui.treeWidget->currentItem();
  if (!curItem || !curItem->parent()) return;
  CommentaireDialog *commentaireDialog = new CommentaireDialog(this);
  if (commentaireDialog->exec())
      {
      QString commentaire=commentaireDialog->ui.lineEditCommentaire->text();
      commentaire.remove("#");
      //commentaire.remove("\"");
      QString code="";
      if (!commentaire.isEmpty())
	  {
		  curItem->setText(0,"//"+commentaire);
		  code="19#"+commentaire;
		  curItem->setData(0,Qt::UserRole,QString(code));
		  curItem->setFont(0,commentFont);
		  ui.treeWidget->setCurrentItem(curItem);
		  ActualiserStatut();
	  }
      else
	  {
	  QMessageBox::warning( this,QString::fromUtf8("Erreur"),QString::fromUtf8("Commentaire vide"));
	  }
      }
  ActualiserArbre();
  }
else
  {
  ui.EditorView->editor->insertTag(QString::fromUtf8("//"),2,0);
  }
}

void MainWindow::AjouterFonction()
{
if (modeNormal)
  {
  QTreeWidgetItem *curItem = ui.treeWidget->currentItem();
  if (!curItem || !curItem->parent()) return;
  FonctionDialog *fonctionDialog = new FonctionDialog(this);
  if (fonctionDialog->exec())
      {
      QString nom=fonctionDialog->ui.lineEditNom->text();
      nom.remove("#");
      QString param=fonctionDialog->ui.lineEditParam->text();
      param.remove("#");
      //commentaire.remove("\"");
      QString code="";
      QTreeWidgetItem *newItem;
      if (!nom.isEmpty())
	  {
        if (param.isEmpty()) param=QString(" ");  
        curItem->setText(0,"FONCTION "+nom+"("+param+")");
        curItem->setExpanded(true);
        code="201#"+nom+"#"+param;
        curItem->setData(0,Qt::UserRole,QString(code));
        newItem = new QTreeWidgetItem(curItem);
        newItem->setText(0,QString::fromUtf8("VARIABLES_FONCTION"));
        newItem->setData(0,Qt::UserRole,QString("202#declarationsvariablesfonction"));
        newItem->setExpanded(true);
        newItem = new QTreeWidgetItem(curItem);
        newItem->setText(0,QString::fromUtf8("DEBUT_FONCTION"));
        newItem->setData(0,Qt::UserRole,QString("203#debutfonction"));
        newItem = new QTreeWidgetItem(curItem);
        ui.treeWidget->setCurrentItem(newItem);
        newItem = new QTreeWidgetItem(curItem);
        newItem->setText(0,QString::fromUtf8("FIN_FONCTION"));
        newItem->setData(0,Qt::UserRole,QString("204#finfonction"));
        ActualiserStatut();
	  }
      else
	  {
	  QMessageBox::warning( this,QString::fromUtf8("Erreur"),QString::fromUtf8("Fonction non valide"));
	  }
      }
  ActualiserArbre();
  }
else
  {
  QTextCursor cur=ui.EditorView->editor->textCursor();
  int pos=cur.position();
  ui.EditorView->editor->insertPlainText(QString::fromUtf8("FONCTION •(•)"));
  ui.EditorView->editor->insertNewLine();
  ui.EditorView->editor->insertPlainText(QString::fromUtf8("VARIABLES_FONCTION"));
  ui.EditorView->editor->insertNewLine();
  ui.EditorView->editor->insertPlainText(QString::fromUtf8("\tDEBUT_FONCTION"));
  ui.EditorView->editor->insertNewLine();
  ui.EditorView->editor->insertNewLine();
  ui.EditorView->editor->insertPlainText(QString::fromUtf8("FIN_FONCTION"));    
  cur.setPosition(pos,QTextCursor::MoveAnchor);
  ui.EditorView->editor->setTextCursor(cur);
  ui.EditorView->editor->setFocus();
  ui.EditorView->editor->search(QString(QChar(0x2022)) ,true,true,true,true);
  }
}

void MainWindow::AjouterVariableLocale()
{
if (modeNormal)
  {
  QTreeWidgetItem *curItem = ui.treeWidget->currentItem();
  VariableDialog *variableDialog = new VariableDialog(this);
  if (variableDialog->exec())
      {
      QString nomvariable=variableDialog->ui.lineEditVariable->text();
      nomvariable=FiltreNomVariable(nomvariable);
      QString type=variableDialog->ui.comboBoxVariable->currentText();
      QString code="";
      if (NomInterdit(nomvariable))
	  {
	  QMessageBox::warning( this,QString::fromUtf8("Erreur"),QString::fromUtf8("Nom de variable interdit"));
	  }
      else
	  {
	  QTreeWidgetItem *newItem =new QTreeWidgetItem(curItem);
	  newItem->setText(0,nomvariable+" EST_DU_TYPE "+type);
	  code="1#"+type+"#"+nomvariable;
	  newItem->setData(0,Qt::UserRole,QString(code));
	  if ((curItem)) ui.treeWidget->setCurrentItem(curItem);
	  ActualiserStatut();
	  }
      }
  ActualiserArbre();
  }
else
  {
  ui.EditorView->editor->insertTag(QString::fromUtf8("• EST_DU_TYPE •"),0,0);
  ui.EditorView->editor->search(QString(QChar(0x2022)) ,true,true,true,true);
  }
}

void MainWindow::AjouterRenvoyer()
{
if (modeNormal)
  {
  QTreeWidgetItem *curItem = ui.treeWidget->currentItem();
  if (!curItem || !curItem->parent()) return;
  RenvoyerDialog *renvoyerDialog = new RenvoyerDialog(this);
  if (renvoyerDialog->exec())
      {
      QString retour=renvoyerDialog->ui.lineEdit->text();
      retour.remove("#");
      QString code="";
      if (!retour.isEmpty())
	  {
		  curItem->setText(0,"RENVOYER "+retour);
		  code="205#"+retour;
		  curItem->setData(0,Qt::UserRole,QString(code));
		  ui.treeWidget->setCurrentItem(curItem);
		  ActualiserStatut();
	  }
      else
	  {
	  QMessageBox::warning( this,QString::fromUtf8("Erreur"),QString::fromUtf8("Retour vide"));
	  }
      }
  ActualiserArbre();
  }
else
  {
  ui.EditorView->editor->insertTag(QString::fromUtf8("RENVOYER •"),2,0);
  }
}

void MainWindow::AjouterAppelerFct()
{
if (modeNormal)
  {
  QTreeWidgetItem *curItem = ui.treeWidget->currentItem();
  if (!curItem || !curItem->parent()) return;
  AppelDialog *appelDialog = new AppelDialog(this);
  if (appelDialog->exec())
      {
      QString fct=appelDialog->ui.lineEdit->text();
      fct.remove("#");
      QString code="";
      if (!fct.isEmpty())
	  {
		  curItem->setText(0,"APPELER_FONCTION "+fct);
		  code="206#"+fct;
		  curItem->setData(0,Qt::UserRole,QString(code));
		  ui.treeWidget->setCurrentItem(curItem);
		  ActualiserStatut();
	  }
      else
	  {
	  QMessageBox::warning( this,QString::fromUtf8("Erreur"),QString::fromUtf8("Retour vide"));
	  }
      }
  ActualiserArbre();
  }
else
  {
  ui.EditorView->editor->insertTag(QString::fromUtf8("APPELER_FONCTION •"),2,0);
  }
}
//***********************************************
void MainWindow::ModifierLire()
{
QTreeWidgetItem *curItem = ui.treeWidget->currentItem();
if (!curItem || !curItem->parent()) return;
if (ListeNomsVariables.isEmpty())
    {
    QMessageBox::warning( this,QString::fromUtf8("Erreur"),QString::fromUtf8("Aucune variable n'a encore été définie."));
    return;
    }

QString excode=curItem->data(0,Qt::UserRole).toString();
QStringList parametres=excode.split("#");
if (parametres.count()==3)
    {
    QString exnomvariable=parametres.at(1);
    int i=ListeNomsVariables.indexOf(exnomvariable);
    QString exrang=parametres.at(2);
    if (i>-1)
	{
	QString extype=ListeTypesVariables.at(i);
	LireDialog *lireDialog = new LireDialog(this,ListeNomsVariables.join("#"),ListeTypesVariables.join("#"));
	int f=lireDialog->ui.comboBoxLire->findText(exnomvariable,Qt::MatchExactly | Qt::MatchCaseSensitive);
	lireDialog->ui.comboBoxLire->setCurrentIndex(f);
	if (extype=="LISTE")
	    {
	    lireDialog->ui.labelLire2->setEnabled(true);
	    lireDialog->ui.lineEditLire->setEnabled(true);
	    lireDialog->ui.lineEditLire->setText(exrang);
	    }
	else
	    {
	    lireDialog->ui.lineEditLire->clear();
	    lireDialog->ui.labelLire2->setEnabled(false);
	    lireDialog->ui.lineEditLire->setEnabled(false);
	    }
	if (lireDialog->exec())
	    {
	    QString nomvariable=lireDialog->ui.comboBoxLire->currentText();
	    QString rang=lireDialog->ui.lineEditLire->text();
	    QString code,type;
	    code="";
	    if (!nomvariable.isEmpty())
		{
		if (curItem && curItem->parent()) 
			{
			int i=ListeNomsVariables.indexOf(nomvariable);
			if (i>-1)
			    {
			    type=ListeTypesVariables.at(i);
			    if ((type=="LISTE") && (!rang.isEmpty())) 
				{
				curItem->setText(0,QString::fromUtf8("LIRE ")+nomvariable+"["+rang+"]");
				code="2#"+nomvariable+"#"+rang;
				}
			    else 
				{
				curItem->setText(0,QString::fromUtf8("LIRE ")+nomvariable);
				code="2#"+nomvariable+"#pasliste";
				}
			    curItem->setData(0,Qt::UserRole,QString(code));
			    }
			ui.treeWidget->setCurrentItem(curItem);
			ActualiserStatut();
			}
		}
	    else
		{
		QMessageBox::warning( this,QString::fromUtf8("Erreur"),QString::fromUtf8("Pas de variable définie"));
		}
	    }
	ActualiserArbre();
	}
    }
}

void MainWindow::ModifierAfficher()
{
QTreeWidgetItem *curItem = ui.treeWidget->currentItem();
if (!curItem || !curItem->parent()) return;
if (ListeNomsVariables.isEmpty())
    {
    QMessageBox::warning( this,QString::fromUtf8("Erreur"),QString::fromUtf8("Aucune variable n'a encore été définie."));
    return;
    }
QString excode=curItem->data(0,Qt::UserRole).toString();
QStringList parametres=excode.split("#");
if (parametres.count()==4)
    {
    QString exnomvariable=parametres.at(1);
    QString exretourligne=parametres.at(2);
    QString exrang=parametres.at(3);
    int i=ListeNomsVariables.indexOf(exnomvariable);
    if (i>-1)
	{
	QString extype=ListeTypesVariables.at(i);
	AfficherDialog *afficherDialog = new AfficherDialog(this,ListeNomsVariables.join("#"),ListeTypesVariables.join("#"));
	int f=afficherDialog->ui.comboBoxAfficher->findText(exnomvariable,Qt::MatchExactly | Qt::MatchCaseSensitive);
	afficherDialog->ui.comboBoxAfficher->setCurrentIndex(f);
	if (extype=="LISTE")
	    {
	    afficherDialog->ui.labelAfficher2->setEnabled(true);
	    afficherDialog->ui.lineEditAfficher->setEnabled(true);
	    afficherDialog->ui.lineEditAfficher->setText(exrang);
	    }
	else
	    {
	    afficherDialog->ui.lineEditAfficher->clear();
	    afficherDialog->ui.labelAfficher2->setEnabled(false);
	    afficherDialog->ui.lineEditAfficher->setEnabled(false);
	    }
	if (exretourligne=="1") afficherDialog->ui.checkBoxAfficher->setChecked(true);
	else afficherDialog->ui.checkBoxAfficher->setChecked(false);
	if (afficherDialog->exec())
	    {
	    QString nomvariable=afficherDialog->ui.comboBoxAfficher->currentText();
	    QString rang=afficherDialog->ui.lineEditAfficher->text();
	    QString code="";
	    if (!nomvariable.isEmpty())
		{
		int i=ListeNomsVariables.indexOf(nomvariable);
		if (i>-1)
		    {
		    QString type=ListeTypesVariables.at(i);
		    if (afficherDialog->ui.checkBoxAfficher->isChecked()) code="3#"+nomvariable+"#1";
		    else code="3#"+nomvariable+"#0";
		    if ((type=="LISTE") && (!rang.isEmpty())) 
			{
			curItem->setText(0,QString::fromUtf8("AFFICHER ")+nomvariable+"["+rang+"]");
			code+="#"+rang;
			}
		    else 
			{
			curItem->setText(0,QString::fromUtf8("AFFICHER ")+nomvariable);
			code+="#pasliste";
			}
		    curItem->setData(0,Qt::UserRole,QString(code));
		    }
		ui.treeWidget->setCurrentItem(curItem);
		ActualiserStatut();
		}
	    else
		{
		QMessageBox::warning( this,QString::fromUtf8("Erreur"),QString::fromUtf8("Pas de variable définie"));
		}
	    }
	ActualiserArbre();
	}
    }
}

void MainWindow::ModifierMessage()
{
QTreeWidgetItem *curItem = ui.treeWidget->currentItem();
if (!curItem || !curItem->parent()) return;
QString excode=curItem->data(0,Qt::UserRole).toString();
QStringList parametres=excode.split("#");
if (parametres.count()==3)
    {
    QString exmessage=parametres.at(1);
    QString exretourligne=parametres.at(2);
    MessageDialog *messageDialog = new MessageDialog(this);
    messageDialog->ui.lineEditMessage->setText(exmessage);
    if (exretourligne=="1") messageDialog->ui.checkBoxMessage->setChecked(true);
    else messageDialog->ui.checkBoxMessage->setChecked(false);
    if (messageDialog->exec())
	{
	QString message=messageDialog->ui.lineEditMessage->text();
	message.remove("#");
	message.remove("\"");
//	message.remove("'");
	QString code="";
	if (!message.isEmpty())
	    {
		    curItem->setText(0,QString::fromUtf8("AFFICHER \"")+message+"\"");
		    if (messageDialog->ui.checkBoxMessage->isChecked()) code="4#"+message+"#1";
		    else code="4#"+message+"#0";
		    curItem->setData(0,Qt::UserRole,QString(code));
		    ui.treeWidget->setCurrentItem(curItem);
		    ActualiserStatut();
	    }
	else
	    {
	    QMessageBox::warning( this,QString::fromUtf8("Erreur"),QString::fromUtf8("Message vide"));
	    }
	}
    ActualiserArbre();
    }
}

void MainWindow::ModifierCalcul()
{
QTreeWidgetItem *curItem = ui.treeWidget->currentItem();
if (!curItem || !curItem->parent()) return;
QString excode=curItem->data(0,Qt::UserRole).toString();
QStringList parametres=excode.split("#");
if (parametres.count()==3)
    {
    QString excalcul=parametres.at(1);
    QString exretourligne=parametres.at(2);
    AffichercalculDialog *calculDialog = new AffichercalculDialog(this,ListeNomsVariables.join("#"),ListeTypesVariables.join("#"));
    calculDialog->ui.lineEditAffectation->setText(excalcul);
    if (exretourligne=="1") calculDialog->ui.checkBoxAfficher->setChecked(true);
    else calculDialog->ui.checkBoxAfficher->setChecked(false);
    if (calculDialog->exec())
	{
	QString calcul=calculDialog->ui.lineEditAffectation->text();
	calcul.remove("#");
	//calcul.remove("\"");
//	message.remove("'");
	QString code="";
      if (!calcul.isEmpty())
	  {
		  curItem->setText(0,QString::fromUtf8("AFFICHERCALCUL ")+calcul);
		  if (calculDialog->ui.checkBoxAfficher->isChecked()) code="20#"+calcul+"#1";
		  else code="20#"+calcul+"#0";
		  curItem->setData(0,Qt::UserRole,QString(code));
		  ui.treeWidget->setCurrentItem(curItem);
		  ActualiserStatut();
	  }
      else
	  {
	  QMessageBox::warning( this,QString::fromUtf8("Erreur"),QString::fromUtf8("Calcul vide"));
	  }
	}
    ActualiserArbre();
    }
}

void MainWindow::ModifierAffectation()
{
QTreeWidgetItem *curItem = ui.treeWidget->currentItem();
if (!curItem || !curItem->parent()) return;
if (ListeNomsVariables.isEmpty())
    {
    QMessageBox::warning( this,QString::fromUtf8("Erreur"),QString::fromUtf8("Aucune variable n'a encore été définie."));
    return;
    }
QString excode=curItem->data(0,Qt::UserRole).toString();
QStringList parametres=excode.split("#");
if (parametres.count()==4)
    {
    QString exnomvariable=parametres.at(1);
    QString excontenu=parametres.at(2);
    QString exrang=parametres.at(3);
    int i=ListeNomsVariables.indexOf(exnomvariable);
    if (i>-1)
	{
	QString extype=ListeTypesVariables.at(i);
	AffectationDialog *affectationDialog = new AffectationDialog(this,ListeNomsVariables.join("#"),ListeTypesVariables.join("#"));
	int f=affectationDialog->ui.comboBoxAffectation->findText(exnomvariable,Qt::MatchExactly | Qt::MatchCaseSensitive);
	affectationDialog->ui.comboBoxAffectation->setCurrentIndex(f);
	affectationDialog->ui.lineEditAffectation->setText(excontenu);
	if (extype=="LISTE")
	    {
	    affectationDialog->ui.labelAffectation3->setEnabled(true);
	    affectationDialog->ui.lineEditAffectation2->setEnabled(true);
	    affectationDialog->ui.lineEditAffectation2->setText(exrang);
	    }
	else
	    {
	    affectationDialog->ui.lineEditAffectation2->clear();
	    affectationDialog->ui.lineEditAffectation2->setEnabled(false);
	    affectationDialog->ui.labelAffectation3->setEnabled(false);
	    }
	if (affectationDialog->exec())
	    {
	    QString nomvariable=affectationDialog->ui.comboBoxAffectation->currentText();
	    QString contenu=affectationDialog->ui.lineEditAffectation->text();
	    QString rang=affectationDialog->ui.lineEditAffectation2->text();
	    contenu.remove("#");
	    QString code="";
	    if (!nomvariable.isEmpty())
		{
		int i=ListeNomsVariables.indexOf(nomvariable);
		if (i>-1)
		    {
		    QString type=ListeTypesVariables.at(i);
		    code="5#"+nomvariable+"#"+contenu;
		    if ((type=="LISTE") && (!rang.isEmpty())) 
			{
			curItem->setText(0,nomvariable+"["+rang+"]"+QString::fromUtf8(" PREND_LA_VALEUR ")+contenu);
			code+="#"+rang;
			}
		    else 
			{
			curItem->setText(0,nomvariable+QString::fromUtf8(" PREND_LA_VALEUR ")+contenu);
			code+="#pasliste";
			}
		    curItem->setData(0,Qt::UserRole,QString(code));
		    }
		ui.treeWidget->setCurrentItem(curItem);
		ActualiserStatut();
		}
	    else
		{
		QMessageBox::warning( this,QString::fromUtf8("Erreur"),QString::fromUtf8("Pas de variable définie"));
		}
	    }
	ActualiserArbre();

	}
    }
}


void MainWindow::ModifierCondition()
{
QTreeWidgetItem *lastChildItem, *sinonItem, *newItem;
QTreeWidgetItem *curItem = ui.treeWidget->currentItem();
if (!curItem || !curItem->parent()) return;
QString excode=curItem->data(0,Qt::UserRole).toString();
QStringList parametres=excode.split("#");
if (parametres.count()==2)
    {
    QString excondition=parametres.at(1);
    ConditionDialog *conditionDialog = new ConditionDialog(this,ListeNomsVariables.join("#"),ListeTypesVariables.join("#"));
    conditionDialog->ui.lineEditCondition->setText(excondition);
    int idx=curItem->childCount()-1;
    if (idx>=0)
	{
	lastChildItem=curItem->child(idx);
	if (lastChildItem->text(0)=="SINON") 
	    {
	    conditionDialog->ui.checkBoxCondition->hide();
	    conditionDialog->ui.checkBoxCondition->setChecked(false);
	    }
	if (conditionDialog->exec())
	    {
	    QString condition=conditionDialog->ui.lineEditCondition->text();
	    condition.remove("#");
	    QString code="";
	    if (!condition.isEmpty())
		{
		curItem->setText(0,QString::fromUtf8("SI (")+condition+") "+QString::fromUtf8("ALORS"));
        curItem->setExpanded(true);
		code="6#"+condition;
		curItem->setData(0,Qt::UserRole,QString(code));
		ui.treeWidget->setCurrentItem(curItem);
		if (conditionDialog->ui.checkBoxCondition->isChecked())
		    {
		    sinonItem = new QTreeWidgetItem(curItem,true);
		    sinonItem->setText(0,QString::fromUtf8("SINON"));
            sinonItem->setExpanded(true);
		    code="9#sinon";
		    sinonItem->setData(0,Qt::UserRole,QString(code));
		    newItem = new QTreeWidgetItem(sinonItem);
		    newItem->setText(0,QString::fromUtf8("DEBUT_SINON"));
		    newItem->setData(0,Qt::UserRole,QString("10#debutsinon"));
		    newItem = new QTreeWidgetItem(sinonItem);
		    ui.treeWidget->setCurrentItem(newItem);
		    newItem = new QTreeWidgetItem(sinonItem);
		    newItem->setText(0,QString::fromUtf8("FIN_SINON"));
		    newItem->setData(0,Qt::UserRole,QString("11#finsinon"));
		    }
		ActualiserStatut();
		}
	    else
		{
		QMessageBox::warning( this,QString::fromUtf8("Erreur"),QString::fromUtf8("Pas de condition définie"));
		}
	    }
	}
    ActualiserArbre();
    }
}

void MainWindow::ModifierBoucle()
{
QTreeWidgetItem *curItem = ui.treeWidget->currentItem();
if (!curItem || !curItem->parent()) return;
QString excode=curItem->data(0,Qt::UserRole).toString();
QStringList parametres=excode.split("#");
if (parametres.count()==4)
    {
    QString exnomvariable=parametres.at(1);
    QString exdebut=parametres.at(2);
    QString exfin=parametres.at(3);
    QStringList ListePourVariables;
    for (int i = 0; i < ListeNomsVariables.count(); i++) 
	{
	if (ListeTypesVariables.at(i)=="NOMBRE") ListePourVariables.append(ListeNomsVariables.at(i));
	}
    PourDialog *pourDialog = new PourDialog(this,ListePourVariables.join("#"));
    int f=pourDialog->ui.comboBoxBoucle->findText(exnomvariable,Qt::MatchExactly | Qt::MatchCaseSensitive);
    pourDialog->ui.comboBoxBoucle->setCurrentIndex(f);
    pourDialog->ui.lineEditBoucle1->setText(exdebut);
    pourDialog->ui.lineEditBoucle2->setText(exfin);
    if (pourDialog->exec())
	{
	QString nomvariable=pourDialog->ui.comboBoxBoucle->currentText();
	QString debut=pourDialog->ui.lineEditBoucle1->text();
	debut.remove("#");
	QString fin=pourDialog->ui.lineEditBoucle2->text();
	fin.remove("#");
	QString code="";
	if (!nomvariable.isEmpty())
	    {
	    int i=ListeNomsVariables.indexOf(nomvariable);
	    if (i>-1)
		{
		curItem->setText(0,QString::fromUtf8("POUR ")+nomvariable+" ALLANT_DE "+debut+ " A "+fin);
		code="12#"+nomvariable+"#"+debut+"#"+fin;
		curItem->setData(0,Qt::UserRole,QString(code));
		ui.treeWidget->setCurrentItem(curItem);
		}
	    ActualiserStatut();
	    }
	else
	    {
	    QMessageBox::warning( this,QString::fromUtf8("Erreur"),QString::fromUtf8("Pas de variable définie"));
	    }
	}
    ActualiserArbre();
    }
}

void MainWindow::ModifierTantque()
{
QTreeWidgetItem *curItem = ui.treeWidget->currentItem();
if (!curItem || !curItem->parent()) return;
QString excode=curItem->data(0,Qt::UserRole).toString();
QStringList parametres=excode.split("#");
if (parametres.count()==2)
    {
    QString excondition=parametres.at(1);
    TantqueDialog *tantqueDialog = new TantqueDialog(this,ListeNomsVariables.join("#"),ListeTypesVariables.join("#"));
    tantqueDialog->ui.lineEditTantque->setText(excondition);
    if (tantqueDialog->exec())
	{
	QString condition=tantqueDialog->ui.lineEditTantque->text();
	condition.remove("#");
	QString code="";
	if (!condition.isEmpty())
	    {
	    curItem->setText(0,QString::fromUtf8("TANT_QUE (")+condition+") "+QString::fromUtf8("FAIRE"));
        curItem->setExpanded(true);
	    code="15#"+condition;
	    curItem->setData(0,Qt::UserRole,QString(code));
	    ui.treeWidget->setCurrentItem(curItem);
	    ActualiserStatut();
	    }
	else
	    {
	    QMessageBox::warning( this,QString::fromUtf8("Erreur"),QString::fromUtf8("Pas de condition définie"));
	    }
	}
    ActualiserArbre();
    }
}

void MainWindow::ModifierPoint()
{
QTreeWidgetItem *curItem = ui.treeWidget->currentItem();
if (!curItem || !curItem->parent()) return;
QString excode=curItem->data(0,Qt::UserRole).toString();
QStringList parametres=excode.split("#");
if (parametres.count()==4)
    {
    QString ex_x=parametres.at(1);
    QString ex_y=parametres.at(2);
    QString ex_couleur=parametres.at(3);
    PointDialog *pointDialog = new PointDialog(this,ListeNomsVariables.join("#"),ListeTypesVariables.join("#"));
    pointDialog->ui.lineEditX->setText(ex_x);
    pointDialog->ui.lineEditY->setText(ex_y);
    int index=pointDialog->ui.comboBoxCouleur->findText (ex_couleur , Qt::MatchExactly);
    pointDialog->ui.comboBoxCouleur->setCurrentIndex(index);
    if (pointDialog->exec())
	{
	QString x=pointDialog->ui.lineEditX->text();
	QString y=pointDialog->ui.lineEditY->text();
	QString couleur=pointDialog->ui.comboBoxCouleur->currentText();
	x.remove("#");
	y.remove("#");
	if ((!x.isEmpty()) && (!y.isEmpty()))
	    {
	    curItem->setText(0,QString::fromUtf8("TRACER_POINT (")+x+","+y+")");
	    QString code="50#"+x+"#"+y+"#"+couleur;
	    curItem->setData(0,Qt::UserRole,QString(code));
	    ui.treeWidget->setCurrentItem(curItem);
	    ActualiserStatut();
	    }
	else
	    {
	    QMessageBox::warning( this,QString::fromUtf8("Erreur"),QString::fromUtf8("Au moins une des coordonnées n'est pas définie"));
	    }
	ActualiserArbre();
	}
    }
}

void MainWindow::ModifierSegment()
{
QTreeWidgetItem *curItem = ui.treeWidget->currentItem();
if (!curItem || !curItem->parent()) return;
QString excode=curItem->data(0,Qt::UserRole).toString();
QStringList parametres=excode.split("#");
if (parametres.count()==6)
    {
    QString ex_xdep=parametres.at(1);
    QString ex_ydep=parametres.at(2);
    QString ex_xfin=parametres.at(3);
    QString ex_yfin=parametres.at(4);
    QString ex_couleur=parametres.at(5);
    SegmentDialog *segmentDialog = new SegmentDialog(this,ListeNomsVariables.join("#"),ListeTypesVariables.join("#"));
    segmentDialog->ui.lineEditXdep->setText(ex_xdep);
    segmentDialog->ui.lineEditYdep->setText(ex_ydep);
    segmentDialog->ui.lineEditXfin->setText(ex_xfin);
    segmentDialog->ui.lineEditYfin->setText(ex_yfin);
    int index=segmentDialog->ui.comboBoxCouleur->findText (ex_couleur , Qt::MatchExactly);
    segmentDialog->ui.comboBoxCouleur->setCurrentIndex(index);
    if (segmentDialog->exec())
	{
	QString xdep=segmentDialog->ui.lineEditXdep->text();
	QString ydep=segmentDialog->ui.lineEditYdep->text();
	QString xfin=segmentDialog->ui.lineEditXfin->text();
	QString yfin=segmentDialog->ui.lineEditYfin->text();
	QString couleur=segmentDialog->ui.comboBoxCouleur->currentText();
	xdep.remove("#");
	ydep.remove("#");
	xfin.remove("#");
	yfin.remove("#");
	if ((!xdep.isEmpty()) && (!ydep.isEmpty()) && (!xfin.isEmpty()) && (!yfin.isEmpty()))
	    {
	    curItem->setText(0,QString::fromUtf8("TRACER_SEGMENT (")+xdep+","+ydep+")->("+xfin+","+yfin+")");
	    QString code="51#"+xdep+"#"+ydep+"#"+xfin+"#"+yfin+"#"+couleur;
	    curItem->setData(0,Qt::UserRole,QString(code));
	    ui.treeWidget->setCurrentItem(curItem);
	    ActualiserStatut();
	    }
	else
	    {
	    QMessageBox::warning( this,QString::fromUtf8("Erreur"),QString::fromUtf8("Au moins une des coordonnées n'est pas définie"));
	    }
	}
    ActualiserArbre();
    }
}

void MainWindow::ModifierCommentaire()
{
QFont commentFont = qApp->font();
commentFont.setItalic(true);
QTreeWidgetItem *curItem = ui.treeWidget->currentItem();
if (!curItem || !curItem->parent()) return;
QString excode=curItem->data(0,Qt::UserRole).toString();
QStringList parametres=excode.split("#");
if (parametres.count()==2)
    {
    QString excommentaire=parametres.at(1);
    CommentaireDialog *commentaireDialog = new CommentaireDialog(this);
    commentaireDialog->ui.lineEditCommentaire->setText(excommentaire);
    if (commentaireDialog->exec())
	{
	QString commentaire=commentaireDialog->ui.lineEditCommentaire->text();
	commentaire.remove("#");
	//commentaire.remove("\"");
	QString code="";
	if (!commentaire.isEmpty())
	    {
		    curItem->setText(0,"//"+commentaire);
		    code="19#"+commentaire;
		    curItem->setData(0,Qt::UserRole,QString(code));
		    curItem->setFont(0,commentFont);
		    ui.treeWidget->setCurrentItem(curItem);
		    ActualiserStatut();
	    }
	else
	    {
	    QMessageBox::warning( this,QString::fromUtf8("Erreur"),QString::fromUtf8("Commentaire vide"));
	    }
	}
    ActualiserArbre();
    }
}

void MainWindow::ModifierFonction()
{
QTreeWidgetItem *curItem = ui.treeWidget->currentItem();
if (!curItem || !curItem->parent()) return;
QString excode=curItem->data(0,Qt::UserRole).toString();
QStringList parametres=excode.split("#");
if (parametres.count()==3)
    {
    QString nom=parametres.at(1);
    QString param=parametres.at(2);
    FonctionDialog *fonctionDialog = new FonctionDialog(this);
    fonctionDialog->ui.lineEditNom->setText(nom);
    fonctionDialog->ui.lineEditParam->setText(param);
  if (fonctionDialog->exec())
      {
      QString nouveau_nom=fonctionDialog->ui.lineEditNom->text();
      nouveau_nom.remove("#");
      QString nouveau_param=fonctionDialog->ui.lineEditParam->text();
      nouveau_param.remove("#");
      QString code="";
      QTreeWidgetItem *newItem;
      if (!nouveau_nom.isEmpty())
	  {
        if (nouveau_param.isEmpty()) nouveau_param=QString(" ");
        curItem->setText(0,"FONCTION "+nouveau_nom+"("+nouveau_param+")");
        curItem->setExpanded(true);
        code="201#"+nouveau_nom+"#"+nouveau_param;
        curItem->setData(0,Qt::UserRole,QString(code));
        ActualiserStatut();
	  }
      else
	  {
	  QMessageBox::warning( this,QString::fromUtf8("Erreur"),QString::fromUtf8("Fonction non valide"));
	  }
      }
	}
    ActualiserArbre();
}

void MainWindow::ModifierRenvoyer()
{
QTreeWidgetItem *curItem = ui.treeWidget->currentItem();
if (!curItem || !curItem->parent()) return;
QString excode=curItem->data(0,Qt::UserRole).toString();
QStringList parametres=excode.split("#");
if (parametres.count()==2)
    {
    QString exretour=parametres.at(1);
    RenvoyerDialog *renvoyerDialog = new RenvoyerDialog(this);
    renvoyerDialog->ui.lineEdit->setText(exretour);
    if (renvoyerDialog->exec())
	{
	QString retour=renvoyerDialog->ui.lineEdit->text();
	retour.remove("#");
	QString code="";
	if (!retour.isEmpty())
	    {
		    curItem->setText(0,"RENVOYER "+retour);
		    code="205#"+retour;
		    curItem->setData(0,Qt::UserRole,QString(code));
		    ui.treeWidget->setCurrentItem(curItem);
		    ActualiserStatut();
	    }
	else
	    {
	    QMessageBox::warning( this,QString::fromUtf8("Erreur"),QString::fromUtf8("Retour vide"));
	    }
	}
    ActualiserArbre();
    }
}
void MainWindow::ModifierAppelerFct()
{
QTreeWidgetItem *curItem = ui.treeWidget->currentItem();
if (!curItem || !curItem->parent()) return;
QString excode=curItem->data(0,Qt::UserRole).toString();
QStringList parametres=excode.split("#");
if (parametres.count()==2)
    {
    QString exfct=parametres.at(1);
    AppelDialog *appelDialog = new AppelDialog(this);
    appelDialog->ui.lineEdit->setText(exfct);
    if (appelDialog->exec())
	{
	QString fct=appelDialog->ui.lineEdit->text();
	fct.remove("#");
	QString code="";
	if (!fct.isEmpty())
	    {
		    curItem->setText(0,"APPELER_FONCTION "+fct);
		    code="206#"+fct;
		    curItem->setData(0,Qt::UserRole,QString(code));
		    ui.treeWidget->setCurrentItem(curItem);
		    ActualiserStatut();
	    }
	else
	    {
	    QMessageBox::warning( this,QString::fromUtf8("Erreur"),QString::fromUtf8("Retour vide"));
	    }
	}
    ActualiserArbre();
    }
}
//***********************************************
void MainWindow::LireConfig()
{
#ifdef USB_VERSION
QSettings *config=new QSettings(QCoreApplication::applicationDirPath()+"/algobox.ini",QSettings::IniFormat);
#else
QSettings *config=new QSettings(QSettings::IniFormat,QSettings::UserScope,"xm1","algobox");
#endif
config->beginGroup( "algobox" );
dernierRepertoire=config->value("dernier repertoire",QDir::homePath()).toString();
#if (QT_VERSION >= QT_VERSION_CHECK(5, 14, 0))
   QRect screen = QWidget::screen()->geometry();
 #else
  QRect screen = (window() && window()->windowHandle() ? window()->windowHandle()->screen()->geometry() : QGuiApplication::primaryScreen()->geometry());
#endif
int w= config->value( "Geometries/MainwindowWidth",900).toInt();
int h= config->value( "Geometries/MainwindowHeight",screen.height()-200).toInt() ;
int x= config->value( "Geometries/MainwindowX",10).toInt();
int y= config->value( "Geometries/MainwindowY",10).toInt() ;
resize(w,h);
move(x,y);
windowstate=config->value("MainWindowState").toByteArray();
afficheCadrePresentation=config->value("Presentation",true).toBool();
modeNormal=config->value("Edition normale",true).toBool();
blackconsole=config->value("Black Console",true).toBool();
if (!afficheCadrePresentation)	ui.frameDescription->hide();
browserwidth=config->value( "Geometries/BrowserWidth",850).toInt();
browserheight=config->value( "Geometries/BrowserHeight",540).toInt();

arrondiAuto=config->value("Options/Arrondi",true).toBool();
maxBoucle=config->value("Options/MaxBoucle",500000).toInt();
totalBoucles=config->value("Options/TotalBoucles",5000000).toInt();
totalAffichages=config->value("Options/TotalAffichages",1000).toInt();

epaisseurLigne=config->value("Options/EpaisseurLigne",2).toInt();
epaisseurPoint=config->value("Options/EpaisseurPoint",2).toInt();
nbDecimales=config->value("Options/NbDecimales",8).toInt();

arrondiAutoAlgo=arrondiAuto;
maxBoucleAlgo=maxBoucle;
totalBouclesAlgo=totalBoucles;
totalAffichagesAlgo=totalAffichages;
epaisseurLigneAlgo=epaisseurLigne;
epaisseurPointAlgo=epaisseurPoint;
nbDecimalesAlgo=nbDecimales;

QFontDatabase fdb;
QStringList xf = fdb.families();
QString deft;
// if (xf.contains("DejaVu Sans",Qt::CaseInsensitive)) deft="DejaVu Sans";
// else if (xf.contains("DejaVu Sans LGC",Qt::CaseInsensitive)) deft="DejaVu Sans LGC";
// else if (xf.contains("Bitstream Vera Sans",Qt::CaseInsensitive)) deft="Bitstream Vera Sans";
// else if (xf.contains("Luxi Sans",Qt::CaseInsensitive)) deft="Luxi Sans";
// else 
deft=qApp->font().family();
x11fontfamily=config->value("X11/Font Family",deft).toString();
x11fontsize=config->value( "X11/Font Size",qApp->font().pointSize()).toInt();
QFont x11Font (x11fontfamily,x11fontsize);
QApplication::setFont(x11Font);
ui.EditorView->setFontSize(x11fontsize);
#if defined(Q_OS_UNIX) && !defined(Q_OS_MAC)
int desktop_env=1; // 1 : no kde ; 2: kde ; 3 : kde4 ; 
QStringList styles = QStyleFactory::keys();
QString kdesession= ::getenv("KDE_FULL_SESSION");
QString kdeversion= ::getenv("KDE_SESSION_VERSION");
if (!kdesession.isEmpty()) desktop_env=2;
if (!kdeversion.isEmpty()) desktop_env=3;

if(desktop_env == 1)
{
if (styles.contains("GTK+")) qApp->setStyle(QLatin1String("gtkstyle"));
else if (styles.contains("Breeze")) qApp->setStyle(QLatin1String("breeze"));
else qApp->setStyle(QLatin1String("fusion"));    
}
else if (styles.contains("Breeze")) qApp->setStyle(QLatin1String("breeze"));
else qApp->setStyle(QLatin1String("fusion"));


#ifdef STATIC_VERSION
QPalette pal = QApplication::palette();
pal.setColor( QPalette::Active, QPalette::Highlight, QColor("#4490d8") );
pal.setColor( QPalette::Inactive, QPalette::Highlight, QColor("#4490d8") );
pal.setColor( QPalette::Disabled, QPalette::Highlight, QColor("#4490d8") );

pal.setColor( QPalette::Active, QPalette::HighlightedText, QColor("#ffffff") );
pal.setColor( QPalette::Inactive, QPalette::HighlightedText, QColor("#ffffff") );
pal.setColor( QPalette::Disabled, QPalette::HighlightedText, QColor("#ffffff") );

pal.setColor( QPalette::Active, QPalette::Base, QColor("#ffffff") );
pal.setColor( QPalette::Inactive, QPalette::Base, QColor("#ffffff") );
pal.setColor( QPalette::Disabled, QPalette::Base, QColor("#ffffff") );

pal.setColor( QPalette::Active, QPalette::WindowText, QColor("#000000") );
pal.setColor( QPalette::Inactive, QPalette::WindowText, QColor("#000000") );
pal.setColor( QPalette::Disabled, QPalette::WindowText, QColor("#000000") );

pal.setColor( QPalette::Active, QPalette::Text, QColor("#000000") );
pal.setColor( QPalette::Inactive, QPalette::Text, QColor("#000000") );
pal.setColor( QPalette::Disabled, QPalette::Text, QColor("#000000") );

pal.setColor( QPalette::Active, QPalette::ButtonText, QColor("#000000") );
pal.setColor( QPalette::Inactive, QPalette::ButtonText, QColor("#000000") );
pal.setColor( QPalette::Disabled, QPalette::ButtonText, QColor("#000000") );

if (desktop_env ==3)
	{
	pal.setColor( QPalette::Active, QPalette::Window, QColor("#eae9e9") );
	pal.setColor( QPalette::Inactive, QPalette::Window, QColor("#eae9e9") );
	pal.setColor( QPalette::Disabled, QPalette::Window, QColor("#eae9e9") );

	pal.setColor( QPalette::Active, QPalette::Button, QColor("#eae9e9") );
	pal.setColor( QPalette::Inactive, QPalette::Button, QColor("#eae9e9") );
	pal.setColor( QPalette::Disabled, QPalette::Button, QColor("#eae9e9") );
	}
else
	{
	pal.setColor( QPalette::Active, QPalette::Window, QColor("#f6f3eb") );
	pal.setColor( QPalette::Inactive, QPalette::Window, QColor("#f6f3eb") );
	pal.setColor( QPalette::Disabled, QPalette::Window, QColor("#f6f3eb") );

	pal.setColor( QPalette::Active, QPalette::Button, QColor("#f6f3eb") );
	pal.setColor( QPalette::Inactive, QPalette::Button, QColor("#f6f3eb") );
	pal.setColor( QPalette::Disabled, QPalette::Button, QColor("#f6f3eb") );
	}

QApplication::setPalette(pal);
#endif
#endif
recentFilesList=config->value("Files/Recent Files").toStringList();
config->endGroup();
}

void MainWindow::SauverConfig()
{
#ifdef USB_VERSION
QSettings config(QCoreApplication::applicationDirPath()+"/algobox.ini",QSettings::IniFormat);
#else
QSettings config(QSettings::IniFormat,QSettings::UserScope,"xm1","algobox");
#endif
config.beginGroup( "algobox" );
config.setValue("dernier repertoire",dernierRepertoire);
config.setValue("MainWindowState",saveState(0));
config.setValue("Presentation",afficheCadrePresentation);
config.setValue("Edition normale",modeNormal);
config.setValue("Black Console",blackconsole);
config.setValue("Geometries/MainwindowWidth", width() );
config.setValue("Geometries/MainwindowHeight", height() );
config.setValue("Geometries/MainwindowX", x() );
config.setValue("Geometries/MainwindowY", y() );
config.setValue("Geometries/BrowserWidth",browserwidth);
config.setValue("Geometries/BrowserHeight",browserheight);

config.setValue("Options/Arrondi",arrondiAuto);
config.setValue("Options/MaxBoucle",maxBoucle);
config.setValue("Options/TotalBoucles",totalBoucles);
config.setValue("Options/TotalAffichages",totalAffichages);
config.setValue("Options/EpaisseurLigne",epaisseurLigne);
config.setValue("Options/EpaisseurPoint",epaisseurPoint);
config.setValue("Options/NbDecimales",nbDecimales);


config.setValue("X11/Font Family",x11fontfamily);
config.setValue( "X11/Font Size",x11fontsize);
if (recentFilesList.count()>0) config.setValue("Files/Recent Files",recentFilesList); 
config.endGroup();
}
//**************************
void MainWindow::InitOuvrir()
{
AnnulerExtension();
Init();
// ui.treeWidget->clear();
// clipboardItem=new QTreeWidgetItem(QStringList(QString("")));
// ui.treeWidget->setColumnCount(1);
// ui.treeWidget->header()->hide();
// ui.treeWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);
// ui.treeWidget->header()->setHorizontalScrollMode(QAbstractItemView::ScrollPerPixel);
// //ui.treeWidget->header()->setResizeMode(0, QHeaderView::Stretch);
// ui.treeWidget->header()->setResizeMode(0, QHeaderView::ResizeToContents);
// ui.treeWidget->header()->setStretchLastSection(false);
// ui.textEditDescription->clear();
// 
// ui.checkBoxFonction->setChecked(false);
// ui.lineEditFonction->setText("");
// ui.lineEditFonction->setEnabled(false);
// 
// 
// ui.checkBoxRepere->setChecked(false);
// ui.lineEditXmin->setText("-10");
// ui.lineEditXmax->setText("10");
// ui.lineEditYmin->setText("-10");
// ui.lineEditYmax->setText("10");
// ui.lineEditGradX->setText("2");
// ui.lineEditGradY->setText("2");
// ui.lineEditXmin->setEnabled(false);
// ui.lineEditXmax->setEnabled(false);
// ui.lineEditYmin->setEnabled(false);
// ui.lineEditYmax->setEnabled(false);
// ui.lineEditGradX->setEnabled(false);
// ui.lineEditGradY->setEnabled(false);
// 
// editor->clear();
// ui.EditorView->editor->insertTag(QString::fromUtf8("VARIABLES\n\nDEBUT_ALGORITHME\n\nFIN_ALGORITHME"),0,1);
// if (!modeNormal) ui.EditorView->editor->setFocus();
// 
// ui.tabWidget->setCurrentIndex(0);
}

void MainWindow::Init()
{
ui.tabWidget->removeTab(ui.tabWidget->indexOf(ui.tabWidget->findChild<QWidget *>("tab_4"))); 
ui.treeWidget->clear();
clipboardItem=new QTreeWidgetItem(QStringList(QString("")));
ui.treeWidget->setColumnCount(1);
ui.treeWidget->header()->hide();
ui.treeWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);
ui.treeWidget->header()->setHorizontalScrollMode(QAbstractItemView::ScrollPerPixel);
//ui.treeWidget->header()->setResizeMode(0, QHeaderView::Stretch);
ui.treeWidget->header()->setSectionResizeMode(QHeaderView::ResizeToContents);
ui.treeWidget->header()->setStretchLastSection(false);
ui.textEditDescription->clear();
ui.comboBoxParam->clear();

QFont titleFont = qApp->font();
titleFont.setBold(true);

fctsItem =new QTreeWidgetItem;
fctsItem->setText(0,QString("FONCTIONS_UTILISEES"));
fctsItem->setData(0,Qt::UserRole,QString("200#declarationfonctions"));
fctsItem->setFont(0,titleFont);
ui.treeWidget->addTopLevelItem(fctsItem);
fctsItem->setExpanded(true);

variablesItem =new QTreeWidgetItem;
variablesItem->setText(0,QString("VARIABLES"));
variablesItem->setData(0,Qt::UserRole,QString("100#declarationsvariables"));
variablesItem->setFont(0,titleFont);
ui.treeWidget->addTopLevelItem(variablesItem);
variablesItem->setExpanded(true);

debutItem =new QTreeWidgetItem;
debutItem->setText(0,QString("DEBUT_ALGORITHME"));
debutItem->setData(0,Qt::UserRole,QString("101#debutalgo"));
debutItem->setFont(0,titleFont);
ui.treeWidget->addTopLevelItem(debutItem);
debutItem->setExpanded(true);

finItem =new QTreeWidgetItem;
finItem->setText(0,QString("FIN_ALGORITHME"));
finItem->setData(0,Qt::UserRole,QString("102#finalgo"));
finItem->setFont(0,titleFont);
ui.treeWidget->addTopLevelItem(finItem);

ui.treeWidget->setCurrentItem(debutItem);

ui.checkBoxFonction->setChecked(false);
ui.lineEditFonction->setText("");
ui.lineEditFonction->setEnabled(false);
ui.listWidgetOp->setEnabled(false);

ui.checkBoxF2->setChecked(false);
ui.lineEditParametresF2->setText("");
ui.lineEditConditionF2->setText("");
ui.lineEditRetourF2->setText("");
ui.lineEditDefautF2->setText("");
ui.listWidgetF2->clear();
ui.lineEditParametresF2->setEnabled(false);
ui.lineEditConditionF2->setEnabled(false);
ui.lineEditRetourF2->setEnabled(false);
ui.lineEditDefautF2->setEnabled(false);
ui.listWidgetF2->setEnabled(false);
ui.pushButtonAjouterF2->setEnabled(false);
ui.pushButtonHautF2->setEnabled(false);
ui.pushButtonBasF2->setEnabled(false);
ui.pushButtonSupprimerF2->setEnabled(false);

ui.checkBoxRepere->setChecked(false);
ui.lineEditXmin->setText("-10");
ui.lineEditXmax->setText("10");
ui.lineEditYmin->setText("-10");
ui.lineEditYmax->setText("10");
ui.lineEditGradX->setText("2");
ui.lineEditGradY->setText("2");
ui.lineEditXmin->setEnabled(false);
ui.lineEditXmax->setEnabled(false);
ui.lineEditYmin->setEnabled(false);
ui.lineEditYmax->setEnabled(false);
ui.lineEditGradX->setEnabled(false);
ui.lineEditGradY->setEnabled(false);

ui.EditorView->editor->clear();
ui.EditorView->editor->insertTag(QString::fromUtf8("FONCTIONS_UTILISEES\n\nVARIABLES\n\nDEBUT_ALGORITHME\n\t\nFIN_ALGORITHME"),0,1);
if (!modeNormal) ui.EditorView->editor->setFocus();
else ui.treeWidget->setFocus();

ActualiserVariables();  
ui.tabWidget->setCurrentIndex(0);
arrondiAutoAlgo=arrondiAuto;
maxBoucleAlgo=maxBoucle;
totalBouclesAlgo=totalBoucles;
totalAffichagesAlgo=totalAffichages;
epaisseurLigneAlgo=epaisseurLigne;
epaisseurPointAlgo=epaisseurPoint;
nbDecimalesAlgo=nbDecimales;
ActualiserTexteParam();

}

void MainWindow::EffaceArbre()
{
ui.treeWidget->clear();
clipboardItem=new QTreeWidgetItem(QStringList(QString("")));
ui.treeWidget->setColumnCount(1);
ui.treeWidget->header()->hide();
ui.treeWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);
ui.treeWidget->header()->setHorizontalScrollMode(QAbstractItemView::ScrollPerPixel);
//ui.treeWidget->header()->setResizeMode(0, QHeaderView::Stretch);

ui.treeWidget->header()->setSectionResizeMode(QHeaderView::ResizeToContents);


ui.treeWidget->header()->setStretchLastSection(false);

QFont titleFont = qApp->font();
titleFont.setBold(true);

fctsItem =new QTreeWidgetItem;
fctsItem->setText(0,QString("FONCTIONS_UTILISEES"));
fctsItem->setData(0,Qt::UserRole,QString("200#declarationfonctions"));
fctsItem->setFont(0,titleFont);
ui.treeWidget->addTopLevelItem(fctsItem);
fctsItem->setExpanded(true);

variablesItem =new QTreeWidgetItem;
variablesItem->setText(0,QString("VARIABLES"));
variablesItem->setData(0,Qt::UserRole,QString("100#declarationsvariables"));
variablesItem->setFont(0,titleFont);
ui.treeWidget->addTopLevelItem(variablesItem);
variablesItem->setExpanded(true);

debutItem =new QTreeWidgetItem;
debutItem->setText(0,QString("DEBUT_ALGORITHME"));
debutItem->setData(0,Qt::UserRole,QString("101#debutalgo"));
debutItem->setFont(0,titleFont);
ui.treeWidget->addTopLevelItem(debutItem);
debutItem->setExpanded(true);

finItem =new QTreeWidgetItem;
finItem->setText(0,QString("FIN_ALGORITHME"));
finItem->setData(0,Qt::UserRole,QString("102#finalgo"));
finItem->setFont(0,titleFont);
ui.treeWidget->addTopLevelItem(finItem);
ui.treeWidget->setCurrentItem(debutItem);  

ActualiserVariables();
}

void MainWindow::ActiverBoutons()
{
ui.pushButtonLire->setEnabled(true);
ui.pushButtonAfficher->setEnabled(true);
ui.pushButtonMessage->setEnabled(true);
ui.pushButtonCalcul->setEnabled(true);
ui.pushButtonPause->setEnabled(true);
ui.pushButtonAffectation->setEnabled(true);
ui.pushButtonCondition->setEnabled(true);
ui.pushButtonBoucle->setEnabled(true);
ui.pushButtonTantque->setEnabled(true);
ui.pushButtonCommentaire->setEnabled(true);
ui.pushButtonRetourFct->setEnabled(true);
ui.pushButtonAppelFonction->setEnabled(true);
if (ui.checkBoxRepere->isChecked())
    {
    ui.pushButtonPoint->setEnabled(true);
    ui.pushButtonSegment->setEnabled(true);
    ui.pushButtonEfface->setEnabled(true);
    }
}

void MainWindow::DesactiverBoutons()
{
ui.pushButtonLire->setEnabled(false);
ui.pushButtonAfficher->setEnabled(false);
ui.pushButtonMessage->setEnabled(false);
ui.pushButtonCalcul->setEnabled(false);
ui.pushButtonPause->setEnabled(false);
ui.pushButtonAffectation->setEnabled(false);
ui.pushButtonCondition->setEnabled(false);
ui.pushButtonBoucle->setEnabled(false);
ui.pushButtonTantque->setEnabled(false);
ui.pushButtonPoint->setEnabled(false);
ui.pushButtonSegment->setEnabled(false);
ui.pushButtonEfface->setEnabled(false);
ui.pushButtonCommentaire->setEnabled(false);
ui.pushButtonRetourFct->setEnabled(false);
ui.pushButtonAppelFonction->setEnabled(false);
}

void MainWindow::ActiverFonction(bool etat)
{
ui.lineEditFonction->setEnabled(etat);
ui.listWidgetOp->setEnabled(etat);
ActualiserStatut();
}

void MainWindow::ActiverF2(bool etat)
{
ui.lineEditParametresF2->setEnabled(etat);
ui.lineEditConditionF2->setEnabled(etat);
ui.lineEditRetourF2->setEnabled(etat);
ui.lineEditDefautF2->setEnabled(etat);
ui.listWidgetF2->setEnabled(etat);
ui.pushButtonAjouterF2->setEnabled(etat);
ui.pushButtonHautF2->setEnabled(etat);
ui.pushButtonBasF2->setEnabled(etat);
ui.pushButtonSupprimerF2->setEnabled(etat);
ActualiserStatut();
}

void MainWindow::ActiverRepere(bool etat)
{
ui.lineEditXmin->setEnabled(etat);
ui.lineEditXmax->setEnabled(etat);
ui.lineEditYmin->setEnabled(etat);
ui.lineEditYmax->setEnabled(etat);
ui.lineEditGradX->setEnabled(etat);
ui.lineEditGradY->setEnabled(etat);

if (ui.pushButtonLire->isEnabled()) //ligne vide
    {
    ui.pushButtonPoint->setEnabled(etat);
    ui.pushButtonSegment->setEnabled(etat);
    ui.pushButtonEfface->setEnabled(etat);
    }
else
    {
    ui.pushButtonPoint->setEnabled(false);
    ui.pushButtonSegment->setEnabled(false);
    ui.pushButtonEfface->setEnabled(false);
    }
ActualiserStatut();
}

//********************************
bool MainWindow::NomInterdit(QString nom)
{
bool retour=false;
if ((ListeNomsInterdits.contains(nom,Qt::CaseInsensitive)) || (nom.isEmpty()) || (ListeNomsVariables.contains(nom,Qt::CaseInsensitive))) retour=true;
else
    {
    QStringList motcle=QString("algobox,F1(,acos(,asin(,atan(,sqrt(,pow(,random(,floor(,cos(,sin(,tan(,exp(,log(,round(,max(,min(,abs(").split(",");
    for (int i = 0; i < motcle.count(); i++)
	{
	if (nom.contains(motcle.at(i),Qt::CaseInsensitive) )
	    {
	    retour=true;
	    break;
	    }
	}
    }
return retour;
}
//*******************************
QString MainWindow::GenererCode(bool exporthtml)
{
QString code=QString::fromUtf8("<!-- Généré par AlgoBox -->\n");
QTextCodec *codec = QTextCodec::codecForName("UTF-8");
QFile modelefile;
if (exporthtml) 
  {
    modelefile.setFileName(":/documents/modeleexportweb.txt");
  }
else 
  {
#if defined(Q_OS_MAC)
modelefile.setFileName(":/documents/modelepagewebmac.txt");
#else
modelefile.setFileName(":/documents/modelepageweb.txt");
#endif
  }
modelefile.open(QIODevice::ReadOnly);
QString filecontent=codec->toUnicode(modelefile.readAll() );
QTextStream t(&filecontent);

while (!t.atEnd()) 
	{
	code+= t.readLine()+"\n";
	}
modelefile.close();

code.replace("#MAX_AFFICHAGES#",QString::number(totalAffichagesAlgo));

QString variablescode="";
for (int i = 0; i < ListeNomsVariables.count(); i++) 
    {
    if (ListeTypesVariables.at(i)=="NOMBRE")
      {
      variablescode+="ALGOBOX_AFFICHE_PASAPAS(\""+ListeNomsVariables.at(i)+":\",false);\n";
      variablescode+="ALGOBOX_AFFICHE_PASAPAS(ALGOBOX_FORMAT_TEXTE("+ListeNomsVariables.at(i)+"),false);\n";
      if (i<ListeNomsVariables.count()-1) variablescode+="ALGOBOX_AFFICHE_PASAPAS(\" | \",false);\n";    
      }
    else if (ListeTypesVariables.at(i)=="CHAINE")
      {
      variablescode+="ALGOBOX_AFFICHE_PASAPAS(\""+ListeNomsVariables.at(i)+":\",false);\n";
      variablescode+="ALGOBOX_AFFICHE_PASAPAS("+ListeNomsVariables.at(i)+",false);\n";
      if (i<ListeNomsVariables.count()-1) variablescode+="ALGOBOX_AFFICHE_PASAPAS(\" | \",false);\n";    
      }
    }
for (int i = 0; i < ListeNomsVariables.count(); i++) 
    {
    if (ListeTypesVariables.at(i)=="LISTE")
      {
      variablescode+="ALGOBOX_AFFICHE_PASAPAS(\" \",true);\n";
      variablescode+="ALGOBOX_AFFICHE_PASAPAS(\"#\"+ALGOBOX_COMPTEUR_ETAPE+\" Liste "+ListeNomsVariables.at(i)+" ("+QString::fromUtf8("ligne")+" \"+ALGOBOX_ID_LIGNE+\") -> \",false);\n";
      variablescode+="for (ALGOBOX_CPT=0;ALGOBOX_CPT<"+ListeNomsVariables.at(i)+".length;ALGOBOX_CPT++)\n";
      variablescode+="{\n";
//      variablescode+="if ("+ListeNomsVariables.at(i)+"[0]==\"Erreur\") \n";
      variablescode+="if (ALGOBOX_FORMAT_TEXTE("+ListeNomsVariables.at(i)+"[ALGOBOX_CPT])!=\"Erreur\") ALGOBOX_AFFICHE_PASAPAS(ALGOBOX_FORMAT_TEXTE("+ListeNomsVariables.at(i)+"[ALGOBOX_CPT]),false);\n";
      variablescode+="else ALGOBOX_AFFICHE_PASAPAS(\"?\",false);\n";
      variablescode+="if (ALGOBOX_CPT<"+ListeNomsVariables.at(i)+".length-1) ALGOBOX_AFFICHE_PASAPAS(\" | \",false);\n";
      variablescode+="}\n";
      }
    }
code.replace("#AFFICHE_VARIABLES#",variablescode);

if (arrondiAutoAlgo) code.replace("#INSTRUCTION_ARRONDI#","else return parseFloat(nb.toPrecision(12));");
else code.replace("#INSTRUCTION_ARRONDI#","else return nb;");

QString initcode="";
for (int i = 0; i < ListeNomsVariables.count(); i++) 
    {
    if (ListeTypesVariables.at(i)=="NOMBRE")
      {
      initcode+=ListeNomsVariables.at(i)+"=0;\n";
      }
    else if (ListeTypesVariables.at(i)=="CHAINE")
      {
      initcode+=ListeNomsVariables.at(i)+"=\"\";\n";
      }
    else if (ListeTypesVariables.at(i)=="LISTE")
      {
      initcode+="for (ALGOBOX_CPT=0;ALGOBOX_CPT<"+ListeNomsVariables.at(i)+".length;ALGOBOX_CPT++)\n";
      initcode+="{\n";
      initcode+=ListeNomsVariables.at(i)+"[ALGOBOX_CPT]=0;\n";
      initcode+="}\n";
      }
    }

code.replace("#INIT_VARIABLES#",initcode);

int nb_branches=ui.treeWidget->topLevelItemCount();
if (nb_branches==0) return code;
QString extensioncode="";
if (!fichier_extension.isEmpty())
  {
  QFileInfo fic(fichier_extension);
  if (fic.exists() && fic.isReadable() )
    {
    QFile jsext(fichier_extension);
    jsext.open( QIODevice::ReadOnly);
    extensioncode=codec->toUnicode(jsext.readAll() );
    jsext.close();
    extensioncode+="\n";
    }
  }
QString jscriptcode="";
indent=0;
idligne=0;
//repereDefini=false;
dansFct=false;
for (int i = 0; i < nb_branches; i++) jscriptcode+=CodeNoeud(ui.treeWidget->topLevelItem(i),exporthtml);
code.replace("#JAVASCRIPT#",extensioncode+jscriptcode);

if (ui.checkBoxRepere->isChecked()) 
{
code.replace("#GRAPHIQUE#",QString("<fieldset>\n<legend class=\"title\">Graphique</legend>\n<div id=\"graphique\" style=\"position:relative;\"></div>\n<p class=\"graphique\" id=\"legende\"></p>\n<br clear=\"all\">\n</fieldset>"));
}
else
{
code.replace("#GRAPHIQUE#","");
}

code.replace("#LINEWIDTH#",QString::number(epaisseurLigneAlgo));
code.replace("#POINTWIDTH#",QString::number(epaisseurPointAlgo));
code.replace("#NB_DECIMALES_AFFICHEES#",QString::number(nbDecimalesAlgo));

QFileInfo fic(nomFichier);
if (nomFichier!="sanstitre") code.replace("#TITRE#","AlgoBox : "+fic.baseName());
else code.replace("#TITRE#","AlgoBox : "+nomFichier);


QString desc=ui.textEditDescription->toPlainText();
if (!desc.isEmpty())
    {
    desc.replace(QString("<"),QString("&lt;"));
    desc.replace(QString(">"),QString("&gt;"));
    desc.replace(QString("\n"),QString("<br>"));
    desc="<p>\n"+desc+"\n</p>\n";
    code.replace("#DESCRIPTION#",desc);
    }
else code.replace("#DESCRIPTION#","");

QString pseudocode="";
indent=0;
idligne=0;
for (int i = 0; i < nb_branches; i++) 
    {
    pseudocode+=AlgoNoeud(ui.treeWidget->topLevelItem(i));
    }
if (ui.checkBoxFonction->isChecked())
	{
	QString expression=ui.lineEditFonction->text();
	if (!expression.isEmpty() && !expression.contains("F1(",Qt::CaseInsensitive)) pseudocode+="<br>\n"+QString::fromUtf8("Fonction numérique utilisée :")+"<br>\nF1(x)="+expression+"\n";
	}
if (ui.checkBoxF2->isChecked())
	{
	if (ui.checkBoxFonction->isChecked()) pseudocode+="<br>\n";
	QString role,condition,commande;
	QStringList tagList;
	QListWidgetItem *item;
	pseudocode+="<br>\nfonction F2("+ui.lineEditParametresF2->text()+"):<br>\n";
	for (int i = 0; i < ui.listWidgetF2->count(); ++i)
	  {
	  tagList.clear();
	  item=ui.listWidgetF2->item(i);
	  role=item->data(Qt::UserRole).toString();
	  tagList= role.split("@");
	  if (tagList.count()==2) 
	    {
	    condition=tagList.at(0);
	    commande=tagList.at(1);
	    if ((!condition.isEmpty()) && (!commande.isEmpty()))
	      {
	      pseudocode+="SI ("+condition+") RENVOYER "+commande+"<br>\n";
	      }
	    }
	  }
	  if (!ui.lineEditDefautF2->text().isEmpty()) pseudocode+=QString::fromUtf8("Dans les autres cas, RENVOYER ")+ui.lineEditDefautF2->text()+"<br>\n";
	  }
if (!fichier_extension.isEmpty())
  {
  QFileInfo fic(fichier_extension);
  if (fic.exists() && fic.isReadable() )
    {
    pseudocode+="<br>\n"+QString::fromUtf8("Extension utilisée : ")+fic.fileName()+"\n";
    }
  }
  
QTextStream nl(&pseudocode,QIODevice::ReadOnly);
//nl.setCodec(codec);
int numligne=1;
while (!nl.atEnd()) 
	{
	nl.readLine();
	numligne++;
	}
QString maxl=QString::number(numligne);
QString texte="";	
QTextStream l(&pseudocode,QIODevice::ReadOnly);
//l.setCodec(codec);
int ligne=1;
QString num="";
QString blanc="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
blanc=blanc.left(6*maxl.length()+6);
while (!l.atEnd()) 
	{
	num=QString::number(ligne);
	num+=blanc.left(blanc.length()-6*num.length());
	texte+=num+l.readLine()+"\n";
	ligne++;
	}

code.replace("#CODE#",texte);
return code;
}

QString MainWindow::CodeVersJavascript(QString algocode, bool exporthtml,int id)
{
QStringList parametres=algocode.split("#");
QString code="//***************";
QString varboucle;
if (parametres.count()>0)
  {
  int type_para=parametres.at(0).toInt();
  switch (type_para)
    {
    case 201: //FONCTION
        dansFct=true;
	    if (parametres.count()==3)
		{
		QString nom=parametres.at(1);
		QString param=parametres.at(2);
		if (param==" ") code="function "+nom+"()\n{\n";
        else code="function "+nom+"("+param+")\n{\n";
		}
	    break;
    case 202: //VARIABLES LOCALES FONCTION
        if (parametres.count()==2)
		{
		code="//VARIABLES_LOCALES";
		}
	    break;
// 	    if (parametres.count()==3)
// 		{
// 		QString type=parametres.at(1);
// 		QString nomvariable=parametres.at(2);
// 		code="var ";
// 		if (type=="NOMBRE") code+=nomvariable+"=new Number();\n";
// 		else if (type=="CHAINE") code+=nomvariable+"=new String();\n";
// 		else if (type=="LISTE") code+=nomvariable+"=new Array();\n";
// 		}
// 	    break;
    case 203: //DEBUT_FONCTION
	    if (parametres.count()==2)
		{
		code="//DEBUT_FONCTION";
		}
	    break;
    case 204: //FIN_FONCTION
	    if (parametres.count()==2)
		{
		code="}\n";
		}
	    break;
    case 205: //RENVOYER_FONCTION
	    if (parametres.count()==2)
		{
		  QString calcul=FiltreCalcul(parametres.at(1));
		  code="return ALGOBOX_ARRONDI("+calcul+");\n";  
		}
	    break;
    case 206: //APPELER_FONCTION
	    if (parametres.count()==2)
		{
         code="ALGOBOX_LIGNE_COURANTE="+QString::number(id)+";\n";
		 code+=parametres.at(1)+";\n";  
		}
	    break;
    case 1: //VARIABLES
	    if (parametres.count()==3)
		{
		QString type=parametres.at(1);
		QString nomvariable=parametres.at(2);
        if (!dansFct)
            {
            code="ALGOBOX_LIGNE_COURANTE="+QString::number(id)+";\n";
            if (type=="NOMBRE") code+=nomvariable+"=new Number();";
            else if (type=="CHAINE") code+=nomvariable+"=new String();";
            else if (type=="LISTE") code+=nomvariable+"=new Array();";
            }
        else
            {
            code="var ";
            if (type=="NOMBRE") code+=nomvariable+"=new Number();";
            else if (type=="CHAINE") code+=nomvariable+"=new String();";
            else if (type=="LISTE") code+=nomvariable+"=new Array();";
            }        
		}
	    break;
    case 2: //LIRE
	    if (parametres.count()==3)
		{
		QString nomvariable=parametres.at(1);
		int i=ListeNomsVariables.indexOf(nomvariable);
		QString rang=parametres.at(2);
		if (i>-1)
		    {
		    QString type=ListeTypesVariables.at(i);
		    code="ALGOBOX_LIGNE_COURANTE="+QString::number(id)+";\nif (ALGOBOX_EMERGENCY_STOP) {throw(\"erreur_emergency\");}\n";
		    if (type=="CHAINE") 
		      {
		      code+=nomvariable+"=prompt(\"Entrer "+nomvariable+" :\");\n"+nomvariable+"="+nomvariable+".replace(new RegExp(\"\\\"\",\"g\"),\"\");\n";
		      if (!exporthtml && !dansFct) code+="if (ALGOBOX_PAS_A_PAS) {if (!ALGOBOX_PAUSE_PAS_A_PAS("+QString::number(id)+")) {throw(\"erreur_pause\");}}";
		      }
		    else if (type=="NOMBRE") 
		      {
		      code+=nomvariable+"_temp=prompt(\"Entrer "+nomvariable+" :\\n"+QString::fromUtf8("(utiliser le . comme séparateur décimal)")+"\\n(exemples de syntaxe possible : -3 ; 2.6 ; 4/3 ; 1+sqrt(3) ; ...)\");\n";
		      code+="if (!"+nomvariable+"_temp) {throw(\"erreur_input\");}\n";
		      code+="if (ALGOBOX_CALCULER_EXPRESSION("+nomvariable+"_temp)==\"Erreur\") {throw(\"erreur_calcul\");}\n";
		      code+=nomvariable+"=ALGOBOX_CALCULER_EXPRESSION("+nomvariable+"_temp);\n";
		      if (!exporthtml && !dansFct) code+="if (ALGOBOX_PAS_A_PAS) {if (!ALGOBOX_PAUSE_PAS_A_PAS("+QString::number(id)+")) {throw(\"erreur_pause\");}}";
		      }
		    else if (type=="LISTE") 
		      {
		      code+=nomvariable+"_temp=prompt(\"Entrer le terme de rang "+rang+" de la liste "+nomvariable+" :\\n"+QString::fromUtf8("(utiliser le . comme séparateur décimal)")+"\\n(exemples de syntaxe possible : -3 ; 2.6 ; 4/3 ; 1+sqrt(3) ; ...)\");\n";
		      code+=nomvariable+"_temp_tab="+nomvariable+"_temp.split(new RegExp(\"[:]+\", \"g\"));\n";
		      code+="for ("+nomvariable+"_temp_tab_compteur=0;"+nomvariable+"_temp_tab_compteur<"+nomvariable+"_temp_tab.length;"+nomvariable+"_temp_tab_compteur++)\n{\n";
		      code+="if (!"+nomvariable+"_temp_tab["+nomvariable+"_temp_tab_compteur]) {throw(\"erreur_input\");}\n";
		      code+="if (ALGOBOX_CALCULER_EXPRESSION("+nomvariable+"_temp_tab["+nomvariable+"_temp_tab_compteur])==\"Erreur\") {throw(\"erreur_calcul\");}\n";
		      code+=nomvariable+"["+rang+"+"+nomvariable+"_temp_tab_compteur]=ALGOBOX_CALCULER_EXPRESSION("+nomvariable+"_temp_tab["+nomvariable+"_temp_tab_compteur]);\n}\n";
		      code+="while ("+nomvariable+"_temp_tab.length>0) "+nomvariable+"_temp_tab.pop();\n";
//		      code+="if (!"+nomvariable+"_temp) {throw(\"erreur_input\");}\n";
//		      code+="if (ALGOBOX_CALCULER_EXPRESSION("+nomvariable+"_temp)==\"Erreur\") {throw(\"erreur_calcul\");}\n";
		      //code+=nomvariable+"["+rang+"]=ALGOBOX_CALCULER_EXPRESSION("+nomvariable+"_temp);\n";
		      if (!exporthtml && !dansFct) code+="if (ALGOBOX_PAS_A_PAS) {if (!ALGOBOX_PAUSE_PAS_A_PAS("+QString::number(id)+")) {throw(\"erreur_pause\");}}";
		      }
		    }
		}
	    break;
    case 3: //AFFICHER
	    if (parametres.count()==4)
		{
		  QString nomvariable=parametres.at(1);
		  QString retourligne=parametres.at(2);
		  QString rang=parametres.at(3);
		  int i=ListeNomsVariables.indexOf(nomvariable);
		  if (i>-1)
		      {
		      QString type=ListeTypesVariables.at(i);
		      code="ALGOBOX_LIGNE_COURANTE="+QString::number(id)+";\nif (ALGOBOX_EMERGENCY_STOP) {throw(\"erreur_emergency\");}\n";
		      if (retourligne=="1") 
			  {
			  if (type=="LISTE") code+="ALGOBOX_AJOUTE_OUTPUT(ALGOBOX_FORMAT_TEXTE("+nomvariable+"["+rang+"]"+"),true);";
			  else if (type=="NOMBRE") code+="ALGOBOX_AJOUTE_OUTPUT(ALGOBOX_FORMAT_TEXTE("+nomvariable+"),true);";
			  else code+="ALGOBOX_AJOUTE_OUTPUT("+nomvariable+",true);";
			  }
		      else 
			  {
			  if (type=="LISTE") code+="ALGOBOX_AJOUTE_OUTPUT(ALGOBOX_FORMAT_TEXTE("+nomvariable+"["+rang+"]"+"),false);";
			  else if (type=="NOMBRE") code+="ALGOBOX_AJOUTE_OUTPUT(ALGOBOX_FORMAT_TEXTE("+nomvariable+"),false);";
			  else code+="ALGOBOX_AJOUTE_OUTPUT("+nomvariable+",false);";
			  }
		      }	  
		}
	    break;
    case 4: //MESSAGE
	    if (parametres.count()==3)
		{
		  QString message=parametres.at(1);
		  QString retourligne=parametres.at(2);
		  code="ALGOBOX_LIGNE_COURANTE="+QString::number(id)+";\nif (ALGOBOX_EMERGENCY_STOP) {throw(\"erreur_emergency\");}\n";
		  if (retourligne=="1") code+="ALGOBOX_AJOUTE_OUTPUT(\""+message+"\",true);";
		  else code+="ALGOBOX_AJOUTE_OUTPUT(\""+message+"\",false);";	  
		}
	    break;
    case 5: //AFFECTATION
	    if (parametres.count()==4)
		{
		QString nomvariable=parametres.at(1);
		QString contenu=FiltreCalcul(parametres.at(2));
		QString rang=parametres.at(3);
		int i=ListeNomsVariables.indexOf(nomvariable);
		if (i>-1)
		    {
		    QString type=ListeTypesVariables.at(i);
		    code="ALGOBOX_LIGNE_COURANTE="+QString::number(id)+";\nif (ALGOBOX_EMERGENCY_STOP) {throw(\"erreur_emergency\");}\n";
		    if (type=="LISTE") 
		      {
		      //code+=nomvariable+"["+rang+"]=ALGOBOX_ARRONDI("+contenu+");\n";
		     // if (!exporthtml && !dansFct) code+="if (ALGOBOX_PAS_A_PAS) {if (!ALGOBOX_PAUSE_PAS_A_PAS("+QString::number(id)+")) {throw(\"erreur_pause\");}}\n";
		      code+=nomvariable+"_temp_tab=\""+contenu+"\".split(new RegExp(\"[:]+\", \"g\"));\n";
		      code+="for ("+nomvariable+"_temp_tab_compteur=0;"+nomvariable+"_temp_tab_compteur<"+nomvariable+"_temp_tab.length;"+nomvariable+"_temp_tab_compteur++)\n{\n";
		      //code+="if (ALGOBOX_ARRONDI(eval("+nomvariable+"_temp_tab["+nomvariable+"_temp_tab_compteur]))==\"Erreur\") {throw(\"erreur_calcul\");}\n";
		      code+=nomvariable+"["+rang+"+"+nomvariable+"_temp_tab_compteur]=ALGOBOX_ARRONDI(eval("+nomvariable+"_temp_tab["+nomvariable+"_temp_tab_compteur]));\n}\n";
		      code+="while ("+nomvariable+"_temp_tab.length>0) "+nomvariable+"_temp_tab.pop();\n";
		      //code+="if (ALGOBOX_ARRONDI("+contenu+")==\"Erreur\") {throw(\"erreur_calcul\");}\n";
		      //code+=nomvariable+"["+rang+"]=ALGOBOX_ARRONDI("+contenu+");\n";
		      if (!exporthtml && !dansFct) code+="if (ALGOBOX_PAS_A_PAS) {if (!ALGOBOX_PAUSE_PAS_A_PAS("+QString::number(id)+")) {throw(\"erreur_pause\");}}\n";
		      }
		    else if (type=="NOMBRE") 
		      {
		      //code+=nomvariable+"=ALGOBOX_ARRONDI("+contenu+");\n";
		      //if (!exporthtml && !dansFct) code+="if (ALGOBOX_PAS_A_PAS) {if (!ALGOBOX_PAUSE_PAS_A_PAS("+QString::number(id)+")) {throw(\"erreur_pause\");}}\n";
		      //code+="if (ALGOBOX_ARRONDI("+contenu+")==\"Erreur\") {throw(\"erreur_calcul\");}\n";
		      code+=nomvariable+"=ALGOBOX_ARRONDI("+contenu+");\n";
		      if (!exporthtml && !dansFct) code+="if (ALGOBOX_PAS_A_PAS) {if (!ALGOBOX_PAUSE_PAS_A_PAS("+QString::number(id)+")) {throw(\"erreur_pause\");}}\n";
		      }
		    else 
		      {
		      code+=nomvariable+"="+contenu+";\n";
		      if (!exporthtml && !dansFct) code+="if (ALGOBOX_PAS_A_PAS) {if (!ALGOBOX_PAUSE_PAS_A_PAS("+QString::number(id)+")) {throw(\"erreur_pause\");}}";
		      }
		    }
		}
	    break;
    case 6: //SI
	    if (parametres.count()==2)
		{
		QString condition=FiltreCondition(parametres.at(1));
		condition=FiltreCalcul(condition);
		code="ALGOBOX_LIGNE_COURANTE="+QString::number(id)+";\nif (ALGOBOX_EMERGENCY_STOP) {throw(\"erreur_emergency\");}\n";
/*		if ((!repereDefini) && (ui.checkBoxRepere->isChecked()))
		  {
		  code+="\ndocument.getElementById(\"contentgraphic\").style.visibility=\"visible\";\n";
		  code+="if (ALGOBOX_ARRONDI("+FiltreCalcul(ui.lineEditXmin->text())+")==\"Erreur\") {throw(\"erreur_repere\");}\n";
		  code+="if (ALGOBOX_ARRONDI("+FiltreCalcul(ui.lineEditXmax->text())+")==\"Erreur\") {throw(\"erreur_repere\");}\n";
		  code+="if (ALGOBOX_ARRONDI("+FiltreCalcul(ui.lineEditYmin->text())+")==\"Erreur\") {throw(\"erreur_repere\");}\n";
		  code+="if (ALGOBOX_ARRONDI("+FiltreCalcul(ui.lineEditYmax->text())+")==\"Erreur\") {throw(\"erreur_repere\");}\n";
		  code+="if (ALGOBOX_ARRONDI("+FiltreCalcul(ui.lineEditGradX->text())+")==\"Erreur\") {throw(\"erreur_repere\");}\n";
		  code+="if (ALGOBOX_ARRONDI("+FiltreCalcul(ui.lineEditGradY->text())+")==\"Erreur\") {throw(\"erreur_repere\");}\n";
		  code+="ALGOBOX_REPERE("+FiltreCalcul(ui.lineEditXmin->text())+","+FiltreCalcul(ui.lineEditXmax->text())+","+FiltreCalcul(ui.lineEditYmin->text())+","+FiltreCalcul(ui.lineEditYmax->text())+","+FiltreCalcul(ui.lineEditGradX->text())+","+FiltreCalcul(ui.lineEditGradY->text())+");\n";
		  repereDefini=true;
		  }*/
		if (!exporthtml && !dansFct)
		  {
		  code+="\nif (ALGOBOX_PAS_A_PAS)\n";
		  code+="{\n";
		  code+="if ("+condition+") {if (!ALGOBOX_INFO_PAS_A_PAS("+QString::number(id)+",\""+QString::fromUtf8("La condition est vérifiée")+"\")) {throw(\"erreur_pause\");}}";
		  code+="else {if (!ALGOBOX_INFO_PAS_A_PAS("+QString::number(id)+",\""+QString::fromUtf8("La condition n'est pas vérifiée")+"\")) {throw(\"erreur_pause\");}}";
		  code+="}\n";
		  }
		code+="if ("+condition+")";
		}
	    break;
    case 7: //DEBUT_SI
	    if (parametres.count()==2)
		{
		code="{\n";
		code+="ALGOBOX_LIGNE_COURANTE="+QString::number(id)+";\nif (ALGOBOX_EMERGENCY_STOP) {throw(\"erreur_emergency\");}\n";
		if (!exporthtml && !dansFct) code+="\nif (ALGOBOX_PAS_A_PAS) {if (!ALGOBOX_INFO_PAS_A_PAS("+QString::number(id)+",\""+QString::fromUtf8("Entrée dans le bloc DEBUT_SI/FIN_SI")+"\")) {throw(\"erreur_pause\");}}";
		}
	    break;
    case 8: //FIN_SI
	    if (parametres.count()==2)
		{
		code="ALGOBOX_LIGNE_COURANTE="+QString::number(id)+";\nif (ALGOBOX_EMERGENCY_STOP) {throw(\"erreur_emergency\");}\n";
		if (!exporthtml && !dansFct) code+="\nif (ALGOBOX_PAS_A_PAS) {if (!ALGOBOX_INFO_PAS_A_PAS("+QString::number(id)+",\""+QString::fromUtf8("Sortie du bloc DEBUT_SI/FIN_SI")+"\")) {throw(\"erreur_pause\");}}";
		code+="}";
		}
	    break;
    case 9: //SINON
	    if (parametres.count()==2)
		{
		code="else";
		}
	    break;
    case 10: //DEBUT_SINON
	    if (parametres.count()==2)
		{
		code="{\n";
		code+="ALGOBOX_LIGNE_COURANTE="+QString::number(id)+";\nif (ALGOBOX_EMERGENCY_STOP) {throw(\"erreur_emergency\");}\n";
	    	if (!exporthtml && !dansFct) code+="\nif (ALGOBOX_PAS_A_PAS) {if (!ALGOBOX_INFO_PAS_A_PAS("+QString::number(id)+",\""+QString::fromUtf8("Entrée dans le bloc DEBUT_SINON/FIN_SINON")+"\")) {throw(\"erreur_pause\");}}";
		}
	    break;
    case 11: //FIN_SINON
	    if (parametres.count()==2)
		{
		code="ALGOBOX_LIGNE_COURANTE="+QString::number(id)+";\nif (ALGOBOX_EMERGENCY_STOP) {throw(\"erreur_emergency\");}\n";
	    	if (!exporthtml && !dansFct) code+="\nif (ALGOBOX_PAS_A_PAS) {if (!ALGOBOX_INFO_PAS_A_PAS("+QString::number(id)+",\""+QString::fromUtf8("Sortie du bloc DEBUT_SINON/FIN_SINON")+"\")) {throw(\"erreur_pause\");}}";
		code+="}";
		}
	    break;
    case 12: //POUR
	    if (parametres.count()==4)
		{
		QString nomvariable=parametres.at(1);
		QString debut=FiltreCalcul(parametres.at(2));
		QString fin=FiltreCalcul(parametres.at(3));
		varboucle="ALGOBOX_SECURITE_BOUCLE"+QString::number(indent);
		code=varboucle+"=0;\n";
		code+="ALGOBOX_LIGNE_COURANTE="+QString::number(id)+";\nif (ALGOBOX_EMERGENCY_STOP) {throw(\"erreur_emergency\");}\n";
/*		if ((!repereDefini) && (ui.checkBoxRepere->isChecked()))
		  {
		  code+="\ndocument.getElementById(\"contentgraphic\").style.visibility=\"visible\";\n";
		  code+="if (ALGOBOX_ARRONDI("+FiltreCalcul(ui.lineEditXmin->text())+")==\"Erreur\") {throw(\"erreur_repere\");}\n";
		  code+="if (ALGOBOX_ARRONDI("+FiltreCalcul(ui.lineEditXmax->text())+")==\"Erreur\") {throw(\"erreur_repere\");}\n";
		  code+="if (ALGOBOX_ARRONDI("+FiltreCalcul(ui.lineEditYmin->text())+")==\"Erreur\") {throw(\"erreur_repere\");}\n";
		  code+="if (ALGOBOX_ARRONDI("+FiltreCalcul(ui.lineEditYmax->text())+")==\"Erreur\") {throw(\"erreur_repere\");}\n";
		  code+="if (ALGOBOX_ARRONDI("+FiltreCalcul(ui.lineEditGradX->text())+")==\"Erreur\") {throw(\"erreur_repere\");}\n";
		  code+="if (ALGOBOX_ARRONDI("+FiltreCalcul(ui.lineEditGradY->text())+")==\"Erreur\") {throw(\"erreur_repere\");}\n";
		  code+="ALGOBOX_REPERE("+FiltreCalcul(ui.lineEditXmin->text())+","+FiltreCalcul(ui.lineEditXmax->text())+","+FiltreCalcul(ui.lineEditYmin->text())+","+FiltreCalcul(ui.lineEditYmax->text())+","+FiltreCalcul(ui.lineEditGradX->text())+","+FiltreCalcul(ui.lineEditGradY->text())+");\n";
		  repereDefini=true;
		  }*/
		code+="for ("+nomvariable+"="+debut+";"+nomvariable+"<="+fin+";"+nomvariable+"++)";
		}
	    break;
    case 13: //DEBUT_POUR
	    if (parametres.count()==2)
		{
		varboucle="ALGOBOX_SECURITE_BOUCLE"+QString::number(indent-1);
		code="{\n"+varboucle+"++;\n";
		code+="ALGOBOX_COMPTEUR_BOUCLE_GLOBAL++;\n";
	        code+="ALGOBOX_LIGNE_COURANTE="+QString::number(id)+";\nif (ALGOBOX_EMERGENCY_STOP) {throw(\"erreur_emergency\");}\n";
	    	if (!exporthtml && !dansFct) code+="if (ALGOBOX_PAS_A_PAS) {ALGOBOX_AFFICHE_PASAPAS(\""+QString::fromUtf8("Entrée dans le bloc DEBUT_POUR/FIN_POUR")+" (ligne "+QString::number(id)+")\",true); if (!ALGOBOX_PAUSE_PAS_A_PAS("+QString::number(id)+")) {throw(\"erreur_pause\");}}";
		}
	    break;
    case 14: //FIN_POUR
	    if (parametres.count()==2)
		{
		varboucle="ALGOBOX_SECURITE_BOUCLE"+QString::number(indent-1);
	        code="ALGOBOX_LIGNE_COURANTE="+QString::number(id)+";\nif (ALGOBOX_EMERGENCY_STOP) {throw(\"erreur_emergency\");}\n";
		if (!exporthtml && !dansFct) code+="\nif (ALGOBOX_PAS_A_PAS) {if (!ALGOBOX_INFO_PAS_A_PAS("+QString::number(id)+",\""+QString::fromUtf8("Sortie du bloc DEBUT_POUR/FIN_POUR")+"\")) {throw(\"erreur_pause\");}}";
		code+="if (ALGOBOX_DEPASSEMENT_AFFICHAGE) {throw(\"erreur_affiche\"); break;}\n";
		code+="if ("+varboucle+">"+QString::number(maxBoucleAlgo)+") {throw(\"erreur_boucle\"); break;}\n";
	    	if (!exporthtml && !dansFct) code+="if (ALGOBOX_COMPTEUR_BOUCLE_GLOBAL%10000==0) ALGOBOX_PING_OUTPUT();\n";
	    	code+="if (ALGOBOX_COMPTEUR_BOUCLE_GLOBAL>"+QString::number(totalBouclesAlgo)+") {throw(\"erreur_boucle\"); break;}\n}";
		}
	    break;
    case 15: //TANT_QUE
	    if (parametres.count()==2)
		{
		QString condition=FiltreCondition(parametres.at(1));
		condition=FiltreCalcul(condition);
		varboucle="ALGOBOX_SECURITE_BOUCLE"+QString::number(indent);
		code=varboucle+"=0;\n";
		code+="ALGOBOX_LIGNE_COURANTE="+QString::number(id)+";\nif (ALGOBOX_EMERGENCY_STOP) {throw(\"erreur_emergency\");}\n";
// 		if (!exporthtml && !dansFct)
// 		  {
// 		  code+="\nif (ALGOBOX_PAS_A_PAS)\n";
// 		  code+="{\n";
// 		  code+="if ("+condition+") {if (!ALGOBOX_INFO_PAS_A_PAS("+QString::number(id)+",\""+QString::fromUtf8("La condition est vérifiée")+"\")) {throw(\"erreur_pause\");}}";
// 		  code+="else {if (!ALGOBOX_INFO_PAS_A_PAS("+QString::number(id)+",\""+QString::fromUtf8("La condition n'est pas vérifiée")+"\")) {throw(\"erreur_pause\");}}";
// 		  code+="}\n";
// 		  }
/*		if ((!repereDefini) && (ui.checkBoxRepere->isChecked()))
		  {
		  code+="\ndocument.getElementById(\"contentgraphic\").style.visibility=\"visible\";\n";
		  code+="if (ALGOBOX_ARRONDI("+FiltreCalcul(ui.lineEditXmin->text())+")==\"Erreur\") {throw(\"erreur_repere\");}\n";
		  code+="if (ALGOBOX_ARRONDI("+FiltreCalcul(ui.lineEditXmax->text())+")==\"Erreur\") {throw(\"erreur_repere\");}\n";
		  code+="if (ALGOBOX_ARRONDI("+FiltreCalcul(ui.lineEditYmin->text())+")==\"Erreur\") {throw(\"erreur_repere\");}\n";
		  code+="if (ALGOBOX_ARRONDI("+FiltreCalcul(ui.lineEditYmax->text())+")==\"Erreur\") {throw(\"erreur_repere\");}\n";
		  code+="if (ALGOBOX_ARRONDI("+FiltreCalcul(ui.lineEditGradX->text())+")==\"Erreur\") {throw(\"erreur_repere\");}\n";
		  code+="if (ALGOBOX_ARRONDI("+FiltreCalcul(ui.lineEditGradY->text())+")==\"Erreur\") {throw(\"erreur_repere\");}\n";
		  code+="ALGOBOX_REPERE("+FiltreCalcul(ui.lineEditXmin->text())+","+FiltreCalcul(ui.lineEditXmax->text())+","+FiltreCalcul(ui.lineEditYmin->text())+","+FiltreCalcul(ui.lineEditYmax->text())+","+FiltreCalcul(ui.lineEditGradX->text())+","+FiltreCalcul(ui.lineEditGradY->text())+");\n";
		  repereDefini=true;
		  }*/
		code+="while ("+condition+")";
		}
	    break;
    case 16: //DEBUT_TANT_QUE
	    if (parametres.count()==2)
		{
		varboucle="ALGOBOX_SECURITE_BOUCLE"+QString::number(indent-1);
		code="{\n"+varboucle+"++;\n";
	    	code+="ALGOBOX_COMPTEUR_BOUCLE_GLOBAL++;";
	        code+="ALGOBOX_LIGNE_COURANTE="+QString::number(id)+";\nif (ALGOBOX_EMERGENCY_STOP) {throw(\"erreur_emergency\");}\n";
		if (!exporthtml && !dansFct) code+="\nif (ALGOBOX_PAS_A_PAS) {if (!ALGOBOX_INFO_PAS_A_PAS("+QString::number(id)+",\""+QString::fromUtf8("Entrée dans le bloc DEBUT_TANT_QUE/FIN_TANT_QUE : condition vérifiée")+"\")) {throw(\"erreur_pause\");}}";
		}
	    break;
    case 17: //FIN_TANT_QUE
	    if (parametres.count()==2)
		{
		varboucle="ALGOBOX_SECURITE_BOUCLE"+QString::number(indent-1);
		code="ALGOBOX_LIGNE_COURANTE="+QString::number(id)+";\nif (ALGOBOX_EMERGENCY_STOP) {throw(\"erreur_emergency\");}\n";
		if (!exporthtml && !dansFct) code+="\nif (ALGOBOX_PAS_A_PAS) {if (!ALGOBOX_INFO_PAS_A_PAS("+QString::number(id)+",\""+QString::fromUtf8("Sortie du bloc DEBUT_TANT_QUE/FIN_TANT_QUE")+"\")) {throw(\"erreur_pause\");}}";
	    	code+="if (ALGOBOX_DEPASSEMENT_AFFICHAGE) {throw(\"erreur_affiche\"); break;}\n";
		code+="if ("+varboucle+">"+QString::number(maxBoucle)+") {throw(\"erreur_boucle\"); break;}\n";
	    	if (!exporthtml && !dansFct) code+="if (ALGOBOX_COMPTEUR_BOUCLE_GLOBAL%10000==0) ALGOBOX_PING_OUTPUT();\n";
	    	code+="if (ALGOBOX_COMPTEUR_BOUCLE_GLOBAL>"+QString::number(totalBouclesAlgo)+") {throw(\"erreur_boucle\"); break;}\n}";
		}
	    break;
    case 18: //PAUSE
	    if (parametres.count()==2)
		{
		code="ALGOBOX_LIGNE_COURANTE="+QString::number(id)+";\nif (ALGOBOX_EMERGENCY_STOP) {throw(\"erreur_emergency\");}\n";
	    	code+="if (!ALGOBOX_PAUSE()) {throw(\"erreur_pause\");}\n";
		}
	    break;
    case 19: //COMMENTAIRE
	    if (parametres.count()==2)
		{
		code="ALGOBOX_LIGNE_COURANTE="+QString::number(id)+";\nif (ALGOBOX_EMERGENCY_STOP) {throw(\"erreur_emergency\");}\n";
		code+="//***************";
		}
	    break;
    case 20: //AFFICHERCALCUL
	    if (parametres.count()==3)
		{
		  QString calcul=FiltreCalcul(parametres.at(1));
		  QString retourligne=parametres.at(2);
		  code="ALGOBOX_LIGNE_COURANTE="+QString::number(id)+";\nif (ALGOBOX_EMERGENCY_STOP) {throw(\"erreur_emergency\");}\n";
		  code+="if (ALGOBOX_ARRONDI("+calcul+")==\"Erreur\") {throw(\"erreur_calcul\");}\n";
		  code+="algobox_temp_affichcalcul=ALGOBOX_ARRONDI("+calcul+");\n";
		  if (!exporthtml && !dansFct) code+="if (ALGOBOX_PAS_A_PAS) {if (!ALGOBOX_PAUSE_PAS_A_PAS("+QString::number(id)+")) {throw(\"erreur_pause\");}}\n";
		  if (retourligne=="1") code+="ALGOBOX_AJOUTE_OUTPUT(ALGOBOX_FORMAT_TEXTE(algobox_temp_affichcalcul),true);";
		  else code+="ALGOBOX_AJOUTE_OUTPUT(ALGOBOX_FORMAT_TEXTE(algobox_temp_affichcalcul),false);";	  
		}
	    break;
    case 50: //POINT
	    if ((parametres.count()==4) && (ui.checkBoxRepere->isChecked()))
		{
		code="ALGOBOX_LIGNE_COURANTE="+QString::number(id)+";\nif (ALGOBOX_EMERGENCY_STOP) {throw(\"erreur_emergency\");}\n";
		//if (!repereDefini)
		  //{
		  code+="\nif (!ALGOBOX_REPERE_DEFINI) {"; 
		  code+="\ndocument.getElementById(\"contentgraphic\").style.visibility=\"visible\";\n";
		  code+="if (ALGOBOX_ARRONDI("+FiltreCalcul(ui.lineEditXmin->text())+")==\"Erreur\") {throw(\"erreur_repere\");}\n";
		  code+="if (ALGOBOX_ARRONDI("+FiltreCalcul(ui.lineEditXmax->text())+")==\"Erreur\") {throw(\"erreur_repere\");}\n";
		  code+="if (ALGOBOX_ARRONDI("+FiltreCalcul(ui.lineEditYmin->text())+")==\"Erreur\") {throw(\"erreur_repere\");}\n";
		  code+="if (ALGOBOX_ARRONDI("+FiltreCalcul(ui.lineEditYmax->text())+")==\"Erreur\") {throw(\"erreur_repere\");}\n";
		  code+="if (ALGOBOX_ARRONDI("+FiltreCalcul(ui.lineEditGradX->text())+")==\"Erreur\") {throw(\"erreur_repere\");}\n";
		  code+="if (ALGOBOX_ARRONDI("+FiltreCalcul(ui.lineEditGradY->text())+")==\"Erreur\") {throw(\"erreur_repere\");}\n";
		  code+="ALGOBOX_REPERE("+FiltreCalcul(ui.lineEditXmin->text())+","+FiltreCalcul(ui.lineEditXmax->text())+","+FiltreCalcul(ui.lineEditYmin->text())+","+FiltreCalcul(ui.lineEditYmax->text())+","+FiltreCalcul(ui.lineEditGradX->text())+","+FiltreCalcul(ui.lineEditGradY->text())+");\n";
		  code+="}\n";
		  //repereDefini=true;
		  //}
		QString x=FiltreCalcul(parametres.at(1));
		QString y=FiltreCalcul(parametres.at(2));
		QString col="2";
		if (parametres.at(3)=="Rouge") col="3";
		else if (parametres.at(3)=="Vert") col="4";
		else if (parametres.at(3)=="Blanc") col="9";
		code+="if (ALGOBOX_ARRONDI("+x+")==\"Erreur\") {throw(\"erreur_calcul\");}\n";
		code+="if (ALGOBOX_ARRONDI("+y+")==\"Erreur\") {throw(\"erreur_calcul\");}\n";
		if (!exporthtml && !dansFct) 
		  {
		  code+="\nif (ALGOBOX_PAS_A_PAS) {if (!ALGOBOX_INFO_PAS_A_PAS("+QString::number(id)+",\""+QString::fromUtf8("Tracé du point (")+"\"";
		  code+="+ALGOBOX_ARRONDI("+x+")+\",\"";
		  code+="+ALGOBOX_ARRONDI("+y+")+\")\"))"; 
		  code+="{throw(\"erreur_pause\");}}\n";
		  }
		code+="ALGOBOX_POINT("+x+","+y+","+col+");";
		}
	    break;
    case 51: //SEGMENT
	    if ((parametres.count()==6) && (ui.checkBoxRepere->isChecked()))
		{
		code="ALGOBOX_LIGNE_COURANTE="+QString::number(id)+";\nif (ALGOBOX_EMERGENCY_STOP) {throw(\"erreur_emergency\");}\n";
		//if (!repereDefini)
		  //{
		  code+="\nif (!ALGOBOX_REPERE_DEFINI) {"; 
		  code+="\ndocument.getElementById(\"contentgraphic\").style.visibility=\"visible\";\n";
		  code+="if (ALGOBOX_ARRONDI("+FiltreCalcul(ui.lineEditXmin->text())+")==\"Erreur\") {throw(\"erreur_repere\");}\n";
		  code+="if (ALGOBOX_ARRONDI("+FiltreCalcul(ui.lineEditXmax->text())+")==\"Erreur\") {throw(\"erreur_repere\");}\n";
		  code+="if (ALGOBOX_ARRONDI("+FiltreCalcul(ui.lineEditYmin->text())+")==\"Erreur\") {throw(\"erreur_repere\");}\n";
		  code+="if (ALGOBOX_ARRONDI("+FiltreCalcul(ui.lineEditYmax->text())+")==\"Erreur\") {throw(\"erreur_repere\");}\n";
		  code+="if (ALGOBOX_ARRONDI("+FiltreCalcul(ui.lineEditGradX->text())+")==\"Erreur\") {throw(\"erreur_repere\");}\n";
		  code+="if (ALGOBOX_ARRONDI("+FiltreCalcul(ui.lineEditGradY->text())+")==\"Erreur\") {throw(\"erreur_repere\");}\n";
		  code+="ALGOBOX_REPERE("+FiltreCalcul(ui.lineEditXmin->text())+","+FiltreCalcul(ui.lineEditXmax->text())+","+FiltreCalcul(ui.lineEditYmin->text())+","+FiltreCalcul(ui.lineEditYmax->text())+","+FiltreCalcul(ui.lineEditGradX->text())+","+FiltreCalcul(ui.lineEditGradY->text())+");\n";
		  code+="}\n";
		  //repereDefini=true;
		  //}
		QString xdep=FiltreCalcul(parametres.at(1));
		QString ydep=FiltreCalcul(parametres.at(2));
		QString xfin=FiltreCalcul(parametres.at(3));
		QString yfin=FiltreCalcul(parametres.at(4));
		QString coul="2";
		if (parametres.at(5)=="Rouge") coul="3";
		else if (parametres.at(5)=="Vert") coul="4";
		else if (parametres.at(5)=="Blanc") coul="9";
		code+="if (ALGOBOX_ARRONDI("+xdep+")==\"Erreur\") {throw(\"erreur_calcul\");}\n";
		code+="if (ALGOBOX_ARRONDI("+ydep+")==\"Erreur\") {throw(\"erreur_calcul\");}\n";
		code+="if (ALGOBOX_ARRONDI("+xfin+")==\"Erreur\") {throw(\"erreur_calcul\");}\n";
		code+="if (ALGOBOX_ARRONDI("+yfin+")==\"Erreur\") {throw(\"erreur_calcul\");}\n";
		if (!exporthtml && !dansFct) 
		  {
		  code+="\nif (ALGOBOX_PAS_A_PAS) {if (!ALGOBOX_INFO_PAS_A_PAS("+QString::number(id)+",\""+QString::fromUtf8("Tracé du segment (")+"\"";
		  code+="+ALGOBOX_ARRONDI("+xdep+")+\",\"";
		  code+="+ALGOBOX_ARRONDI("+ydep+")+\",\"";
		  code+="+ALGOBOX_ARRONDI("+xfin+")+\",\"";
		  code+="+ALGOBOX_ARRONDI("+yfin+")+\")\"))"; 
		  code+="{throw(\"erreur_pause\");}}\n";
		  }
		code+="ALGOBOX_TRAIT("+xdep+","+ydep+","+xfin+","+yfin+","+coul+");";
		}
	    break;
    case 52: //EFFACE
	    if (parametres.count()==2)
		{
		code="ALGOBOX_LIGNE_COURANTE="+QString::number(id)+";\n";
	    	code+="ALGOBOX_EFFACE_GRAPHIQUE();\n";
		}
	    break;
    case 100: //DECLARATIONS VARIABLES
	    if (parametres.count()==2)
		{
        dansFct=false;
		code="ALGOBOX_LIGNE_COURANTE="+QString::number(id)+";\n";
		code+="//VARIABLES";
		}
	    break;
    case 101: //DEBUT_ALGO
	    if (parametres.count()==2)
		{
		code="function F1(x)\n{\n";
		if (ui.checkBoxFonction->isChecked())
			{
			QString expression=ui.lineEditFonction->text();
			if (!expression.isEmpty() && !expression.contains("F1(",Qt::CaseInsensitive)) code+="return "+FiltreCalcul(expression)+";\n";
			else code+="return x;\n";
			}
		else code+="return x;\n";
		code+="}\n";
		if (ui.checkBoxF2->isChecked())
			{
			QString role,condition,commande;
			QStringList tagList;
			QListWidgetItem *item;
			code+="function F2("+ui.lineEditParametresF2->text()+")\n{\n";
			code+="ALGOBOX_COMPTEUR_RECURSION++;\n";
			code+="if (ALGOBOX_COMPTEUR_RECURSION>100000) return \"Erreur\";\n";
			for (int i = 0; i < ui.listWidgetF2->count(); ++i)
			  {
			  tagList.clear();
			  item=ui.listWidgetF2->item(i);
			  role=item->data(Qt::UserRole).toString();
			  tagList= role.split("@");
			  if (tagList.count()==2) 
			    {
			    condition=tagList.at(0);
			    commande=tagList.at(1);
			    if ((!condition.isEmpty()) && (!commande.isEmpty()))
			      {
			      condition=FiltreCondition(condition);
			      condition=FiltreCalcul(condition);
			      commande=FiltreCalcul(commande);
			      code+="if ("+condition+") return "+commande+";\n";
			      }
			    }
			  }
			  if (ui.lineEditDefautF2->text().isEmpty()) code+="return 1;\n";
			  else code+="return "+FiltreCalcul(ui.lineEditDefautF2->text())+";\n";
			  code+="}\n";
			  }
		code+="function ALGOBOX_ALGO(ALGOBOX_PAS_A_PAS)\n{\n";
		code+="ALGOBOX_DESACTIVER_LIGNE(ALGOBOX_LIGNE_COURANTE);\n";
		code+="ALGOBOX_LIGNE_COURANTE="+QString::number(id)+";\n";
		code+="ALGOBOX_COMPTEUR_BOUCLE_GLOBAL=0\n";
		code+="ALGOBOX_COMPTEUR_RECURSION=0\n";
		code+="ALGOBOX_EFFACE_OUTPUT();\n";
		code+="ALGOBOX_INIT_VARIABLES();\n";
		code+="ALGOBOX_REPERE_DEFINI=false;\n";
		if (ui.checkBoxRepere->isChecked())
		    {
		    code+="output_graph=document.getElementById(\"graphique\");\n";
		    code+="if (output_graph.firstChild) output_graph.removeChild(output_graph.firstChild);\n";
		    code+="output_graph.appendChild(ALGOBOX_canevas_repere);\n";
		    code+="output_graph.appendChild(ALGOBOX_canevas);\n";
		    code+="if (navigator.appName!=\"Microsoft Internet Explorer\")\n";
		    code+="{\n";
		    code+="ALGOBOX_contexte_repere.clearRect(0, 0, ALGOBOX_canevas_repere.width,ALGOBOX_canevas_repere.height);\n";
		    code+="ALGOBOX_contexte.clearRect(0, 0, ALGOBOX_canevas.width,ALGOBOX_canevas.height);\n";
		    code+="\n}\n";
		    }
		if (!exporthtml && !dansFct) code+="browserDialog.scriptLaunched();\n";
		code+="if (ALGOBOX_PAS_A_PAS) ALGOBOX_AJOUTE_OUTPUT(\""+QString::fromUtf8("***Algorithme lancé en mode pas à pas***")+"\",true);\nelse ALGOBOX_AJOUTE_OUTPUT(\""+QString::fromUtf8("***Algorithme lancé***")+"\",true);\n";    
		code+="try\n{";
		}
	    break;
    case 102: //FIN_ALGO
	    if (parametres.count()==2)
		{
		  code="ALGOBOX_AJOUTE_OUTPUT(\" \",true);\nALGOBOX_AJOUTE_OUTPUT(\""+QString::fromUtf8("***Algorithme terminé***")+"\",false);\n";
		  if (!exporthtml && !dansFct) code+="browserDialog.scriptFinished();\n";
		  code+="}\ncatch(ALGOBOX_MESSAGE)\n{\n";
		  //erreur_input
		  code+="if (ALGOBOX_MESSAGE==\"erreur_input\")\n{\n";
		  code+="ALGOBOX_AJOUTE_OUTPUT(\" \",true);\n";
		  code+="ALGOBOX_AJOUTE_OUTPUT(\""+QString::fromUtf8("***Algorithme interrompu ligne ")+"\"+ALGOBOX_LIGNE_COURANTE+\""+QString::fromUtf8(" : erreur dans la lecture d'une variable ***")+"\",false);\n";
		  code+="}\n";
		  //erreur_calcul
		  code+="else if (ALGOBOX_MESSAGE==\"erreur_calcul\")\n{\n";
		  code+="ALGOBOX_AJOUTE_OUTPUT(\" \",true);\n";
		  code+="ALGOBOX_AJOUTE_OUTPUT(\""+QString::fromUtf8("***Algorithme interrompu ligne ")+"\"+ALGOBOX_LIGNE_COURANTE+\""+QString::fromUtf8(" : erreur de calcul***")+"\",false);\n";
		  code+="}\n";
		  //erreur_boucle
		  code+="else if (ALGOBOX_MESSAGE==\"erreur_boucle\")\n{\n";
		  code+="ALGOBOX_AJOUTE_OUTPUT(\" \",true);\n";
		  code+="ALGOBOX_AJOUTE_OUTPUT(\""+QString::fromUtf8("***Algorithme interrompu ligne ")+"\"+ALGOBOX_LIGNE_COURANTE+\""+QString::fromUtf8(" : dépassement de la capacité autorisée pour les boucles***")+"\",false);\n";
		  code+="}\n";
		  //erreur_affiche
		  code+="else if (ALGOBOX_MESSAGE==\"erreur_affiche\")\n{\n";
		  code+="ALGOBOX_AJOUTE_OUTPUT(\" \",true);\n";
		  code+="ALGOBOX_AJOUTE_OUTPUT(\""+QString::fromUtf8("***Algorithme interrompu ligne ")+"\"+ALGOBOX_LIGNE_COURANTE+\""+QString::fromUtf8(" : affichage trop important de données***")+"\",false);\n";
		  code+="}\n";
		  //erreur_repere
		  code+="else if (ALGOBOX_MESSAGE==\"erreur_repere\")\n{\n";
		  code+="ALGOBOX_AJOUTE_OUTPUT(\" \",true);\n";
		  code+="ALGOBOX_AJOUTE_OUTPUT(\""+QString::fromUtf8("***Algorithme interrompu : erreur dans la définition du repère graphique***")+"\",false);\n";
		  code+="}\n";
		  //erreur_pause
		  code+="else if (ALGOBOX_MESSAGE==\"erreur_pause\")\n{\n";
		  code+="ALGOBOX_AJOUTE_OUTPUT(\" \",true);\n";
		  code+="ALGOBOX_AJOUTE_OUTPUT(\""+QString::fromUtf8("***Algorithme interrompu ligne ")+"\"+ALGOBOX_LIGNE_COURANTE+\""+QString::fromUtf8("***")+"\",false);\n";
		  code+="}\n";
		  //erreur_emergency_stop
		  code+="else if (ALGOBOX_MESSAGE==\"erreur_emergency\")\n{\n";
		  code+="ALGOBOX_AJOUTE_OUTPUT(\" \",true);\n";
		  code+="ALGOBOX_AJOUTE_OUTPUT(\""+QString::fromUtf8("***Algorithme interrompu sur demande***")+"\",false);\n";
		  code+="}\n";
		  //erreur inconnue
		  code+="else\n{\n";
		  code+="ALGOBOX_AJOUTE_OUTPUT(\" \",true);\n";
		  code+="ALGOBOX_AJOUTE_OUTPUT(\""+QString::fromUtf8("***Algorithme interrompu ligne ")+"\"+ALGOBOX_LIGNE_COURANTE+\""+QString::fromUtf8(" suite à une erreur dans son exécution***")+"\",false);\n";
		  code+="}\n";  
		  //
		  code+="ALGOBOX_ACTIVER_LIGNE(ALGOBOX_LIGNE_COURANTE);\n";
		  code+="window.location.href = \"#\"+ALGOBOX_LIGNE_COURANTE;\n";
		  if (!exporthtml && !dansFct) code+="\nbrowserDialog.scriptFinished();\n";
		  code+="\n}\n}";		  
		}
	    break;
    case 103: //autres
    default:
	    if (parametres.count()==2)
		{
		code="ALGOBOX_LIGNE_COURANTE="+QString::number(id)+";\nif (ALGOBOX_EMERGENCY_STOP) {throw(\"erreur_emergency\");}\n";
		code+="//***************";
		}
	    break;
    }
  }
return code;
}

QString MainWindow::FiltreNomVariable(QString orig)
{
QStringList motcle=QString("*,+,-,/,%,(,),{,},[,],;,:,!,',\",&,|,!,:,^,#").split(",");
QString result=orig.trimmed();
for (int i = 0; i < motcle.count(); i++)
    {
    result=result.remove(motcle.at(i));
    }
result.remove(",");
result.replace(" ","_");
return result;
}

QString MainWindow::FiltreCalcul(QString orig)
{
QStringList motcle=QString("acos(,asin(,atan(,sqrt(,pow(,random(,floor(,exp(,log(,round(,max(,min(,abs(").split(",");
QString result=orig;
for (int i = 0; i < motcle.count(); i++)
    {
    if (result.contains(motcle.at(i),Qt::CaseInsensitive)) 
      {
      result=result.replace(motcle.at(i),"Math."+motcle.at(i),Qt::CaseInsensitive);
      }
    }
QStringList motclebis=QString("cos(,sin(,tan(").split(",");
int leftPos;
for (int i = 0; i < motclebis.count(); i++)
    {
    leftPos = result.indexOf(motclebis.at(i),Qt::CaseInsensitive);
    while ( leftPos != -1 ) 
      {
      if (leftPos==0) 
	{
	result=result.replace(leftPos,motclebis.at(i).length(),"Math."+motclebis.at(i));
	leftPos = result.indexOf(motclebis.at(i), leftPos+5+motclebis.at(i).length(),Qt::CaseInsensitive );
	}
      else if (result.at(leftPos-1)!='a')
	{
	result=result.replace(leftPos,motclebis.at(i).length(),"Math."+motclebis.at(i));
	leftPos = result.indexOf(motclebis.at(i), leftPos+5+motclebis.at(i).length(),Qt::CaseInsensitive );
	}
      else leftPos = result.indexOf(motclebis.at(i), leftPos+1,Qt::CaseInsensitive );
      }
    }
return result;
}

QString MainWindow::FiltreCondition(QString orig)
{
QString result=orig;
result.replace(" OU "," || ",Qt::CaseInsensitive);
result.replace(" ET "," && ",Qt::CaseInsensitive);
return result;
}

QString MainWindow::CodeNoeud(QTreeWidgetItem *item,bool exporthtml)
{
QString code="";
if (!item) return code;
idligne++;
code=CodeVersJavascript(item->data(0,Qt::UserRole).toString(),exporthtml,idligne)+"\n";
int nb_branches=item->childCount();
if (nb_branches>0)
	{
	indent+=1;
	for (int i = 0; i < nb_branches; i++) code+=CodeNoeud(item->child(i),exporthtml);
	indent-=1;
	}
return code;
}

QString MainWindow::AlgoNoeud(QTreeWidgetItem *item)
{
QString code="";
for (int i = 0; i < indent; i++) code+="&nbsp;&nbsp;";
if (!item) return code;
idligne++;
QString ligne=item->text(0);
ligne.replace(QString("<"),QString("&lt;"));
ligne.replace(QString(">"),QString("&gt;"));
if (ligne.startsWith("//",Qt::CaseInsensitive)) ligne="<i>"+ligne+"</i>";
code+=highlightHtmlLine("<a name=\""+QString::number(idligne)+"\">&nbsp;</a><span id=\"ligne"+QString::number(idligne)+"\" >"+ligne+"</span><br>\n");
int nb_branches=item->childCount();
if (nb_branches>0)
	{
	indent+=1;
	for (int i = 0; i < nb_branches; i++) code+=AlgoNoeud(item->child(i));
	indent-=1;
	}
return code;
}

QString MainWindow::AlgoNoeudTexte(QTreeWidgetItem *item)
{
QString code="";
for (int i = 0; i < indent; i++) code+="  ";
if (!item) return code;
QString texte=item->text(0);
code+=texte+"\n";
int nb_branches=item->childCount();
if (nb_branches>0)
	{
	indent+=1;
	for (int i = 0; i < nb_branches; i++) code+=AlgoNoeudTexte(item->child(i));
	indent-=1;
	}
return code;
}

QString MainWindow::AlgoNoeudCode(QTreeWidgetItem *item)
{
QString code="";
for (int i = 0; i < indent; i++) code+="  ";
if (!item) return code;
QString texte=item->text(0);
QString data=item->data(0,Qt::UserRole).toString();
QStringList parametres=data.split("#");
QRegularExpression rxaffichcalcul("AFFICHERCALCUL\\s+(.*)",QRegularExpression::CaseInsensitiveOption /* | QRegularExpression::InvertedGreedinessOption */);
QRegularExpressionMatch rxaffichcalculMatch=rxaffichcalcul.match(texte);

QRegularExpression rxaffich("AFFICHER\\s+(.*)",QRegularExpression::CaseInsensitiveOption /* | QRegularExpression::InvertedGreedinessOption */);
QRegularExpressionMatch rxaffichMatch=rxaffich.match(texte);

QRegularExpression rxpoint("TRACER_POINT\\s+(.*)",QRegularExpression::CaseInsensitiveOption /* | QRegularExpression::InvertedGreedinessOption */);
QRegularExpressionMatch rxpointMatch=rxpoint.match(texte);

QRegularExpression rxsegment("TRACER_SEGMENT\\s+(.*)",QRegularExpression::CaseInsensitiveOption /* | QRegularExpression::InvertedGreedinessOption */);
QRegularExpressionMatch rxsegmentMatch=rxsegment.match(texte);

if (rxaffichcalculMatch.hasMatch())
  {
  if ((parametres.count()==4) || (parametres.count()==3))
      {
      if (parametres.at(2)=="1") code+="AFFICHERCALCUL* "+rxaffichcalculMatch.captured(1)+"\n";
      else code+=texte+"\n";
      }
  }
else if (rxaffichMatch.hasMatch())
  {
  if ((parametres.count()==4) || (parametres.count()==3))
      {
      if (parametres.at(2)=="1") code+="AFFICHER* "+rxaffichMatch.captured(1)+"\n";
      else code+=texte+"\n";
      }
  }
else if (rxpointMatch.hasMatch())
  {
  if (parametres.count()==4)
    {
    code+="TRACER_POINT_"+parametres.at(3)+" "+rxpointMatch.captured(1)+"\n";
    }
  }
else if (rxsegmentMatch.hasMatch())
  {
  if (parametres.count()==6)
    {
    code+="TRACER_SEGMENT_"+parametres.at(5)+" "+rxsegmentMatch.captured(1)+"\n";
    }
  }
else code+=texte+"\n";
int nb_branches=item->childCount();
if (nb_branches>0)
	{
	indent+=1;
	for (int i = 0; i < nb_branches; i++) code+=AlgoNoeudCode(item->child(i));
	indent-=1;
	}
return code;
}

void MainWindow::JavascriptExport()
{
if (!modeNormal)
  {
  QString rep_analyse=EditeurVersArbre();
  if (rep_analyse!="ok")
    {
    QMessageBox::warning( this,QString::fromUtf8("Erreur"),QString::fromUtf8("Le code n'est pas valide. Impossible de tester l'algorithme.\nErreur détectée : ")+rep_analyse);
    return;
    }
  }
QTextCodec *codec = QTextCodec::codecForName("UTF-8");
QString htmlfichier=QDir::homePath();
htmlfichier="algobox_temp_"+htmlfichier.section('/',-1);
htmlfichier=QString(QUrl::toPercentEncoding(htmlfichier));
htmlfichier.remove("%");
htmlfichier=htmlfichier+".html";
QString tempDir=QDir::tempPath();
htmlfichier=tempDir+"/"+htmlfichier;
QFile fichier;
fichier.setFileName(htmlfichier);
fichier.open(QIODevice::WriteOnly);
fichier.write(codec->fromUnicode(GenererCode(false)));
// QTextStream out (&fichier);
// out.setCodec(codec);
// out << GenererCode(false);
fichier.close();
QFileInfo fic(htmlfichier);
BrowserDialog *browserDialog = new BrowserDialog(this,htmlfichier,blackconsole);
if (fic.exists() && fic.isReadable() )
	{
	
	browserDialog->resize(browserwidth,browserheight);
	if (browserDialog->exec())
	  {
	  browserwidth=browserDialog->width();
	  browserheight=browserDialog->height();
	  }
	}
if (browserDialog) delete browserDialog;
}

void MainWindow::ExporterVersTexte()
{
if (!modeNormal)
  {
  QString rep_analyse=EditeurVersArbre();
  if (rep_analyse!="ok")
    {
    QMessageBox::warning( this,QString::fromUtf8("Erreur"),QString::fromUtf8("Le code n'est pas valide. Impossible d'exporter l'algorithme.\nErreur détectée : ")+rep_analyse);
    return;
    }
  }
QTextCodec *codec = QTextCodec::codecForName("UTF-8");
QString texte="";
QFileInfo fic(nomFichier);
if (nomFichier!="sanstitre") texte=fic.baseName();
else texte=nomFichier;
texte+="  -  "; 
QDate CurrDate = QDate::currentDate();
texte+=CurrDate.toString("dd.MM.yyyy");
texte+="\n\n";
texte+="******************************************\n";
texte+=ui.textEditDescription->toPlainText()+"\n";
texte+="******************************************\n\n";
QString code="";
int nb_branches=ui.treeWidget->topLevelItemCount();
if (nb_branches>0)
    {
    indent=0;
    for (int i = 0; i < nb_branches; i++) 
	{
	code+=AlgoNoeudTexte(ui.treeWidget->topLevelItem(i));
	}
    }
QTextStream t(&code,QIODevice::ReadOnly);
//t.setCodec(codec);
int ligne=1;
QString num="";
QString blanc="    ";
while (!t.atEnd()) 
	{
	num=QString::number(ligne);
	num+=blanc.left(blanc.length()-num.length());
	texte+=num+t.readLine()+"\n";
	ligne++;
	}
if (ui.checkBoxFonction->isChecked())
	{
	QString expression=ui.lineEditFonction->text();
	if (!expression.isEmpty() && !expression.contains("F1(",Qt::CaseInsensitive)) texte+=QString::fromUtf8("\nFonction numérique utilisée :\nF1(x)=")+expression+"\n";
	}
if (ui.checkBoxF2->isChecked())
	{
	if (ui.checkBoxFonction->isChecked()) texte+="\n";
	QString role,condition,commande;
	QStringList tagList;
	QListWidgetItem *item;
	texte+="\nfonction F2("+ui.lineEditParametresF2->text()+"):\n";
	for (int i = 0; i < ui.listWidgetF2->count(); ++i)
	  {
	  tagList.clear();
	  item=ui.listWidgetF2->item(i);
	  role=item->data(Qt::UserRole).toString();
	  tagList= role.split("@");
	  if (tagList.count()==2) 
	    {
	    condition=tagList.at(0);
	    commande=tagList.at(1);
	    if ((!condition.isEmpty()) && (!commande.isEmpty()))
	      {
	      texte+="SI ("+condition+") RENVOYER "+commande+"\n";
	      }
	    }
	  }
	  if (!ui.lineEditDefautF2->text().isEmpty()) texte+=QString::fromUtf8("Dans les autres cas, RENVOYER ")+ui.lineEditDefautF2->text()+"\n";
	  }
if (!fichier_extension.isEmpty())
  {
  QFileInfo fic(fichier_extension);
  if (fic.exists() && fic.isReadable() )
    {
    texte+=QString::fromUtf8("\nExtension utilisée : ")+fic.fileName()+"\n";
    }
  }
QString exportFichier = QFileDialog::getSaveFileName(this,QString::fromUtf8("Enregistrer sous"),dernierRepertoire,QString::fromUtf8("Fichier texte (*.txt)"));
if (!exportFichier.isEmpty()) 
    {
    QFileInfo fi(exportFichier);
    dernierRepertoire=fi.absolutePath();
    QFile fichier;
    fichier.setFileName(exportFichier);
    fichier.open(QIODevice::WriteOnly);
    fichier.write(codec->fromUnicode(texte));
//    QTextStream out (&fichier);
//    out.setCodec(codec);
//    out << texte;
    fichier.close();
    }
}

void MainWindow::ExporterVersODF()
{
if (!modeNormal)
  {
  QString rep_analyse=EditeurVersArbre();
  if (rep_analyse!="ok")
    {
    QMessageBox::warning( this,QString::fromUtf8("Erreur"),QString::fromUtf8("Le code n'est pas valide. Impossible d'exporter l'algorithme.\nErreur détectée : ")+rep_analyse);
    return;
    }
  }
QTextCodec *codec = QTextCodec::codecForName("UTF-8");
QFontDatabase fdb;
QStringList xf = fdb.families();
QString deft;
if (xf.contains("Liberation Mono",Qt::CaseInsensitive)) deft="Liberation Mono";
else if (xf.contains("DejaVu Sans Mono",Qt::CaseInsensitive)) deft="DejaVu Sans Mono";
#if defined(Q_OS_MAC)
else if (xf.contains("Monaco",Qt::CaseInsensitive)) deft="Monaco";
else if (xf.contains("Courier New",Qt::CaseInsensitive)) deft="Courier New";
else if (xf.contains("PT Mono",Qt::CaseInsensitive)) deft="PT Mono";
else if (xf.contains("Andale Mono",Qt::CaseInsensitive)) deft="Andale Mono";
#endif
#if defined(Q_OS_WIN32)
else if (xf.contains("Courier New",Qt::CaseInsensitive)) deft="Courier New";
#endif
else deft=qApp->font().family();
QTextCharFormat output_format;
output_format.setFont(QFont(deft,9));  //bug Qt 4.5
output_format.setFontFamily(deft);
QTextDocument *document =new QTextDocument(this);
document->setDefaultFont(QFont(deft,9));
QTextCursor cursor(document);
QString texte="";
QFileInfo fic(nomFichier);
if (nomFichier!="sanstitre") texte=fic.baseName();
else texte=nomFichier;
texte+="  -  "; 
QDate CurrDate = QDate::currentDate();
texte+=CurrDate.toString("dd.MM.yyyy");
texte+="\n\n";
texte+=ui.textEditDescription->toPlainText()+"\n\n";
QString code="";
int nb_branches=ui.treeWidget->topLevelItemCount();
if (nb_branches>0)
    {
    indent=0;
    for (int i = 0; i < nb_branches; i++) 
	{
	code+=AlgoNoeudTexte(ui.treeWidget->topLevelItem(i));
	}
    }
QTextStream t(&code,QIODevice::ReadOnly);
//t.setCodec(codec);
int ligne=1;
QString num="";
QString blanc="    ";
while (!t.atEnd()) 
	{
	num=QString::number(ligne);
	num+=blanc.left(blanc.length()-num.length());
	texte+=num+t.readLine()+"\n";
	ligne++;
	}
if (ui.checkBoxFonction->isChecked())
	{
	QString expression=ui.lineEditFonction->text();
	if (!expression.isEmpty() && !expression.contains("F1(",Qt::CaseInsensitive)) texte+=QString::fromUtf8("\nFonction numérique utilisée :\nF1(x)=")+expression+"\n";
	}
if (ui.checkBoxF2->isChecked())
	{
	if (ui.checkBoxFonction->isChecked()) texte+="\n";
	QString role,condition,commande;
	QStringList tagList;
	QListWidgetItem *item;
	texte+="\nfonction F2("+ui.lineEditParametresF2->text()+"):\n";
	for (int i = 0; i < ui.listWidgetF2->count(); ++i)
	  {
	  tagList.clear();
	  item=ui.listWidgetF2->item(i);
	  role=item->data(Qt::UserRole).toString();
	  tagList= role.split("@");
	  if (tagList.count()==2) 
	    {
	    condition=tagList.at(0);
	    commande=tagList.at(1);
	    if ((!condition.isEmpty()) && (!commande.isEmpty()))
	      {
	      texte+="SI ("+condition+") RENVOYER "+commande+"\n";
	      }
	    }
	  }
	  if (!ui.lineEditDefautF2->text().isEmpty()) texte+=QString::fromUtf8("Dans les autres cas, RENVOYER ")+ui.lineEditDefautF2->text()+"\n";
	  }
if (!fichier_extension.isEmpty())
  {
  QFileInfo fic(fichier_extension);
  if (fic.exists() && fic.isReadable() )
    {
    texte+=QString::fromUtf8("\nExtension utilisée : ")+fic.fileName()+"\n";
    }
  }
cursor.mergeCharFormat(output_format);
cursor.insertText(texte);

QString exportFichier = QFileDialog::getSaveFileName(this,QString::fromUtf8("Enregistrer sous"),dernierRepertoire,QString::fromUtf8("Fichier ODF (*.odt)"));
if (!exportFichier.isEmpty()) 
    {
    QFileInfo fi(exportFichier);
    dernierRepertoire=fi.absolutePath();
    QTextDocumentWriter fich(exportFichier);
    fich.setFormat("odf");
//    fich.setCodec(codec);
    fich.write(document);
    }
}


void MainWindow::ExporterVersLatex()
{
if (!modeNormal)
  {
  QString rep_analyse=EditeurVersArbre();
  if (rep_analyse!="ok")
    {
    QMessageBox::warning( this,QString::fromUtf8("Erreur"),QString::fromUtf8("Le code n'est pas valide. Impossible d'exporter l'algorithme.\nErreur détectée : ")+rep_analyse);
    return;
    }
  }
int query= QMessageBox::question(this,"AlgoBox : ", QString::fromUtf8("Quel encodage voulez-vous utiliser?"),QString::fromUtf8("UTF-8"), QString::fromUtf8("ISO-8859-1"),QString::fromUtf8("Abandon"),0,2 );
if (query==2) return;
QString texte="";
QTextCodec *readcodec = QTextCodec::codecForName("UTF-8");
QFile texfile(":/documents/modelelatex.txt");
texfile.open(QIODevice::ReadOnly);
QString texfilecontent=readcodec->toUnicode(texfile.readAll() );
QTextStream t(&texfilecontent);
//t.setCodec(readcodec);
while (!t.atEnd()) 
	{
	texte+= t.readLine()+"\n";
	}
texfile.close();
QDate CurrDate = QDate::currentDate();
QString date=CurrDate.toString("dd.MM.yyyy");
QString code="";
int nb_branches=ui.treeWidget->topLevelItemCount();
QString item;
QRegularExpression fctreg("FONCTION (.*)");
QRegularExpression pourreg("POUR (.*) ALLANT_DE (.*) A (.*)");
QRegularExpression sireg("SI (.*) ALORS");
QRegularExpression tantquereg("TANT_QUE (.*) FAIRE");
QString element;
if (nb_branches>0)
    {
    indent=0;
    for (int i = 0; i < nb_branches; i++) 
	{
	code+=AlgoNoeudTexte(ui.treeWidget->topLevelItem(i));
	}
    }
/**********************************/
QStringList lignes=code.split("\n");
code="";
for(int j=0;j<lignes.count();j++)
{
 	element="";
 	item=lignes.at(j).trimmed();;
 	item=item.replace(QRegularExpression("^([0-9]+)"),"");
 	item=item.replace(QRegularExpression("(^[\\s]*)?"),"");
 	if (item.startsWith("POUR"))
	  {
      QRegularExpressionMatch pourregMatch=pourreg.match(item);
	  if (pourregMatch.hasMatch()) element="\\Pour{"+pourregMatch.captured(1)+"}{"+pourregMatch.captured(2)+"}{"+pourregMatch.captured(3)+"}\n";
	  else if (item!="") element+="\\Ligne "+item+"\n";
	  }
	else if (item=="SINON") element="\\Sinon\n";  
	else if (item.startsWith("SI"))
	  {
      QRegularExpressionMatch siregMatch=sireg.match(item);
	  if (siregMatch.hasMatch())  element="\\Si{"+siregMatch.captured(1)+"}\n";
	  else if (item!="") element+="\\Ligne "+item+"\n";
	  }
	else if (item.startsWith("TANT_QUE"))
	  {
      QRegularExpressionMatch tantqueregMatch=tantquereg.match(item);
	  if (tantqueregMatch.hasMatch())  element="\\Tantque{"+tantqueregMatch.captured(1)+"}\n";
	  else if (item!="") element+="\\Ligne "+item+"\n";
	  }
	else if (item.startsWith("FONCTION "))
	  {
      QRegularExpressionMatch fctregMatch=fctreg.match(item);
	  if (fctregMatch.hasMatch())  element="\\Fct{"+fctregMatch.captured(1)+"}\n";
	  else if (item!="") element+="\\Ligne "+item+"\n";
	  }
	else if (item=="FONCTIONS_UTILISEES") element="\\Fonctions\n";
    else if (item=="DEBUT_FONCTION") element="\\DebutFonction\n";
    else if (item=="FIN_FONCTION") element="\\FinFonction\n";
	else if (item=="VARIABLES") element="\\Variables\n";
	else if (item=="DEBUT_ALGORITHME") element="\\DebutAlgo\n";
	else if (item=="DEBUT_SINON") element="\\DebutSinon\n";
	else if (item=="DEBUT_SI") element="\\DebutSi\n";
	else if (item=="DEBUT_POUR") element="\\DebutPour\n";
	else if (item=="DEBUT_TANT_QUE") element="\\DebutTantQue\n";
	else if (item=="FIN_ALGORITHME") element="\\FinAlgo\n";
	else if (item=="FIN_SINON") element="\\FinSinon\n";
	else if (item=="FIN_SI") element="\\FinSi\n";
	else if (item=="FIN_POUR") element="\\FinPour\n";
	else if (item=="FIN_TANT_QUE") element="\\FinTantQue\n";
	else if (item!="") element+="\\Ligne "+item+"\n";
	if(element!="" && item!="") 
	{
	element=element.replace("_","\\_");
	element=element.replace("%","\\%");
	}
	if(element!="") code+=element;
}
/*********************************/
QString codenum=code;
QString expression=ui.lineEditFonction->text();
if ((ui.checkBoxFonction->isChecked()) && (!expression.isEmpty()) && (!expression.contains("F1(",Qt::CaseInsensitive)))
	{
	codenum+="\n"+QString::fromUtf8("Fonction numérique utilisée : F1(x)=")+expression;
	}
if (ui.checkBoxF2->isChecked())
	{
	if (ui.checkBoxFonction->isChecked()) texte+="\n";
	QString role,condition,commande;
	QStringList tagList;
	QListWidgetItem *item;
	codenum+="\nfonction F2("+ui.lineEditParametresF2->text()+"):\n";
	for (int i = 0; i < ui.listWidgetF2->count(); ++i)
	  {
	  tagList.clear();
	  item=ui.listWidgetF2->item(i);
	  role=item->data(Qt::UserRole).toString();
	  tagList= role.split("@");
	  if (tagList.count()==2) 
	    {
	    condition=tagList.at(0);
	    commande=tagList.at(1);
	    if ((!condition.isEmpty()) && (!commande.isEmpty()))
	      {
	      codenum+="SI ("+condition+") RENVOYER "+commande+"\n";
	      }
	    }
	  }
	  if (!ui.lineEditDefautF2->text().isEmpty()) codenum+=QString::fromUtf8("Dans les autres cas, RENVOYER ")+ui.lineEditDefautF2->text()+"\n";
	  }
if (!fichier_extension.isEmpty())
  {
  QFileInfo fic(fichier_extension);
  if (fic.exists() && fic.isReadable() )
    {
    codenum+=QString::fromUtf8("\nExtension utilisée : ")+fic.fileName()+"\n";
    }
  }
QFileInfo fic(nomFichier);
if (nomFichier!="sanstitre") texte.replace("#TITRE#",fic.baseName()+".alg");
else texte.replace("#TITRE#",nomFichier);

texte.replace("#DATE#",date);
QString descr=ui.textEditDescription->toPlainText();
descr.replace("%","\\%");
descr.replace("$","\\$");
descr.replace("_","\\_");
texte.replace("#DESCRIPTION#",descr);
texte.replace("#CODE#",codenum);

QTextCodec *codec;
if (query==1)
    {
    texte.replace("#ENCODAGE#","\\usepackage[latin1]{inputenc}");
    }
else
    {
    texte.replace("#ENCODAGE#","\\usepackage[utf8x]{inputenc}\n\\usepackage{ucs}");
    }
LatexviewDialog *lvDialog = new LatexviewDialog(this,query);
lvDialog->ui.textEdit->setPlainText(texte);
lvDialog->exec();
    
}

void MainWindow::ExporterVersHtml()
{
if (!modeNormal)
  {
  QString rep_analyse=EditeurVersArbre();
  if (rep_analyse!="ok")
    {
    QMessageBox::warning( this,QString::fromUtf8("Erreur"),QString::fromUtf8("Le code n'est pas valide. Impossible d'exporter l'algorithme.\nErreur détectée : ")+rep_analyse);
    return;
    }
  }
QTextCodec *codec = QTextCodec::codecForName("UTF-8");
QString htmlfichier=QFileDialog::getSaveFileName(this,QString::fromUtf8("Enregistrer sous"),dernierRepertoire,QString::fromUtf8("Page web (*.html)"));
if (!htmlfichier.isEmpty()) 
    {
    QFile fichier;
    fichier.setFileName(htmlfichier);
    fichier.open(QIODevice::WriteOnly);
    fichier.write(codec->fromUnicode(GenererCode(true)));
//     QTextStream out (&fichier);
//     out.setCodec(codec);
//     out << GenererCode(true);
    fichier.close();
    }
}

void MainWindow::CopieEcran()
{
if (!modeNormal)
  {
    QMessageBox::warning( this,QString::fromUtf8("Erreur"),QString::fromUtf8("Fonction non disponible dans ce mode d'édition."));
    return;
  }
//QPixmap pix = QPixmap::grabWidget(ui.treeWidget);
QPixmap pix(ui.treeWidget->size());
ui.treeWidget->render(&pix);
QString nouveauFichier = QFileDialog::getSaveFileName(this,QString::fromUtf8("Enregistrer l'image sous"),dernierRepertoire,QString::fromUtf8("Image PNG (*.png)"));
if (!nouveauFichier.isEmpty()) 
    {
    if (!nouveauFichier.contains('.')) nouveauFichier += ".png";
    pix.save(nouveauFichier, "PNG");
    }
}

void MainWindow::Imprimer()
{
if (!modeNormal)
  {
  QString rep_analyse=EditeurVersArbre();
  if (rep_analyse!="ok")
    {
    QMessageBox::warning( this,QString::fromUtf8("Erreur"),QString::fromUtf8("Le code n'est pas valide. Impossible d'imprimer l'algorithme.\nErreur détectée : ")+rep_analyse);
    return;
    }
  }
QTextCodec *codec = QTextCodec::codecForName("UTF-8");
QString texte="";
QFileInfo fic(nomFichier);
if (nomFichier!="sanstitre") texte=fic.baseName();
else texte=nomFichier;
texte+="  -  "; 
QDate CurrDate = QDate::currentDate();
texte+=CurrDate.toString("dd.MM.yyyy");
texte+="\n\n";
texte+="******************************************\n";
texte+=ui.textEditDescription->toPlainText()+"\n";
texte+="******************************************\n\n";
QString code="";
int nb_branches=ui.treeWidget->topLevelItemCount();
if (nb_branches>0)
    {
    indent=0;
    for (int i = 0; i < nb_branches; i++) 
	{
	code+=AlgoNoeudTexte(ui.treeWidget->topLevelItem(i));
	}
    }
QTextStream t(&code,QIODevice::ReadOnly);
//t.setCodec(codec);
int ligne=1;
QString num="";
QString blanc="    ";
while (!t.atEnd()) 
	{
	num=QString::number(ligne);
	num+=blanc.left(blanc.length()-num.length());
	texte+=num+t.readLine()+"\n";
	ligne++;
	}	
if (ui.checkBoxFonction->isChecked())
	{
	QString expression=ui.lineEditFonction->text();
	if (!expression.isEmpty() && !expression.contains("F1(",Qt::CaseInsensitive)) texte+=QString::fromUtf8("\nFonction numérique utilisée :\nF1(x)=")+expression+"\n";
	}
if (ui.checkBoxF2->isChecked())
	{
	if (ui.checkBoxFonction->isChecked()) texte+="\n";
	QString role,condition,commande;
	QStringList tagList;
	QListWidgetItem *item;
	texte+="\nfonction F2("+ui.lineEditParametresF2->text()+"):\n";
	for (int i = 0; i < ui.listWidgetF2->count(); ++i)
	  {
	  tagList.clear();
	  item=ui.listWidgetF2->item(i);
	  role=item->data(Qt::UserRole).toString();
	  tagList= role.split("@");
	  if (tagList.count()==2) 
	    {
	    condition=tagList.at(0);
	    commande=tagList.at(1);
	    if ((!condition.isEmpty()) && (!commande.isEmpty()))
	      {
	      texte+="SI ("+condition+") RENVOYER "+commande+"\n";
	      }
	    }
	  }
	  if (!ui.lineEditDefautF2->text().isEmpty()) texte+=QString::fromUtf8("Dans les autres cas, RENVOYER ")+ui.lineEditDefautF2->text()+"\n";
	  }
if (!fichier_extension.isEmpty())
  {
  QFileInfo fic(fichier_extension);
  if (fic.exists() && fic.isReadable() )
    {
    texte+=QString::fromUtf8("\nExtension utilisée : ")+fic.fileName()+"\n";
    }
  }
QPrinter printer;
QPrintDialog *dlg = new QPrintDialog(&printer, this);
if (dlg->exec() != QDialog::Accepted) return;
QTextDocument *document =new QTextDocument(texte);
QFontDatabase fdb;
QStringList xf = fdb.families();
QString deft;
if (xf.contains("DejaVu Sans Mono",Qt::CaseInsensitive)) deft="DejaVu Sans Mono";
else if (xf.contains("Liberation Mono",Qt::CaseInsensitive)) deft="Liberation Mono";
#if defined(Q_OS_MAC)
else if (xf.contains("Monaco",Qt::CaseInsensitive)) deft="Monaco";
else if (xf.contains("Courier New",Qt::CaseInsensitive)) deft="Courier New";
else if (xf.contains("PT Mono",Qt::CaseInsensitive)) deft="PT Mono";
else if (xf.contains("Andale Mono",Qt::CaseInsensitive)) deft="Andale Mono";
#endif
#if defined(Q_OS_WIN32)
else if (xf.contains("Courier New",Qt::CaseInsensitive)) deft="Courier New";
#endif
else deft=qApp->font().family();
QFont printFont (deft,8);
document->setDefaultFont(printFont);
document->print(&printer);
}

//********************************
void MainWindow::NouvelAlgo()
{
if (estModifie) 
	{
	switch(  QMessageBox::warning(this,QString::fromUtf8("AlgoBox : "),
					QString::fromUtf8("L'algorithme courant a été modifié.\nVoulez-vous l'enregistrer avant d'en créer un nouveau?"),
					QString::fromUtf8("Enregistrer"), QString::fromUtf8("Ne pas enregistrer"), QString::fromUtf8("Abandon"),
					0,
					2 ) )
		{
		case 0:
		SauverAlgo();
		break;
		case 1:
		break;
		case 2:
		default:
		return;
		break;
		}
	}
AnnulerExtension();
Init();
ActualiserArbre();
estVierge=true;
estModifie=false;
nomFichier="sanstitre";
setWindowTitle(QString("AlgoBox ")+QString(VERSION_STR)+QString(" : ")+nomFichier);
ui.EditorView->editor->document()->setModified(false);
}

void MainWindow::ItemVersXml(QTreeWidgetItem *item,QDomDocument doc ,QDomElement parent)
{
QDomElement element;
element=doc.createElement("item");
element.setAttribute("algoitem",item->text(0));
element.setAttribute("code",item->data(0,Qt::UserRole).toString());
parent.appendChild(element);
int nb_branches=item->childCount();
if (nb_branches>0)
	{
	for (int i = 0; i < nb_branches; i++) 
		{
		ItemVersXml(item->child(i),doc,element);
		}
	}
}

void MainWindow::SauverAlgo()
{
if (nomFichier=="sanstitre" ) SauverSousAlgo();
else
    {
    if (!modeNormal)
      {
      QString rep_analyse=EditeurVersArbre();
      if (rep_analyse!="ok")
	{
	QMessageBox::warning( this,QString::fromUtf8("Erreur"),QString::fromUtf8("Le code n'est pas valide. Impossible d'enregistrer l'algorithme.\nErreur détectée : ")+rep_analyse);
	return;
	}
      ui.EditorView->editor->document()->setModified(false);
      }
    QFile fichier(nomFichier);
    if (!fichier.open(QIODevice::WriteOnly))
	    {
	    QMessageBox::warning( this,QString::fromUtf8("Erreur"),QString::fromUtf8("Impossible d'enregistrer le fichier"));
	    return;
	    }
    int nb_branches=ui.treeWidget->topLevelItemCount();
    if (nb_branches==0) return ;
    QDomDocument doc;
    QDomProcessingInstruction instr =  doc.createProcessingInstruction("xml","version=\"1.0\" encoding=\"UTF-8\"");
    doc.appendChild(instr);
    QDomElement root=doc.createElement("Algo");
    doc.appendChild(root);
    QDomElement element;
    element=doc.createElement("description");
    element.setAttribute("texte",ui.textEditDescription->toPlainText());
    QTreeWidgetItem *curItem = ui.treeWidget->currentItem();
    if (curItem) element.setAttribute("courant",curItem->text(0));
    else element.setAttribute("courant","");
    root.appendChild(element);

    element=doc.createElement("extension");
    if (!fichier_extension.isEmpty()) 
      {
      QFileInfo fic(fichier_extension);
      if (fic.exists() && fic.isReadable() )
	      {
	      element.setAttribute("extnom",fic.fileName());
	      }
	  else element.setAttribute("extnom","inactif");
      }
    else 
      {
      element.setAttribute("extnom","inactif");
      }
    root.appendChild(element);    
    
    element=doc.createElement("fonction");
    if (ui.checkBoxFonction->isChecked()) element.setAttribute("fctetat","actif");
    else element.setAttribute("fctetat","inactif");
    QString code= ui.lineEditFonction->text();
    if (!code.isEmpty()) element.setAttribute("fctcode",code);
    else element.setAttribute("fctcode","");
    root.appendChild(element);
    
    element=doc.createElement("F2");
    if (ui.checkBoxF2->isChecked()) element.setAttribute("F2etat","actif");
    else element.setAttribute("F2etat","inactif");
    QString para= ui.lineEditParametresF2->text();
    if (!para.isEmpty()) element.setAttribute("F2para",para);
    else element.setAttribute("F2para","");
    QString defaut= ui.lineEditDefautF2->text();
    if (!defaut.isEmpty()) element.setAttribute("F2defaut",defaut);
    else element.setAttribute("F2defaut","");
    QString lignes="";
    for (int i = 0; i < ui.listWidgetF2->count(); ++i) lignes+=ui.listWidgetF2->item(i)->data(Qt::UserRole).toString()+"#";
    element.setAttribute("F2lignes",lignes);
    root.appendChild(element);
    
    element=doc.createElement("repere");
    if (ui.checkBoxRepere->isChecked()) element.setAttribute("repetat","actif");
    else element.setAttribute("repetat","inactif");
    code="";
    QString part=ui.lineEditXmin->text();
    if (!part.isEmpty()) code+=part.remove("#")+"#";
    else code+="-10#";
    part=ui.lineEditXmax->text();
    if (!part.isEmpty()) code+=part.remove("#")+"#";
    else code+="10#";
    part=ui.lineEditYmin->text();
    if (!part.isEmpty()) code+=part.remove("#")+"#";
    else code+="-10#";
    part=ui.lineEditYmax->text();
    if (!part.isEmpty()) code+=part.remove("#")+"#";
    else code+="10#";
    part=ui.lineEditGradX->text();
    if (!part.isEmpty()) code+=part.remove("#")+"#";
    else code+="2#";
    part=ui.lineEditGradY->text();
    if (!part.isEmpty()) code+=part.remove("#");
    else code+="2";
    element.setAttribute("repcode",code);
    root.appendChild(element);
    
    element=doc.createElement("param");
    if (arrondiAutoAlgo) element.setAttribute("arrondiAuto","vrai");
    else element.setAttribute("arrondiAuto","faux");
    element.setAttribute("maxBoucle",QString::number(maxBoucleAlgo));
    element.setAttribute("totalBoucles",QString::number(totalBouclesAlgo));
    element.setAttribute("totalAffichages",QString::number(totalAffichagesAlgo));
    element.setAttribute("epaisseurLigne",QString::number(epaisseurLigneAlgo));
    element.setAttribute("epaisseurPoint",QString::number(epaisseurPointAlgo));
    element.setAttribute("nbDecimales",QString::number(nbDecimalesAlgo));
    root.appendChild(element);

    for (int i = 0; i < nb_branches; i++) ItemVersXml(ui.treeWidget->topLevelItem(i),doc,root);
    QTextStream out (&fichier);
    doc.save(out,4);
    fichier.close();
    QFileInfo fi(nomFichier);
    dernierRepertoire=fi.absolutePath();
    estModifie=false;
    AddRecentFile(nomFichier);
    }
setWindowTitle(QString("AlgoBox ")+QString(VERSION_STR)+QString(" : ")+nomFichier);
}

void MainWindow::SauverSousAlgo()
{
QString nouveauFichier;
if (nomFichier=="sanstitre" ) 
	{
	nouveauFichier = QFileDialog::getSaveFileName(this,QString::fromUtf8("Enregistrer sous"),dernierRepertoire,QString::fromUtf8("Algorithme (*.alg)"));
	}
else 
	{
	nouveauFichier = QFileDialog::getSaveFileName(this,QString::fromUtf8("Enregistrer sous"),nomFichier,QString::fromUtf8("Algorithme (*.alg)"));
	}
if (!nouveauFichier.isEmpty()) 
    {
    if (!nouveauFichier.contains('.')) nouveauFichier += ".alg";
    nomFichier=nouveauFichier;
    SauverAlgo();
    }
// else
//     {
//     QMessageBox::warning( this,QString::fromUtf8("Erreur"),QString::fromUtf8("Impossible d'enregistrer le fichier"));
//     }
setWindowTitle(QString("AlgoBox ")+QString(VERSION_STR)+QString(" : ")+nomFichier);
}

void MainWindow::XmlVersItem(QTreeWidgetItem *parentItem, QDomElement element)
{
QFont titleFont = qApp->font();
titleFont.setBold(true);
QFont commentFont = qApp->font();
commentFont.setItalic(true);

if (!element.hasAttribute("algoitem") || !element.hasAttribute("code")) return;
progressCompteur++;
if (progressDialog) {progressDialog->setValue(progressCompteur);/*qApp->processEvents();*/}
QTreeWidgetItem *newItem;
QString texte=element.attribute("algoitem");
if (texte=="FONCTIONS_UTILISEES")
    {
    newItem=fctsItem;
    }
else if (texte=="VARIABLES")
    {
    newItem=variablesItem;
    }
else if (texte=="DEBUT_ALGORITHME")
    {
    newItem=debutItem; 
    }
else if (texte=="FIN_ALGORITHME")
    {
    newItem=finItem;
    }
else 
  {
  if (parentItem) newItem = new QTreeWidgetItem(parentItem);
  else newItem = new QTreeWidgetItem(ui.treeWidget);
  newItem->setText(0,element.attribute("algoitem"));
  newItem->setData(0,Qt::UserRole,QString(element.attribute("code")));
  if (newItem->text(0).startsWith("//",Qt::CaseInsensitive)) newItem->setFont(0,commentFont);
  newItem->setExpanded(true);
  }
ui.treeWidget->setCurrentItem(newItem);
//ActualiserArbre();
QDomElement enfant = element.firstChildElement();
while (!enfant.isNull()) 
	{
	XmlVersItem(newItem,enfant);
	enfant = enfant.nextSiblingElement();
	}
}

void MainWindow::Ouvrir(QString nouveauFichier)
{
QDomDocument doc;
QFileInfo fi(nouveauFichier);
if (!fi.exists()) return;
dernierRepertoire=fi.absolutePath();
QFile fichier(nouveauFichier);
if (!fichier.open(QIODevice::ReadOnly)) 
	{
	QMessageBox::warning( this,QString::fromUtf8("Erreur"),QString::fromUtf8("Impossible de lire le fichier"));
	return;
	}
QTextCodec *codec = QTextCodec::codecForName("UTF-8");
QString streamcontent=codec->toUnicode(fichier.readAll() );
QTextStream stream( &streamcontent );
//stream.setCodec(codec);
QString line,fulltext;
fulltext="";
int count = 0;
while ( !stream.atEnd() ) {
line = stream.readLine( );
fulltext+=line+"\n";
if (line.contains("<item")) count++;
}
fichier.close();
if (!progressDialog) progressDialog = new QProgressDialog(QString::fromUtf8("Chargement de l'Algorithme..."),0,0,count,this);
else progressDialog->setMaximum(count);
progressDialog->setMinimumDuration(0);
progressDialog->show();
progressDialog->raise();
progressDialog->setValue(0);
qApp->processEvents();
progressCompteur=0;
if (!doc.setContent(fulltext)) 
	{
	QMessageBox::warning( this,QString::fromUtf8("Erreur"),QString::fromUtf8("Impossible de lire le fichier"));
	return;
	}
QDomElement root = doc.documentElement();
if (root.tagName() != "Algo")
	{
	QMessageBox::warning( this,QString::fromUtf8("Erreur"),QString::fromUtf8("Ce fichier ne correspond pas à un algorithme"));
	return;
	}
nomFichier=nouveauFichier;
InitOuvrir();
QApplication::setOverrideCursor(QCursor(Qt::WaitCursor));
QString courant="";
QDomElement enfant = root.firstChildElement();
bool avecExtension=false;
QString nomExtension="";
while (!enfant.isNull()) 
	{
	if (enfant.hasAttribute("texte") && enfant.hasAttribute("courant"))
	    {
	    ui.textEditDescription->setPlainText(enfant.attribute("texte"));
	    courant=enfant.attribute("courant");
	    }
	else if (enfant.hasAttribute("extnom"))
	    {
	    if (enfant.attribute("extnom")!="inactif") {avecExtension=true;nomExtension=enfant.attribute("extnom");}
	    }
	else if (enfant.hasAttribute("fctetat") && enfant.hasAttribute("fctcode"))
	    {
	    if (enfant.attribute("fctetat")=="actif") ui.checkBoxFonction->setChecked(true);
	    else ui.checkBoxFonction->setChecked(false);
	    ui.lineEditFonction->setText(enfant.attribute("fctcode"));
	    }
	else if (enfant.hasAttribute("F2etat") && enfant.hasAttribute("F2para") && enfant.hasAttribute("F2defaut"))
	    {
	    if (enfant.attribute("F2etat")=="actif") ui.checkBoxF2->setChecked(true);
	    else ui.checkBoxF2->setChecked(false);
	    ui.lineEditParametresF2->setText(enfant.attribute("F2para"));
	    ui.lineEditDefautF2->setText(enfant.attribute("F2defaut"));
	    QStringList lignes=enfant.attribute("F2lignes").split("#");
	    QStringList tagList;
	    QListWidgetItem *ligne;
	    for (int i = 0; i < lignes.count(); ++i)
		{
		tagList= lignes.at(i).split("@");
		if (tagList.count()==2)
		  {
		  ligne=new QListWidgetItem(ui.listWidgetF2);
		  ligne->setText("SI ("+tagList.at(0)+") RENVOYER "+tagList.at(1));
		  ligne->setData(Qt::UserRole,lignes.at(i));
		  }
		}
	    }	
	else if (enfant.hasAttribute("repetat") && enfant.hasAttribute("repcode"))
	    {
	    if (enfant.attribute("repetat")=="actif") ui.checkBoxRepere->setChecked(true);
	    else ui.checkBoxRepere->setChecked(false);
	    QStringList parametres=enfant.attribute("repcode").split("#");
	    if (parametres.count()==6)
		{
		ui.lineEditXmin->setText(parametres.at(0));
		ui.lineEditXmax->setText(parametres.at(1));
		ui.lineEditYmin->setText(parametres.at(2));
		ui.lineEditYmax->setText(parametres.at(3));
		ui.lineEditGradX->setText(parametres.at(4));
		ui.lineEditGradY->setText(parametres.at(5));
		}
	    }
	else if (enfant.hasAttribute("arrondiAuto"))
        {
        if (enfant.attribute("arrondiAuto")=="vrai") arrondiAutoAlgo=true;
        else arrondiAutoAlgo=false;
        if (enfant.hasAttribute("maxBoucle")) maxBoucleAlgo=enfant.attribute("maxBoucle").toInt();
        if (enfant.hasAttribute("totalBoucles")) totalBouclesAlgo=enfant.attribute("totalBoucles").toInt();
        if (enfant.hasAttribute("totalAffichages")) totalAffichagesAlgo=enfant.attribute("totalAffichages").toInt();
        if (enfant.hasAttribute("epaisseurLigne")) epaisseurLigneAlgo=enfant.attribute("epaisseurLigne").toInt();
        if (enfant.hasAttribute("epaisseurPoint")) epaisseurPointAlgo=enfant.attribute("epaisseurPoint").toInt();
        if (enfant.hasAttribute("nbDecimales")) nbDecimalesAlgo=enfant.attribute("nbDecimales").toInt();
        }

	else 
	{
	XmlVersItem(0,enfant);
	}
	enfant = enfant.nextSiblingElement();
	}
estVierge=false;
estModifie=false;
setWindowTitle(QString("AlgoBox ")+QString(VERSION_STR)+QString(" : ")+nomFichier);
AddRecentFile(nouveauFichier);

if (!courant.isEmpty()) 
    {
    QList<QTreeWidgetItem *> listItems=ui.treeWidget->findItems(courant,Qt::MatchRecursive,0);
    if (listItems.count()>0) ui.treeWidget->setCurrentItem(listItems.at(0));
    }
ActualiserArbre();
if (!modeNormal)
    {
    //disconnect(ui.EditorView->editor, SIGNAL(textChanged()), this, SLOT(ActualiserStatut()));
    ui.EditorView->editor->setPlainText(ArbreVersCodeTexte());
    ui.EditorView->editor->document()->setModified(false);
    //connect(ui.EditorView->editor, SIGNAL(textChanged()), this, SLOT(ActualiserStatut()));
    EffaceArbre();
    }
QApplication::restoreOverrideCursor();



if (avecExtension) 
  {
  QMessageBox::warning( this,"Attention",QString::fromUtf8("Cet algorithme utilise une extension nommée '")+nomExtension+"'\n"+QString::fromUtf8("Cette extension doit-être chargée pour un bon fonctionnement de l'algorithme."));
  QString nouveauFichier = QFileDialog::getOpenFileName(this,QString::fromUtf8("Ouvrir Extension"),dernierRepertoire,"Fichier javascript (*.js)");
  if (!nouveauFichier.isEmpty()) 
    {
    QFileInfo fic(nouveauFichier);
    if (fic.exists() && fic.isReadable() )
	    {
	    if (fic.fileName()!=nomExtension) ActualiserStatut();
	    fichier_extension=nouveauFichier;
	    annulerExtensionAct->setEnabled(true);
	    ui.lineEditExtension->setText(fic.fileName());
	    }
    }
  else 
    {
    AnnulerExtension();
    }
  }
ActualiserTexteParam();	    
}

void MainWindow::ChargerAlgo()
{
if (estModifie) 
	{
	switch(  QMessageBox::warning(this,"AlgoBox : ",
					QString::fromUtf8("L'algorithme courant a été modifié.\nVoulez-vous l'enregistrer avant de charger un autre algorithme?"),
					QString::fromUtf8("Enregistrer"), QString::fromUtf8("Ne pas enregistrer"), QString::fromUtf8("Abandon"),
					0,
					2 ) )
		{
		case 0:
		SauverAlgo();
		break;
		case 1:
		break;
		case 2:
		default:
		return;
		break;
		}
	}
QString nouveauFichier = QFileDialog::getOpenFileName(this,QString::fromUtf8("Ouvrir Fichier"),dernierRepertoire,"Algorithme (*.alg)");
if (nouveauFichier.isEmpty()) return;
Ouvrir(nouveauFichier);
}

void MainWindow::OuvrirNouvelAlgo(QString nouveauFichier)
{
if (estModifie) 
	{
	switch(  QMessageBox::warning(this,"AlgoBox : ",
					QString::fromUtf8("L'algorithme courant a été modifié.\nVoulez-vous l'enregistrer avant de charger un autre algorithme?"),
					QString::fromUtf8("Enregistrer"), QString::fromUtf8("Ne pas enregistrer"), QString::fromUtf8("Abandon"),
					0,
					2 ) )
		{
		case 0:
		SauverAlgo();
		break;
		case 1:
		break;
		case 2:
		default:
		return;
		break;
		}
	}
Ouvrir(nouveauFichier);
}

void MainWindow::ActualiserStatut()
{
estVierge=false;
estModifie=true;
setWindowTitle(QString("AlgoBox ")+QString(VERSION_STR)+QString::fromUtf8(" [modifié]")+" : "+nomFichier);
}

void MainWindow::NouveauStatut(bool m)
{
if (m) ActualiserStatut();
else
  {
//  estVierge=false;
  estModifie=false;
  setWindowTitle(QString("AlgoBox ")+QString(VERSION_STR)+QString(" : ")+nomFichier);
  }
}
//************************
void MainWindow::Aide()
{
#if defined(Q_OS_UNIX) && !defined(Q_OS_MAC)
#ifdef USB_VERSION
QString docfile=QCoreApplication::applicationDirPath() + "/ressources/aidealgobox.html";
#else
QString docfile=PREFIX"/share/algobox/aidealgobox.html";
#endif
#endif

#if defined(Q_OS_MAC)
QString docfile=QCoreApplication::applicationDirPath() + "/../Resources/aidealgobox.html";
#endif
#if defined(Q_OS_WIN32)
QString docfile=QCoreApplication::applicationDirPath() + "/ressources/aidealgobox.html";
#endif
QFileInfo fic(docfile);
if (fic.exists() && fic.isReadable() )
	{
        if (browserWindow)
          {
          browserWindow->close();
		  delete browserWindow;
          }
	browserWindow=new Browser("file:///"+docfile, 0);

	browserWindow->raise();
	browserWindow->show();
	}
else { QMessageBox::warning( this,"Erreur",QString::fromUtf8("Fichier d'aide non trouvé"));}
}

void MainWindow::Tutoriel()
{
QDesktopServices::openUrl(QUrl("https://www.xm1math.net/algobox/tutoalgobox/index.html"));
}

void MainWindow::APropos()
{
AproposDialog *abDlg = new AproposDialog(this);
abDlg->exec();
}

void MainWindow::SetInterfaceFont()
{
X11FontDialog *xfdlg = new X11FontDialog(this);
int ft=xfdlg->ui.comboBoxFont->findText (x11fontfamily , Qt::MatchExactly);
xfdlg->ui.comboBoxFont->setCurrentIndex(ft);
xfdlg->ui.spinBoxSize->setValue(x11fontsize);
if (xfdlg->exec())
	{
	x11fontfamily=xfdlg->ui.comboBoxFont->currentText();
	x11fontsize=xfdlg->ui.spinBoxSize->value();
	if (x11fontsize<6) x11fontsize=6;
	QFont x11Font (x11fontfamily,x11fontsize);
	QApplication::setFont(x11Font);
	}
}

void MainWindow::CouleurConsole()
{
ConsoleDialog *dlg = new ConsoleDialog(this);
if (blackconsole) dlg->ui.radioButtonBlack->setChecked(true);
else dlg->ui.radioButtonWhite->setChecked(true);
if (dlg->exec())
	{
	blackconsole=dlg->ui.radioButtonBlack->isChecked();
	}
}

void MainWindow::fileOpenRecent()
{
QAction *action = qobject_cast<QAction *>(sender());
if (action) 
    {
    if (estModifie) 
	    {
	    switch(  QMessageBox::warning(this,"AlgoBox : ",
					    QString::fromUtf8("L'algorithme courant a été modifié.\nVoulez-vous l'enregistrer avant de charger un autre algorithme?"),
					    QString::fromUtf8("Enregistrer"), QString::fromUtf8("Ne pas enregistrer"), QString::fromUtf8("Abandon"),
					    0,
					    2 ) )
		    {
		    case 0:
		    SauverAlgo();
		    break;
		    case 1:
		    break;
		    case 2:
		    default:
		    return;
		    break;
		    }
	    }
    QString f=action->data().toString();
    Ouvrir(f);
    }
}

void MainWindow::AddRecentFile(const QString &f)
{
if (recentFilesList.contains(f)) return;

if (recentFilesList.count() < 5) recentFilesList.prepend(f);
else
	{
	recentFilesList.removeLast();
	recentFilesList.prepend(f);
	}
UpdateRecentFile();
}

void MainWindow::UpdateRecentFile()
{
for (int i=0; i < recentFilesList.count(); i++)
	{
        recentFileActs[i]->setText(recentFilesList.at(i));
        recentFileActs[i]->setData(recentFilesList.at(i));
        recentFileActs[i]->setVisible(true);
	}
for (int j = recentFilesList.count(); j < 5; ++j) recentFileActs[j]->setVisible(false);
}

void MainWindow::ChargerExemple()
{
#if defined(Q_OS_UNIX) && !defined(Q_OS_MAC)
#ifdef USB_VERSION
QString exempleRepertoire=QCoreApplication::applicationDirPath() + "/ressources";
#else
QString exempleRepertoire=PREFIX"/share/algobox";
#endif
#endif

#if defined(Q_OS_MAC)
QString exempleRepertoire=QCoreApplication::applicationDirPath() + "/../Resources";
#endif
#if defined(Q_OS_WIN32)
QString exempleRepertoire=QCoreApplication::applicationDirPath() + "/ressources";
#endif
QString f = QFileDialog::getOpenFileName(this,QString::fromUtf8("Ouvrir Fichier"),exempleRepertoire,"Algorithme (*.alg)");
if (!f.isEmpty()) Ouvrir(f);
}
//**********************************
void MainWindow::EditCopier()
{
if (modeNormal)
  {
  QTreeWidgetItem *curItem = ui.treeWidget->currentItem();
  if (!curItem) return;
  clipboardItem=curItem->clone();
  }
else
  {
  ui.EditorView->editor->copy(); 
  }
}

void MainWindow::EditColler()
{
if (modeNormal)
  {
  QTreeWidgetItem *curItem = ui.treeWidget->currentItem();
  QTreeWidgetItem *newItem=clipboardItem->clone();
  int idx;
  if (!curItem) return;
  if (clipboardItem) 
    {
    if (!clipboardItem->text(0).isEmpty()) 
      {
      if (curItem->parent()) 
	{
	idx = curItem->parent()->indexOfChild(curItem);
	curItem->parent()->insertChild(idx,newItem);
	ui.treeWidget->setCurrentItem(newItem);
	ExpandBranche(newItem);
	delete curItem;
	}
      }
    }
  ActualiserArbre();
  }
else
  {
  ui.EditorView->editor->paste(); 
  }
}

void MainWindow::EditCouper()
{
if (modeNormal)
  {
  QTreeWidgetItem *curItem = ui.treeWidget->currentItem();
  if (!curItem) return;
  clipboardItem=curItem->clone();
  SupprimerLigne();
  }
else
  {
  ui.EditorView->editor->cut(); 
  }
}
//**********************************
void MainWindow::ToggleCadrePresentation()
{
if (!afficheCadrePresentation)
	{
	afficheCadrePresentation=true;
	ui.frameDescription->show();
	}
else 
  	{
	afficheCadrePresentation=false;
	ui.frameDescription->hide();
	}
ToggleAct->setChecked(afficheCadrePresentation);
}

void MainWindow::ActualiserModeButton()
{
if (buttonModeNormal->isChecked())
	{
    QString rep_analyse=EditeurVersArbre();
    if (rep_analyse!="ok")
        {
        QMessageBox::warning( this,QString::fromUtf8("Erreur"),QString::fromUtf8("Le code n'est pas valide. Impossible de créer l'arbre de l'algorithme.\nErreur détectée : ")+rep_analyse);
        modeNormal=false;
        buttonModeTexte->setChecked(true);
        actionModeTexte->setChecked(true);
        return;
        }
	modeNormal=true;
	ui.stackedWidget->setCurrentWidget(ui.page_arbre);
    ActualiserStatut();
	}
else 
  	{
	modeNormal=false;
	ui.stackedWidget->setCurrentWidget(ui.page_editeur);
    ui.EditorView->editor->setPlainText(ArbreVersCodeTexte());
    ui.EditorView->editor->document()->setModified(true);
	}
if (modeNormal) actionModeNormal->setChecked(true);
else actionModeTexte->setChecked(true);
ActualiserArbre();
}

void MainWindow::ActualiserMode()
{
QAction *action = qobject_cast<QAction *>(sender());
QString choix=action->text();
int query=1;
// if (!estVierge)
//   {
//   query =QMessageBox::warning(this, "AlgoBox", QString::fromUtf8("Le changement de mode d'édition n'est possible qu'à partir d'un nouvel algorithme vierge."),QString::fromUtf8("Créer un nouvel algorithme vierge"), QString::fromUtf8("Abandonner") );
//   if (query==1) return;
//   NouvelAlgo();
//   }
if (choix=="Mode &normal")
	{
    QString rep_analyse=EditeurVersArbre();
    if (rep_analyse!="ok")
        {
        QMessageBox::warning( this,QString::fromUtf8("Erreur"),QString::fromUtf8("Le code n'est pas valide. Impossible de créer l'arbre de l'algorithme.\nErreur détectée : ")+rep_analyse);
        modeNormal=false;
        buttonModeTexte->setChecked(true);
        actionModeTexte->setChecked(true);
        return;
        }
	modeNormal=true;
	ui.stackedWidget->setCurrentWidget(ui.page_arbre);
    ActualiserStatut();
	}
else 
  	{
	modeNormal=false;
	ui.stackedWidget->setCurrentWidget(ui.page_editeur);
    ui.EditorView->editor->setPlainText(ArbreVersCodeTexte());
    ui.EditorView->editor->document()->setModified(true);
	}
if (modeNormal) buttonModeNormal->setChecked(true);
else buttonModeTexte->setChecked(true);
ActualiserArbre();
}
//**********************************
void MainWindow::dragEnterEvent(QDragEnterEvent *event)
{
if (event->mimeData()->hasFormat("text/uri-list")) event->acceptProposedAction();
}

void MainWindow::dropEvent(QDropEvent *event)
{
#if defined(Q_OS_WIN32)
QRegularExpression rx("^file:(/+)(.*\\.alg)");
#else
QRegularExpression rx("^file:(//)(.*\\.alg)");
#endif
QList<QUrl> uris=event->mimeData()->urls();
QString uri;
for (int i = 0; i < uris.size(); ++i)
	{
	uri=uris.at(i).toString();
    QRegularExpressionMatch rxMatch=rx.match(uri);
    if (rxMatch.hasMatch()) {OuvrirNouvelAlgo(rxMatch.captured(2));}
	}
event->acceptProposedAction();
}
//**********************************
// void MainWindow::ImporterCodeTexte()
// {
// if (estModifie) 
// 	{
// 	switch(  QMessageBox::warning(this,"AlgoBox : ",
// 					QString::fromUtf8("L'algorithme courant a été modifié.\nVoulez-vous l'enregistrer avant de charger un autre algorithme?"),
// 					QString::fromUtf8("Enregistrer"), QString::fromUtf8("Ne pas enregistrer"), QString::fromUtf8("Abandon"),
// 					0,
// 					2 ) )
// 		{
// 		case 0:
// 		SauverAlgo();
// 		break;
// 		case 1:
// 		break;
// 		case 2:
// 		default:
// 		return;
// 		break;
// 		}
// 	}
// QString nouveauFichier = QFileDialog::getOpenFileName(this,QString::fromUtf8("Ouvrir Fichier"),dernierRepertoire,"Code Texte (*.txt)");
// if (nouveauFichier.isEmpty()) return;
// QFileInfo fi(nouveauFichier);
// if (!fi.exists()) return;
// dernierRepertoire=fi.absolutePath();
// QFile fichier(nouveauFichier);
// if (!fichier.open(QIODevice::ReadOnly)) 
// 	{
// 	QMessageBox::warning( this,QString::fromUtf8("Erreur"),QString::fromUtf8("Impossible de lire le fichier"));
// 	return;
// 	}
// 	
// Init();
// 
// QTextCodec *codec = QTextCodec::codecForName("UTF-8");
// QStringList liste_Lignes;
// QTextStream in(&fichier);
// in.setCodec(codec);
// ui.EditorView->editor->setPlainText( in.readAll());
// ui.stackedWidget->setCurrentWidget(ui.page_editeur);
// }

QString MainWindow::EditeurVersArbre()
{
EffaceArbre();
QString texteEditeur=ui.EditorView->editor->toPlainText();
QTextCodec *codec = QTextCodec::codecForName("UTF-8");
QStringList liste_Lignes, num_Lignes;
QTextStream in(&texteEditeur,QIODevice::ReadOnly);
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
in.setCodec(codec);
#else
in.setEncoding(QStringConverter::Encoding::Utf8);
#endif

bool erreur=false;
QString message_erreur="ok";
QString ligne,nomvariable,type,rang,message,calcul,contenu,x,y,couleur,xdep,ydep,xfin,yfin,condition,debut,fin,code;
int i=0;
QRegularExpression rxnumline("^([0-9]+)");
QRegularExpression rxvar("(.*)\\s+EST_DU_TYPE\\s+(.*)",QRegularExpression::CaseInsensitiveOption /* | QRegularExpression::InvertedGreedinessOption */);
QRegularExpression rxfixe("^(FONCTIONS_UTILISEES|VARIABLES|DEBUT_ALGORITHME|FIN_ALGORITHME)(?!_)",QRegularExpression::CaseInsensitiveOption /* | QRegularExpression::InvertedGreedinessOption */);
QRegularExpression rxpause("^PAUSE",QRegularExpression::CaseInsensitiveOption /* | QRegularExpression::InvertedGreedinessOption */);
QRegularExpression rxcommentaire("^//(.*)");
QRegularExpression rxlire("^LIRE\\s+(.+)",QRegularExpression::CaseInsensitiveOption /* | QRegularExpression::InvertedGreedinessOption */);
QRegularExpression rxliste("\\[(.*)\\]");
QRegularExpression rxaffichcalculnl("^AFFICHERCALCUL\\*\\s+(.*)",QRegularExpression::CaseInsensitiveOption /* | QRegularExpression::InvertedGreedinessOption */);
QRegularExpression rxaffichcalcul("^AFFICHERCALCUL\\s+(.*)",QRegularExpression::CaseInsensitiveOption /* | QRegularExpression::InvertedGreedinessOption */);
QRegularExpression rxaffichmessagenl("^AFFICHER\\*\\s+\"(.*)\"",QRegularExpression::CaseInsensitiveOption /* | QRegularExpression::InvertedGreedinessOption */);
QRegularExpression rxaffichmessage("^AFFICHER\\s+\"(.*)\"",QRegularExpression::CaseInsensitiveOption /* | QRegularExpression::InvertedGreedinessOption */);
QRegularExpression rxaffichvariablenl("^AFFICHER\\*\\s+(.*)",QRegularExpression::CaseInsensitiveOption /* | QRegularExpression::InvertedGreedinessOption */);
QRegularExpression rxaffichvariable("^AFFICHER\\s+(.*)",QRegularExpression::CaseInsensitiveOption /* | QRegularExpression::InvertedGreedinessOption */);
QRegularExpression rxaffectation("^(.*)\\s+PREND_LA_VALEUR\\s+(.*)",QRegularExpression::CaseInsensitiveOption /* | QRegularExpression::InvertedGreedinessOption */);
QRegularExpression rxpointcouleur("^TRACER_POINT_(Rouge|Vert|Bleu|Blanc)\\s+\\((.*),(.*)\\)",QRegularExpression::CaseInsensitiveOption /* | QRegularExpression::InvertedGreedinessOption */);
QRegularExpression rxpoint("^TRACER_POINT\\s+\\((.*),(.*)\\)",QRegularExpression::CaseInsensitiveOption /* | QRegularExpression::InvertedGreedinessOption */);
QRegularExpression rxsegmentcouleur("^TRACER_SEGMENT_(Rouge|Vert|Bleu|Blanc)\\s+\\((.*),(.*)\\)\\s*\\->\\s*\\((.*),(.*)\\)",QRegularExpression::CaseInsensitiveOption /* | QRegularExpression::InvertedGreedinessOption */);
QRegularExpression rxsegment("^TRACER_SEGMENT\\s+\\((.*),(.*)\\)\\s*\\->\\s*\\((.*),(.*)\\)",QRegularExpression::CaseInsensitiveOption /* | QRegularExpression::InvertedGreedinessOption */);
QRegularExpression rxeffacer("^EFFACER_GRAPHIQUE",QRegularExpression::CaseInsensitiveOption /* | QRegularExpression::InvertedGreedinessOption */);
QRegularExpression rxtantque("^TANT_QUE\\s+\\((.*)\\)\\s+FAIRE",QRegularExpression::CaseInsensitiveOption /* | QRegularExpression::InvertedGreedinessOption */);
QRegularExpression rxdebuttantque("^DEBUT_TANT_QUE",QRegularExpression::CaseInsensitiveOption /* | QRegularExpression::InvertedGreedinessOption */);
QRegularExpression rxfintantque("^FIN_TANT_QUE",QRegularExpression::CaseInsensitiveOption /* | QRegularExpression::InvertedGreedinessOption */);
QRegularExpression rxpour("^POUR\\s+(.*)\\s+ALLANT_DE\\s+(.*)\\s+A\\s+(.*)",QRegularExpression::CaseInsensitiveOption /* | QRegularExpression::InvertedGreedinessOption */);
QRegularExpression rxdebutpour("^DEBUT_POUR",QRegularExpression::CaseInsensitiveOption /* | QRegularExpression::InvertedGreedinessOption */);
QRegularExpression rxfinpour("^FIN_POUR",QRegularExpression::CaseInsensitiveOption /* | QRegularExpression::InvertedGreedinessOption */);
QRegularExpression rxsi("^SI\\s+\\((.*)\\)\\s+ALORS",QRegularExpression::CaseInsensitiveOption /* | QRegularExpression::InvertedGreedinessOption */);
QRegularExpression rxdebutsi("^DEBUT_SI",QRegularExpression::CaseInsensitiveOption /* | QRegularExpression::InvertedGreedinessOption */);
QRegularExpression rxfinsi("^FIN_SI",QRegularExpression::CaseInsensitiveOption /* | QRegularExpression::InvertedGreedinessOption */);
QRegularExpression rxsinon("^SINON",QRegularExpression::CaseInsensitiveOption /* | QRegularExpression::InvertedGreedinessOption */);
QRegularExpression rxdebutsinon("^DEBUT_SINON",QRegularExpression::CaseInsensitiveOption /* | QRegularExpression::InvertedGreedinessOption */);
QRegularExpression rxfinsinon("^FIN_SINON",QRegularExpression::CaseInsensitiveOption /* | QRegularExpression::InvertedGreedinessOption */);

QRegularExpression rxdebutvar("^VARIABLES(?!_)",QRegularExpression::CaseInsensitiveOption /* | QRegularExpression::InvertedGreedinessOption */);
QRegularExpression rxdebut("^DEBUT_ALGORITHME",QRegularExpression::CaseInsensitiveOption /* | QRegularExpression::InvertedGreedinessOption */);
QRegularExpression rxfin("^FIN_ALGORITHME",QRegularExpression::CaseInsensitiveOption /* | QRegularExpression::InvertedGreedinessOption */);

QRegularExpression rxvarfcts("^VARIABLES_FONCTION",QRegularExpression::CaseInsensitiveOption /* | QRegularExpression::InvertedGreedinessOption */);
QRegularExpression rxfonction("^FONCTION\\s+(.*)\\((.*)\\)",QRegularExpression::CaseInsensitiveOption /* | QRegularExpression::InvertedGreedinessOption */);
QRegularExpression rxdebutfonction("^DEBUT_FONCTION",QRegularExpression::CaseInsensitiveOption /* | QRegularExpression::InvertedGreedinessOption */);
QRegularExpression rxfinfonction("^FIN_FONCTION",QRegularExpression::CaseInsensitiveOption /* | QRegularExpression::InvertedGreedinessOption */);
QRegularExpression rxrenvoyer("^RENVOYER\\s+(.*)",QRegularExpression::CaseInsensitiveOption /* | QRegularExpression::InvertedGreedinessOption */);
QRegularExpression rxappelfct("^APPELER_FONCTION\\s+(.*)",QRegularExpression::CaseInsensitiveOption /* | QRegularExpression::InvertedGreedinessOption */);

QRegularExpression rxerreurligne("ligne ([0-9]+)");

QTreeWidgetItem *curItem=0;
QTreeWidgetItem *newItem=0;

while (!in.atEnd()) 
  {
  ligne=in.readLine();
  i++;
  ligne=ligne.remove(rxnumline);
  ligne=ligne.trimmed();
  if (ligne.contains(QString(QChar(0x2022))))
    {
    erreur=true;
    message_erreur=QString::fromUtf8("Le code contient un champ • non complété")+" (ligne "+QString::number(i)+")";
    break;    
    }
  else if (/*(rxfixe.indexIn(ligne)<0) &&*/ (!ligne.isEmpty())) 
    {
    liste_Lignes.append(ligne);
    num_Lignes.append(QString::number(i));
    }
  }
int numligne=0;
while (numligne < liste_Lignes.count() && (!rxdebutvar.match(ligne).hasMatch()))
//while (numligne < liste_Lignes.count() && (rxdebutvar.indexIn(ligne)<0))
{
ligne=liste_Lignes.at(numligne);
numligne++;    
}
while (numligne < liste_Lignes.count() && (!rxfin.match(ligne).hasMatch()))
{ 
ligne=liste_Lignes.at(numligne);
QRegularExpressionMatch rxvarMatch=rxvar.match(ligne);
  if (rxvarMatch.hasMatch())
    {
    nomvariable=rxvarMatch.captured(1);
    type=rxvarMatch.captured(2);
    nomvariable=FiltreNomVariable(nomvariable);
    if (NomInterdit(nomvariable))
	{
	erreur=true;
	message_erreur=QString::fromUtf8("Nom de variable interdit")+" (ligne "+QString::number(i)+")";
	break;
	}
    if ((type!="NOMBRE") && (type!="CHAINE") && (type!="LISTE"))
	{
	erreur=true;
	message_erreur=QString::fromUtf8("Type de variable non reconnu")+" (ligne "+QString::number(i)+")";
	break;
	}
    newItem =new QTreeWidgetItem(variablesItem);
    newItem->setText(0,nomvariable+" EST_DU_TYPE "+type);
    code="1#"+type+"#"+nomvariable;
    newItem->setData(0,Qt::UserRole,QString(code));
    }
numligne++;
}
if (erreur)
  {
  EffaceArbre();
  ui.EditorView->editor->setCursorPosition(i-1 , 0);
  return message_erreur;
  }
ActualiserVariables();
/*****************************************/

numligne=0;
QFont commentFont = qApp->font();
commentFont.setItalic(true);  

ui.treeWidget->setCurrentItem(fctsItem);
curItem = ui.treeWidget->currentItem();  

QTreeWidgetItem *curFonctionItem=0;
while (numligne < liste_Lignes.count() && (!rxdebutvar.match(ligne).hasMatch()))
{
ligne=liste_Lignes.at(numligne);
if (!(rxfixe.match(ligne).hasMatch()))
{
QRegularExpressionMatch rxvarMatch=rxvar.match(ligne);
QRegularExpressionMatch rxpauseMatch=rxpause.match(ligne);
QRegularExpressionMatch rxvarfctsMatch=rxvarfcts.match(ligne);
QRegularExpressionMatch rxcommentaireMatch=rxcommentaire.match(ligne);
QRegularExpressionMatch rxrenvoyerMatch=rxrenvoyer.match(ligne);
QRegularExpressionMatch rxappelfctMatch=rxappelfct.match(ligne);
QRegularExpressionMatch rxlireMatch=rxlire.match(ligne);
QRegularExpressionMatch rxaffichcalculnlMatch=rxaffichcalculnl.match(ligne);
QRegularExpressionMatch rxaffichcalculMatch=rxaffichcalcul.match(ligne);
QRegularExpressionMatch rxaffichmessagenlMatch=rxaffichmessagenl.match(ligne);
QRegularExpressionMatch rxaffichmessageMatch=rxaffichmessage.match(ligne);
QRegularExpressionMatch rxaffichvariablenlMatch=rxaffichvariablenl.match(ligne);
QRegularExpressionMatch rxaffichvariableMatch=rxaffichvariable.match(ligne);
QRegularExpressionMatch rxaffectationMatch=rxaffectation.match(ligne);
QRegularExpressionMatch rxpointcouleurMatch=rxpointcouleur.match(ligne);
QRegularExpressionMatch rxpointMatch=rxpoint.match(ligne);
QRegularExpressionMatch rxsegmentcouleurMatch=rxsegmentcouleur.match(ligne);
QRegularExpressionMatch rxsegmentMatch=rxsegment.match(ligne);
QRegularExpressionMatch rxeffacerMatch=rxeffacer.match(ligne);
QRegularExpressionMatch rxtantqueMatch=rxtantque.match(ligne);
QRegularExpressionMatch rxdebuttantqueMatch=rxdebuttantque.match(ligne);
QRegularExpressionMatch rxfintantqueMatch=rxfintantque.match(ligne);
QRegularExpressionMatch rxpourMatch=rxpour.match(ligne);
QRegularExpressionMatch rxdebutpourMatch=rxdebutpour.match(ligne);
QRegularExpressionMatch rxfinpourMatch=rxfinpour.match(ligne);
QRegularExpressionMatch rxsinonMatch=rxsinon.match(ligne);
QRegularExpressionMatch rxdebutsinonMatch=rxdebutsinon.match(ligne);
QRegularExpressionMatch rxfinsinonMatch=rxfinsinon.match(ligne);
QRegularExpressionMatch rxsiMatch=rxsi.match(ligne);
QRegularExpressionMatch rxdebutsiMatch=rxdebutsi.match(ligne);
QRegularExpressionMatch rxfinsiMatch=rxfinsi.match(ligne);
QRegularExpressionMatch rxfonctionMatch=rxfonction.match(ligne);
QRegularExpressionMatch rxdebutfonctionMatch=rxdebutfonction.match(ligne);
QRegularExpressionMatch rxfinfonctionMatch=rxfinfonction.match(ligne);
  if (rxvarMatch.hasMatch())
    {
    nomvariable=rxvarMatch.captured(1);
    type=rxvarMatch.captured(2);
    nomvariable=FiltreNomVariable(nomvariable);
    if (NomInterdit(nomvariable))
	{
	erreur=true;
	message_erreur=QString::fromUtf8("Nom de variable interdit")+" (ligne "+QString::number(i)+")";
	break;
	}
    if ((type!="NOMBRE") && (type!="CHAINE") && (type!="LISTE"))
	{
	erreur=true;
	message_erreur=QString::fromUtf8("Type de variable non reconnu")+" (ligne "+QString::number(i)+")";
	break;
	}
    curItem = ui.treeWidget->currentItem();
    if (curItem && curItem->parent())
      {
      newItem = new QTreeWidgetItem(curItem);
      newItem->setText(0,nomvariable+" EST_DU_TYPE "+type);
      code="1#"+type+"#"+nomvariable;
      newItem->setData(0,Qt::UserRole,QString(code));
      curItem->setExpanded(true);
      ActualiserVariables();
      ActualiserArbre();
      }	
    }
  else if (rxpauseMatch.hasMatch()) //PAUSE
    {
    NouvelleLigne(ui.treeWidget->currentItem());
    curItem = ui.treeWidget->currentItem();
    if (curItem && curItem->parent())
      {
      code="18#pause";
      curItem->setText(0,QString::fromUtf8("PAUSE"));
      curItem->setData(0,Qt::UserRole,QString(code));
      ui.treeWidget->setCurrentItem(curItem);
      ActualiserArbre();
      }
    }
  else if (rxvarfctsMatch.hasMatch()) //VARIABLES_FONCTION
    {
    
    curItem = ui.treeWidget->currentItem();
    if (curItem && curItem->parent())
      {
      newItem = new QTreeWidgetItem(curItem);
      code="202#declarationsvariablesfonction";
      newItem->setText(0,QString::fromUtf8("VARIABLES_FONCTION"));
      newItem->setData(0,Qt::UserRole,QString(code));
      ui.treeWidget->setCurrentItem(newItem);
      ActualiserArbre();
      }
    }
    else if (rxcommentaireMatch.hasMatch()) //COMMENTAIRE
      {
      NouvelleLigne(ui.treeWidget->currentItem());
      curItem = ui.treeWidget->currentItem();
      if (curItem && curItem->parent())
	{
	QString commentaire=rxcommentaireMatch.captured(1);
	commentaire.remove("#");
	if (!commentaire.isEmpty())
	  {
	  curItem->setText(0,"//"+commentaire);
	  code="19#"+commentaire;
	  curItem->setData(0,Qt::UserRole,QString(code));
	  curItem->setFont(0,commentFont);
	  ui.treeWidget->setCurrentItem(curItem);
	  ActualiserArbre();
	  }
	}  
      }
    else if (rxrenvoyerMatch.hasMatch()) //RENVOYER
      {
      NouvelleLigne(ui.treeWidget->currentItem());
      curItem = ui.treeWidget->currentItem();
      if (curItem && curItem->parent())
	{
	QString retour=rxrenvoyerMatch.captured(1);
	retour.remove("#");
	if (!retour.isEmpty())
	  {
	  curItem->setText(0,"RENVOYER "+retour);
	  code="205#"+retour;
	  curItem->setData(0,Qt::UserRole,QString(code));
	  ui.treeWidget->setCurrentItem(curItem);
	  ActualiserArbre();
	  }
	}  
      }
    else if (rxappelfctMatch.hasMatch()) //APPELER_FONCTION
      {
      NouvelleLigne(ui.treeWidget->currentItem());
      curItem = ui.treeWidget->currentItem();
      if (curItem && curItem->parent())
	{
	QString fct=rxappelfctMatch.captured(1);
	fct.remove("#");
	if (!fct.isEmpty())
	  {
	  curItem->setText(0,"APPELER_FONCTION "+fct);
	  code="206#"+fct;
	  curItem->setData(0,Qt::UserRole,QString(code));
	  ui.treeWidget->setCurrentItem(curItem);
	  ActualiserArbre();
	  }
	}  
      }      
    else if (rxlireMatch.hasMatch()) //LIRE
      {
      NouvelleLigne(ui.treeWidget->currentItem());
      curItem = ui.treeWidget->currentItem();
      if (curItem && curItem->parent())
	{
	nomvariable=rxlireMatch.captured(1);
	rang="";
    QRegularExpressionMatch rxlisteMatch=rxliste.match(nomvariable);
	if (rxlisteMatch.hasMatch())
	  {
	  rang=rxlisteMatch.captured(1);
	  nomvariable.remove(rxliste);
	  }
	if (!nomvariable.isEmpty())
	    {
	    i=ListeNomsVariables.indexOf(nomvariable);
	    if (i>-1)
		{
		type=ListeTypesVariables.at(i);
		if ((type=="LISTE") && (!rang.isEmpty())) 
		    {
		    curItem->setText(0,QString::fromUtf8("LIRE ")+nomvariable+"["+rang+"]");
		    code="2#"+nomvariable+"#"+rang;
		    }
		else 
		    {
		    curItem->setText(0,QString::fromUtf8("LIRE ")+nomvariable);
		    code="2#"+nomvariable+"#pasliste";
		    }
		curItem->setData(0,Qt::UserRole,QString(code));
		}
	    else
		{
		erreur=true;
		message_erreur=QString::fromUtf8("Variable non déclarée.")+" (ligne "+num_Lignes.at(numligne)+")";
		break;
		}
	    ui.treeWidget->setCurrentItem(curItem);
	    ActualiserArbre();
	    }
	  else
	    {
	    erreur=true;
	    message_erreur=QString::fromUtf8("Nom de variable non reconnu.")+" (ligne "+num_Lignes.at(numligne)+")";
	    break;
	    }
	}
      }
    else if (rxaffichcalculnlMatch.hasMatch()) //AFFICHER CALCUL NL
      {
      NouvelleLigne(ui.treeWidget->currentItem());
      curItem = ui.treeWidget->currentItem();
      if (curItem && curItem->parent())
	{      
	calcul=rxaffichcalculnlMatch.captured(1);
	calcul.remove("#");
	//calcul.remove("\"");
	code="";
	if (!calcul.isEmpty())
	    {
		    curItem->setText(0,QString::fromUtf8("AFFICHERCALCUL ")+calcul);
		    code="20#"+calcul+"#1";
		    curItem->setData(0,Qt::UserRole,QString(code));
		    ui.treeWidget->setCurrentItem(curItem);
		    ActualiserArbre();
	    }
	else
	    {
 	    erreur=true;
 	    message_erreur=QString::fromUtf8("Calcul vide.")+" (ligne "+num_Lignes.at(numligne)+")";
 	    break;
	    }      
      
      
// 	message.remove("#");
// 	message.remove("\"");
// 	code="";
// 	if (!message.isEmpty())
// 	    {
// 	    curItem->setText(0,QString::fromUtf8("AFFICHER \"")+message+"\"");
// 	    code="4#"+message+"#1";
// 	    curItem->setData(0,Qt::UserRole,QString(code));
// 	    ui.treeWidget->setCurrentItem(curItem);
// 	    ActualiserArbre();
// 	    }
// 	else
// 	    {
// 	    erreur=true;
// 	    message_erreur=QString::fromUtf8("Message vide.")+" (ligne "+num_Lignes.at(numligne)+")";
// 	    break;
// 	    }
	}
      }	  
    else if (rxaffichcalculMatch.hasMatch()) //AFFICHER CALCUL
      {
      NouvelleLigne(ui.treeWidget->currentItem());
      curItem = ui.treeWidget->currentItem();
      if (curItem && curItem->parent())
	{
	calcul=rxaffichcalculMatch.captured(1);

	calcul.remove("#");
	//calcul.remove("\"");
	code="";
	if (!calcul.isEmpty())
	  {
		  curItem->setText(0,QString::fromUtf8("AFFICHERCALCUL ")+calcul);
		  code="20#"+calcul+"#0";
		  curItem->setData(0,Qt::UserRole,QString(code));
		  ui.treeWidget->setCurrentItem(curItem);
		  ActualiserArbre();
	  }
	else
	  {
 	    erreur=true;
 	    message_erreur=QString::fromUtf8("Calcul vide.")+" (ligne "+num_Lignes.at(numligne)+")";
 	    break;
	  }      
// 	message=rxaffichmessage.cap(1);
// 	message.remove("#");
// 	message.remove("\"");
// 	code="";
// 	if (!message.isEmpty())
// 	    {
// 	    curItem->setText(0,QString::fromUtf8("AFFICHER \"")+message+"\"");
// 	    code="4#"+message+"#0";
// 	    curItem->setData(0,Qt::UserRole,QString(code));
// 	    ui.treeWidget->setCurrentItem(curItem);
// 	    ActualiserArbre();
// 	    }
// 	else
// 	    {
// 	    erreur=true;
// 	    message_erreur=QString::fromUtf8("Message vide.")+" (ligne "+num_Lignes.at(numligne)+")";
// 	    break;
// 	    }
	}
      }
    else if (rxaffichmessagenlMatch.hasMatch()) //AFFICHER MESSAGE NL
      {
      NouvelleLigne(ui.treeWidget->currentItem());
      curItem = ui.treeWidget->currentItem();
      if (curItem && curItem->parent())
	{      
	message=rxaffichmessagenlMatch.captured(1);
	message.remove("#");
	message.remove("\"");
	code="";
	if (!message.isEmpty())
	    {
	    curItem->setText(0,QString::fromUtf8("AFFICHER \"")+message+"\"");
	    code="4#"+message+"#1";
	    curItem->setData(0,Qt::UserRole,QString(code));
	    ui.treeWidget->setCurrentItem(curItem);
	    ActualiserArbre();
	    }
	else
	    {
	    erreur=true;
	    message_erreur=QString::fromUtf8("Message vide.")+" (ligne "+num_Lignes.at(numligne)+")";
	    break;
	    }
	}
      }	  
    else if (rxaffichmessageMatch.hasMatch()) //AFFICHER MESSAGE
      {
      NouvelleLigne(ui.treeWidget->currentItem());
      curItem = ui.treeWidget->currentItem();
      if (curItem && curItem->parent())
	{      
	message=rxaffichmessageMatch.captured(1);
	message.remove("#");
	message.remove("\"");
	code="";
	if (!message.isEmpty())
	    {
	    curItem->setText(0,QString::fromUtf8("AFFICHER \"")+message+"\"");
	    code="4#"+message+"#0";
	    curItem->setData(0,Qt::UserRole,QString(code));
	    ui.treeWidget->setCurrentItem(curItem);
	    ActualiserArbre();
	    }
	else
	    {
	    erreur=true;
	    message_erreur=QString::fromUtf8("Message vide.")+" (ligne "+num_Lignes.at(numligne)+")";
	    break;
	    }
	}
      }
    else if (rxaffichvariablenlMatch.hasMatch()) //AFFICHER VARIABLE NL
      {
      NouvelleLigne(ui.treeWidget->currentItem());
      curItem = ui.treeWidget->currentItem();
      if (curItem && curItem->parent())
	{      
	nomvariable=rxaffichvariablenlMatch.captured(1);
	rang="";
    QRegularExpressionMatch rxlisteMatch=rxliste.match(nomvariable);
	if (rxlisteMatch.hasMatch())
	  {
	  rang=rxlisteMatch.captured(1);
	  nomvariable.remove(rxliste);
	  }
	code="";
	if (!nomvariable.isEmpty())
	    {
	    i=ListeNomsVariables.indexOf(nomvariable);
	    if (i>-1)
		{
		QString type=ListeTypesVariables.at(i);
		code="3#"+nomvariable+"#1";
		if ((type=="LISTE") && (!rang.isEmpty())) 
		    {
		    curItem->setText(0,QString::fromUtf8("AFFICHER ")+nomvariable+"["+rang+"]");
		    code+="#"+rang;
		    }
		else 
		    {
		    curItem->setText(0,QString::fromUtf8("AFFICHER ")+nomvariable);
		    code+="#pasliste";
		    }
		curItem->setData(0,Qt::UserRole,QString(code));
		}
	    else
		{
		erreur=true;
		message_erreur=QString::fromUtf8("Variable non déclarée.")+" (ligne "+num_Lignes.at(numligne)+")";
		break;
		}
	    ui.treeWidget->setCurrentItem(curItem);
	    ActualiserArbre();
	    }
	  else
	    {
	    erreur=true;
	    message_erreur=QString::fromUtf8("Nom de variable non reconnu.")+" (ligne "+num_Lignes.at(numligne)+")";
	    break;
	    }
	}
      }	  
    else if (rxaffichvariableMatch.hasMatch()) //AFFICHER VARIABLE
      {
      NouvelleLigne(ui.treeWidget->currentItem());
      curItem = ui.treeWidget->currentItem();
      if (curItem && curItem->parent())
	{      
	nomvariable=rxaffichvariableMatch.captured(1);
	rang="";
    QRegularExpressionMatch rxlisteMatch=rxliste.match(nomvariable);
	if (rxlisteMatch.hasMatch())
	  {
	  rang=rxlisteMatch.captured(1);
	  nomvariable.remove(rxliste);
	  }
	code="";
	if (!nomvariable.isEmpty())
	    {
	    i=ListeNomsVariables.indexOf(nomvariable);
	    if (i>-1)
		{
		QString type=ListeTypesVariables.at(i);
		code="3#"+nomvariable+"#0";
		if ((type=="LISTE") && (!rang.isEmpty())) 
		    {
		    curItem->setText(0,QString::fromUtf8("AFFICHER ")+nomvariable+"["+rang+"]");
		    code+="#"+rang;
		    }
		else 
		    {
		    curItem->setText(0,QString::fromUtf8("AFFICHER ")+nomvariable);
		    code+="#pasliste";
		    }
		curItem->setData(0,Qt::UserRole,QString(code));
		}
	    else
		{
		erreur=true;
		message_erreur=QString::fromUtf8("Variable non déclarée.")+" (ligne "+num_Lignes.at(numligne)+")";
		break;
		}
	    ui.treeWidget->setCurrentItem(curItem);
	    ActualiserArbre();
	    }
	  else
	    {
	    erreur=true;
	    message_erreur=QString::fromUtf8("Nom de variable non reconnu.")+" (ligne "+num_Lignes.at(numligne)+")";
	    break;
	    }
	}
      }	  
    else if (rxaffectationMatch.hasMatch()) //AFFECTATION
      {
      NouvelleLigne(ui.treeWidget->currentItem());
      curItem = ui.treeWidget->currentItem();
      if (curItem && curItem->parent())
	{
	if (ListeNomsVariables.isEmpty())
	    {
	    erreur=true;
	    message_erreur=QString::fromUtf8("Aucune variable n'a encore été définie.")+" (ligne "+num_Lignes.at(numligne)+")";
	    break;
	    }
	nomvariable=rxaffectationMatch.captured(1);
	contenu=rxaffectationMatch.captured(2);
	rang="";
    QRegularExpressionMatch rxlisteMatch=rxliste.match(nomvariable);
	if (rxlisteMatch.hasMatch())
	  {
	  rang=rxlisteMatch.captured(1);
	  nomvariable.remove(rxliste);
	  }
	contenu.remove("#");
	code="";
	if (!nomvariable.isEmpty())
	    {
	    if (!contenu.isEmpty())
		{
		i=ListeNomsVariables.indexOf(nomvariable);
		if (i>-1)
		    {
		    type=ListeTypesVariables.at(i);
		    code="5#"+nomvariable+"#"+contenu;
		    if ((type=="LISTE") && (!rang.isEmpty())) 
			{
			curItem->setText(0,nomvariable+"["+rang+"]"+QString::fromUtf8(" PREND_LA_VALEUR ")+contenu);
			code+="#"+rang;
			}
		    else 
			{
			curItem->setText(0,nomvariable+QString::fromUtf8(" PREND_LA_VALEUR ")+contenu);
			code+="#pasliste";
			}
		    curItem->setData(0,Qt::UserRole,QString(code));
		    }
		else
		    {
		    erreur=true;
		    message_erreur=QString::fromUtf8("Variable non déclarée.")+" (ligne "+num_Lignes.at(numligne)+")";
		    break;
		    }
		                 
		ui.treeWidget->setCurrentItem(curItem);
		ActualiserArbre();
		}
	    else
		{
		erreur=true;
		message_erreur=QString::fromUtf8("Affectation vide")+" (ligne "+num_Lignes.at(numligne)+")";
		break;
		}
	    }
	else
	    {
	    erreur=true;
	    message_erreur=QString::fromUtf8("Pas de variable définie")+" (ligne "+num_Lignes.at(numligne)+")";
	    break;
	    }	  
	}
      }
    else if (rxpointcouleurMatch.hasMatch()) //POINT COULEUR
      {
      NouvelleLigne(ui.treeWidget->currentItem());
      curItem = ui.treeWidget->currentItem();
      if (curItem && curItem->parent())
	{
	couleur=rxpointcouleurMatch.captured(1);
	x=rxpointcouleurMatch.captured(2);
	y=rxpointcouleurMatch.captured(3);
	couleur.remove("#");
	x.remove("#");
	y.remove("#");
	if ((!x.isEmpty()) && (!y.isEmpty()))
	    {
	    curItem->setText(0,QString::fromUtf8("TRACER_POINT (")+x+","+y+")");
	    code="50#"+x+"#"+y+"#"+couleur;
	    curItem->setData(0,Qt::UserRole,QString(code));
	    ui.treeWidget->setCurrentItem(curItem);
	    ActualiserArbre();
	    }
	else
	    {
	    erreur=true;
	    message_erreur=QString::fromUtf8("Au moins une des coordonnées n'est pas définie")+" (ligne "+num_Lignes.at(numligne)+")";
	    break;
	    }
	}
      }
    else if (rxpointMatch.hasMatch()) //POINT
      {
      NouvelleLigne(ui.treeWidget->currentItem());
      curItem = ui.treeWidget->currentItem();
      if (curItem && curItem->parent())
	{   
	x=rxpointMatch.captured(1);
	y=rxpointMatch.captured(2);
	couleur="Rouge"; 
	x.remove("#");
	y.remove("#");
	if ((!x.isEmpty()) && (!y.isEmpty()))
	    {
	    curItem->setText(0,QString::fromUtf8("TRACER_POINT (")+x+","+y+")");
	    code="50#"+x+"#"+y+"#"+couleur;
	    curItem->setData(0,Qt::UserRole,QString(code));
	    ui.treeWidget->setCurrentItem(curItem);
	    ActualiserArbre();
	    }
	else
	    {
	    erreur=true;
	    message_erreur=QString::fromUtf8("Au moins une des coordonnées n'est pas définie")+" (ligne "+num_Lignes.at(numligne)+")";
	    break;
	    }
	}
      }
    else if (rxsegmentcouleurMatch.hasMatch()) //SEGMENT COULEUR
      {
      NouvelleLigne(ui.treeWidget->currentItem());
      curItem = ui.treeWidget->currentItem();
      if (curItem && curItem->parent())
	{
	couleur=rxsegmentcouleurMatch.captured(1);
	xdep=rxsegmentcouleurMatch.captured(2);
	ydep=rxsegmentcouleurMatch.captured(3);
	xfin=rxsegmentcouleurMatch.captured(4);
	yfin=rxsegmentcouleurMatch.captured(5);
	couleur.remove("#");
	xdep.remove("#");
	ydep.remove("#");
	xfin.remove("#");
	yfin.remove("#");
	if ((!xdep.isEmpty()) && (!ydep.isEmpty()) && (!xfin.isEmpty()) && (!yfin.isEmpty()))
	    {
	    curItem->setText(0,QString::fromUtf8("TRACER_SEGMENT (")+xdep+","+ydep+")->("+xfin+","+yfin+")");
	    code="51#"+xdep+"#"+ydep+"#"+xfin+"#"+yfin+"#"+couleur;
	    curItem->setData(0,Qt::UserRole,QString(code));
	    ui.treeWidget->setCurrentItem(curItem);
	    ActualiserArbre();
	    }
	else
	    {
	    erreur=true;
	    message_erreur=QString::fromUtf8("Au moins une des coordonnées n'est pas définie")+" (ligne "+num_Lignes.at(numligne)+")";
	    break;
	    }      
	}
      }
    else if (rxsegmentMatch.hasMatch()) //SEGMENT
      {
      NouvelleLigne(ui.treeWidget->currentItem());
      curItem = ui.treeWidget->currentItem();
      if (curItem && curItem->parent())
	{         
	xdep=rxsegmentMatch.captured(1);
	ydep=rxsegmentMatch.captured(2);
	xfin=rxsegmentMatch.captured(3);
	yfin=rxsegmentMatch.captured(4);
	couleur="Rouge"; 
	xdep.remove("#");
	ydep.remove("#");
	xfin.remove("#");
	yfin.remove("#");
	if ((!xdep.isEmpty()) && (!ydep.isEmpty()) && (!xfin.isEmpty()) && (!yfin.isEmpty()))
	    {
	    curItem->setText(0,QString::fromUtf8("TRACER_SEGMENT (")+xdep+","+ydep+")->("+xfin+","+yfin+")");
	    code="51#"+xdep+"#"+ydep+"#"+xfin+"#"+yfin+"#"+couleur;
	    curItem->setData(0,Qt::UserRole,QString(code));
	    ui.treeWidget->setCurrentItem(curItem);
	    ActualiserArbre();
	    }
	else
	    {
	    erreur=true;
	    message_erreur=QString::fromUtf8("Au moins une des coordonnées n'est pas définie")+" (ligne "+num_Lignes.at(numligne)+")";
	    break;
	    }      
	}
      }
  else if (rxeffacerMatch.hasMatch()) //EFFACER
    {
    NouvelleLigne(ui.treeWidget->currentItem());
    curItem = ui.treeWidget->currentItem();
    if (curItem && curItem->parent())
      {
      code="52#effacer";
      curItem->setText(0,QString::fromUtf8("EFFACER_GRAPHIQUE"));
      curItem->setData(0,Qt::UserRole,QString(code));
      ui.treeWidget->setCurrentItem(curItem);
      ActualiserArbre();
      }
    }
    else if (rxtantqueMatch.hasMatch()) //TANT_QUE
      {
      NouvelleLigne(ui.treeWidget->currentItem());
      curItem = ui.treeWidget->currentItem();
      if (curItem && curItem->parent())
	{
	condition=rxtantqueMatch.captured(1);
	condition.remove("#");
	code="";
	if (!condition.isEmpty())
	    {
	    curItem->setText(0,QString::fromUtf8("TANT_QUE (")+condition+") "+QString::fromUtf8("FAIRE"));
	    code="15#"+condition;
	    curItem->setData(0,Qt::UserRole,QString(code));
	    ActualiserArbre();
	    }
	else
	    {
	    erreur=true;
	    message_erreur=QString::fromUtf8("Pas de condition définie")+" (ligne "+num_Lignes.at(numligne)+")";
	    break;
	    }
	}
      }
    else if (rxdebuttantqueMatch.hasMatch()) //DEBUT_TANT_QUE
      {
      curItem = ui.treeWidget->currentItem();
      if (curItem && curItem->parent())
	{
	newItem = new QTreeWidgetItem(curItem);
	newItem->setText(0,QString::fromUtf8("DEBUT_TANT_QUE"));
	newItem->setData(0,Qt::UserRole,QString("16#debuttantque"));
    curItem->setExpanded(true);
	ui.treeWidget->setCurrentItem(newItem);
	ActualiserArbre();
	}
      }
    else if (rxfintantqueMatch.hasMatch()) //FIN_TANT_QUE
      {
      NouvelleLigne(ui.treeWidget->currentItem());
      curItem = ui.treeWidget->currentItem();
      if (curItem && curItem->parent())
	{
	    curItem->setText(0,QString::fromUtf8("FIN_TANT_QUE"));
	    curItem->setData(0,Qt::UserRole,QString("17#fintantque"));
	    ActualiserArbre();
	}
      }
    else if (rxpourMatch.hasMatch()) //POUR
      {
      NouvelleLigne(ui.treeWidget->currentItem());
      curItem = ui.treeWidget->currentItem();
      if (curItem && curItem->parent())
	{
	nomvariable=rxpourMatch.captured(1);
	debut=rxpourMatch.captured(2);
	debut.remove("#");
	fin=rxpourMatch.captured(3);
	fin.remove("#");
	code="";
	if (!nomvariable.isEmpty())
	    {
	    if (!debut.isEmpty() && !fin.isEmpty())
		{
		i=ListeNomsVariables.indexOf(nomvariable);
		if (i>-1)
		    {
		    curItem->setText(0,QString::fromUtf8("POUR ")+nomvariable+" ALLANT_DE "+debut+ " A "+fin);
		    code="12#"+nomvariable+"#"+debut+"#"+fin;
		    curItem->setData(0,Qt::UserRole,QString(code));
		    }
		else
		    {
		    erreur=true;
		    message_erreur=QString::fromUtf8("Variable non déclarée.")+" (ligne "+num_Lignes.at(numligne)+")";
		    break;
		    }
		ActualiserArbre();
		}
	    else
		{
		erreur=true;
		message_erreur=QString::fromUtf8("Les valeurs minimales et maximales du compteur ne sont pas définies")+" (ligne "+num_Lignes.at(numligne)+")";
		break;
		}
	    }
	else
	    {
	    erreur=true;
	    message_erreur=QString::fromUtf8("Pas de variable définie")+" (ligne "+num_Lignes.at(numligne)+")";
	    break;
	    }
	}
      }
    else if (rxdebutpourMatch.hasMatch()) //DEBUT_POUR
      {
      curItem = ui.treeWidget->currentItem();
      if (curItem && curItem->parent())
	{
	newItem = new QTreeWidgetItem(curItem);
	newItem->setText(0,QString::fromUtf8("DEBUT_POUR"));
	newItem->setData(0,Qt::UserRole,QString("13#debutpour"));
    curItem->setExpanded(true);
	ui.treeWidget->setCurrentItem(newItem);
	ActualiserArbre();
	}
      }
    else if (rxfinpourMatch.hasMatch()) //FIN_POUR
      {
      NouvelleLigne(ui.treeWidget->currentItem());
      curItem = ui.treeWidget->currentItem();
      if (curItem && curItem->parent())
	{
	    curItem->setText(0,QString::fromUtf8("FIN_POUR"));
	    curItem->setData(0,Qt::UserRole,QString("14#finpour"));
	    ActualiserArbre();
	}
      }
    else if (rxsinonMatch.hasMatch()) //SINON
      {
      curItem = ui.treeWidget->currentItem();
      if (curItem && curItem->parent())
	{
	  newItem = new QTreeWidgetItem(curItem->parent(), curItem);
	  newItem->setText(0,QString::fromUtf8("SINON"));
	  code="9#sinon";
	  newItem->setData(0,Qt::UserRole,QString(code));
	  ui.treeWidget->setCurrentItem(newItem);
	  ActualiserArbre();
	}
      }
    else if (rxdebutsinonMatch.hasMatch()) //DEBUT_SINON
      {
      curItem = ui.treeWidget->currentItem();
      if (curItem && curItem->parent())
	{
	newItem = new QTreeWidgetItem(curItem);
	newItem->setText(0,QString::fromUtf8("DEBUT_SINON"));
	newItem->setData(0,Qt::UserRole,QString("10#debutsinon"));
    curItem->setExpanded(true);
	ui.treeWidget->setCurrentItem(newItem);
	ActualiserArbre();
	}
      }
    else if (rxfinsinonMatch.hasMatch()) //FIN_SINON
      {
      NouvelleLigne(ui.treeWidget->currentItem());
      curItem = ui.treeWidget->currentItem();
      if (curItem && curItem->parent())
	{
	curItem->setText(0,QString::fromUtf8("FIN_SINON"));
	curItem->setData(0,Qt::UserRole,QString("11#finsinon"));
	ActualiserArbre();
	}
      }      
    else if (rxsiMatch.hasMatch()) //SI
      {
      NouvelleLigne(ui.treeWidget->currentItem());
      curItem = ui.treeWidget->currentItem();
      if (curItem && curItem->parent())
	{
	condition=rxsiMatch.captured(1);
	condition.remove("#");
	code="";
	if (!condition.isEmpty())
	    {
	    curItem->setText(0,QString::fromUtf8("SI (")+condition+") "+QString::fromUtf8("ALORS"));
	    code="6#"+condition;
	    curItem->setData(0,Qt::UserRole,QString(code));
	    ActualiserArbre();
	    }
	else
	    {
	    erreur=true;
	    message_erreur=QString::fromUtf8("Pas de condition définie")+" (ligne "+num_Lignes.at(numligne)+")";
	    break;
	    }
	}
      }
    else if (rxdebutsiMatch.hasMatch()) //DEBUT_SI
      {
      curItem = ui.treeWidget->currentItem();
      if (curItem && curItem->parent())
	{
	newItem = new QTreeWidgetItem(curItem);
	newItem->setText(0,QString::fromUtf8("DEBUT_SI"));
	newItem->setData(0,Qt::UserRole,QString("7#debutsi"));
    curItem->setExpanded(true);
	ui.treeWidget->setCurrentItem(newItem);
	ActualiserArbre();
	}
      }
    else if (rxfinsiMatch.hasMatch()) //FIN_SI
      {
      NouvelleLigne(ui.treeWidget->currentItem());
      curItem = ui.treeWidget->currentItem();
      if (curItem && curItem->parent())
	{
	curItem->setText(0,QString::fromUtf8("FIN_SI"));
	curItem->setData(0,Qt::UserRole,QString("8#finsi"));
	ActualiserArbre();
	}
      }
else if (rxfonctionMatch.hasMatch()) //FONCTION
      {
    QString nomfct=rxfonctionMatch.captured(1);
    QString paramfct=rxfonctionMatch.captured(2);
	if (!nomfct.isEmpty())
	    {
        if (paramfct.isEmpty()) paramfct=QString(" ");
        newItem =new QTreeWidgetItem(fctsItem);
        newItem->setText(0,QString::fromUtf8("FONCTION ")+nomfct+"("+paramfct+")");
	    code="201#"+nomfct+"#"+paramfct;
	    newItem->setData(0,Qt::UserRole,QString(code));
	    ui.treeWidget->setCurrentItem(newItem);
        curFonctionItem=newItem;
	    ActualiserArbre();
	    }
	else
	    {
	    erreur=true;
	    message_erreur=QString::fromUtf8("Déclaration de fonction incomplète")+" (ligne "+num_Lignes.at(numligne)+")";
	    break;
	    }
	}
    else if (rxdebutfonctionMatch.hasMatch()) //DEBUT_FONCTION
      {
      if (curFonctionItem)
        {
        newItem =new QTreeWidgetItem(curFonctionItem);
        newItem->setText(0,QString::fromUtf8("DEBUT_FONCTION"));
        newItem->setData(0,Qt::UserRole,QString("203#debutfonction"));
        ui.treeWidget->setCurrentItem(newItem);
        curFonctionItem->setExpanded(true);
        ActualiserArbre();
        }
      else
	    {
	    erreur=true;
	    message_erreur=QString::fromUtf8("Pas de fonction correspondante")+" (ligne "+num_Lignes.at(numligne)+")";
	    break;
	    }
      }
    else if (rxfinfonctionMatch.hasMatch()) //FIN_FONCTION
      {
      NouvelleLigne(ui.treeWidget->currentItem());
      curItem = ui.treeWidget->currentItem();
      if (curItem && curItem->parent())
        {
        curItem->setText(0,QString::fromUtf8("FIN_FONCTION"));
        curItem->setData(0,Qt::UserRole,QString("204#finfonction"));
        ActualiserArbre();
        }
      }

    else
      {
      erreur=true;
      message_erreur=QString::fromUtf8("Instruction non reconnue")+" (ligne "+num_Lignes.at(numligne)+")";
      break;      
      }        
}
numligne++;
}
/*****************************************/
ui.treeWidget->setCurrentItem(debutItem);
curItem = ui.treeWidget->currentItem();

while (numligne < liste_Lignes.count() && (!rxdebut.match(ligne).hasMatch()))
{
ligne=liste_Lignes.at(numligne);
numligne++;    
}
while (numligne < liste_Lignes.count() && (!rxfin.match(ligne).hasMatch()))
{ 
ligne=liste_Lignes.at(numligne);

QRegularExpressionMatch rxpauseMatch=rxpause.match(ligne);
QRegularExpressionMatch rxvarfctsMatch=rxvarfcts.match(ligne);
QRegularExpressionMatch rxcommentaireMatch=rxcommentaire.match(ligne);
QRegularExpressionMatch rxappelfctMatch=rxappelfct.match(ligne);
QRegularExpressionMatch rxlireMatch=rxlire.match(ligne);
QRegularExpressionMatch rxaffichcalculnlMatch=rxaffichcalculnl.match(ligne);
QRegularExpressionMatch rxaffichcalculMatch=rxaffichcalcul.match(ligne);
QRegularExpressionMatch rxaffichmessagenlMatch=rxaffichmessagenl.match(ligne);
QRegularExpressionMatch rxaffichmessageMatch=rxaffichmessage.match(ligne);
QRegularExpressionMatch rxaffichvariablenlMatch=rxaffichvariablenl.match(ligne);
QRegularExpressionMatch rxaffichvariableMatch=rxaffichvariable.match(ligne);
QRegularExpressionMatch rxaffectationMatch=rxaffectation.match(ligne);
QRegularExpressionMatch rxpointcouleurMatch=rxpointcouleur.match(ligne);
QRegularExpressionMatch rxpointMatch=rxpoint.match(ligne);
QRegularExpressionMatch rxsegmentcouleurMatch=rxsegmentcouleur.match(ligne);
QRegularExpressionMatch rxsegmentMatch=rxsegment.match(ligne);
QRegularExpressionMatch rxeffacerMatch=rxeffacer.match(ligne);
QRegularExpressionMatch rxtantqueMatch=rxtantque.match(ligne);
QRegularExpressionMatch rxdebuttantqueMatch=rxdebuttantque.match(ligne);
QRegularExpressionMatch rxfintantqueMatch=rxfintantque.match(ligne);
QRegularExpressionMatch rxpourMatch=rxpour.match(ligne);
QRegularExpressionMatch rxdebutpourMatch=rxdebutpour.match(ligne);
QRegularExpressionMatch rxfinpourMatch=rxfinpour.match(ligne);
QRegularExpressionMatch rxsinonMatch=rxsinon.match(ligne);
QRegularExpressionMatch rxdebutsinonMatch=rxdebutsinon.match(ligne);
QRegularExpressionMatch rxfinsinonMatch=rxfinsinon.match(ligne);
QRegularExpressionMatch rxsiMatch=rxsi.match(ligne);
QRegularExpressionMatch rxdebutsiMatch=rxdebutsi.match(ligne);
QRegularExpressionMatch rxfinsiMatch=rxfinsi.match(ligne);


if (!(rxfixe.match(ligne).hasMatch()) /*&& (rxvar.indexIn(ligne)<0)*/ )
{
  if (rxpauseMatch.hasMatch()) //PAUSE
    {
    NouvelleLigne(ui.treeWidget->currentItem());
    curItem = ui.treeWidget->currentItem();
    if (curItem && curItem->parent())
      {
      code="18#pause";
      curItem->setText(0,QString::fromUtf8("PAUSE"));
      curItem->setData(0,Qt::UserRole,QString(code));
      ui.treeWidget->setCurrentItem(curItem);
      ActualiserArbre();
      }
    }
  else if (rxvarfctsMatch.hasMatch()) //PAUSE
    {
    NouvelleLigne(ui.treeWidget->currentItem());
    curItem = ui.treeWidget->currentItem();
    if (curItem && curItem->parent())
      {
      code="202#declarationsvariablesfonction";
      curItem->setText(0,QString::fromUtf8("VARIABLES_FONCTION"));
      curItem->setData(0,Qt::UserRole,QString(code));
      ui.treeWidget->setCurrentItem(curItem);
      ActualiserArbre();
      }
    }
    else if (rxcommentaireMatch.hasMatch()) //COMMENTAIRE
      {
      NouvelleLigne(ui.treeWidget->currentItem());
      curItem = ui.treeWidget->currentItem();
      if (curItem && curItem->parent())
	{
	QString commentaire=rxcommentaireMatch.captured(1);
	commentaire.remove("#");
	if (!commentaire.isEmpty())
	  {
	  curItem->setText(0,"//"+commentaire);
	  code="19#"+commentaire;
	  curItem->setData(0,Qt::UserRole,QString(code));
	  curItem->setFont(0,commentFont);
	  ui.treeWidget->setCurrentItem(curItem);
	  ActualiserArbre();
	  }
	}  
      }
    else if (rxappelfctMatch.hasMatch()) //APPELER_FONCTION
      {
      NouvelleLigne(ui.treeWidget->currentItem());
      curItem = ui.treeWidget->currentItem();
      if (curItem && curItem->parent())
	{
	QString fct=rxappelfctMatch.captured(1);
	fct.remove("#");
	if (!fct.isEmpty())
	  {
	  curItem->setText(0,"APPELER_FONCTION "+fct);
	  code="206#"+fct;
	  curItem->setData(0,Qt::UserRole,QString(code));
	  ui.treeWidget->setCurrentItem(curItem);
	  ActualiserArbre();
	  }
	}  
      }      
    else if (rxlireMatch.hasMatch()) //LIRE
      {
      NouvelleLigne(ui.treeWidget->currentItem());
      curItem = ui.treeWidget->currentItem();
      if (curItem && curItem->parent())
	{
	nomvariable=rxlireMatch.captured(1);
	rang="";
    QRegularExpressionMatch rxlisteMatch=rxliste.match(nomvariable);
	if (rxlisteMatch.hasMatch())
	  {
	  rang=rxlisteMatch.captured(1);
	  nomvariable.remove(rxliste);
	  }
	if (!nomvariable.isEmpty())
	    {
	    i=ListeNomsVariables.indexOf(nomvariable);
	    if (i>-1)
		{
		type=ListeTypesVariables.at(i);
		if ((type=="LISTE") && (!rang.isEmpty())) 
		    {
		    curItem->setText(0,QString::fromUtf8("LIRE ")+nomvariable+"["+rang+"]");
		    code="2#"+nomvariable+"#"+rang;
		    }
		else 
		    {
		    curItem->setText(0,QString::fromUtf8("LIRE ")+nomvariable);
		    code="2#"+nomvariable+"#pasliste";
		    }
		curItem->setData(0,Qt::UserRole,QString(code));
		}
	    else
		{
		erreur=true;
		message_erreur=QString::fromUtf8("Variable non déclarée.")+" (ligne "+num_Lignes.at(numligne)+")";
		break;
		}
	    ui.treeWidget->setCurrentItem(curItem);
	    ActualiserArbre();
	    }
	  else
	    {
	    erreur=true;
	    message_erreur=QString::fromUtf8("Nom de variable non reconnu.")+" (ligne "+num_Lignes.at(numligne)+")";
	    break;
	    }
	}
      }
    else if (rxaffichcalculnlMatch.hasMatch()) //AFFICHER CALCUL NL
      {
      NouvelleLigne(ui.treeWidget->currentItem());
      curItem = ui.treeWidget->currentItem();
      if (curItem && curItem->parent())
	{      
	calcul=rxaffichcalculnlMatch.captured(1);
	calcul.remove("#");
	//calcul.remove("\"");
	code="";
	if (!calcul.isEmpty())
	    {
		    curItem->setText(0,QString::fromUtf8("AFFICHERCALCUL ")+calcul);
		    code="20#"+calcul+"#1";
		    curItem->setData(0,Qt::UserRole,QString(code));
		    ui.treeWidget->setCurrentItem(curItem);
		    ActualiserArbre();
	    }
	else
	    {
 	    erreur=true;
 	    message_erreur=QString::fromUtf8("Calcul vide.")+" (ligne "+num_Lignes.at(numligne)+")";
 	    break;
	    }      
      
      
// 	message.remove("#");
// 	message.remove("\"");
// 	code="";
// 	if (!message.isEmpty())
// 	    {
// 	    curItem->setText(0,QString::fromUtf8("AFFICHER \"")+message+"\"");
// 	    code="4#"+message+"#1";
// 	    curItem->setData(0,Qt::UserRole,QString(code));
// 	    ui.treeWidget->setCurrentItem(curItem);
// 	    ActualiserArbre();
// 	    }
// 	else
// 	    {
// 	    erreur=true;
// 	    message_erreur=QString::fromUtf8("Message vide.")+" (ligne "+num_Lignes.at(numligne)+")";
// 	    break;
// 	    }
	}
      }	  
    else if (rxaffichcalculMatch.hasMatch()) //AFFICHER CALCUL
      {
      NouvelleLigne(ui.treeWidget->currentItem());
      curItem = ui.treeWidget->currentItem();
      if (curItem && curItem->parent())
	{
	calcul=rxaffichcalculMatch.captured(1);

	calcul.remove("#");
	//calcul.remove("\"");
	code="";
	if (!calcul.isEmpty())
	  {
		  curItem->setText(0,QString::fromUtf8("AFFICHERCALCUL ")+calcul);
		  code="20#"+calcul+"#0";
		  curItem->setData(0,Qt::UserRole,QString(code));
		  ui.treeWidget->setCurrentItem(curItem);
		  ActualiserArbre();
	  }
	else
	  {
 	    erreur=true;
 	    message_erreur=QString::fromUtf8("Calcul vide.")+" (ligne "+num_Lignes.at(numligne)+")";
 	    break;
	  }      
// 	message=rxaffichmessage.cap(1);
// 	message.remove("#");
// 	message.remove("\"");
// 	code="";
// 	if (!message.isEmpty())
// 	    {
// 	    curItem->setText(0,QString::fromUtf8("AFFICHER \"")+message+"\"");
// 	    code="4#"+message+"#0";
// 	    curItem->setData(0,Qt::UserRole,QString(code));
// 	    ui.treeWidget->setCurrentItem(curItem);
// 	    ActualiserArbre();
// 	    }
// 	else
// 	    {
// 	    erreur=true;
// 	    message_erreur=QString::fromUtf8("Message vide.")+" (ligne "+num_Lignes.at(numligne)+")";
// 	    break;
// 	    }
	}
      }
    else if (rxaffichmessagenlMatch.hasMatch()) //AFFICHER MESSAGE NL
      {
      NouvelleLigne(ui.treeWidget->currentItem());
      curItem = ui.treeWidget->currentItem();
      if (curItem && curItem->parent())
	{      
	message=rxaffichmessagenlMatch.captured(1);
	message.remove("#");
	message.remove("\"");
	code="";
	if (!message.isEmpty())
	    {
	    curItem->setText(0,QString::fromUtf8("AFFICHER \"")+message+"\"");
	    code="4#"+message+"#1";
	    curItem->setData(0,Qt::UserRole,QString(code));
	    ui.treeWidget->setCurrentItem(curItem);
	    ActualiserArbre();
	    }
	else
	    {
	    erreur=true;
	    message_erreur=QString::fromUtf8("Message vide.")+" (ligne "+num_Lignes.at(numligne)+")";
	    break;
	    }
	}
      }	  
    else if (rxaffichmessageMatch.hasMatch()) //AFFICHER MESSAGE
      {
      NouvelleLigne(ui.treeWidget->currentItem());
      curItem = ui.treeWidget->currentItem();
      if (curItem && curItem->parent())
	{      
	message=rxaffichmessageMatch.captured(1);
	message.remove("#");
	message.remove("\"");
	code="";
	if (!message.isEmpty())
	    {
	    curItem->setText(0,QString::fromUtf8("AFFICHER \"")+message+"\"");
	    code="4#"+message+"#0";
	    curItem->setData(0,Qt::UserRole,QString(code));
	    ui.treeWidget->setCurrentItem(curItem);
	    ActualiserArbre();
	    }
	else
	    {
	    erreur=true;
	    message_erreur=QString::fromUtf8("Message vide.")+" (ligne "+num_Lignes.at(numligne)+")";
	    break;
	    }
	}
      }
    else if (rxaffichvariablenlMatch.hasMatch()) //AFFICHER VARIABLE NL
      {
      NouvelleLigne(ui.treeWidget->currentItem());
      curItem = ui.treeWidget->currentItem();
      if (curItem && curItem->parent())
	{      
	nomvariable=rxaffichvariablenlMatch.captured(1);
	rang="";
    QRegularExpressionMatch rxlisteMatch=rxliste.match(nomvariable);
	if (rxlisteMatch.hasMatch())
	  {
	  rang=rxlisteMatch.captured(1);
	  nomvariable.remove(rxliste);
	  }
	code="";
	if (!nomvariable.isEmpty())
	    {
	    i=ListeNomsVariables.indexOf(nomvariable);
	    if (i>-1)
		{
		QString type=ListeTypesVariables.at(i);
		code="3#"+nomvariable+"#1";
		if ((type=="LISTE") && (!rang.isEmpty())) 
		    {
		    curItem->setText(0,QString::fromUtf8("AFFICHER ")+nomvariable+"["+rang+"]");
		    code+="#"+rang;
		    }
		else 
		    {
		    curItem->setText(0,QString::fromUtf8("AFFICHER ")+nomvariable);
		    code+="#pasliste";
		    }
		curItem->setData(0,Qt::UserRole,QString(code));
		}
	    else
		{
		erreur=true;
		message_erreur=QString::fromUtf8("Variable non déclarée.")+" (ligne "+num_Lignes.at(numligne)+")";
		break;
		}
	    ui.treeWidget->setCurrentItem(curItem);
	    ActualiserArbre();
	    }
	  else
	    {
	    erreur=true;
	    message_erreur=QString::fromUtf8("Nom de variable non reconnu.")+" (ligne "+num_Lignes.at(numligne)+")";
	    break;
	    }
	}
      }	  
    else if (rxaffichvariableMatch.hasMatch()) //AFFICHER VARIABLE
      {
      NouvelleLigne(ui.treeWidget->currentItem());
      curItem = ui.treeWidget->currentItem();
      if (curItem && curItem->parent())
	{      
	nomvariable=rxaffichvariableMatch.captured(1);
	rang="";
    QRegularExpressionMatch rxlisteMatch=rxliste.match(nomvariable);
	if (rxlisteMatch.hasMatch())
	  {
	  rang=rxlisteMatch.captured(1);
	  nomvariable.remove(rxliste);
	  }
	code="";
	if (!nomvariable.isEmpty())
	    {
	    i=ListeNomsVariables.indexOf(nomvariable);
	    if (i>-1)
		{
		QString type=ListeTypesVariables.at(i);
		code="3#"+nomvariable+"#0";
		if ((type=="LISTE") && (!rang.isEmpty())) 
		    {
		    curItem->setText(0,QString::fromUtf8("AFFICHER ")+nomvariable+"["+rang+"]");
		    code+="#"+rang;
		    }
		else 
		    {
		    curItem->setText(0,QString::fromUtf8("AFFICHER ")+nomvariable);
		    code+="#pasliste";
		    }
		curItem->setData(0,Qt::UserRole,QString(code));
		}
	    else
		{
		erreur=true;
		message_erreur=QString::fromUtf8("Variable non déclarée.")+" (ligne "+num_Lignes.at(numligne)+")";
		break;
		}
	    ui.treeWidget->setCurrentItem(curItem);
	    ActualiserArbre();
	    }
	  else
	    {
	    erreur=true;
	    message_erreur=QString::fromUtf8("Nom de variable non reconnu.")+" (ligne "+num_Lignes.at(numligne)+")";
	    break;
	    }
	}
      }	  
    else if (rxaffectationMatch.hasMatch()) //AFFECTATION
      {
      NouvelleLigne(ui.treeWidget->currentItem());
      curItem = ui.treeWidget->currentItem();
      if (curItem && curItem->parent())
	{   
	if (ListeNomsVariables.isEmpty())
	    {
	    erreur=true;
	    message_erreur=QString::fromUtf8("Aucune variable n'a encore été définie.")+" (ligne "+num_Lignes.at(numligne)+")";
	    break;
	    }
	nomvariable=rxaffectationMatch.captured(1);
	contenu=rxaffectationMatch.captured(2);
	rang="";
    QRegularExpressionMatch rxlisteMatch=rxliste.match(nomvariable);
	if (rxlisteMatch.hasMatch())
	  {
	  rang=rxlisteMatch.captured(1);
	  nomvariable.remove(rxliste);
	  }
	contenu.remove("#");
	code="";
	if (!nomvariable.isEmpty())
	    {
	    if (!contenu.isEmpty())
		{
		i=ListeNomsVariables.indexOf(nomvariable);
		if (i>-1)
		    {
		    type=ListeTypesVariables.at(i);
		    code="5#"+nomvariable+"#"+contenu;
		    if ((type=="LISTE") && (!rang.isEmpty())) 
			{
			curItem->setText(0,nomvariable+"["+rang+"]"+QString::fromUtf8(" PREND_LA_VALEUR ")+contenu);
			code+="#"+rang;
			}
		    else 
			{
			curItem->setText(0,nomvariable+QString::fromUtf8(" PREND_LA_VALEUR ")+contenu);
			code+="#pasliste";
			}
		    curItem->setData(0,Qt::UserRole,QString(code));
		    }
		else
		    {
		    erreur=true;
		    message_erreur=QString::fromUtf8("Variable non déclarée.")+" (ligne "+num_Lignes.at(numligne)+")";
		    break;
		    }
		ui.treeWidget->setCurrentItem(curItem);
		ActualiserArbre();
		}
	    else
		{
		erreur=true;
		message_erreur=QString::fromUtf8("Affectation vide")+" (ligne "+num_Lignes.at(numligne)+")";
		break;
		}
	    }
	else
	    {
	    erreur=true;
	    message_erreur=QString::fromUtf8("Pas de variable définie")+" (ligne "+num_Lignes.at(numligne)+")";
	    break;
	    }	  
	}
      }
    else if (rxpointcouleurMatch.hasMatch()) //POINT COULEUR
      {
      NouvelleLigne(ui.treeWidget->currentItem());
      curItem = ui.treeWidget->currentItem();
      if (curItem && curItem->parent())
	{
	couleur=rxpointcouleurMatch.captured(1);
	x=rxpointcouleurMatch.captured(2);
	y=rxpointcouleurMatch.captured(3);
	couleur.remove("#");
	x.remove("#");
	y.remove("#");
	if ((!x.isEmpty()) && (!y.isEmpty()))
	    {
	    curItem->setText(0,QString::fromUtf8("TRACER_POINT (")+x+","+y+")");
	    code="50#"+x+"#"+y+"#"+couleur;
	    curItem->setData(0,Qt::UserRole,QString(code));
	    ui.treeWidget->setCurrentItem(curItem);
	    ActualiserArbre();
	    }
	else
	    {
	    erreur=true;
	    message_erreur=QString::fromUtf8("Au moins une des coordonnées n'est pas définie")+" (ligne "+num_Lignes.at(numligne)+")";
	    break;
	    }
	}
      }
    else if (rxpointMatch.hasMatch()) //POINT
      {
      NouvelleLigne(ui.treeWidget->currentItem());
      curItem = ui.treeWidget->currentItem();
      if (curItem && curItem->parent())
	{   
	x=rxpointMatch.captured(1);
	y=rxpointMatch.captured(2);
	couleur="Rouge"; 
	x.remove("#");
	y.remove("#");
	if ((!x.isEmpty()) && (!y.isEmpty()))
	    {
	    curItem->setText(0,QString::fromUtf8("TRACER_POINT (")+x+","+y+")");
	    code="50#"+x+"#"+y+"#"+couleur;
	    curItem->setData(0,Qt::UserRole,QString(code));
	    ui.treeWidget->setCurrentItem(curItem);
	    ActualiserArbre();
	    }
	else
	    {
	    erreur=true;
	    message_erreur=QString::fromUtf8("Au moins une des coordonnées n'est pas définie")+" (ligne "+num_Lignes.at(numligne)+")";
	    break;
	    }
	}
      }
    else if (rxsegmentcouleurMatch.hasMatch()) //SEGMENT COULEUR
      {
      NouvelleLigne(ui.treeWidget->currentItem());
      curItem = ui.treeWidget->currentItem();
      if (curItem && curItem->parent())
	{
	couleur=rxsegmentcouleurMatch.captured(1);
	xdep=rxsegmentcouleurMatch.captured(2);
	ydep=rxsegmentcouleurMatch.captured(3);
	xfin=rxsegmentcouleurMatch.captured(4);
	yfin=rxsegmentcouleurMatch.captured(5);
	couleur.remove("#");
	xdep.remove("#");
	ydep.remove("#");
	xfin.remove("#");
	yfin.remove("#");
	if ((!xdep.isEmpty()) && (!ydep.isEmpty()) && (!xfin.isEmpty()) && (!yfin.isEmpty()))
	    {
	    curItem->setText(0,QString::fromUtf8("TRACER_SEGMENT (")+xdep+","+ydep+")->("+xfin+","+yfin+")");
	    code="51#"+xdep+"#"+ydep+"#"+xfin+"#"+yfin+"#"+couleur;
	    curItem->setData(0,Qt::UserRole,QString(code));
	    ui.treeWidget->setCurrentItem(curItem);
	    ActualiserArbre();
	    }
	else
	    {
	    erreur=true;
	    message_erreur=QString::fromUtf8("Au moins une des coordonnées n'est pas définie")+" (ligne "+num_Lignes.at(numligne)+")";
	    break;
	    }      
	}
      }
    else if (rxsegmentMatch.hasMatch()) //SEGMENT
      {
      NouvelleLigne(ui.treeWidget->currentItem());
      curItem = ui.treeWidget->currentItem();
      if (curItem && curItem->parent())
	{         
	xdep=rxsegmentMatch.captured(1);
	ydep=rxsegmentMatch.captured(2);
	xfin=rxsegmentMatch.captured(3);
	yfin=rxsegmentMatch.captured(4);
	couleur="Rouge"; 
	xdep.remove("#");
	ydep.remove("#");
	xfin.remove("#");
	yfin.remove("#");
	if ((!xdep.isEmpty()) && (!ydep.isEmpty()) && (!xfin.isEmpty()) && (!yfin.isEmpty()))
	    {
	    curItem->setText(0,QString::fromUtf8("TRACER_SEGMENT (")+xdep+","+ydep+")->("+xfin+","+yfin+")");
	    code="51#"+xdep+"#"+ydep+"#"+xfin+"#"+yfin+"#"+couleur;
	    curItem->setData(0,Qt::UserRole,QString(code));
	    ui.treeWidget->setCurrentItem(curItem);
	    ActualiserArbre();
	    }
	else
	    {
	    erreur=true;
	    message_erreur=QString::fromUtf8("Au moins une des coordonnées n'est pas définie")+" (ligne "+num_Lignes.at(numligne)+")";
	    break;
	    }      
	}
      }
  else if (rxeffacerMatch.hasMatch()) //EFFACER
    {
    NouvelleLigne(ui.treeWidget->currentItem());
    curItem = ui.treeWidget->currentItem();
    if (curItem && curItem->parent())
      {
      code="52#effacer";
      curItem->setText(0,QString::fromUtf8("EFFACER_GRAPHIQUE"));
      curItem->setData(0,Qt::UserRole,QString(code));
      ui.treeWidget->setCurrentItem(curItem);
      ActualiserArbre();
      }
    }
    else if (rxtantqueMatch.hasMatch()) //TANT_QUE
      {
      NouvelleLigne(ui.treeWidget->currentItem());
      curItem = ui.treeWidget->currentItem();
      if (curItem && curItem->parent())
	{
	condition=rxtantqueMatch.captured(1);
	condition.remove("#");
	code="";
	if (!condition.isEmpty())
	    {
	    curItem->setText(0,QString::fromUtf8("TANT_QUE (")+condition+") "+QString::fromUtf8("FAIRE"));
	    code="15#"+condition;
	    curItem->setData(0,Qt::UserRole,QString(code));
	    ActualiserArbre();
	    }
	else
	    {
	    erreur=true;
	    message_erreur=QString::fromUtf8("Pas de condition définie")+" (ligne "+num_Lignes.at(numligne)+")";
	    break;
	    }
	}
      }
    else if (rxdebuttantqueMatch.hasMatch()) //DEBUT_TANT_QUE
      {
      curItem = ui.treeWidget->currentItem();
      if (curItem && curItem->parent())
	{
	newItem = new QTreeWidgetItem(curItem);
	newItem->setText(0,QString::fromUtf8("DEBUT_TANT_QUE"));
	newItem->setData(0,Qt::UserRole,QString("16#debuttantque"));
    curItem->setExpanded(true);
	ui.treeWidget->setCurrentItem(newItem);
	ActualiserArbre();
	}
      }
    else if (rxfintantqueMatch.hasMatch()) //FIN_TANT_QUE
      {
      NouvelleLigne(ui.treeWidget->currentItem());
      curItem = ui.treeWidget->currentItem();
      if (curItem && curItem->parent())
	{
	    curItem->setText(0,QString::fromUtf8("FIN_TANT_QUE"));
	    curItem->setData(0,Qt::UserRole,QString("17#fintantque"));
	    ActualiserArbre();
	}
      }
    else if (rxpourMatch.hasMatch()) //POUR
      {
      NouvelleLigne(ui.treeWidget->currentItem());
      curItem = ui.treeWidget->currentItem();
      if (curItem && curItem->parent())
	{
	nomvariable=rxpourMatch.captured(1);
	debut=rxpourMatch.captured(2);
	debut.remove("#");
	fin=rxpourMatch.captured(3);
	fin.remove("#");
	code="";
	if (!nomvariable.isEmpty())
	    {
	    if (!debut.isEmpty() && !fin.isEmpty())
		{
		i=ListeNomsVariables.indexOf(nomvariable);
		if (i>-1)
		    {
		    curItem->setText(0,QString::fromUtf8("POUR ")+nomvariable+" ALLANT_DE "+debut+ " A "+fin);
		    code="12#"+nomvariable+"#"+debut+"#"+fin;
		    curItem->setData(0,Qt::UserRole,QString(code));
		    }
		else
		    {
		    erreur=true;
		    message_erreur=QString::fromUtf8("Variable non déclarée.")+" (ligne "+num_Lignes.at(numligne)+")";
		    break;
		    }
		ActualiserArbre();
		}
	    else
		{
		erreur=true;
		message_erreur=QString::fromUtf8("Les valeurs minimales et maximales du compteur ne sont pas définies")+" (ligne "+num_Lignes.at(numligne)+")";
		break;
		}
	    }
	else
	    {
	    erreur=true;
	    message_erreur=QString::fromUtf8("Pas de variable définie")+" (ligne "+num_Lignes.at(numligne)+")";
	    break;
	    }
	}
      }
    else if (rxdebutpourMatch.hasMatch()) //DEBUT_POUR
      {
      curItem = ui.treeWidget->currentItem();
      if (curItem && curItem->parent())
	{
	newItem = new QTreeWidgetItem(curItem);
	newItem->setText(0,QString::fromUtf8("DEBUT_POUR"));
	newItem->setData(0,Qt::UserRole,QString("13#debutpour"));
    curItem->setExpanded(true);
	ui.treeWidget->setCurrentItem(newItem);
	ActualiserArbre();
	}
      }
    else if (rxfinpourMatch.hasMatch()) //FIN_POUR
      {
      NouvelleLigne(ui.treeWidget->currentItem());
      curItem = ui.treeWidget->currentItem();
      if (curItem && curItem->parent())
	{
	    curItem->setText(0,QString::fromUtf8("FIN_POUR"));
	    curItem->setData(0,Qt::UserRole,QString("14#finpour"));
	    ActualiserArbre();
	}
      }
    else if (rxsinonMatch.hasMatch()) //SINON
      {
      curItem = ui.treeWidget->currentItem();
      if (curItem && curItem->parent())
	{
	  newItem = new QTreeWidgetItem(curItem->parent(), curItem);
	  newItem->setText(0,QString::fromUtf8("SINON"));
	  code="9#sinon";
	  newItem->setData(0,Qt::UserRole,QString(code));
	  ui.treeWidget->setCurrentItem(newItem);
	  ActualiserArbre();
	}
      }
    else if (rxdebutsinonMatch.hasMatch()) //DEBUT_SINON
      {
      curItem = ui.treeWidget->currentItem();
      if (curItem && curItem->parent())
	{
	newItem = new QTreeWidgetItem(curItem);
	newItem->setText(0,QString::fromUtf8("DEBUT_SINON"));
	newItem->setData(0,Qt::UserRole,QString("10#debutsinon"));
    curItem->setExpanded(true);
	ui.treeWidget->setCurrentItem(newItem);
	ActualiserArbre();
	}
      }
    else if (rxfinsinonMatch.hasMatch()) //FIN_SINON
      {
      NouvelleLigne(ui.treeWidget->currentItem());
      curItem = ui.treeWidget->currentItem();
      if (curItem && curItem->parent())
	{
	curItem->setText(0,QString::fromUtf8("FIN_SINON"));
	curItem->setData(0,Qt::UserRole,QString("11#finsinon"));
	ActualiserArbre();
	}
      }      
    else if (rxsiMatch.hasMatch()) //SI
      {
      NouvelleLigne(ui.treeWidget->currentItem());
      curItem = ui.treeWidget->currentItem();
      if (curItem && curItem->parent())
	{
	condition=rxsiMatch.captured(1);
	condition.remove("#");
	code="";
	if (!condition.isEmpty())
	    {
	    curItem->setText(0,QString::fromUtf8("SI (")+condition+") "+QString::fromUtf8("ALORS"));
	    code="6#"+condition;
	    curItem->setData(0,Qt::UserRole,QString(code));
	    ActualiserArbre();
	    }
	else
	    {
	    erreur=true;
	    message_erreur=QString::fromUtf8("Pas de condition définie")+" (ligne "+num_Lignes.at(numligne)+")";
	    break;
	    }
	}
      }
    else if (rxdebutsiMatch.hasMatch()) //DEBUT_SI
      {
      curItem = ui.treeWidget->currentItem();
      if (curItem && curItem->parent())
	{
	newItem = new QTreeWidgetItem(curItem);
	newItem->setText(0,QString::fromUtf8("DEBUT_SI"));
	newItem->setData(0,Qt::UserRole,QString("7#debutsi"));
    curItem->setExpanded(true);
	ui.treeWidget->setCurrentItem(newItem);
	ActualiserArbre();
	}
      }
    else if (rxfinsiMatch.hasMatch()) //FIN_SI
      {
      NouvelleLigne(ui.treeWidget->currentItem());
      curItem = ui.treeWidget->currentItem();
      if (curItem && curItem->parent())
	{
	curItem->setText(0,QString::fromUtf8("FIN_SI"));
	curItem->setData(0,Qt::UserRole,QString("8#finsi"));
	ActualiserArbre();
	}
      }

    else
      {
      erreur=true;
      message_erreur=QString::fromUtf8("Instruction non reconnue")+" (ligne "+num_Lignes.at(numligne)+")";
      break;      
      }    
}
numligne++;
}
/*****************************************/



int erreurligne=1;
if (erreur)
  {
  EffaceArbre();
QRegularExpressionMatch rxerreurligneMatch=rxerreurligne.match(message_erreur);
  if (rxerreurligneMatch.hasMatch()) erreurligne=rxerreurligneMatch.captured(1).toInt();
  ui.EditorView->editor->setCursorPosition(erreurligne-1 , 0);
  }
return message_erreur;
}

QString MainWindow::ArbreVersCodeTexte()
{
QString code="";
int nb_branches=ui.treeWidget->topLevelItemCount();
if (nb_branches>0)
    {
    indent=0;
    for (int i = 0; i < nb_branches; i++) 
	{
	code+=AlgoNoeudCode(ui.treeWidget->topLevelItem(i));
	}
    }
return code;
}

void MainWindow::VerifierCodeTexte()
{
QTreeWidgetItem *newItem;
if (!modeNormal)
  {
  QString rep_analyse=EditeurVersArbre();
  if (rep_analyse!="ok")
    {
    QMessageBox::warning( this,QString::fromUtf8("Erreur"),QString::fromUtf8("Le code n'est pas valide. Impossible de vérifier l'algorithme.\nErreur détectée : ")+rep_analyse);
    return;
    }
  VerifDialog *verifDlg = new VerifDialog(this);
  int nb_branches=ui.treeWidget->topLevelItemCount();
  if (nb_branches>0)
    {
    for (int i = 0; i < nb_branches; i++) 
	{
	newItem=ui.treeWidget->topLevelItem(i)->clone();
	verifDlg->ui.treeWidget->addTopLevelItem(newItem);
	verifDlg->ExpandBranche(newItem);
	}
    }
  verifDlg->exec();
  }  
}

void MainWindow::ExpandBranche(QTreeWidgetItem *item)
{
ui.treeWidget->expandItem(item);
int nb_branches=item->childCount();
if (nb_branches>0)
	{
	for (int i = 0; i < nb_branches; i++) 
		{
		ExpandBranche(item->child(i));
		}
	}
}

void MainWindow::InsertOperation(QListWidgetItem *item)
{
if (item && item->font().bold())
    {
    QString role=item->data(Qt::UserRole).toString();
    QStringList tagList= role.split("#");
    int pos=ui.lineEditFonction->cursorPosition();
    int dx=tagList.at(1).toInt();
    ui.lineEditFonction->insert(tagList.at(0));
    ui.lineEditFonction->setCursorPosition(pos+dx);
    ui.lineEditFonction->setFocus();
    }
}

void MainWindow::AjouterF2()
{
QListWidgetItem *ligne;
ligne=new QListWidgetItem(ui.listWidgetF2);
ligne->setText("SI ("+ui.lineEditConditionF2->text()+") RENVOYER "+ui.lineEditRetourF2->text());
ligne->setData(Qt::UserRole,ui.lineEditConditionF2->text()+"@"+ui.lineEditRetourF2->text());
if (ui.listWidgetF2->count()>0)
	{
	ui.listWidgetF2->setCurrentItem(ui.listWidgetF2->item(ui.listWidgetF2->count()-1));
	//ui.listWidgetF2->setItemSelected(ui.listWidgetF2->currentItem(), true);
    ui.listWidgetF2->currentItem()->setSelected(true);
	}
ui.lineEditConditionF2->setText("");
ui.lineEditRetourF2->setText("");
ActualiserStatut();
}

void MainWindow::HautF2()
{
int current=ui.listWidgetF2->currentRow();
if (current<=0) return;
QListWidgetItem *item=ui.listWidgetF2->item(current)->clone();
delete ui.listWidgetF2->item(current);
ui.listWidgetF2->insertItem(current-1,item);
ui.listWidgetF2->setCurrentItem(ui.listWidgetF2->item(current-1));
//ui.listWidgetF2->setItemSelected(ui.listWidgetF2->item(current-1),true);
ui.listWidgetF2->item(current-1)->setSelected(true);
ActualiserStatut();
}

void MainWindow::BasF2()
{
int current=ui.listWidgetF2->currentRow();
if (current>=ui.listWidgetF2->count()-1 || current<0) return;
QListWidgetItem *item=ui.listWidgetF2->item(current)->clone();
delete ui.listWidgetF2->item(current);
ui.listWidgetF2->insertItem(current+1,item);
ui.listWidgetF2->setCurrentItem(ui.listWidgetF2->item(current+1));
//ui.listWidgetF2->setItemSelected(ui.listWidgetF2->item(current+1),true);
ui.listWidgetF2->item(current+1)->setSelected(true);
ActualiserStatut();
}

void MainWindow::SupprimerF2()
{
int current=ui.listWidgetF2->currentRow();
if (current<0) return;
delete ui.listWidgetF2->currentItem();
if (current==ui.listWidgetF2->count())
	{
	ui.listWidgetF2->setCurrentItem(ui.listWidgetF2->item(current-1));
	//ui.listWidgetF2->setItemSelected(ui.listWidgetF2->item(current-1),true);
    ui.listWidgetF2->item(current-1)->setSelected(true);
	}
else
	{
	ui.listWidgetF2->setCurrentItem(ui.listWidgetF2->item(current));
	//ui.listWidgetF2->setItemSelected(ui.listWidgetF2->item(current),true);
    ui.listWidgetF2->item(current)->setSelected(true);
	}
ActualiserStatut();
}

void MainWindow::ModifierLigneF2(QListWidgetItem *item)
{
if (item)
  {
  QString role,condition,commande;
  QStringList tagList;
  role=item->data(Qt::UserRole).toString();
  tagList= role.split("@");
  if (tagList.count()==2) 
      {
      condition=tagList.at(0);
      commande=tagList.at(1);
      ModifierLigneDialog *dlg = new ModifierLigneDialog(this);
      dlg->ui.lineEditConditionF2->setText(condition);
      dlg->ui.lineEditRetourF2->setText(commande);
      if (dlg->exec())
	  {
	  item->setText("SI ("+dlg->ui.lineEditConditionF2->text()+") RENVOYER "+dlg->ui.lineEditRetourF2->text());
	  item->setData(Qt::UserRole,dlg->ui.lineEditConditionF2->text()+"@"+dlg->ui.lineEditRetourF2->text());
	  }
      }
  }
ActualiserStatut();
}

void MainWindow::ChargerExtension()
{
QString nouveauFichier = QFileDialog::getOpenFileName(this,QString::fromUtf8("Ouvrir Extension"),dernierRepertoire,"Fichier javascript (*.js)");
if (!nouveauFichier.isEmpty()) 
  {
  QFileInfo fic(nouveauFichier);
  if (fic.exists() && fic.isReadable() )
	  {
	  if (nouveauFichier!=fichier_extension) ActualiserStatut();
	  fichier_extension=nouveauFichier;
	  annulerExtensionAct->setEnabled(true);
	  ui.lineEditExtension->setText(fic.fileName());
	  }
  }
else 
  {
  AnnulerExtension();
  }
}

void MainWindow::AnnulerExtension()
{
  fichier_extension="";
  annulerExtensionAct->setEnabled(false);
  ui.lineEditExtension->setText("");
  ActualiserStatut();
}

void MainWindow::LancerJSEditeur()
{
if (!jseditWindow) jseditWindow=new JSMainWindow(0,x11fontsize);
jseditWindow->resize(browserwidth,browserheight);
jseditWindow->raise();
jseditWindow->show();
}
/**************************************************/
QString MainWindow::highlightHtmlLine(const QString &text)
{
QString SpanComment, SpanBloc, SpanCommande, SpanSi, SpanTantQue, SpanPour, SpanFin, SpanFonction;
QStringList BlocWords,SiWords,TantQueWords,PourWords, CommandeWords, FonctionWords;
SpanComment = "<span style=\"color:#000000;\">";
SpanBloc="<span style=\"color:#800000;font-weight:bold;\">";
SpanCommande="<span style=\"color:#0000CC;\">";
SpanSi="<span style=\"color:#800080;font-weight:bold;\">";
SpanTantQue="<span style=\"color:#DD6F06;font-weight:bold;\">";
SpanPour="<span style=\"color:#BB8800;font-weight:bold;\">";
SpanFin="</span>";
SpanFonction="<span style=\"color:#9A4D00;font-weight:bold;\">";
FonctionWords=QString("\\bFONCTION\\b,\\bFONCTIONS_UTILISEES\\b,\\bDEBUT_FONCTION\\b,\\bFIN_FONCTION\\b,\\bVARIABLES_FONCTION\\b").split(",");
BlocWords= QString("\\bVARIABLES\\b,\\bDEBUT_ALGORITHME\\b,\\bFIN_ALGORITHME\\b").split(",");
CommandeWords= QString("\\bEST_DU_TYPE\\b,\\bPAUSE\\b,\\bLIRE\\b,\\bAFFICHERCALCUL\\b,\\bAFFICHER\\b,\\bPREND_LA_VALEUR\\b,\\bTRACER_POINT\\b,\\bTRACER_POINT_Rouge\\b,\\bTRACER_POINT_Vert\\b,\\bTRACER_POINT_Bleu\\b,\\bTRACER_POINT_Blanc\\b,\\bTRACER_SEGMENT\\b,\\bTRACER_SEGMENT_Rouge\\b,\\bTRACER_SEGMENT_Vert\\b,\\bTRACER_SEGMENT_Bleu\\b,\\bTRACER_SEGMENT_Blanc\\b,\\bRENVOYER\\b,\\bAPPELER_FONCTION\\b").split(",");
SiWords=QString("\\bSI\\b,\\bALORS\\b,\\bSINON\\b,\\bDEBUT_SI\\b,\\bFIN_SI\\b,\\bDEBUT_SINON\\b,\\bFIN_SINON\\b").split(",");
TantQueWords=QString("\\bTANT_QUE\\b,\\bDEBUT_TANT_QUE\\b,\\bFIN_TANT_QUE\\b,\\bFAIRE\\b").split(",");
PourWords=QString("\\bPOUR\\b,\\bALLANT_DE\\b,\\bDEBUT_POUR\\b,\\bFIN_POUR\\b").split(",");

if (text.isEmpty()) return QString("");

QString buffer=text;
if (buffer.contains("<i>//",Qt::CaseInsensitive)) return SpanComment+buffer+SpanFin;
  
QList<int> code=detectChaine(buffer);
if ( buffer.length() > 0 )
      {
      for ( QStringList::Iterator it = FonctionWords.begin(); it != FonctionWords.end(); ++it ) 
	      {
	      QRegularExpression expression(( *it ),QRegularExpression::CaseInsensitiveOption /* | QRegularExpression::InvertedGreedinessOption */);
	      int index = expression.match(buffer).capturedStart();
	      while (index >= 0) 
		{
		int length = expression.match(buffer).capturedLength();
		if (index<code.count() && code[index]==0) buffer=buffer.left(index)+SpanFonction+ buffer.mid(index,length)+SpanFin+buffer.mid(index+length,buffer.length()-index-length);
		code=detectChaine(buffer);
		index = expression.match(buffer, index + length+SpanFonction.length()+SpanFin.length()).capturedStart();
		}
	      }
      for ( QStringList::Iterator it = BlocWords.begin(); it != BlocWords.end(); ++it ) 
	      {
	      QRegularExpression expression(( *it ),QRegularExpression::CaseInsensitiveOption /* | QRegularExpression::InvertedGreedinessOption */);
	      int index = expression.match(buffer).capturedStart();
	      while (index >= 0) 
		{
		int length = expression.match(buffer).capturedLength();
		if (index<code.count() && code[index]==0) buffer=buffer.left(index)+SpanBloc+ buffer.mid(index,length)+SpanFin+buffer.mid(index+length,buffer.length()-index-length);
		code=detectChaine(buffer);
		index = expression.match(buffer, index + length+SpanBloc.length()+SpanFin.length()).capturedStart();
		}
	      }
      for ( QStringList::Iterator it = CommandeWords.begin(); it != CommandeWords.end(); ++it ) 
	      {
	      QRegularExpression expression(( *it ),QRegularExpression::CaseInsensitiveOption /* | QRegularExpression::InvertedGreedinessOption */);
	      int index = expression.match(buffer).capturedStart();
	      while (index >= 0) 
		{
		int length = expression.match(buffer).capturedLength();
		if (index<code.count() && code[index]==0) buffer=buffer.left(index)+SpanCommande+ buffer.mid(index,length)+SpanFin+buffer.mid(index+length,buffer.length()-index-length);
		code=detectChaine(buffer);
		index = expression.match(buffer, index + length+SpanCommande.length()+SpanFin.length()).capturedStart();
		}
	      }
      for ( QStringList::Iterator it = SiWords.begin(); it != SiWords.end(); ++it ) 
	      {
	      QRegularExpression expression(( *it ),QRegularExpression::CaseInsensitiveOption /* | QRegularExpression::InvertedGreedinessOption */);
	      int index = expression.match(buffer).capturedStart();
	      while (index >= 0) 
		{
		int length = expression.match(buffer).capturedLength();
		if (index<code.count() && code[index]==0) buffer=buffer.left(index)+SpanSi+ buffer.mid(index,length)+SpanFin+buffer.mid(index+length,buffer.length()-index-length);
		code=detectChaine(buffer);
		index = expression.match(buffer, index + length+SpanSi.length()+SpanFin.length()).capturedStart();
		}
	      }
      for ( QStringList::Iterator it = TantQueWords.begin(); it != TantQueWords.end(); ++it ) 
	      {
	      QRegularExpression expression(( *it ),QRegularExpression::CaseInsensitiveOption /* | QRegularExpression::InvertedGreedinessOption */);
	      int index = expression.match(buffer).capturedStart();
	      while (index >= 0) 
		{
		int length = expression.match(buffer).capturedLength();
		if (index<code.count() && code[index]==0) buffer=buffer.left(index)+SpanTantQue+ buffer.mid(index,length)+SpanFin+buffer.mid(index+length,buffer.length()-index-length);
		code=detectChaine(buffer);
		index = expression.match(buffer, index + length+SpanTantQue.length()+SpanFin.length()).capturedStart();
		}
	      }
      for ( QStringList::Iterator it = PourWords.begin(); it != PourWords.end(); ++it ) 
	      {
	      QRegularExpression expression(( *it ),QRegularExpression::CaseInsensitiveOption /* | QRegularExpression::InvertedGreedinessOption */);
	      int index = expression.match(buffer).capturedStart();
	      while (index >= 0) 
		{
		int length = expression.match(buffer).capturedLength();
		if (index<code.count() && code[index]==0) buffer=buffer.left(index)+SpanPour+ buffer.mid(index,length)+SpanFin+buffer.mid(index+length,buffer.length()-index-length);
		code=detectChaine(buffer);
		index = expression.match(buffer, index + length+SpanPour.length()+SpanFin.length()).capturedStart();
		}
	      }
      }

return buffer;
}

QList<int> MainWindow::detectChaine(const QString &text)
{
QList<int> code;
code.clear(); 
for (int j=0; j < text.length(); j++) code.append(0);
const int StateStandard = 0;
const int StateString = 2;
int state =StateStandard;
int i = 0;
QChar tmp;
while (i < text.length())
    {
    switch (state) 
    {
	case StateStandard: 
	{
	tmp=text.at( i );
	if (tmp=='"') 
	    {
	    code[i]=1;
	    state=StateString;
	    }
	else state=StateStandard;
	} break;
	case StateString: 
	{
	tmp=text.at( i );
	if (tmp== '"') 
	    {
	    code[i]=1;
	    state=StateStandard;
	    }
	else
	    {
	    code[i]=1;
	    state=StateString;
	    }
	} break;
    }
    i++; 
    }
return code;
}

/**************************************************/
void  MainWindow::Configurer()
{
ConfigDialog* cfDialog=new ConfigDialog(this);
cfDialog->ui.checkBoxArrondi->setChecked(arrondiAuto);
cfDialog->ui.spinBoxBoucle->setValue(maxBoucle);
cfDialog->ui.spinBoxTotal->setValue(totalBoucles);
cfDialog->ui.spinBoxAffichage->setValue(totalAffichages);
cfDialog->ui.spinBoxLigne->setValue(epaisseurLigne);
cfDialog->ui.spinBoxPoint->setValue(epaisseurPoint);
cfDialog->ui.spinBoxDecimales->setValue(nbDecimales);
if (cfDialog->exec())
    {
    arrondiAuto=cfDialog->ui.checkBoxArrondi->isChecked();
    maxBoucle=cfDialog->ui.spinBoxBoucle->value();
    totalBoucles=cfDialog->ui.spinBoxTotal->value();
    totalAffichages=cfDialog->ui.spinBoxAffichage->value();
    epaisseurLigne=cfDialog->ui.spinBoxLigne->value();
    epaisseurPoint=cfDialog->ui.spinBoxPoint->value();
    nbDecimales=cfDialog->ui.spinBoxDecimales->value();

    arrondiAutoAlgo=arrondiAuto;
    maxBoucleAlgo=maxBoucle;
    totalBouclesAlgo=totalBoucles;
    totalAffichagesAlgo=totalAffichages;
    epaisseurLigneAlgo=epaisseurLigne;
    epaisseurPointAlgo=epaisseurPoint;
    nbDecimalesAlgo=nbDecimales;
    ActualiserTexteParam();
    if (!estVierge) ActualiserStatut();
    }

}

void  MainWindow::ActualiserTexteParam()
{
ui.comboBoxParam->clear();
if (arrondiAutoAlgo) ui.comboBoxParam->addItem(QString::fromUtf8("Arrondi auto : vrai"));
else ui.comboBoxParam->addItem(QString::fromUtf8("Arrondi auto : faux"));
ui.comboBoxParam->addItem(QString::fromUtf8("max itérations par boucles :")+QString::number(maxBoucleAlgo));
ui.comboBoxParam->addItem(QString::fromUtf8("max itérations pour l'algo :")+QString::number(totalBouclesAlgo));
ui.comboBoxParam->addItem(QString::fromUtf8("max affichage console :")+QString::number(totalAffichagesAlgo));
ui.comboBoxParam->addItem(QString::fromUtf8("épaisseur lignes :")+QString::number(epaisseurLigneAlgo));
ui.comboBoxParam->addItem(QString::fromUtf8("épaisseur points :")+QString::number(epaisseurPointAlgo));
ui.comboBoxParam->addItem(QString::fromUtf8("nb décimales affichées :")+QString::number(nbDecimalesAlgo));   
}
