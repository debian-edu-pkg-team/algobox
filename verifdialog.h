/***************************************************************************
 *   copyright       : (C) 2009-2022 by Pascal Brachet                     *
 *   https://www.xm1math.net/algobox/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   @license GPL-2.0+ <https://spdx.org/licenses/GPL-2.0+.html>           *
 ***************************************************************************/

#ifndef VERIFDIALOG_H
#define VERIFDIALOG_H

#include "ui_verifdialog.h"

class VerifDialog : public QDialog  {
   Q_OBJECT
public:
	VerifDialog(QWidget *parent=0);
	~VerifDialog();
	Ui::VerifDialog ui;
public slots:
void ExpandBranche(QTreeWidgetItem *item);
};


#endif
