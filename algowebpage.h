/***************************************************************************
 *   copyright       : (C) 2009-2022 by Pascal Brachet                     *
 *   https://www.xm1math.net/algobox/                                       *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   @license GPL-2.0+ <https://spdx.org/licenses/GPL-2.0+.html>           *
 ***************************************************************************/

#ifndef ALGOWEBPAGE_H
#define ALGOWEBPAGE_H

#include "algoconsole.h"
#include "algowebview.h"
#include <QWebEnginePage>
#include <QObject>
#include <QEventLoop>
#include <QTimer>
#include <QDebug>

class AlgoWebPage : public QWebEnginePage
{
Q_OBJECT
public:
          AlgoWebPage(QObject* parent =0, AlgoConsole* cons=0);
	  ~AlgoWebPage();
AlgoConsole *console;
AlgoWebView *view;
protected:
   virtual void javaScriptAlert(const QUrl &securityOrigin, const QString& msg);
   virtual bool javaScriptPrompt(const QUrl &securityOrigin, const QString& msg, const QString& defaultValue, QString* result);
   virtual bool javaScriptConfirm(const QUrl &securityOrigin, const QString& msg);
private:
bool result, stop;
QEventLoop loop_aff, loop_pause;
QTimer timer;
public slots:
void continuer();
void arreter();
void minipause();
void stopaffichage();
bool shouldInterruptJavaScript();
};

#endif
