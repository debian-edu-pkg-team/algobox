/***************************************************************************
 *   copyright       : (C) 2009-2022 by Pascal Brachet                     *
 *   https://www.xm1math.net/algobox/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   @license GPL-2.0+ <https://spdx.org/licenses/GPL-2.0+.html>           *
 ***************************************************************************/

#ifndef FONCTIONDIALOG_H
#define FONCTIONDIALOG_H

#include "ui_fonctiondialog.h"

class FonctionDialog : public QDialog  {
   Q_OBJECT
public:
	FonctionDialog(QWidget *parent=0);
	~FonctionDialog();
	Ui::FonctionDialog ui;
};


#endif
