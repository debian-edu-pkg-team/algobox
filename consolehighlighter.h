/***************************************************************************
 *   copyright       : (C) 2009-2022 by Pascal Brachet                     *
 *   https://www.xm1math.net/algobox/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   @license GPL-2.0+ <https://spdx.org/licenses/GPL-2.0+.html>           *
 ***************************************************************************/


#ifndef CONSOLEHIGHLIGHTER_H
#define CONSOLEHIGHLIGHTER_H

#include <QSyntaxHighlighter>

#include <QHash>
#include <QTextCharFormat>
#include <QColor>

class QTextDocument;

class ConsoleHighlighter : public QSyntaxHighlighter
{
    Q_OBJECT

public:
    ConsoleHighlighter(QTextDocument *parent = 0, bool blackschema=true);
    QTextCharFormat MessageFormat, EntreeFormat;

protected:
    void highlightBlock(const QString &text);
};


#endif
