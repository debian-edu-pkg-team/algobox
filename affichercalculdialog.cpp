/***************************************************************************
 *   copyright       : (C) 2009-2022 by Pascal Brachet                     *
 *   https://www.xm1math.net/algobox/                                       *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   @license GPL-2.0+ <https://spdx.org/licenses/GPL-2.0+.html>           *
 ***************************************************************************/

#include "affichercalculdialog.h"

#include <QMessageBox>
#include <QFile>
#include <QTextStream>
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    #include <QtCore/QTextCodec>
#else
    #include <QtCore5Compat/QTextCodec>
#endif

AffichercalculDialog::AffichercalculDialog(QWidget *parent, QString variables, QString types )
    :QDialog( parent)
{
ui.setupUi(this);
setModal(true);

QTextCodec *codec = QTextCodec::codecForName("UTF-8");
QString contenu;
QFile aide(":/documents/aideaffichagecalcul.txt");
aide.open(QIODevice::ReadOnly);
QTextStream in(&aide);
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
in.setCodec(codec);
#else
in.setEncoding(QStringConverter::Encoding::Utf8);
#endif
while (!in.atEnd()) 
	{
	contenu+= in.readLine()+"\n";
	}
aide.close();
ui.textEdit->setHtml(contenu);

listeVariables=variables.split("#");
listeTypes=types.split("#");

QFont fontCommande=qApp->font();
fontCommande.setBold(true);
QFont fontCommentaire=qApp->font();
//fontCommentaire.setItalic(true);
#if defined(Q_OS_MAC)
fontCommentaire.setPointSize(10);
#else
fontCommentaire.setPointSize(8);
#endif
QListWidgetItem *commande, *commentaire;

for (int i = 0; i < listeVariables.count(); i++)
{
  if (listeTypes.at(i)=="LISTE") listeVarListe.append(listeVariables.at(i)+"[]");
  else if (listeTypes.at(i)=="NOMBRE") listeVarNombre.append(listeVariables.at(i));
  else if (listeTypes.at(i)=="CHAINE") listeVarChaine.append(listeVariables.at(i));
}

for (int i = 0; i < listeVarNombre.count(); i++)
{
commande=new QListWidgetItem(ui.listWidget);
commande->setFont(fontCommande);
commande->setText(listeVarNombre.at(i));
commentaire=new QListWidgetItem(ui.listWidget);
commentaire->setFont(fontCommentaire);
commentaire->setText(QString::fromUtf8("-> variable du type Nombre"));
commentaire->setFlags(Qt::ItemIsEnabled);
}

//if (listeVarListe.count()>0) ui.listWidget->addItems(listeVarListe);
for (int i = 0; i < listeVarListe.count(); i++)
{
commande=new QListWidgetItem(ui.listWidget);
commande->setFont(fontCommande);
commande->setText(listeVarListe.at(i));
commentaire=new QListWidgetItem(ui.listWidget);
commentaire->setFont(fontCommentaire);
commentaire->setText(QString::fromUtf8("-> variable du type Liste"));
commentaire->setFlags(Qt::ItemIsEnabled);
}

//if (listeVarChaine.count()>0) ui.listWidget->addItems(listeVarChaine);
for (int i = 0; i < listeVarChaine.count(); i++)
{
commande=new QListWidgetItem(ui.listWidget);
commande->setFont(fontCommande);
commande->setText(listeVarChaine.at(i));
commentaire=new QListWidgetItem(ui.listWidget);
commentaire->setFont(fontCommentaire);
commentaire->setText(QString::fromUtf8("-> variable du type Chaine"));
commentaire->setFlags(Qt::ItemIsEnabled);
}

QStringList commandeList,commentaireList,roleList;

commandeList << "sqrt(x)";
commentaireList << QString::fromUtf8("-> racine carrée de x");
roleList << "sqrt()#5";

commandeList << "pow(x,n)";
commentaireList << QString::fromUtf8("-> x puissance n");
roleList << "pow(,)#4";

commandeList << "x%y";
commentaireList << QString::fromUtf8("-> reste de la division de x par y");
roleList << "%#0";

commandeList << "cos(x)";
commentaireList << QString::fromUtf8("-> cosinus de x");
roleList << "cos()#4";

commandeList << "sin(x)";
commentaireList << QString::fromUtf8("-> sinus de x");
roleList << "sin()#4";

commandeList << "tan(x)";
commentaireList << QString::fromUtf8("-> tangente de x");
roleList << "tan()#4";

commandeList << "exp(x)";
commentaireList << QString::fromUtf8("-> exponentielle de x");
roleList << "exp()#4";

commandeList << "log(x)";
commentaireList << QString::fromUtf8("-> logarithme népérien de x");
roleList << "log()#4";

commandeList << "abs(x)";
commentaireList << QString::fromUtf8("-> valeur absolue de x");
roleList << "abs()#4";

commandeList << "floor(x)";
commentaireList << QString::fromUtf8("-> partie entière de x");
roleList << "floor()#6";

commandeList << "round(x)";
commentaireList << QString::fromUtf8("-> entier le plus proche de x");
roleList << "round()#6";

commandeList << "acos(x)";
commentaireList << QString::fromUtf8("-> arccosinus de x");
roleList << "acos()#5";

commandeList << "asin(x)";
commentaireList << QString::fromUtf8("-> arcsinus de x");
roleList << "asin()#5";

commandeList << "atan(x)";
commentaireList << QString::fromUtf8("-> arctangente de x");
roleList << "atan()#5";

commandeList << "Math.PI";
commentaireList << QString::fromUtf8("-> constante PI");
roleList << "Math.PI#7";

commandeList << "random()";
commentaireList << QString::fromUtf8("-> nombre pseudo-aléatoire entre 0 et 1");
roleList << "random()#7";

commandeList << "ALGOBOX_ALEA_ENT(p,n)";
commentaireList << QString::fromUtf8("-> entier pseudo-aléatoire entre p et n");
roleList << "ALGOBOX_ALEA_ENT(,)#17";

commandeList << "ALGOBOX_COEFF_BINOMIAL(n,p)";
commentaireList << QString::fromUtf8("-> coefficient binomial 'p parmi n'");
roleList << "ALGOBOX_COEFF_BINOMIAL(,)#23";

commandeList << "ALGOBOX_LOI_BINOMIALE(n,p,k)";
commentaireList << QString::fromUtf8("-> p(X=k) pour la loi binomiale de paramètres (n,p)");
roleList << "ALGOBOX_LOI_BINOMIALE(,,)#22";

commandeList << "ALGOBOX_LOI_NORMALE_CR(x)";
commentaireList << QString::fromUtf8("-> p(X<x) pour la loi normale centrée réduite");
roleList << "ALGOBOX_LOI_NORMALE_CR()#23";

commandeList << "ALGOBOX_LOI_NORMALE(esp,ecart,x)";
commentaireList << QString::fromUtf8("-> p(X<x) pour la loi normale d'espérance 'esp' et d'écart-type 'ecart'");
roleList << "ALGOBOX_LOI_NORMALE(,,)#20";

commandeList << "ALGOBOX_INVERSE_LOI_NORMALE_CR(p)";
commentaireList << QString::fromUtf8("-> opération inverse de ALGOBOX_LOI_NORMALE_CR(x)");
roleList << "ALGOBOX_INVERSE_LOI_NORMALE_CR()#31";

commandeList << "ALGOBOX_INVERSE_LOI_NORMALE(esp,ecart,p)";
commentaireList << QString::fromUtf8("-> opération inverse de ALGOBOX_LOI_NORMALE(esp,ecart,x)");
roleList << "ALGOBOX_INVERSE_LOI_NORMALE(,,)#28";

commandeList << "ALGOBOX_FACTORIELLE(n)";
commentaireList << QString::fromUtf8("-> factorielle de n");
roleList << "ALGOBOX_FACTORIELLE()#20";

commandeList << "ALGOBOX_SOMME(liste,p,n)";
commentaireList << QString::fromUtf8("-> somme des termes de liste[p] à liste[n]");
roleList << "ALGOBOX_SOMME(,,)#14";

commandeList << "ALGOBOX_MOYENNE(liste,p,n)";
commentaireList << QString::fromUtf8("-> moyenne des termes de liste[p] à liste[n]");
roleList << "ALGOBOX_MOYENNE(,,)#16";

commandeList << "ALGOBOX_VARIANCE(liste,p,n)";
commentaireList << QString::fromUtf8("-> variance des termes de liste[p] à liste[n]");
roleList << "ALGOBOX_VARIANCE(,,)#17";

commandeList << "ALGOBOX_ECART_TYPE(liste,p,n)";
commentaireList << QString::fromUtf8("-> écart-type des termes de liste[p] à liste[n]");
roleList << "ALGOBOX_ECART_TYPE(,,)#19";

commandeList << "ALGOBOX_MEDIANE(liste,p,n)";
commentaireList << QString::fromUtf8("-> médiane des termes de liste[p] à liste[n]");
roleList << "ALGOBOX_MEDIANE(,,)#16";

commandeList << "ALGOBOX_QUARTILE1(liste,p,n)";
commentaireList << QString::fromUtf8("-> Q1 (calculatrice) des termes de liste[p] à liste[n]");
roleList << "ALGOBOX_QUARTILE1(,,)#18";

commandeList << "ALGOBOX_QUARTILE3(liste,p,n)";
commentaireList << QString::fromUtf8("-> Q3 (calculatrice) des termes de liste[p] à liste[n]");
roleList << "ALGOBOX_QUARTILE3(,,)#18";

commandeList << "ALGOBOX_QUARTILE1_BIS(liste,p,n)";
commentaireList << QString::fromUtf8("-> Q1 (25%) des termes de liste[p] à liste[n]");
roleList << "ALGOBOX_QUARTILE1_BIS(,,)#22";

commandeList << "ALGOBOX_QUARTILE3_BIS(liste,p,n)";
commentaireList << QString::fromUtf8("-> Q3 (75%) des termes de liste[p] à liste[n]");
roleList << "ALGOBOX_QUARTILE3_BIS(,,)#22";

commandeList << "ALGOBOX_MINIMUM(liste,p,n)";
commentaireList << QString::fromUtf8("-> minimum des termes de liste[p] à liste[n]");
roleList << "ALGOBOX_MINIMUM(,,)#16";

commandeList << "ALGOBOX_MAXIMUM(liste,p,n)";
commentaireList << QString::fromUtf8("-> maximum des termes de liste[p] à liste[n]");
roleList << "ALGOBOX_MAXIMUM(,,)#16";

commandeList << "ALGOBOX_POS_MINIMUM(liste,p,n)";
commentaireList << QString::fromUtf8("-> rang du minimum des termes de liste[p] à liste[n]");
roleList << "ALGOBOX_POS_MINIMUM(,,)#20";

commandeList << "ALGOBOX_POS_MAXIMUM(liste,p,n)";
commentaireList << QString::fromUtf8("-> rang du maximum des termes de liste[p] à liste[n]");
roleList << "ALGOBOX_POS_MAXIMUM(,,)#20";

commandeList << "ALGOBOX_ARRONDIR(x,n)";
commentaireList << QString::fromUtf8("-> arrondit x avec n chiffres après la virgule");
roleList << "ALGOBOX_ARRONDIR(,)#17";

for (int i = 0; i < commandeList.count(); i++)
{
commande=new QListWidgetItem(ui.listWidgetOp);
commande->setFont(fontCommande);
commande->setText(commandeList.at(i));
commande->setData(Qt::UserRole,roleList.at(i));
commentaire=new QListWidgetItem(ui.listWidgetOp);
commentaire->setFont(fontCommentaire);
commentaire->setText(commentaireList.at(i));
commentaire->setFlags(Qt::ItemIsEnabled);
}

connect(ui.listWidget, SIGNAL(itemClicked ( QListWidgetItem*)), this, SLOT(InsertVariable(QListWidgetItem*)));
connect(ui.listWidgetOp, SIGNAL(itemClicked ( QListWidgetItem*)), this, SLOT(InsertOperation(QListWidgetItem*)));
}

AffichercalculDialog::~AffichercalculDialog(){
}

void AffichercalculDialog::accept()
{

QDialog::accept();
}



void AffichercalculDialog::InsertVariable(QListWidgetItem *item)
{
if (item && item->font().bold())
    {
    QString txt=item->text();
    int pos=ui.lineEditAffectation->cursorPosition();
    ui.lineEditAffectation->insert(txt);
    ui.lineEditAffectation->setCursorPosition(pos+txt.length());
    ui.lineEditAffectation->setFocus();
    }
}

void AffichercalculDialog::InsertOperation(QListWidgetItem *item)
{
if (item && item->font().bold())
    {
    QString role=item->data(Qt::UserRole).toString();
    QStringList tagList= role.split("#");
    int pos=ui.lineEditAffectation->cursorPosition();
    int dx=tagList.at(1).toInt();
    ui.lineEditAffectation->insert(tagList.at(0));
    ui.lineEditAffectation->setCursorPosition(pos+dx);
    ui.lineEditAffectation->setFocus();
    }
}
