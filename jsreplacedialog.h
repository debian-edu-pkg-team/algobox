/***************************************************************************
 *   copyright       : (C) 2009-2022 by Pascal Brachet                     *
 *   https://www.xm1math.net/algobox/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   @license GPL-2.0+ <https://spdx.org/licenses/GPL-2.0+.html>           *
 ***************************************************************************/

#ifndef JSREPLACEDIALOG_H
#define JSREPLACEDIALOG_H

#include "ui_replacedialog.h"
#include "jseditor.h"

class JSReplaceDialog : public QDialog
{ 
    Q_OBJECT

public:
    JSReplaceDialog(QWidget* parent = 0, const char* name = 0);
    ~JSReplaceDialog();
    Ui::ReplaceDialog ui;

public slots:
    virtual void doReplace();
    virtual void doReplaceAll();
    void SetEditor(JSEditor *ed);

protected:
    JSEditor *editor;
};

#endif // REPLACEDIALOG_H
