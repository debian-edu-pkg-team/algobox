/***************************************************************************
 *   copyright       : (C) 2009-2022 by Pascal Brachet                     *
 *   https://www.xm1math.net/algobox/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   @license GPL-2.0+ <https://spdx.org/licenses/GPL-2.0+.html>           *
 ***************************************************************************/

#ifndef BROWSER_H
#define BROWSER_H

#include <QMainWindow>
#include <QLineEdit>
#include <QPushButton>
#include <QWebEngineView>



class Browser : public QMainWindow
{
    Q_OBJECT
public:
    Browser( const QString home, QWidget* parent = 0);
    ~Browser();
    bool isClosed;

protected slots:
    void adjustTitle();
    void setProgress(int p);
    void finishLoading(bool);
private slots:
  void Sommaire();
  void Print();
  void Chercher();
private:
    void closeEvent(QCloseEvent *e);
    QWebEngineView *view;
    int progress;
    QLineEdit *searchLineEdit;
    QPushButton *findButton;
    QString pdffichier;
};

#endif

