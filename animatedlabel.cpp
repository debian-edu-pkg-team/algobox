/***************************************************************************
 *   copyright       : (C) 2009-2022 by Pascal Brachet                     *
 *   https://www.xm1math.net/algobox/                                       *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   @license GPL-2.0+ <https://spdx.org/licenses/GPL-2.0+.html>           *
 ***************************************************************************/

#include "animatedlabel.h"
#include <QImage>

AnimatedLabel::AnimatedLabel(QWidget *parent)
: QLabel(parent), currentPixmap(0)
{
QImage img;
img.load(":/images/process.png");
int subImageHeight = img.height() / 8;
 
for (int i = 0; i < 8; i++)
{
QImage subImage = img.copy(0, i * subImageHeight, img.width(), subImageHeight);
pixmaps.push_back(QPixmap::fromImage(subImage));
}
 
connect(&timer, SIGNAL(timeout()), SLOT(changeImage()));
timer.start(100);
changeImage();
}
 
void AnimatedLabel::changeImage()
{
if (currentPixmap >= pixmaps.length())
currentPixmap = 0;
 
setPixmap(pixmaps.at(currentPixmap));
 
currentPixmap++;
}

