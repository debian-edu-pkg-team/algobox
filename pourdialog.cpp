/***************************************************************************
 *   copyright       : (C) 2009-2022 by Pascal Brachet                     *
 *   https://www.xm1math.net/algobox/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   @license GPL-2.0+ <https://spdx.org/licenses/GPL-2.0+.html>           *
 ***************************************************************************/

#include "pourdialog.h"
#include <QFile>
#include <QTextStream>
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    #include <QtCore/QTextCodec>
#else
    #include <QtCore5Compat/QTextCodec>
#endif

PourDialog::PourDialog(QWidget *parent, QString variables)
    :QDialog( parent)
{
ui.setupUi(this);
setModal(true);
QTextCodec *codec = QTextCodec::codecForName("UTF-8");
QString contenu;
QFile aide(":/documents/aideboucle.txt");
aide.open(QIODevice::ReadOnly);
QTextStream in(&aide);
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
in.setCodec(codec);
#else
in.setEncoding(QStringConverter::Encoding::Utf8);
#endif
while (!in.atEnd()) 
	{
	contenu+= in.readLine()+"\n";
	}
aide.close();
ui.textEdit->setHtml(contenu);
listeVariables=variables.split("#");
ui.comboBoxBoucle->addItems(listeVariables);
ui.lineEditBoucle1->setAcceptDrops(true);
ui.lineEditBoucle2->setAcceptDrops(true);
ui.listWidget->setDragEnabled(true);
if (ui.comboBoxBoucle->count()>0) ActualiserWidget(ui.comboBoxBoucle->currentIndex());
connect(ui.comboBoxBoucle, SIGNAL(currentIndexChanged(int)),this,SLOT(ActualiserWidget(int)));
}

PourDialog::~PourDialog(){
}

void PourDialog::ActualiserWidget(int index)
{
QFont fontCommande=qApp->font();
fontCommande.setBold(true);
QListWidgetItem *commande;
ui.listWidget->clear();
QStringList tempList;
for (int i = 0; i < listeVariables.count(); i++)
  {
  if (i!=index) tempList.append(listeVariables.at(i));
  }
//ui.listWidget->addItems(tempList);
for (int i = 0; i < tempList.count(); i++)
  {
  commande=new QListWidgetItem(ui.listWidget);
  commande->setFont(fontCommande);
  commande->setText(tempList.at(i));
  //commande->setFlags(Qt::ItemIsDragEnabled | Qt::ItemIsEnabled);
  }
}
