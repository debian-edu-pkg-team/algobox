/***************************************************************************
 *   copyright       : (C) 2009-2022 by Pascal Brachet                     *
 *   https://www.xm1math.net/algobox/                                       *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   @license GPL-2.0+ <https://spdx.org/licenses/GPL-2.0+.html>           *
 ***************************************************************************/

#ifndef ALGOBOX_H
#define ALGOBOX_H

#include "ui_algobox.h"
#include "browser.h"
#include "jsmainwindow.h"

#include <QMainWindow>
#include <QCloseEvent>
#include <QMenuBar>
#include <QToolBar>
#include <QAction>
#include <QActionGroup>
#include <QTreeWidgetItem>
#include <QDomDocument>
#include <QDomElement>
#include <QDomNode>
#include <QDomNodeList>
#include <QCompleter>
#include <QProgressDialog>

class MainWindow : public QMainWindow  {
   Q_OBJECT
public:
	MainWindow(QWidget *parent=0);
	~MainWindow();
	Ui::MainWindow ui;
QString dernierRepertoire, nomFichier;
QByteArray windowstate;
public slots:
void OuvrirNouvelAlgo(QString nouveauFichier);
private slots:
void closeEvent(QCloseEvent *e);
void Quitter();
//***************
void AjouterLigne();
void NouvelleLigne(QTreeWidgetItem *item);
void SupprimerLigne();
void ModifierLigne();
void ActualiserArbre();
void ActualiserVariables();
//**********************
void AjouterVariable();
void AjouterLire();
void AjouterAfficher();
void AjouterMessage();
void AjouterCalcul();
void AjouterPause();
void AjouterAffectation();
void AjouterCondition();
void AjouterBoucle();
void AjouterTantque();
void AjouterPoint();
void AjouterSegment();
void AjouterEffacer();
void AjouterCommentaire();
void AjouterFonction();
void AjouterVariableLocale();
void AjouterRenvoyer();
void AjouterAppelerFct();
//**********************
void ModifierLire();
void ModifierAfficher();
void ModifierMessage();
void ModifierCalcul();
void ModifierAffectation();
void ModifierCondition();
void ModifierBoucle();
void ModifierTantque();
void ModifierPoint();
void ModifierSegment();
void ModifierCommentaire();
void ModifierFonction();
void ModifierRenvoyer();
void ModifierAppelerFct();
//***************
void LireConfig();
void SauverConfig();
void InitOuvrir();
void Init();
void EffaceArbre();
//****************
void ActiverBoutons();
void DesactiverBoutons();
void ActiverFonction(bool etat);
void ActiverF2(bool etat);
void ActiverRepere(bool etat);
//****************
void AjouterF2();
void HautF2();
void BasF2();
void SupprimerF2();
void ModifierLigneF2(QListWidgetItem *item);
//****************
bool NomInterdit(QString nom);
//****************
QString GenererCode(bool exporthtml);
QString CodeVersJavascript(QString code,bool exporthtml, int id);
QString FiltreNomVariable(QString orig);
QString FiltreCalcul(QString orig);
QString FiltreCondition(QString orig);
QString CodeNoeud(QTreeWidgetItem *item,bool exporthtml);
QString AlgoNoeud(QTreeWidgetItem *item);
QString AlgoNoeudTexte(QTreeWidgetItem *item);
QString AlgoNoeudCode(QTreeWidgetItem *item);
void JavascriptExport();
void ExporterVersTexte();
void ExporterVersODF();
void ExporterVersLatex();
void ExporterVersHtml();
void CopieEcran();
void Imprimer();
//****************
void NouvelAlgo();
void ItemVersXml(QTreeWidgetItem *item,QDomDocument doc ,QDomElement parent);
void SauverAlgo();
void SauverSousAlgo();
void XmlVersItem(QTreeWidgetItem *parentItem, QDomElement element);
void ChargerAlgo();
void Ouvrir(QString nouveauFichier);
void ActualiserStatut();
void NouveauStatut(bool m);
void APropos();
void Aide();
void Tutoriel();
void SetInterfaceFont();
void CouleurConsole();
void fileOpenRecent();
void AddRecentFile(const QString &f);
void UpdateRecentFile();
void ChargerExemple();
//*****************
void EditCopier();
void EditColler();
void EditCouper();
//*****************
void ToggleCadrePresentation();
void ActualiserModeButton();
void ActualiserMode();
//*****************
//void ImporterCodeTexte();
QString EditeurVersArbre();
QString ArbreVersCodeTexte();
void VerifierCodeTexte();
void ExpandBranche(QTreeWidgetItem *item);
void InsertOperation(QListWidgetItem *item);
//*****************
void ChargerExtension();
void AnnulerExtension();
void LancerJSEditeur();
//*****************
void Configurer();
void ActualiserTexteParam();

private :
QMenu *fichierMenu, *aideMenu, *recentMenu, *editMenu, *tutoMenu, *affichageMenu, *modeMenu, *extensionMenu, *optionsMenu;
QActionGroup *modeGroup;
QAction *recentFileActs[5];
QAction *actionCopier, *actionColler, *actionCouper, *ToggleAct, *annulerExtensionAct, *actionModeNormal, *actionModeTexte;
QStringList recentFilesList;
QToolBar *fileToolBar;
QStringList ListeNomsInterdits, ListeNomsVariables, ListeTypesVariables;
QTreeWidgetItem *variablesItem, *debutItem, *finItem, *fctsItem;
int indent, idligne,browserwidth,browserheight;
bool estModifie, afficheCadrePresentation, /*repereDefini,*/ modeNormal, estVierge, blackconsole;
QTreeWidgetItem *clipboardItem;
QCompleter *completer;
QString x11fontfamily;
int x11fontsize;
QPointer<Browser> browserWindow;
QPointer<JSMainWindow> jseditWindow;
QString fichier_extension;
QPointer<QProgressDialog> progressDialog;
int progressCompteur;
QString highlightHtmlLine(const QString &text);
QList<int> detectChaine(const QString &text);
QPushButton *buttonModeNormal, *buttonModeTexte;

bool arrondiAuto;
int maxBoucle, totalBoucles, totalAffichages,epaisseurLigne,epaisseurPoint,nbDecimales;

bool arrondiAutoAlgo;
int maxBoucleAlgo, totalBouclesAlgo, totalAffichagesAlgo,epaisseurLigneAlgo,epaisseurPointAlgo,nbDecimalesAlgo;

bool dansFct;

protected:
void dragEnterEvent(QDragEnterEvent *event);
void dropEvent(QDropEvent *event);
};


#endif

