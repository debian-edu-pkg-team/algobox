/***************************************************************************
 *   copyright       : (C) 2009-2022 by Pascal Brachet                     *
 *   https://www.xm1math.net/algobox/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   @license GPL-2.0+ <https://spdx.org/licenses/GPL-2.0+.html>           *
 ***************************************************************************/

#include <QtGui>
#include <QRegularExpression>

#include "loghighlighter.h"

LogHighlighter::LogHighlighter(QTextDocument *parent, bool blackschema)
    : QSyntaxHighlighter(parent)
{
MessageFormat.setFontWeight(QFont::Bold);
PasAPasFormat.setFontWeight(QFont::Bold);
ConditionFormat.setFontWeight(QFont::Bold);

if (blackschema)
  {
  ConditionFormat.setForeground(QColor("#C06DBB"));
  MessageFormat.setForeground(QColor("#D7B54F"));
  PasAPasFormat.setForeground(QColor("#AACC00"));
  }
else
  {
  ConditionFormat.setForeground(QColor(0x00,0x80, 0x00));
  MessageFormat.setForeground(QColor(0x00, 0x00, 0xCC));
  PasAPasFormat.setForeground(QColor(0x80, 0x00, 0x00));
  }
}

void LogHighlighter::highlightBlock(const QString &text)
{

QString t;
QRegularExpression rxMessage("\\*\\*\\*(.*)\\*\\*\\*");
QRegularExpression rxPasAPas("#([0-9eE\\.\\-]+)\\s*(Nombres/chaines |Liste )(.*)\\(ligne\\s*([0-9eE\\.\\-]+)\\) -> ");
QRegularExpression rxCondition("(La condition|Entr|Sortie)");
if ((rxMessage.match(text).hasMatch()) || text.startsWith("Trac") )
	{
	setFormat(0, text.length(),MessageFormat);
	}
else if (rxPasAPas.match(text).hasMatch())
	{
	t=text;
	t.remove(rxPasAPas);
	setFormat(0, text.length()-t.length(),PasAPasFormat);
	}
else if (rxCondition.match(text).hasMatch())
	{
	setFormat(0, text.length(),ConditionFormat);
	}
}
