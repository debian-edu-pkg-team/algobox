/***************************************************************************
 *   copyright       : (C) 2009-2022 by Pascal Brachet                     *
 *   https://www.xm1math.net/algobox/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   @license GPL-2.0+ <https://spdx.org/licenses/GPL-2.0+.html>           *
 ***************************************************************************/

#ifndef TANTQUEDIALOG_H
#define TANTQUEDIALOG_H

#include "ui_tantquedialog.h"
#include <QListWidget>
#include <QListWidgetItem>

class TantqueDialog : public QDialog  {
   Q_OBJECT
public:
	TantqueDialog(QWidget *parent=0,QString variables="", QString types="");
	~TantqueDialog();
	Ui::TantqueDialog ui;
private :
QStringList listeVariables, listeTypes, listeVarNombre, listeVarListe, listeVarChaine;
private slots:
void InsertVariable(QListWidgetItem *item);
void InsertOperation(QListWidgetItem *item);
};


#endif
