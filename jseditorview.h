/***************************************************************************
 *   copyright       : (C) 2009-2022 by Pascal Brachet                     *
 *   https://www.xm1math.net/algobox/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   @license GPL-2.0+ <https://spdx.org/licenses/GPL-2.0+.html>           *
 ***************************************************************************/

#ifndef JSEDITORVIEW_H
#define JSEDITORVIEW_H

#include <QWidget>
#include <QFont>
#include <QColor>
#include <QPointer>
#include "jseditor.h"
#include "jslinenumberwidget.h"
#include "jsfindwidget.h"
#include "jsreplacedialog.h"

class JSEditorView : public QWidget  {
   Q_OBJECT
public: 
JSEditorView(QWidget *parent);
~JSEditorView();
JSEditor *editor;
JSFindWidget *findwidget;
void setFontSize(int size);
private:
JSLineNumberWidget* m_lineNumberWidget;
QPointer<JSReplaceDialog> replaceDialog;
QFont efont;
private slots:
void setLineNumberWidgetVisible( bool );
void editFind();
void editReplace();
};

#endif
