/***************************************************************************
 *   copyright       : (C) 2009-2022 by Pascal Brachet                     *
 *   https://www.xm1math.net/algobox/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   @license GPL-2.0+ <https://spdx.org/licenses/GPL-2.0+.html>           *
 ***************************************************************************/

#include <QApplication>
#include <QTranslator>
#include <QFontDatabase>
#include <QDebug>
#include <QNetworkProxyFactory>
#include <qtwebenginewidgetsglobal.h>


#include "algobox.h"

#define STRINGIFY_INTERNAL(x) #x
#define STRINGIFY(x) STRINGIFY_INTERNAL(x)

#define VERSION_STR STRINGIFY(ALGOBOXVERSION)

char** appendCommandLineArguments(int argc, char **argv, const QStringList& args)
{
  size_t newSize = (argc + args.length() + 1) * sizeof(char*);
  char** newArgv = (char**)calloc(1, newSize);
  memcpy(newArgv, argv, (size_t)(argc * sizeof(char*)));

  int pos = argc;
  for(const QString& str : args)
    newArgv[pos++] = qstrdup(str.toUtf8().data());

  return newArgv;
}

class AlgoBoxApp : public QApplication
{
private:
    MainWindow *mw;
protected:
    bool event(QEvent *event);
public:
    AlgoBoxApp( int & argc, char ** argv );
    ~AlgoBoxApp();
};

AlgoBoxApp::AlgoBoxApp( int & argc, char ** argv ) : QApplication ( argc, argv )
{
QTranslator* basicTranslator=new QTranslator(this);
#if defined(Q_OS_UNIX) && !defined(Q_OS_MAC)
#ifdef USB_VERSION
QString transdir=QCoreApplication::applicationDirPath()+"/ressources";
#else
QString transdir=PREFIX"/share/algobox";
#endif
#endif
#if defined(Q_OS_MAC)
QString transdir=QCoreApplication::applicationDirPath() + "/../Resources";
#endif
#if defined(Q_OS_WIN32)
QString transdir=QCoreApplication::applicationDirPath()+"/ressources";
#endif
if (basicTranslator->load(QString("qt_fr"),transdir)) installTranslator(basicTranslator);
QFontDatabase::applicationFontFamilies(QFontDatabase::addApplicationFont(":/documents/LiberationMono-Regular.ttf"));
mw=new MainWindow();
mw->show();

for ( int i = 1; i < argc; ++i )
	{
	QString arg = argv[ i ];
	if ( arg[0] != '-' )    mw->OuvrirNouvelAlgo( arg );
	}
}

AlgoBoxApp::~AlgoBoxApp()
{
    delete mw;
}

bool AlgoBoxApp::event ( QEvent * event )
{
    if (event->type() == QEvent::FileOpen) {
        QFileOpenEvent *oe = static_cast<QFileOpenEvent *>(event);
        mw->OuvrirNouvelAlgo(oe->file());
    }
    return QApplication::event(event);
}

int main(int argc, char *argv[])
{
#if !defined(Q_OS_MAC)
QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif
#if QT_VERSION >= QT_VERSION_CHECK(5, 14, 0)
qputenv("QT_ENABLE_HIGHDPI_SCALING", "1");
QGuiApplication::setHighDpiScaleFactorRoundingPolicy(Qt::HighDpiScaleFactorRoundingPolicy::PassThrough);
#endif
QStringList rawargs;
for ( int i = 0; i < argc; ++i )
	{
	rawargs.append( argv[ i ]);
	}
for (QStringList::Iterator it = ++(rawargs.begin()); it != rawargs.end(); it++)
    {
    if ( ( *it == "-dpiscale") && (++it != rawargs.end())) {qputenv("QT_SCALE_FACTOR", (*it).toUtf8());}
    }
QStringList g_qtFlags = {"--disable-gpu","--proxy-bypass-list","--no-proxy-server"};
char **newArgv = appendCommandLineArguments(argc, argv, g_qtFlags);
int newArgc = argc + g_qtFlags.size();

QNetworkProxyFactory::setUseSystemConfiguration(false);
AlgoBoxApp a( newArgc, newArgv);
a.setApplicationName("Algobox");
a.setApplicationVersion(VERSION_STR);
a.setOrganizationName("xm1");
a.setAttribute(Qt::AA_UseHighDpiPixmaps);
#if defined(Q_OS_UNIX) && !defined(Q_OS_MAC)
QApplication::setAttribute(Qt::AA_DontUseNativeMenuBar);
#endif
return a.exec();
}
