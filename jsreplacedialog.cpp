/***************************************************************************
 *   copyright       : (C) 2009-2022 by Pascal Brachet                     *
 *   https://www.xm1math.net/algobox/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   @license GPL-2.0+ <https://spdx.org/licenses/GPL-2.0+.html>           *
 ***************************************************************************/

#include "jsreplacedialog.h"
#include <QMessageBox>

JSReplaceDialog::JSReplaceDialog(QWidget* parent,  const char* name)
    : QDialog( parent)
{
setWindowTitle(name);
setModal(true);
ui.setupUi(this);
connect( ui.findButton, SIGNAL( clicked() ), this, SLOT( doReplace() ) );
connect( ui.replaceallButton, SIGNAL( clicked() ), this, SLOT( doReplaceAll() ) );
connect( ui.closeButton, SIGNAL( clicked() ), this, SLOT( reject() ) );
ui.findButton->setShortcut(Qt::Key_Return);
}


JSReplaceDialog::~JSReplaceDialog()
{
}

void JSReplaceDialog::doReplace()
{
reject();
if ( !editor ) 	return;
bool go=true;
while (go && editor->search( ui.comboFind->currentText(), ui.checkCase->isChecked(),
	ui.checkWords->isChecked(), ui.radioForward->isChecked(), !ui.checkBegin->isChecked()) )
       {
       switch(  QMessageBox::warning(this, "AlgoBox",QString::fromUtf8("Remplacer cette occurence ? "),QString::fromUtf8("Oui"), QString::fromUtf8("Non"), QString::fromUtf8("Abandon"), 0,2 ) )
         {
         case 0:
         editor->replace(ui.comboReplace->currentText() );
         ui.checkBegin->setChecked( false );
    	   break;
         case 1:
         ui.checkBegin->setChecked( true );
    	   break;
         case 2:
         go=false;
    	   break;
         }
       }
if (go) ui.checkBegin->setChecked( true );
}

void JSReplaceDialog::doReplaceAll()
{
if ( !editor ) return;
while ( editor->search( ui.comboFind->currentText(), ui.checkCase->isChecked(),
ui.checkWords->isChecked(), ui.radioForward->isChecked(), !ui.checkBegin->isChecked()) )
    {
    editor->replace(ui.comboReplace->currentText() );
    ui.checkBegin->setChecked( false );
    }
ui.checkBegin->setChecked( true );
}

void JSReplaceDialog::SetEditor(JSEditor *ed)
{
editor=ed;
}



