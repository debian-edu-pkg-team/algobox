algobox (1.1.1+dfsg-1) unstable; urgency=medium

  * New upstream version 1.1.1+dfsg
  * Use DEP-14 branch name
  * Update standards version to 4.6.1
  * Update copyright (years)

 -- David Prévot <taffit@debian.org>  Thu, 03 Nov 2022 20:44:41 +0100

algobox (1.1.0+dfsg-1) unstable; urgency=medium

  * New upstream version 1.1.0+dfsg
  * Update watch file format version to 4.
  * Bump debhelper from old 12 to 13.
  * Update standards version to 4.6.0, no changes needed.
  * Set Rules-Requires-Root: no.
  * Update copyright (years)
  * Use execute_before_ instead of override_

 -- David Prévot <taffit@debian.org>  Fri, 12 Nov 2021 14:47:35 -0400

algobox (1.0.3+dfsg-1) unstable; urgency=medium

  * New upstream version 1.0.3+dfsg
  * Use https in debian/
  * d/control:
    - Bump debhelper from old 9 to 12.
    - Update Standards-Version to 4.4.1
  * d/copyright: Update copyright (years)
  * d/doc-base: Point to symlink path in /u/s/d/
  * d/lintian-overrides: Extend spelling override
  * d/patches: Add Comment entry to .desktop file
  * d/rules
    - Drop get-orig-source target
    - Use hardening options
  * d/watch: Use dversionmangle instead of uversionmangle

 -- David Prévot <taffit@debian.org>  Tue, 15 Oct 2019 20:54:26 -1000

algobox (1.0.2+dfsg-2) unstable; urgency=medium

  * Add missing include
    Thanks to Santiago Vila <sanvila@debian.org> (Closes: #906968)
  * Use priority optional
  * Move project repository to salsa.d.o
  * Update Standards-Version to 4.2.0

 -- David Prévot <taffit@debian.org>  Wed, 22 Aug 2018 10:58:42 -1000

algobox (1.0.2+dfsg-1) unstable; urgency=medium

  * New upstream version 1.0.2+dfsg
  * Update Standards-Version to 4.0.1

 -- David Prévot <taffit@debian.org>  Sat, 19 Aug 2017 17:04:34 -1000

algobox (1.0.1+dfsg-1) unstable; urgency=medium

  * New upstream version 1.0.1+dfsg
  * Update copyright
  * Update Build-Dependencies
    - Drop libqt5opengl5-dev
    - Use webengine instead of webkit
    - Use qtbase5-dev instead of qt5-default
  * Update Standards-Version to 4.0.0

 -- David Prévot <taffit@debian.org>  Thu, 03 Aug 2017 10:10:43 -0400

algobox (0.9+dfsg-1) unstable; urgency=medium

  * Use Files-Excluded feature of uscan
  * Imported Upstream version 0.9+dfsg
  * Update copyright
  * Update build-dependencies for Qt5

 -- David Prévot <taffit@debian.org>  Thu, 28 Aug 2014 08:33:25 -0400

algobox (0.8+dfsg-2) unstable; urgency=low

  * Use fonts-liberation at runtime (Thanks to Pascal Brachet)
  * Bump standards version to 3.9.5

 -- David Prévot <taffit@debian.org>  Mon, 28 Oct 2013 16:39:02 -0400

algobox (0.8+dfsg-1) unstable; urgency=low

  * Initial release (Closes: #723942)

 -- David Prévot <taffit@debian.org>  Sat, 21 Sep 2013 13:21:16 -0400
