/***************************************************************************
 *   copyright       : (C) 2009-2022 by Pascal Brachet                     *
 *   https://www.xm1math.net/algobox/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   @license GPL-2.0+ <https://spdx.org/licenses/GPL-2.0+.html>           *
 ***************************************************************************/

#include <QApplication>
#include "geticon.h"
QIcon getIcon(QString name)
{
QIcon dIcon=QIcon();
QString base=name.remove(".png");
dIcon.addFile(base+".png");
if (qApp->devicePixelRatio()>=2)
{
dIcon.addFile(base+"@2x.png");
}
return dIcon;
}


