/***************************************************************************
 *   copyright       : (C) 2009-2022 by Pascal Brachet                     *
 *   https://www.xm1math.net/algobox/                                       *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   @license GPL-2.0+ <https://spdx.org/licenses/GPL-2.0+.html>           *
 ***************************************************************************/

#ifndef AFFECTATIONDIALOG_H
#define AFFECTATIONDIALOG_H

#include "ui_affectationdialog.h"
#include <QListWidget>
#include <QListWidgetItem>
#include <QFont>

class AffectationDialog : public QDialog  {
   Q_OBJECT
public:
	AffectationDialog(QWidget *parent=0,QString variables="", QString types="");
	~AffectationDialog();
	Ui::AffectationDialog ui;
private :
QStringList listeVariables, listeTypes, listeVarNombre, listeVarListe, listeVarChaine;
QFont fontCommande, fontCommentaire;
private slots:
void accept();
void ActualiserWidget(int index);
void InsertVariable(QListWidgetItem *item);
void InsertOperation(QListWidgetItem *item);
};


#endif
