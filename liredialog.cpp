/***************************************************************************
 *   copyright       : (C) 2009-2022 by Pascal Brachet                     *
 *   https://www.xm1math.net/algobox/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   @license GPL-2.0+ <https://spdx.org/licenses/GPL-2.0+.html>           *
 ***************************************************************************/

#include "liredialog.h"

#include <QMessageBox>

LireDialog::LireDialog(QWidget *parent, QString variables, QString types )
    :QDialog( parent)
{
ui.setupUi(this);
setModal(true);
listeVariables=variables.split("#");
listeTypes=types.split("#");
ui.comboBoxLire->addItems(listeVariables);
ui.labelLire2->setEnabled(false);
ui.lineEditLire->setEnabled(false);
if (ui.comboBoxLire->count()>0) ActualiserWidget(ui.comboBoxLire->currentIndex());
connect(ui.comboBoxLire, SIGNAL(activated(int)),this,SLOT(ActualiserWidget(int)));
}

LireDialog::~LireDialog(){
}

void LireDialog::accept()
{
if ( ui.lineEditLire->isEnabled() && ui.lineEditLire->text().isEmpty()) 
	{
	QMessageBox::warning( this,QString::fromUtf8("Erreur"),QString::fromUtf8("Rectifiez le rang du terme de la liste : il est vide"));
	return;
	}
QDialog::accept();
}

void LireDialog::ActualiserWidget(int index)
{
if (listeTypes.at(index)=="LISTE")
    {
    ui.lineEditLire->clear();
    ui.labelLire2->setEnabled(true);
    ui.lineEditLire->setEnabled(true);
    }
else
    {
    ui.lineEditLire->clear();
    ui.labelLire2->setEnabled(false);
    ui.lineEditLire->setEnabled(false);
    }
}
