/***************************************************************************
 *   copyright       : (C) 2009-2022 by Pascal Brachet                     *
 *   https://www.xm1math.net/algobox/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   @license GPL-2.0+ <https://spdx.org/licenses/GPL-2.0+.html>           *
 ***************************************************************************/

#ifndef JSEDITOR_H
#define JSEDITOR_H

#include <QWidget>
#include <QString>
#include <QTextEdit>
#include <QTextDocument>
#include <QTextCursor>
#include <QTextBlock>

#include "jshighlighter.h"

typedef  int UserBookmarkList[3];

class JSEditor : public QTextEdit  {
   Q_OBJECT
public:
JSEditor(QWidget *parent,QFont & efont);
~JSEditor();
JSHighlighter *highlighter;
UserBookmarkList UserBookmark;
public slots:
void matchAll();
void gotoLine( int line );
bool search( const QString &expr, bool cs, bool wo, bool forward, bool startAtCursor );
void replace( const QString &r);
void commentSelection();
void uncommentSelection();
void indentSelection();
void unindentSelection();
void changeFont(QFont & new_font);
QString getEncoding();
void setEncoding(QString enc);
int getCursorPosition(int parag, int index);
void setCursorPosition(int para, int index);
int numoflines();
int linefromblock(const QTextBlock& p);
void selectword(int line, int col, QString word);
void insertTag(QString Entity, int dx, int dy);
void insertNewLine();
private:
QString encoding;
QString textUnderCursor() const;
bool isWordSeparator(QChar c) const;
bool isSpace(QChar c) const;
bool matchLeftPar ( QTextBlock currentBlock, int index, int numRightPar );
bool matchRightPar( QTextBlock currentBlock, int index, int numLeftPar );
bool matchLeftBrack ( QTextBlock currentBlock, int index, int numRightBrack );
bool matchRightBrack( QTextBlock currentBlock, int index, int numLeftBrack );
void createParSelection( int pos );
void createBrackSelection( int pos );
private slots:
void wantFind();
void wantReplace();
void gotoBookmark1();
void gotoBookmark2();
void gotoBookmark3();
void matchPar();
void matchBrack();
protected:
void paintEvent(QPaintEvent *event);
void contextMenuEvent(QContextMenuEvent *e);
void keyPressEvent ( QKeyEvent * e );
void focusInEvent(QFocusEvent *e);
signals:
void dofind();
void doreplace();
};

#endif
