/***************************************************************************
 *   copyright       : (C) 2009-2022 by Pascal Brachet                     *
 *   https://www.xm1math.net/algobox/                                      *
 *   contains some code from CLedit (C) 2010 Heinz van Saanen -GPL         *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   @license GPL-2.0+ <https://spdx.org/licenses/GPL-2.0+.html>           *
 ***************************************************************************/


#include "blockdata.h"


QVector<ParenthesisInfo *> BlockData::parentheses() {
	return m_parentheses;
}

void BlockData::insertPar( ParenthesisInfo *info ) {
	int i = 0;
	while (
		i < m_parentheses.size() &&
		info->position > m_parentheses.at(i)->position )
		++i;
	m_parentheses.insert( i, info );
}

QVector<ParenthesisInfo *> BlockData::brackets() {
	return m_brackets;
}

void BlockData::insertBrack( ParenthesisInfo *info ) {
	int i = 0;
	while (
		i < m_brackets.size() &&
		info->position > m_brackets.at(i)->position )
		++i;
	m_brackets.insert( i, info );
}
