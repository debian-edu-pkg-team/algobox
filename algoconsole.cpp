/***************************************************************************
 *   copyright       : (C) 2009-2022 by Pascal Brachet                     *
 *   https://www.xm1math.net/algobox/                                       *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   @license GPL-2.0+ <https://spdx.org/licenses/GPL-2.0+.html>           *
 ***************************************************************************/


#include "algoconsole.h"
#include <QScrollBar>
#include <QAction>
#include <QMenu>
#include <QDebug>

AlgoConsole::AlgoConsole(QWidget *parent) :
	QTextEdit(parent)
{
setFocusPolicy(Qt::WheelFocus);
setTextInteractionFlags(Qt::TextSelectableByMouse|Qt::TextSelectableByKeyboard);
    isLocked = true;
    answer="";
    prompt="";
}


void AlgoConsole::contextMenuEvent(QContextMenuEvent *e)
{
QMenu *menu=new QMenu(this);
//menu = createStandardContextMenu();
QAction *a;


a = menu->addAction(tr("Copy"), this, SLOT(copy()));
a->setShortcut(Qt::CTRL+Qt::Key_C);
a->setEnabled(textCursor().hasSelection());

a = menu->addAction(tr("Select All"), this, SLOT(selectAll()));
a->setShortcut(Qt::CTRL+Qt::Key_A);
a->setEnabled(!document()->isEmpty());

menu->exec(e->globalPos());
delete menu;
}

void AlgoConsole::keyPressEvent(QKeyEvent *event)
{
    if(isLocked) return;
    QString t=event->text();
    QChar l;
    if (t.count()>0) 
    {
      l=t.at(0);
      if (l.isLetterOrNumber() || l.isPrint()) 
	{
	QTextEdit::keyPressEvent(event);
	return;
	}
    }
    if(event->key() >= 0x20 && event->key() <= 0x7e
       && (event->modifiers() == Qt::NoModifier || event->modifiers() == Qt::ShiftModifier))
	QTextEdit::keyPressEvent(event);
    if(event->key() == Qt::Key_Backspace
       && event->modifiers() == Qt::NoModifier
       && textCursor().positionInBlock() > prompt.length())
	QTextEdit::keyPressEvent(event);
    if(event->key() == Qt::Key_Return && event->modifiers() == Qt::NoModifier)
	onEnter();
    if(event->key() == Qt::Key_Enter)
	onEnter();
}

void AlgoConsole::onEnter()
{
    if(textCursor().positionInBlock() == prompt.length())
    {
	return;
    }
    answer = textCursor().block().text().mid(prompt.length());
    isLocked = true;
    textCursor().insertBlock();
    scrollDown();
    emit done();
}

void AlgoConsole::output(QString s)
{
    prompt=s+" ";
    if (!textCursor().block().text().isEmpty()) textCursor().insertBlock();
    textCursor().insertText(prompt);
    scrollDown();
    setFocus();
    isLocked = false;
    answer="";
}

void AlgoConsole::scrollDown()
{
    QScrollBar *vbar = verticalScrollBar();
    vbar->setValue(vbar->maximum());
}

void AlgoConsole::mouseMoveEvent(QMouseEvent *e)
{
if (isLocked) QTextEdit::mouseMoveEvent(e);
}
