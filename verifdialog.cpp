/***************************************************************************
 *   copyright       : (C) 2009-2022 by Pascal Brachet                     *
 *   https://www.xm1math.net/algobox/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   @license GPL-2.0+ <https://spdx.org/licenses/GPL-2.0+.html>           *
 ***************************************************************************/

#include "verifdialog.h"


VerifDialog::VerifDialog(QWidget *parent)
    :QDialog( parent)
{
ui.setupUi(this);
setModal(true);
ui.treeWidget->setColumnCount(1);
ui.treeWidget->header()->hide();
ui.treeWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);
ui.treeWidget->header()->setHorizontalScrollMode(QAbstractItemView::ScrollPerPixel);
//ui.treeWidget->header()->setResizeMode(0, QHeaderView::Stretch);
#if (QT_VERSION >= QT_VERSION_CHECK(5, 0, 0))
ui.treeWidget->header()->setSectionResizeMode(QHeaderView::ResizeToContents);
#else
ui.treeWidget->header()->setResizeMode(0, QHeaderView::ResizeToContents);
#endif
ui.treeWidget->header()->setStretchLastSection(false);
}

VerifDialog::~VerifDialog(){
}

void VerifDialog::ExpandBranche(QTreeWidgetItem *item)
{
ui.treeWidget->expandItem(item);
int nb_branches=item->childCount();
if (nb_branches>0)
	{
	for (int i = 0; i < nb_branches; i++) 
		{
		ExpandBranche(item->child(i));
		}
	}
}
