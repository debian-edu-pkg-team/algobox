/***************************************************************************
 *   copyright       : (C) 2009-2022 by Pascal Brachet                     *
 *   https://www.xm1math.net/algobox/                                       *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   @license GPL-2.0+ <https://spdx.org/licenses/GPL-2.0+.html>           *
 ***************************************************************************/

#ifndef ANIMATEDLABEL_H
#define ANIMATEDLABEL_H

#include <QWidget>
#include <QString>
#include <QTimer>
#include <QLabel>
#include <QList>
#include <QPixmap>
 
class AnimatedLabel : public QLabel
{
Q_OBJECT
 
public:
AnimatedLabel(QWidget* parent = 0);
 
private slots:
void changeImage();
 
private:
QList<QPixmap> pixmaps;
int currentPixmap;
QTimer timer;
};
 


#endif
