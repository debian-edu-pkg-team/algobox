/***************************************************************************
 *   copyright       : (C) 2009-2022 by Pascal Brachet                     *
 *   https://www.xm1math.net/algobox/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   @license GPL-2.0+ <https://spdx.org/licenses/GPL-2.0+.html>           *
 ***************************************************************************/
 
#include "browser.h"
#include "geticon.h"

#include <QtGui>
#include <QMenuBar>
#include <QToolBar>
#include <QFileDialog>
#include <QDir>
#include <QDesktopServices>
#include <QUrl>
#include <QTimer>
#include <QDebug>
#include <QApplication>
#include <QScreen>
#include <QWindow>
#include <QGuiApplication>


Browser::Browser( const QString home, QWidget* parent)
    : QMainWindow( parent)
{
isClosed=false;
setWindowTitle("AlgoBox");
#if defined(Q_OS_MAC)
setWindowIcon(QIcon(":/images/algobox128.png"));
#else
setWindowIcon(getIcon(":/images/algobox22.png"));
#endif
setIconSize(QSize(22,22 ));
progress = 0;
view = new QWebEngineView(this);
if ( !home.isEmpty()) view->load(QUrl(home));
connect(view, SIGNAL(titleChanged(QString)), SLOT(adjustTitle()));
connect(view, SIGNAL(loadProgress(int)), SLOT(setProgress(int)));
connect(view, SIGNAL(loadFinished(bool)), SLOT(finishLoading(bool)));


QMenu *fileMenu = menuBar()->addMenu(QString::fromUtf8("&Fichier"));
fileMenu->addAction(QString::fromUtf8("&Imprimer (pdf)"), this, SLOT(Print()));
fileMenu->addSeparator();
fileMenu->addAction(QString::fromUtf8("&Quitter"), this, SLOT(close()));

QToolBar *toolBar = addToolBar("Navigation");
QAction *Act;
Act = new QAction(getIcon(":/images/home.png"), QString::fromUtf8("Sommaire"), this);
connect(Act, SIGNAL(triggered()), this, SLOT(Sommaire()));
toolBar->addAction(Act);
Act=view->pageAction(QWebEnginePage::Back);
Act->setIcon(getIcon(":/images/go-previous.png"));
toolBar->addAction(Act);
Act=view->pageAction(QWebEnginePage::Forward);
Act->setIcon(getIcon(":/images/go-next.png"));
toolBar->addAction(Act);

QWidget* spacer = new QWidget();
spacer->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
toolBar->addWidget(spacer);
searchLineEdit = new QLineEdit(toolBar);
connect(searchLineEdit, SIGNAL(returnPressed()), this, SLOT(Chercher()));
toolBar->addWidget(searchLineEdit);

findButton=new QPushButton(QString::fromUtf8("Chercher"),toolBar);
connect(findButton, SIGNAL(clicked()), this, SLOT(Chercher()));
toolBar->addWidget(findButton);

setCentralWidget(view);
setUnifiedTitleAndToolBarOnMac(true);

pdffichier=QDir::homePath();
pdffichier="algobox_aide_temp_"+pdffichier.section('/',-1);
pdffichier=QString(QUrl::toPercentEncoding(pdffichier));
pdffichier.remove("%");
pdffichier=pdffichier+".pdf";
QString tempDir=QDir::tempPath();
pdffichier=tempDir+"/"+pdffichier;

#if (QT_VERSION >= QT_VERSION_CHECK(5, 14, 0))
   QRect screen = QWidget::screen()->geometry();
 #else
  QRect screen = (window() && window()->windowHandle() ? window()->windowHandle()->screen()->geometry() : QGuiApplication::primaryScreen()->geometry());
#endif
int w= screen.width()/2;
int h= screen.height()-150 ;
int x= screen.width()/2;
int y= 30 ;

resize(w,h);
move(x,y);
}

Browser::~Browser()
{

}

void Browser::closeEvent(QCloseEvent *e)
{
isClosed=true;
e->accept();
}

void Browser::adjustTitle()
{
if (progress <= 0 || progress >= 100)
    setWindowTitle(view->title());
else
    setWindowTitle(QString("%1 (%2%)").arg(view->title()).arg(progress));
}

void Browser::setProgress(int p)
{
progress = p;
adjustTitle();
}

void Browser::finishLoading(bool)
{
progress = 100;
adjustTitle();
view->page()->printToPdf(pdffichier);
}

void Browser::Sommaire()
{
view->page()->runJavaScript("window.location.href='#SOMMAIRE';");
}

void Browser::Print()
{
QFileInfo fic(pdffichier);
if (fic.exists() && fic.isReadable() ) QDesktopServices::openUrl(QUrl("file:///"+pdffichier));
}





void Browser::Chercher()
{
if (searchLineEdit->text().isEmpty()) return;
view->findText(searchLineEdit->text());
}
