/***************************************************************************
 *   copyright       : (C) 2009-2022 by Pascal Brachet                     *
 *   https://www.xm1math.net/algobox/                                       *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   @license GPL-2.0+ <https://spdx.org/licenses/GPL-2.0+.html>           *
 ***************************************************************************/


#ifndef ALGOHIGHLIGHTER_H
#define ALGOHIGHLIGHTER_H


#include <QSyntaxHighlighter>
#include <QHash>
#include <QTextCharFormat>
#include <QColor>
#include <QTextBlockUserData>

class QTextDocument;

class AlgoHighlighter : public QSyntaxHighlighter
{
    Q_OBJECT

public:
    AlgoHighlighter(QTextDocument *parent = 0);
    ~AlgoHighlighter();
    QColor ColorStandard, ColorComment, ColorBloc, ColorCommande, ColorSi, ColorTantQue, ColorPour, ColorFonction;
    QStringList BlocWords,SiWords,TantQueWords,PourWords, CommandeWords,FonctionWords;
private :
bool isWordSeparator(QChar c) const;
bool isSpace(QChar c) const;
protected:
void highlightBlock(const QString &text);
};


#endif
