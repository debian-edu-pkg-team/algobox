/***************************************************************************
 *   copyright       : (C) 2009-2022 by Pascal Brachet                     *
 *   https://www.xm1math.net/algobox/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   @license GPL-2.0+ <https://spdx.org/licenses/GPL-2.0+.html>           *
 ***************************************************************************/

#include <QtGui>
#include <QRegularExpression>

#include "consolehighlighter.h"

ConsoleHighlighter::ConsoleHighlighter(QTextDocument *parent, bool blackschema)
    : QSyntaxHighlighter(parent)
{
MessageFormat.setFontWeight(QFont::Bold);
EntreeFormat.setFontWeight(QFont::Bold);

if (blackschema)
  {
  MessageFormat.setForeground(QColor("#D7B54F"));
  EntreeFormat.setForeground(QColor("#AACC00"));
  }
else
  {
  MessageFormat.setForeground(QColor(0x00, 0x00, 0xCC));
  EntreeFormat.setForeground(QColor(0x80, 0x00, 0x00));
  }
}

void ConsoleHighlighter::highlightBlock(const QString &text)
{

QString t;
QRegularExpression rxMessage("\\*\\*\\*(.*)\\*\\*\\*");
if ((rxMessage.match(text).hasMatch()) || text.startsWith("Trac") )
	{
	setFormat(0, text.length(),MessageFormat);
	}
else if (text.startsWith("Entrer ")) 
	{
	setFormat(0, text.length(),EntreeFormat);
	}
}
