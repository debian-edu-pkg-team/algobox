/***************************************************************************
 *   copyright       : (C) 2009-2022 by Pascal Brachet                     *
 *   https://www.xm1math.net/algobox/                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   @license GPL-2.0+ <https://spdx.org/licenses/GPL-2.0+.html>           *
 ***************************************************************************/

#include "toolbox.h"
#include "geticon.h"

ToolBox :: ToolBox(QWidget* parent/*=0*/)
    :QToolBar(parent)
{

        actionSauver = new QAction(this);
        actionSauver->setObjectName(QString::fromUtf8("actionSauver"));
        actionSauver->setIcon(getIcon(":/images/sauver22.png"));

	
        actionSauver->setText(QString::fromUtf8("Sauver"));
        actionSauver->setToolTip(QString::fromUtf8("Sauver"));
        actionSauver->setShortcut(Qt::CTRL+Qt::Key_S);
	
	
        setObjectName(QString::fromUtf8("toolBar"));
        setMovable(false);
        setAllowedAreas(Qt::TopToolBarArea);
        setIconSize(QSize(22, 22));
        setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
	setOrientation(Qt::Horizontal);
        setFloatable(false);

        addAction(actionSauver);

}
ToolBox :: ~ToolBox()
{
 
}
