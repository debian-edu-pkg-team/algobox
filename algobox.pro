COMPILEUSB=no

TEMPLATE	= app
LANGUAGE	= C++
TARGET	 = algobox

equals(QT_MAJOR_VERSION, 6) {
QT += core gui xml webenginewidgets network widgets printsupport core5compat
} else {
equals(QT_MAJOR_VERSION, 5){
greaterThan(QT_MINOR_VERSION, 6) {
QT += core gui xml webenginewidgets network widgets printsupport
} else {
message("Qt>=5.7 is required.")
}
}
}


ALGOBOXVERSION=1.1.1
DEFINES += ALGOBOXVERSION=$${ALGOBOXVERSION}

CONFIG	+= qt hide_symbols warn_off rtti_off exceptions_off c++11 release
CONFIG -= precompile_header

gcc {
    QMAKE_CXXFLAGS_WARN_ON += -Wno-unused-parameter
}

msvc {
    QMAKE_CXXFLAGS_WARN_ON += -wd"4267"
}

###############################
HEADERS	+= algobox.h \
	algoconsole.h \
	appeldialog.h \
	fonctiondialog.h \
	renvoyerdialog.h \
	browserdialog.h \
	browser.h \
	consoledialog.h \
	animatedlabel.h \
	variabledialog.h \
	modifierlignedialog.h \
	liredialog.h \
	afficherdialog.h \
	messagedialog.h \
	affectationdialog.h \
	conditiondialog.h \
	pourdialog.h \
	pourlineedit.h \
	tantquedialog.h \
	pointdialog.h \
	segmentdialog.h \
	aproposdialog.h \
	algowebview.h \
	algowebpage.h \
	commentairedialog.h \
	loghighlighter.h \
	consolehighlighter.h \
	algoeditor.h \
	algohighlighter.h \
	algoeditorview.h \
	linenumberwidget.h \
	findwidget.h \
	blockdata.h \
	replacedialog.h \
	verifdialog.h \
	treedelegate.h \
	x11fontdialog.h \
	jsmainwindow.h \
	jseditorview.h \
	jseditor.h \
	jshighlighter.h \
	jslinenumberwidget.h \
	jsfindwidget.h \
	jsreplacedialog.h \
	geticon.h \
	latexhighlighter.h \
	toolbox.h \
	latexviewdialog.h \
	configdialog.h \
	affichercalculdialog.h
SOURCES	+= main.cpp \
	algobox.cpp \
	appeldialog.cpp \
	fonctiondialog.cpp \
	renvoyerdialog.cpp \
	algoconsole.cpp \
	browser.cpp \
	consoledialog.cpp \
	browserdialog.cpp \
	animatedlabel.cpp \
	variabledialog.cpp \
	modifierlignedialog.cpp \
	liredialog.cpp \
	afficherdialog.cpp \
	messagedialog.cpp \
	affectationdialog.cpp \
	conditiondialog.cpp \
	pourdialog.cpp \
	pourlineedit.cpp \
	tantquedialog.cpp \
	pointdialog.cpp \
	segmentdialog.cpp \
	aproposdialog.cpp \
	algowebview.cpp \
	algowebpage.cpp \
	commentairedialog.cpp \
	loghighlighter.cpp \
	consolehighlighter.cpp \
	algoeditor.cpp \
	algohighlighter.cpp \
	algoeditorview.cpp \
	linenumberwidget.cpp \
	findwidget.cpp \
	blockdata.cpp \
	replacedialog.cpp \
	verifdialog.cpp \
	treedelegate.cpp \
	x11fontdialog.cpp \
	jsmainwindow.cpp \
	jseditorview.cpp \
	jseditor.cpp \
	jshighlighter.cpp \
	jslinenumberwidget.cpp \
	jsfindwidget.cpp \
	jsreplacedialog.cpp \
	geticon.cpp \
	latexhighlighter.cpp \
	toolbox.cpp \
	latexviewdialog.cpp \
	configdialog.cpp \
	affichercalculdialog.cpp
RESOURCES += algobox.qrc
FORMS += algobox.ui \
    appeldialog.ui \
	fonctiondialog.ui \
	renvoyerdialog.ui \
	browserdialog.ui \
	consoledialog.ui \
	variabledialog.ui \
	modifierlignedialog.ui \
	liredialog.ui \
	afficherdialog.ui \
	messagedialog.ui \
	affectationdialog.ui \
	conditiondialog.ui \
	pourdialog.ui \
	tantquedialog.ui \
	pointdialog.ui \
	segmentdialog.ui \
	aproposdialog.ui \
	commentairedialog.ui \
	findwidget.ui \
	replacedialog.ui \
	verifdialog.ui \
	x11fontdialog.ui \
	latexviewdialog.ui \
	configdialog.ui \
	affichercalculdialog.ui
################################
unix:!macx {

UI_DIR = .ui
MOC_DIR = .moc
OBJECTS_DIR = .obj

isEmpty( PREFIX ) {
    PREFIX=/usr
}
isEmpty( DESKTOPDIR ) {
    DESKTOPDIR=/usr/share/applications
}
isEmpty( ICONDIR ) {
    ICONDIR=/usr/share/pixmaps
}
isEmpty( MIMEDIR ) {
    MIMEDIR=/usr/share/mime/packages
}
isEmpty( METAINFODIR ) {
    METAINFODIR=/usr/share/metainfo
}

DEFINES += PREFIX=\\\"$${PREFIX}\\\"

INCLUDEPATH +=$${QTDIR}/include/


target.path = $${PREFIX}/bin
utilities.path = $${PREFIX}/share/algobox
desktop.path = $${DESKTOPDIR}
icon.path = $${ICONDIR}
mime.path = $${MIMEDIR}
metainfo.path = $${METAINFODIR}


INSTALLS = target

utilities.files = utilities/qt_fr.qm \
		ressources/eleve_calcul_recurrent.alg \
		ressources/eleve_distance_sur_un_axe.alg \
		ressources/eleve_que_fait_lalgo.alg \
		ressources/eleve_simplification_calculs_enchaines.alg \
		ressources/eleve_simul_lancers_de.alg \
		ressources/prof_babylone.alg \
		ressources/prof_balayage_fonctions.alg \
		ressources/prof_courbe_fonction.alg \
		ressources/prof_decomp_facteurspremiers.alg \
		ressources/prof_dichotomie.alg \
		ressources/prof_ductoscane.alg \
		ressources/prof_euler.alg \
		ressources/prof_integrale_trapezes.alg \
		ressources/prof_montecarlo.alg \
		ressources/prof_pgcd_euclide.alg \
		ressources/prof_somme_entiers.alg \
		ressources/prof_suite_syracuse.alg \
		ressources/prof_tri_abulle.alg \
		ressources/prof_pgcd_recursif.alg \
		ressources/aidealgobox.html \
		utilities/AUTHORS \
		utilities/COPYING \
		utilities/CHANGELOG.txt 
INSTALLS += utilities


desktop.files = utilities/algobox.desktop
INSTALLS += desktop

icon.files = utilities/algobox.png
INSTALLS += icon

mime.files = utilities/x-algobox.xml
INSTALLS += mime

isEmpty(NO_APPDATA) {
metainfo.files = utilities/algobox.metainfo.xml
INSTALLS += metainfo
}

}
################################
win32 {
UI_DIR = .ui
MOC_DIR = .moc
OBJECTS_DIR = .obj

RC_FILE = win.rc

equals(COMPILEUSB,no){
target.path = algoboxwin64
utilities.path = algoboxwin64/ressources
} else {
DEFINES += USB_VERSION
target.path = algoboxwin64usb
utilities.path = algoboxwin64usb/ressources
}

LIBS_PRIVATE += -ladvapi32 -lgdi32 -luser32 -lshlwapi

INSTALLS = target

utilities.files = utilities/qt_fr.qm \
		ressources/eleve_calcul_recurrent.alg \
		ressources/eleve_distance_sur_un_axe.alg \
		ressources/eleve_que_fait_lalgo.alg \
		ressources/eleve_simplification_calculs_enchaines.alg \
		ressources/eleve_simul_lancers_de.alg \
		ressources/prof_babylone.alg \
		ressources/prof_balayage_fonctions.alg \
		ressources/prof_courbe_fonction.alg \
		ressources/prof_decomp_facteurspremiers.alg \
		ressources/prof_dichotomie.alg \
		ressources/prof_ductoscane.alg \
		ressources/prof_euler.alg \
		ressources/prof_integrale_trapezes.alg \
		ressources/prof_montecarlo.alg \
		ressources/prof_pgcd_euclide.alg \
		ressources/prof_somme_entiers.alg \
		ressources/prof_suite_syracuse.alg \
		ressources/prof_tri_abulle.alg \
		ressources/prof_pgcd_recursif.alg \
		ressources/aidealgobox.html \
		utilities/AUTHORS \
		utilities/COPYING \
		utilities/CHANGELOG.txt
INSTALLS += utilities


}
###############################
macx {
UI_DIR = .ui
MOC_DIR = .moc
OBJECTS_DIR = .obj

LIBS_PRIVATE += -framework AppKit -framework CoreFoundation

QMAKE_MAC_SDK=macosx

#QMAKE_MACOSX_DEPLOYMENT_TARGET = 10.8

target.path = Algobox


INSTALLS = target
utilities.path = Contents/Resources
utilities.files = utilities/qt_menu.nib \
		utilities/qt_fr.qm \
		ressources/eleve_calcul_recurrent.alg \
		ressources/eleve_distance_sur_un_axe.alg \
		ressources/eleve_que_fait_lalgo.alg \
		ressources/eleve_simplification_calculs_enchaines.alg \
		ressources/eleve_simul_lancers_de.alg \
		ressources/prof_babylone.alg \
		ressources/prof_balayage_fonctions.alg \
		ressources/prof_courbe_fonction.alg \
		ressources/prof_decomp_facteurspremiers.alg \
		ressources/prof_dichotomie.alg \
		ressources/prof_ductoscane.alg \
		ressources/prof_euler.alg \
		ressources/prof_integrale_trapezes.alg \
		ressources/prof_montecarlo.alg \
		ressources/prof_pgcd_euclide.alg \
		ressources/prof_somme_entiers.alg \
		ressources/prof_suite_syracuse.alg \
		ressources/prof_tri_abulle.alg \
		ressources/prof_pgcd_recursif.alg \
		ressources/aidealgobox.html \
		utilities/AUTHORS \
		utilities/COPYING \
		utilities/CHANGELOG.txt
QMAKE_BUNDLE_DATA += utilities
INSTALLS += utilities
ICON = algobox.icns
QMAKE_INFO_PLIST =Info.plist
}
