Name: algobox
Summary: logiciel d'algorithmique
Version: 1.1.1
Release: xm1
License: GPL
Group: Applications/Education
Source: algobox-%{version}.orig.tar.gz
Packager: Pascal Brachet
Url: https://www.xm1math.net/algobox/

%if 0%{?fedora}
BuildRequires: gcc-c++
BuildRequires: qt5-devel  
BuildRequires: qt5-qtbase-devel 
BuildRequires: qt5-qtwebengine-devel
%endif

%if 0%{?suse_version}
%if 0%{?suse_version}>1310
BuildRequires: libQt5Core-devel
BuildRequires: libQt5Core-private-headers-devel
BuildRequires: libQt5Gui-devel
BuildRequires: libQt5Concurrent-devel
BuildRequires: libQt5Core-devel
BuildRequires: libQt5Network-devel
BuildRequires: libQt5PrintSupport-devel
BuildRequires: libQt5Widgets-devel
BuildRequires: libqt5-qtbase-devel 
BuildRequires: libqt5-qtbase-common-devel
BuildRequires: libqt5-qtscript-devel
BuildRequires: libqt5-qttools-devel
BuildRequires: libqt5-qtwebengine-devel
%endif
%endif

%description
Logiciel d'initiation à l'algorithmique et à la programmation

%prep
%setup 

%build
PREFIX=%{buildroot}%{_prefix}
%if 0%{?suse_version}
%if 0%{?suse_version}>1310
qmake-qt5 CONFIG-=debug NO_APPDATA=1 algobox.pro
%endif
%endif
%if 0%{?fedora}
qmake-qt5 CONFIG-=debug algobox.pro
%endif
make INSTALL_ROOT=%{buildroot}

%install
make INSTALL_ROOT=$RPM_BUILD_ROOT install

%files 
%defattr(-,root,root,-)
dir %{_datadir}/pixmaps
%{_datadir}/algobox/
%{_bindir}/algobox
%{_datadir}/applications/algobox.desktop
%if 0%{?suse_version}
%else
%{_datadir}/metainfo/algobox.metainfo.xml
%endif
%{_datadir}/mime/packages/x-algobox.xml
%{_datadir}/pixmaps/algobox.png
%{_datadir}/algobox/eleve_calcul_recurrent.alg
%{_datadir}/algobox/eleve_distance_sur_un_axe.alg
%{_datadir}/algobox/eleve_que_fait_lalgo.alg
%{_datadir}/algobox/eleve_simplification_calculs_enchaines.alg
%{_datadir}/algobox/eleve_simul_lancers_de.alg
%{_datadir}/algobox/prof_babylone.alg
%{_datadir}/algobox/prof_balayage_fonctions.alg
%{_datadir}/algobox/prof_courbe_fonction.alg
%{_datadir}/algobox/prof_decomp_facteurspremiers.alg
%{_datadir}/algobox/prof_dichotomie.alg
%{_datadir}/algobox/prof_ductoscane.alg
%{_datadir}/algobox/prof_euler.alg
%{_datadir}/algobox/prof_integrale_trapezes.alg
%{_datadir}/algobox/prof_montecarlo.alg
%{_datadir}/algobox/prof_pgcd_euclide.alg
%{_datadir}/algobox/prof_somme_entiers.alg
%{_datadir}/algobox/prof_suite_syracuse.alg
%{_datadir}/algobox/prof_tri_abulle.alg
%{_datadir}/algobox/prof_pgcd_recursif.alg
%{_datadir}/algobox/aidealgobox.html
%{_datadir}/algobox/AUTHORS
%{_datadir}/algobox/COPYING
%{_datadir}/algobox/CHANGELOG.txt
%{_datadir}/algobox/qt_fr.qm
