/***************************************************************************
 *   copyright       : (C) 2009-2022 by Pascal Brachet                     *
 *   https://www.xm1math.net/algobox/                                       *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   @license GPL-2.0+ <https://spdx.org/licenses/GPL-2.0+.html>           *
 ***************************************************************************/

#ifndef ALGOCONSOLE_H
#define ALGOCONSOLE_H

#include <QtGui>
#include <QTextEdit>
 
class AlgoConsole : public QTextEdit
{
    Q_OBJECT
public:
    AlgoConsole(QWidget *parent = 0);
    void output(QString);
    void scrollDown();
    QString answer;
protected:
    void keyPressEvent(QKeyEvent *);
    void mouseMoveEvent(QMouseEvent *);
    void contextMenuEvent(QContextMenuEvent *e);
private:
    QString prompt;
    bool isLocked;
    void onEnter();
signals:
    void done();
};
 


#endif
